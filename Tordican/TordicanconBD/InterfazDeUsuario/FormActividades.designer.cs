﻿namespace InterfazDeUsuario
{
    partial class FormActividades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tcActividades = new System.Windows.Forms.TabControl();
            this.tpQuad = new System.Windows.Forms.TabPage();
            this.labelQuadNota = new System.Windows.Forms.Label();
            this.gbQuadResultados = new System.Windows.Forms.GroupBox();
            this.btQuadVolver = new System.Windows.Forms.Button();
            this.btQuadAsignar = new System.Windows.Forms.Button();
            this.dgvQuadResultados = new System.Windows.Forms.DataGridView();
            this.QuadId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadMatricula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadMarca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadModelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadPotencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadPeso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadFechaAlta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuadEnUso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbQuadOpciones = new System.Windows.Forms.GroupBox();
            this.btQuadBorrar = new System.Windows.Forms.Button();
            this.btQuadReset = new System.Windows.Forms.Button();
            this.btQuadInsertar = new System.Windows.Forms.Button();
            this.btQuadEditar = new System.Windows.Forms.Button();
            this.gbQuadDatos = new System.Windows.Forms.GroupBox();
            this.labelQuadID = new System.Windows.Forms.Label();
            this.tbQuadID = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.checkQuadUso = new System.Windows.Forms.CheckBox();
            this.dtpQuadFecha = new System.Windows.Forms.DateTimePicker();
            this.tbQuadPrecio = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbQuadPeso = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbQuadMatricula = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbQuadMarca = new System.Windows.Forms.TextBox();
            this.tbQuadModelo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbQuadPotencia = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gbQuadBusqueda = new System.Windows.Forms.GroupBox();
            this.checkQuadDisponibles = new System.Windows.Forms.CheckBox();
            this.cbQuadFiltro = new System.Windows.Forms.ComboBox();
            this.btQuadBusca = new System.Windows.Forms.Button();
            this.tbQuadBusca = new System.Windows.Forms.TextBox();
            this.gbQuadInfo = new System.Windows.Forms.GroupBox();
            this.labelQuadsEnUso = new System.Windows.Forms.Label();
            this.labelQuadsTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tpBicicleta = new System.Windows.Forms.TabPage();
            this.labelBiciNota = new System.Windows.Forms.Label();
            this.gbBiciOpciones = new System.Windows.Forms.GroupBox();
            this.btBiciBorrar = new System.Windows.Forms.Button();
            this.btBiciReset = new System.Windows.Forms.Button();
            this.btBiciInsertar = new System.Windows.Forms.Button();
            this.btBiciEditar = new System.Windows.Forms.Button();
            this.gbBiciPrecios = new System.Windows.Forms.GroupBox();
            this.btTipoBiciPrecios = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbBiciPrecioTandem = new System.Windows.Forms.TextBox();
            this.tbBiciPrecioAdulto = new System.Windows.Forms.TextBox();
            this.tbBiciPrecioNinyo = new System.Windows.Forms.TextBox();
            this.gbBiciResultados = new System.Windows.Forms.GroupBox();
            this.btBiciVolver = new System.Windows.Forms.Button();
            this.btBiciAsignar = new System.Windows.Forms.Button();
            this.dgvBiciResultados = new System.Windows.Forms.DataGridView();
            this.BiciId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BiciTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BiciFechaAlta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BiciPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BiciEnUso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbBiciDatos = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tbBiciID = new System.Windows.Forms.TextBox();
            this.checkBiciUso = new System.Windows.Forms.CheckBox();
            this.dtpBiciFecha = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.cbBiciTipo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gbBiciBusqueda = new System.Windows.Forms.GroupBox();
            this.checkBiciDisponibles = new System.Windows.Forms.CheckBox();
            this.cbBiciFiltro = new System.Windows.Forms.ComboBox();
            this.btBiciBusca = new System.Windows.Forms.Button();
            this.gbBiciInfo = new System.Windows.Forms.GroupBox();
            this.labelBicisUso = new System.Windows.Forms.Label();
            this.labelBicisTotal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tpCaballo = new System.Windows.Forms.TabPage();
            this.gbCaballoOpciones = new System.Windows.Forms.GroupBox();
            this.btCaballoBorrar = new System.Windows.Forms.Button();
            this.btCaballoReset = new System.Windows.Forms.Button();
            this.btCaballoInsertar = new System.Windows.Forms.Button();
            this.btCaballoEditar = new System.Windows.Forms.Button();
            this.gbCaballoResultados = new System.Windows.Forms.GroupBox();
            this.btCaballoVolver = new System.Windows.Forms.Button();
            this.btCaballoAsignar = new System.Windows.Forms.Button();
            this.dgvCaballoResultados = new System.Windows.Forms.DataGridView();
            this.paseoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paseoNombreRuta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paseoDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paseoDuracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paseoPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paseoConPony = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbCaballoDatos = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbPaseoID = new System.Windows.Forms.TextBox();
            this.checkCaballoPony = new System.Windows.Forms.CheckBox();
            this.tbCaballoPrecio = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbCaballoRuta = new System.Windows.Forms.TextBox();
            this.tbCaballoDescripcion = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbCaballoDuracion = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.gbCaballoInfo = new System.Windows.Forms.GroupBox();
            this.labelPaseosPony = new System.Windows.Forms.Label();
            this.labelPaseosTotal = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tpArco = new System.Windows.Forms.TabPage();
            this.gbArcoOpciones = new System.Windows.Forms.GroupBox();
            this.btArcoBorrar = new System.Windows.Forms.Button();
            this.btArcoReset = new System.Windows.Forms.Button();
            this.btArcoInsertar = new System.Windows.Forms.Button();
            this.btArcoEditar = new System.Windows.Forms.Button();
            this.gbArcoResultados = new System.Windows.Forms.GroupBox();
            this.btArcoVolver = new System.Windows.Forms.Button();
            this.btArcoAsignar = new System.Windows.Forms.Button();
            this.dgvArcoResultados = new System.Windows.Forms.DataGridView();
            this.tiroID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiroNumDianas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiroNumFlechas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tiroPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbArcoDatos = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tbArcoID = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbArcoPrecio = new System.Windows.Forms.TextBox();
            this.tbArcoNumFlechas = new System.Windows.Forms.TextBox();
            this.tbArcoNumDianas = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.gbArcoInfo = new System.Windows.Forms.GroupBox();
            this.labelArcoOfertas = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tpPiraguismo = new System.Windows.Forms.TabPage();
            this.gbPiraguismoOpciones = new System.Windows.Forms.GroupBox();
            this.btPiraguismoBorrar = new System.Windows.Forms.Button();
            this.btPiraguismoReset = new System.Windows.Forms.Button();
            this.btPiraguismoInsertar = new System.Windows.Forms.Button();
            this.btPiraguismoEditar = new System.Windows.Forms.Button();
            this.gbPiraguismoDatos = new System.Windows.Forms.GroupBox();
            this.label55 = new System.Windows.Forms.Label();
            this.tbPiraguismoID = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbPiraguismoPrecio = new System.Windows.Forms.TextBox();
            this.tbPiraguismoNumChalecos = new System.Windows.Forms.TextBox();
            this.tbPiraguismoPantano = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.gbPiraguismoResultados = new System.Windows.Forms.GroupBox();
            this.btPiraguismoVolver = new System.Windows.Forms.Button();
            this.btPiraguismoAsignar = new System.Windows.Forms.Button();
            this.dgvPiraguismoResultados = new System.Windows.Forms.DataGridView();
            this.piragID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.piragPantano = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.piragNumChalecos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.piragPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbPiraguismoInfo = new System.Windows.Forms.GroupBox();
            this.labelPiragChalecos = new System.Windows.Forms.Label();
            this.labelPiragPantanos = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tpRapel = new System.Windows.Forms.TabPage();
            this.gbRapelOpciones = new System.Windows.Forms.GroupBox();
            this.btRapelBorrar = new System.Windows.Forms.Button();
            this.btRapelReset = new System.Windows.Forms.Button();
            this.btRapelInsertar = new System.Windows.Forms.Button();
            this.btRapelEditar = new System.Windows.Forms.Button();
            this.gbRapelDatos = new System.Windows.Forms.GroupBox();
            this.label73 = new System.Windows.Forms.Label();
            this.tbRapelID = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tbRapelNumEquipos = new System.Windows.Forms.TextBox();
            this.tbRapelPrecio = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tbRapelNumCuerdas = new System.Windows.Forms.TextBox();
            this.tbRapelLugar = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.gbRapelResultados = new System.Windows.Forms.GroupBox();
            this.btRapelVolver = new System.Windows.Forms.Button();
            this.btRapelAsignar = new System.Windows.Forms.Button();
            this.dgvRapelResultados = new System.Windows.Forms.DataGridView();
            this.rapelID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rapelLugar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rapelCuerdas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rapelEquipos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rapelPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbRapelInfo = new System.Windows.Forms.GroupBox();
            this.labelRapelEquipos = new System.Windows.Forms.Label();
            this.labelRapelCuerdas = new System.Windows.Forms.Label();
            this.labelRapelLugares = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tpParque = new System.Windows.Forms.TabPage();
            this.gbParqueOpciones = new System.Windows.Forms.GroupBox();
            this.btParqueBorrar = new System.Windows.Forms.Button();
            this.btParqueReset = new System.Windows.Forms.Button();
            this.btParqueInsertar = new System.Windows.Forms.Button();
            this.btParqueEditar = new System.Windows.Forms.Button();
            this.gbParqueDatos = new System.Windows.Forms.GroupBox();
            this.label74 = new System.Windows.Forms.Label();
            this.tbParqueID = new System.Windows.Forms.TextBox();
            this.tbParqueDescripcion = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.tbParquePrecio = new System.Windows.Forms.TextBox();
            this.tbParqueLugar = new System.Windows.Forms.TextBox();
            this.tbParqueNombre = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.gbParqueResultados = new System.Windows.Forms.GroupBox();
            this.btParqueVolver = new System.Windows.Forms.Button();
            this.btParqueAsignar = new System.Windows.Forms.Button();
            this.dgvParqueResultados = new System.Windows.Forms.DataGridView();
            this.parqueID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parqueLocalidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parqueDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parquePrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbParqueInfo = new System.Windows.Forms.GroupBox();
            this.labelParquesTotal = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tpLocalidad = new System.Windows.Forms.TabPage();
            this.gbLocalidadOpciones = new System.Windows.Forms.GroupBox();
            this.btLocalidadBorrar = new System.Windows.Forms.Button();
            this.btLocalidadReset = new System.Windows.Forms.Button();
            this.btLocalidadInsertar = new System.Windows.Forms.Button();
            this.btLocalidadEditar = new System.Windows.Forms.Button();
            this.gbLocalidadDatos = new System.Windows.Forms.GroupBox();
            this.label75 = new System.Windows.Forms.Label();
            this.tbLocalidadID = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.tbLocalidadDistancia = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.tbLocalidadDescripcion = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.tbLocalidadPrecio = new System.Windows.Forms.TextBox();
            this.tbLocalidadLugar = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.gbLocalidadResultados = new System.Windows.Forms.GroupBox();
            this.btLocalidadVolver = new System.Windows.Forms.Button();
            this.btLocalidadAsignar = new System.Windows.Forms.Button();
            this.dgvLocalidadResultados = new System.Windows.Forms.DataGridView();
            this.localidadID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.localidadNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.localidadDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.localidadDistancia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.localidadPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbLocalidadInfo = new System.Windows.Forms.GroupBox();
            this.labelLocalidadTotal = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panelActIni = new System.Windows.Forms.Panel();
            this.btIncidencias = new System.Windows.Forms.Button();
            this.btActividades = new System.Windows.Forms.Button();
            this.panelActividades = new System.Windows.Forms.Panel();
            this.panelIncidencias = new System.Windows.Forms.Panel();
            this.gbIncidenciaOpciones = new System.Windows.Forms.GroupBox();
            this.btIncidenciaBorrar = new System.Windows.Forms.Button();
            this.btIncidenciaReset = new System.Windows.Forms.Button();
            this.btIncidenciaInsertar = new System.Windows.Forms.Button();
            this.btIncidenciaEditar = new System.Windows.Forms.Button();
            this.gbIncidenciaResultados = new System.Windows.Forms.GroupBox();
            this.rbIncidenciaTodas = new System.Windows.Forms.RadioButton();
            this.rbIncidenciaPendientes = new System.Windows.Forms.RadioButton();
            this.dgvIncidenciaResultados = new System.Windows.Forms.DataGridView();
            this.incidenciaID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incidenciaActividad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incidenciaFecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incidenciaDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incidenciaRevisada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbIncidenciaInfo = new System.Windows.Forms.GroupBox();
            this.labelIncidenciaPendientes = new System.Windows.Forms.Label();
            this.labelIncidenciaTotal = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.gbIncidenciaDatos = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.tbIncidenciaID = new System.Windows.Forms.TextBox();
            this.checkIncidenciaRevisada = new System.Windows.Forms.CheckBox();
            this.cbIncidenciaActividad = new System.Windows.Forms.ComboBox();
            this.dtpIncidenciaFecha = new System.Windows.Forms.DateTimePicker();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbIncidenciaDescripcion = new System.Windows.Forms.TextBox();
            this.pbActividad = new System.Windows.Forms.PictureBox();
            this.tcActividades.SuspendLayout();
            this.tpQuad.SuspendLayout();
            this.gbQuadResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuadResultados)).BeginInit();
            this.gbQuadOpciones.SuspendLayout();
            this.gbQuadDatos.SuspendLayout();
            this.gbQuadBusqueda.SuspendLayout();
            this.gbQuadInfo.SuspendLayout();
            this.tpBicicleta.SuspendLayout();
            this.gbBiciOpciones.SuspendLayout();
            this.gbBiciPrecios.SuspendLayout();
            this.gbBiciResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBiciResultados)).BeginInit();
            this.gbBiciDatos.SuspendLayout();
            this.gbBiciBusqueda.SuspendLayout();
            this.gbBiciInfo.SuspendLayout();
            this.tpCaballo.SuspendLayout();
            this.gbCaballoOpciones.SuspendLayout();
            this.gbCaballoResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaballoResultados)).BeginInit();
            this.gbCaballoDatos.SuspendLayout();
            this.gbCaballoInfo.SuspendLayout();
            this.tpArco.SuspendLayout();
            this.gbArcoOpciones.SuspendLayout();
            this.gbArcoResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArcoResultados)).BeginInit();
            this.gbArcoDatos.SuspendLayout();
            this.gbArcoInfo.SuspendLayout();
            this.tpPiraguismo.SuspendLayout();
            this.gbPiraguismoOpciones.SuspendLayout();
            this.gbPiraguismoDatos.SuspendLayout();
            this.gbPiraguismoResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiraguismoResultados)).BeginInit();
            this.gbPiraguismoInfo.SuspendLayout();
            this.tpRapel.SuspendLayout();
            this.gbRapelOpciones.SuspendLayout();
            this.gbRapelDatos.SuspendLayout();
            this.gbRapelResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRapelResultados)).BeginInit();
            this.gbRapelInfo.SuspendLayout();
            this.tpParque.SuspendLayout();
            this.gbParqueOpciones.SuspendLayout();
            this.gbParqueDatos.SuspendLayout();
            this.gbParqueResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParqueResultados)).BeginInit();
            this.gbParqueInfo.SuspendLayout();
            this.tpLocalidad.SuspendLayout();
            this.gbLocalidadOpciones.SuspendLayout();
            this.gbLocalidadDatos.SuspendLayout();
            this.gbLocalidadResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalidadResultados)).BeginInit();
            this.gbLocalidadInfo.SuspendLayout();
            this.panelActIni.SuspendLayout();
            this.panelActividades.SuspendLayout();
            this.panelIncidencias.SuspendLayout();
            this.gbIncidenciaOpciones.SuspendLayout();
            this.gbIncidenciaResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncidenciaResultados)).BeginInit();
            this.gbIncidenciaInfo.SuspendLayout();
            this.gbIncidenciaDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbActividad)).BeginInit();
            this.SuspendLayout();
            // 
            // tcActividades
            // 
            this.tcActividades.Controls.Add(this.tpQuad);
            this.tcActividades.Controls.Add(this.tpBicicleta);
            this.tcActividades.Controls.Add(this.tpCaballo);
            this.tcActividades.Controls.Add(this.tpArco);
            this.tcActividades.Controls.Add(this.tpPiraguismo);
            this.tcActividades.Controls.Add(this.tpRapel);
            this.tcActividades.Controls.Add(this.tpParque);
            this.tcActividades.Controls.Add(this.tpLocalidad);
            this.tcActividades.Location = new System.Drawing.Point(6, 3);
            this.tcActividades.Name = "tcActividades";
            this.tcActividades.SelectedIndex = 0;
            this.tcActividades.Size = new System.Drawing.Size(775, 604);
            this.tcActividades.TabIndex = 0;
            // 
            // tpQuad
            // 
            this.tpQuad.Controls.Add(this.labelQuadNota);
            this.tpQuad.Controls.Add(this.gbQuadResultados);
            this.tpQuad.Controls.Add(this.gbQuadOpciones);
            this.tpQuad.Controls.Add(this.gbQuadDatos);
            this.tpQuad.Controls.Add(this.gbQuadBusqueda);
            this.tpQuad.Controls.Add(this.gbQuadInfo);
            this.tpQuad.Location = new System.Drawing.Point(4, 22);
            this.tpQuad.Name = "tpQuad";
            this.tpQuad.Padding = new System.Windows.Forms.Padding(3);
            this.tpQuad.Size = new System.Drawing.Size(767, 578);
            this.tpQuad.TabIndex = 0;
            this.tpQuad.Text = "Quads";
            this.tpQuad.UseVisualStyleBackColor = true;
            this.tpQuad.Enter += new System.EventHandler(this.tpQuad_Enter);
            // 
            // labelQuadNota
            // 
            this.labelQuadNota.AutoSize = true;
            this.labelQuadNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelQuadNota.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelQuadNota.Location = new System.Drawing.Point(30, 550);
            this.labelQuadNota.Name = "labelQuadNota";
            this.labelQuadNota.Size = new System.Drawing.Size(306, 13);
            this.labelQuadNota.TabIndex = 1;
            this.labelQuadNota.Text = "NOTA: No se pueden BORRAR los Quads que estén EN USO.";
            // 
            // gbQuadResultados
            // 
            this.gbQuadResultados.Controls.Add(this.btQuadVolver);
            this.gbQuadResultados.Controls.Add(this.btQuadAsignar);
            this.gbQuadResultados.Controls.Add(this.dgvQuadResultados);
            this.gbQuadResultados.Location = new System.Drawing.Point(33, 136);
            this.gbQuadResultados.Name = "gbQuadResultados";
            this.gbQuadResultados.Size = new System.Drawing.Size(693, 209);
            this.gbQuadResultados.TabIndex = 20;
            this.gbQuadResultados.TabStop = false;
            this.gbQuadResultados.Text = "Resultados de la búsqueda";
            // 
            // btQuadVolver
            // 
            this.btQuadVolver.Location = new System.Drawing.Point(579, 141);
            this.btQuadVolver.Name = "btQuadVolver";
            this.btQuadVolver.Size = new System.Drawing.Size(100, 23);
            this.btQuadVolver.TabIndex = 2;
            this.btQuadVolver.Text = "VOLVER";
            this.btQuadVolver.UseVisualStyleBackColor = true;
            this.btQuadVolver.Visible = false;
            this.btQuadVolver.Click += new System.EventHandler(this.btQuadVolver_Click);
            // 
            // btQuadAsignar
            // 
            this.btQuadAsignar.Location = new System.Drawing.Point(579, 170);
            this.btQuadAsignar.Name = "btQuadAsignar";
            this.btQuadAsignar.Size = new System.Drawing.Size(100, 23);
            this.btQuadAsignar.TabIndex = 1;
            this.btQuadAsignar.Text = "ASIGNAR";
            this.btQuadAsignar.UseVisualStyleBackColor = true;
            this.btQuadAsignar.Visible = false;
            this.btQuadAsignar.Click += new System.EventHandler(this.btQuadAsignar_Click);
            // 
            // dgvQuadResultados
            // 
            this.dgvQuadResultados.AllowUserToAddRows = false;
            this.dgvQuadResultados.AllowUserToDeleteRows = false;
            this.dgvQuadResultados.AllowUserToResizeColumns = false;
            this.dgvQuadResultados.AllowUserToResizeRows = false;
            this.dgvQuadResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQuadResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.QuadId,
            this.QuadMatricula,
            this.QuadMarca,
            this.QuadModelo,
            this.QuadPotencia,
            this.QuadPeso,
            this.QuadPrecio,
            this.QuadFechaAlta,
            this.QuadEnUso});
            this.dgvQuadResultados.Location = new System.Drawing.Point(15, 24);
            this.dgvQuadResultados.MultiSelect = false;
            this.dgvQuadResultados.Name = "dgvQuadResultados";
            this.dgvQuadResultados.ReadOnly = true;
            this.dgvQuadResultados.Size = new System.Drawing.Size(547, 169);
            this.dgvQuadResultados.TabIndex = 0;
            this.dgvQuadResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvQuadResultados_RowHeaderMouseClick);
            // 
            // QuadId
            // 
            this.QuadId.HeaderText = "ID";
            this.QuadId.Name = "QuadId";
            this.QuadId.ReadOnly = true;
            this.QuadId.Width = 30;
            // 
            // QuadMatricula
            // 
            this.QuadMatricula.HeaderText = "Matrícula";
            this.QuadMatricula.Name = "QuadMatricula";
            this.QuadMatricula.ReadOnly = true;
            this.QuadMatricula.Width = 70;
            // 
            // QuadMarca
            // 
            this.QuadMarca.HeaderText = "Marca";
            this.QuadMarca.Name = "QuadMarca";
            this.QuadMarca.ReadOnly = true;
            // 
            // QuadModelo
            // 
            this.QuadModelo.HeaderText = "Modelo";
            this.QuadModelo.Name = "QuadModelo";
            this.QuadModelo.ReadOnly = true;
            // 
            // QuadPotencia
            // 
            this.QuadPotencia.HeaderText = "Potencia";
            this.QuadPotencia.Name = "QuadPotencia";
            this.QuadPotencia.ReadOnly = true;
            this.QuadPotencia.Width = 60;
            // 
            // QuadPeso
            // 
            this.QuadPeso.HeaderText = "Peso";
            this.QuadPeso.Name = "QuadPeso";
            this.QuadPeso.ReadOnly = true;
            this.QuadPeso.Width = 60;
            // 
            // QuadPrecio
            // 
            this.QuadPrecio.HeaderText = "Precio";
            this.QuadPrecio.Name = "QuadPrecio";
            this.QuadPrecio.ReadOnly = true;
            this.QuadPrecio.Width = 60;
            // 
            // QuadFechaAlta
            // 
            this.QuadFechaAlta.HeaderText = "Fecha Alta";
            this.QuadFechaAlta.Name = "QuadFechaAlta";
            this.QuadFechaAlta.ReadOnly = true;
            this.QuadFechaAlta.Visible = false;
            // 
            // QuadEnUso
            // 
            this.QuadEnUso.HeaderText = "En Uso";
            this.QuadEnUso.Name = "QuadEnUso";
            this.QuadEnUso.ReadOnly = true;
            this.QuadEnUso.Visible = false;
            // 
            // gbQuadOpciones
            // 
            this.gbQuadOpciones.Controls.Add(this.btQuadBorrar);
            this.gbQuadOpciones.Controls.Add(this.btQuadReset);
            this.gbQuadOpciones.Controls.Add(this.btQuadInsertar);
            this.gbQuadOpciones.Controls.Add(this.btQuadEditar);
            this.gbQuadOpciones.Location = new System.Drawing.Point(591, 355);
            this.gbQuadOpciones.Name = "gbQuadOpciones";
            this.gbQuadOpciones.Size = new System.Drawing.Size(135, 179);
            this.gbQuadOpciones.TabIndex = 4;
            this.gbQuadOpciones.TabStop = false;
            this.gbQuadOpciones.Text = "Opciones";
            // 
            // btQuadBorrar
            // 
            this.btQuadBorrar.Enabled = false;
            this.btQuadBorrar.Location = new System.Drawing.Point(21, 86);
            this.btQuadBorrar.Name = "btQuadBorrar";
            this.btQuadBorrar.Size = new System.Drawing.Size(100, 23);
            this.btQuadBorrar.TabIndex = 2;
            this.btQuadBorrar.Text = "BORRAR";
            this.btQuadBorrar.UseVisualStyleBackColor = true;
            this.btQuadBorrar.Click += new System.EventHandler(this.btQuadBorrar_Click);
            // 
            // btQuadReset
            // 
            this.btQuadReset.Location = new System.Drawing.Point(21, 134);
            this.btQuadReset.Name = "btQuadReset";
            this.btQuadReset.Size = new System.Drawing.Size(100, 23);
            this.btQuadReset.TabIndex = 1;
            this.btQuadReset.Text = "RESET";
            this.btQuadReset.UseVisualStyleBackColor = true;
            this.btQuadReset.Click += new System.EventHandler(this.btQuadReset_Click);
            // 
            // btQuadInsertar
            // 
            this.btQuadInsertar.Location = new System.Drawing.Point(21, 28);
            this.btQuadInsertar.Name = "btQuadInsertar";
            this.btQuadInsertar.Size = new System.Drawing.Size(100, 23);
            this.btQuadInsertar.TabIndex = 0;
            this.btQuadInsertar.Text = "INSERTAR";
            this.btQuadInsertar.UseVisualStyleBackColor = true;
            this.btQuadInsertar.Click += new System.EventHandler(this.btQuadInsertar_Click);
            // 
            // btQuadEditar
            // 
            this.btQuadEditar.Enabled = false;
            this.btQuadEditar.Location = new System.Drawing.Point(21, 57);
            this.btQuadEditar.Name = "btQuadEditar";
            this.btQuadEditar.Size = new System.Drawing.Size(100, 23);
            this.btQuadEditar.TabIndex = 0;
            this.btQuadEditar.Text = "EDITAR";
            this.btQuadEditar.UseVisualStyleBackColor = true;
            this.btQuadEditar.Click += new System.EventHandler(this.btQuadModificar_Click);
            // 
            // gbQuadDatos
            // 
            this.gbQuadDatos.Controls.Add(this.labelQuadID);
            this.gbQuadDatos.Controls.Add(this.tbQuadID);
            this.gbQuadDatos.Controls.Add(this.label72);
            this.gbQuadDatos.Controls.Add(this.label71);
            this.gbQuadDatos.Controls.Add(this.label70);
            this.gbQuadDatos.Controls.Add(this.checkQuadUso);
            this.gbQuadDatos.Controls.Add(this.dtpQuadFecha);
            this.gbQuadDatos.Controls.Add(this.tbQuadPrecio);
            this.gbQuadDatos.Controls.Add(this.label10);
            this.gbQuadDatos.Controls.Add(this.tbQuadPeso);
            this.gbQuadDatos.Controls.Add(this.label3);
            this.gbQuadDatos.Controls.Add(this.tbQuadMatricula);
            this.gbQuadDatos.Controls.Add(this.label5);
            this.gbQuadDatos.Controls.Add(this.tbQuadMarca);
            this.gbQuadDatos.Controls.Add(this.tbQuadModelo);
            this.gbQuadDatos.Controls.Add(this.label8);
            this.gbQuadDatos.Controls.Add(this.tbQuadPotencia);
            this.gbQuadDatos.Controls.Add(this.label7);
            this.gbQuadDatos.Controls.Add(this.label4);
            this.gbQuadDatos.Controls.Add(this.label6);
            this.gbQuadDatos.Location = new System.Drawing.Point(33, 355);
            this.gbQuadDatos.Name = "gbQuadDatos";
            this.gbQuadDatos.Size = new System.Drawing.Size(517, 179);
            this.gbQuadDatos.TabIndex = 3;
            this.gbQuadDatos.TabStop = false;
            this.gbQuadDatos.Text = "Datos del Quad";
            // 
            // labelQuadID
            // 
            this.labelQuadID.AutoSize = true;
            this.labelQuadID.ForeColor = System.Drawing.SystemColors.GrayText;
            this.labelQuadID.Location = new System.Drawing.Point(449, 144);
            this.labelQuadID.Name = "labelQuadID";
            this.labelQuadID.Size = new System.Drawing.Size(21, 13);
            this.labelQuadID.TabIndex = 21;
            this.labelQuadID.Text = "ID:";
            this.labelQuadID.Visible = false;
            // 
            // tbQuadID
            // 
            this.tbQuadID.Enabled = false;
            this.tbQuadID.Location = new System.Drawing.Point(470, 141);
            this.tbQuadID.Name = "tbQuadID";
            this.tbQuadID.Size = new System.Drawing.Size(31, 20);
            this.tbQuadID.TabIndex = 20;
            this.tbQuadID.Visible = false;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(407, 36);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(34, 13);
            this.label72.TabIndex = 19;
            this.label72.Text = "Euros";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(199, 144);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(23, 13);
            this.label71.TabIndex = 18;
            this.label71.Text = "Kg.";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(199, 115);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(27, 13);
            this.label70.TabIndex = 17;
            this.label70.Text = "C.V.";
            // 
            // checkQuadUso
            // 
            this.checkQuadUso.AutoSize = true;
            this.checkQuadUso.Location = new System.Drawing.Point(301, 89);
            this.checkQuadUso.Name = "checkQuadUso";
            this.checkQuadUso.Size = new System.Drawing.Size(59, 17);
            this.checkQuadUso.TabIndex = 8;
            this.checkQuadUso.Text = "En uso";
            this.checkQuadUso.UseVisualStyleBackColor = true;
            // 
            // dtpQuadFecha
            // 
            this.dtpQuadFecha.Location = new System.Drawing.Point(301, 59);
            this.dtpQuadFecha.Name = "dtpQuadFecha";
            this.dtpQuadFecha.Size = new System.Drawing.Size(200, 20);
            this.dtpQuadFecha.TabIndex = 7;
            // 
            // tbQuadPrecio
            // 
            this.tbQuadPrecio.Location = new System.Drawing.Point(301, 33);
            this.tbQuadPrecio.Name = "tbQuadPrecio";
            this.tbQuadPrecio.Size = new System.Drawing.Size(100, 20);
            this.tbQuadPrecio.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(218, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Precio Alquiler:";
            // 
            // tbQuadPeso
            // 
            this.tbQuadPeso.Location = new System.Drawing.Point(93, 141);
            this.tbQuadPeso.Name = "tbQuadPeso";
            this.tbQuadPeso.Size = new System.Drawing.Size(100, 20);
            this.tbQuadPeso.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Marca:";
            // 
            // tbQuadMatricula
            // 
            this.tbQuadMatricula.Location = new System.Drawing.Point(93, 33);
            this.tbQuadMatricula.MaxLength = 10;
            this.tbQuadMatricula.Name = "tbQuadMatricula";
            this.tbQuadMatricula.Size = new System.Drawing.Size(100, 20);
            this.tbQuadMatricula.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Matrícula:";
            // 
            // tbQuadMarca
            // 
            this.tbQuadMarca.Location = new System.Drawing.Point(93, 59);
            this.tbQuadMarca.MaxLength = 20;
            this.tbQuadMarca.Name = "tbQuadMarca";
            this.tbQuadMarca.Size = new System.Drawing.Size(100, 20);
            this.tbQuadMarca.TabIndex = 2;
            // 
            // tbQuadModelo
            // 
            this.tbQuadModelo.Location = new System.Drawing.Point(93, 85);
            this.tbQuadModelo.MaxLength = 20;
            this.tbQuadModelo.Name = "tbQuadModelo";
            this.tbQuadModelo.Size = new System.Drawing.Size(100, 20);
            this.tbQuadModelo.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(218, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Fecha de Alta:";
            // 
            // tbQuadPotencia
            // 
            this.tbQuadPotencia.Location = new System.Drawing.Point(93, 112);
            this.tbQuadPotencia.Name = "tbQuadPotencia";
            this.tbQuadPotencia.Size = new System.Drawing.Size(100, 20);
            this.tbQuadPotencia.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Peso:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Modelo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Potencia:";
            // 
            // gbQuadBusqueda
            // 
            this.gbQuadBusqueda.Controls.Add(this.checkQuadDisponibles);
            this.gbQuadBusqueda.Controls.Add(this.cbQuadFiltro);
            this.gbQuadBusqueda.Controls.Add(this.btQuadBusca);
            this.gbQuadBusqueda.Controls.Add(this.tbQuadBusca);
            this.gbQuadBusqueda.Location = new System.Drawing.Point(316, 29);
            this.gbQuadBusqueda.Name = "gbQuadBusqueda";
            this.gbQuadBusqueda.Size = new System.Drawing.Size(410, 91);
            this.gbQuadBusqueda.TabIndex = 1;
            this.gbQuadBusqueda.TabStop = false;
            this.gbQuadBusqueda.Text = "Búsqueda de Quads";
            // 
            // checkQuadDisponibles
            // 
            this.checkQuadDisponibles.AutoSize = true;
            this.checkQuadDisponibles.Checked = true;
            this.checkQuadDisponibles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkQuadDisponibles.Location = new System.Drawing.Point(33, 58);
            this.checkQuadDisponibles.Name = "checkQuadDisponibles";
            this.checkQuadDisponibles.Size = new System.Drawing.Size(154, 17);
            this.checkQuadDisponibles.TabIndex = 3;
            this.checkQuadDisponibles.Text = "Mostrar sólo los disponibles";
            this.checkQuadDisponibles.UseVisualStyleBackColor = true;
            // 
            // cbQuadFiltro
            // 
            this.cbQuadFiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbQuadFiltro.FormattingEnabled = true;
            this.cbQuadFiltro.Items.AddRange(new object[] {
            "(Sin filtro)",
            "Marca",
            "Matricula"});
            this.cbQuadFiltro.Location = new System.Drawing.Point(169, 26);
            this.cbQuadFiltro.Name = "cbQuadFiltro";
            this.cbQuadFiltro.Size = new System.Drawing.Size(121, 21);
            this.cbQuadFiltro.TabIndex = 1;
            this.cbQuadFiltro.SelectedIndexChanged += new System.EventHandler(this.cbQuadFiltro_SelectedIndexChanged);
            // 
            // btQuadBusca
            // 
            this.btQuadBusca.Location = new System.Drawing.Point(296, 24);
            this.btQuadBusca.Name = "btQuadBusca";
            this.btQuadBusca.Size = new System.Drawing.Size(100, 23);
            this.btQuadBusca.TabIndex = 0;
            this.btQuadBusca.Text = "BUSCAR";
            this.btQuadBusca.UseVisualStyleBackColor = true;
            this.btQuadBusca.Click += new System.EventHandler(this.btQuadBusca_Click);
            // 
            // tbQuadBusca
            // 
            this.tbQuadBusca.BackColor = System.Drawing.SystemColors.Window;
            this.tbQuadBusca.Enabled = false;
            this.tbQuadBusca.Location = new System.Drawing.Point(39, 26);
            this.tbQuadBusca.Name = "tbQuadBusca";
            this.tbQuadBusca.Size = new System.Drawing.Size(119, 20);
            this.tbQuadBusca.TabIndex = 0;
            // 
            // gbQuadInfo
            // 
            this.gbQuadInfo.Controls.Add(this.labelQuadsEnUso);
            this.gbQuadInfo.Controls.Add(this.labelQuadsTotal);
            this.gbQuadInfo.Controls.Add(this.label1);
            this.gbQuadInfo.Controls.Add(this.label2);
            this.gbQuadInfo.Location = new System.Drawing.Point(33, 29);
            this.gbQuadInfo.Name = "gbQuadInfo";
            this.gbQuadInfo.Size = new System.Drawing.Size(243, 91);
            this.gbQuadInfo.TabIndex = 0;
            this.gbQuadInfo.TabStop = false;
            this.gbQuadInfo.Text = "Información General";
            // 
            // labelQuadsEnUso
            // 
            this.labelQuadsEnUso.AutoSize = true;
            this.labelQuadsEnUso.Location = new System.Drawing.Point(144, 56);
            this.labelQuadsEnUso.Name = "labelQuadsEnUso";
            this.labelQuadsEnUso.Size = new System.Drawing.Size(0, 13);
            this.labelQuadsEnUso.TabIndex = 3;
            // 
            // labelQuadsTotal
            // 
            this.labelQuadsTotal.AutoSize = true;
            this.labelQuadsTotal.Location = new System.Drawing.Point(144, 33);
            this.labelQuadsTotal.Name = "labelQuadsTotal";
            this.labelQuadsTotal.Size = new System.Drawing.Size(0, 13);
            this.labelQuadsTotal.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "- Total de Quads:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "- Actualmente en uso:";
            // 
            // tpBicicleta
            // 
            this.tpBicicleta.Controls.Add(this.labelBiciNota);
            this.tpBicicleta.Controls.Add(this.gbBiciOpciones);
            this.tpBicicleta.Controls.Add(this.gbBiciPrecios);
            this.tpBicicleta.Controls.Add(this.gbBiciResultados);
            this.tpBicicleta.Controls.Add(this.gbBiciDatos);
            this.tpBicicleta.Controls.Add(this.gbBiciBusqueda);
            this.tpBicicleta.Controls.Add(this.gbBiciInfo);
            this.tpBicicleta.Location = new System.Drawing.Point(4, 22);
            this.tpBicicleta.Name = "tpBicicleta";
            this.tpBicicleta.Padding = new System.Windows.Forms.Padding(3);
            this.tpBicicleta.Size = new System.Drawing.Size(767, 578);
            this.tpBicicleta.TabIndex = 1;
            this.tpBicicleta.Text = "Bicicletas";
            this.tpBicicleta.UseVisualStyleBackColor = true;
            this.tpBicicleta.Enter += new System.EventHandler(this.tpBicicleta_Enter);
            // 
            // labelBiciNota
            // 
            this.labelBiciNota.AutoSize = true;
            this.labelBiciNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelBiciNota.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labelBiciNota.Location = new System.Drawing.Point(30, 550);
            this.labelBiciNota.Name = "labelBiciNota";
            this.labelBiciNota.Size = new System.Drawing.Size(320, 13);
            this.labelBiciNota.TabIndex = 24;
            this.labelBiciNota.Text = "NOTA: No se pueden BORRAR las Bicicletas que estén EN USO.";
            // 
            // gbBiciOpciones
            // 
            this.gbBiciOpciones.Controls.Add(this.btBiciBorrar);
            this.gbBiciOpciones.Controls.Add(this.btBiciReset);
            this.gbBiciOpciones.Controls.Add(this.btBiciInsertar);
            this.gbBiciOpciones.Controls.Add(this.btBiciEditar);
            this.gbBiciOpciones.Location = new System.Drawing.Point(352, 348);
            this.gbBiciOpciones.Name = "gbBiciOpciones";
            this.gbBiciOpciones.Size = new System.Drawing.Size(135, 179);
            this.gbBiciOpciones.TabIndex = 23;
            this.gbBiciOpciones.TabStop = false;
            this.gbBiciOpciones.Text = "Opciones";
            // 
            // btBiciBorrar
            // 
            this.btBiciBorrar.Enabled = false;
            this.btBiciBorrar.Location = new System.Drawing.Point(21, 86);
            this.btBiciBorrar.Name = "btBiciBorrar";
            this.btBiciBorrar.Size = new System.Drawing.Size(100, 23);
            this.btBiciBorrar.TabIndex = 2;
            this.btBiciBorrar.Text = "BORRAR";
            this.btBiciBorrar.UseVisualStyleBackColor = true;
            this.btBiciBorrar.Click += new System.EventHandler(this.btBiciBorrar_Click);
            // 
            // btBiciReset
            // 
            this.btBiciReset.Location = new System.Drawing.Point(21, 134);
            this.btBiciReset.Name = "btBiciReset";
            this.btBiciReset.Size = new System.Drawing.Size(100, 23);
            this.btBiciReset.TabIndex = 1;
            this.btBiciReset.Text = "RESET";
            this.btBiciReset.UseVisualStyleBackColor = true;
            this.btBiciReset.Click += new System.EventHandler(this.btBiciReset_Click);
            // 
            // btBiciInsertar
            // 
            this.btBiciInsertar.Location = new System.Drawing.Point(21, 28);
            this.btBiciInsertar.Name = "btBiciInsertar";
            this.btBiciInsertar.Size = new System.Drawing.Size(100, 23);
            this.btBiciInsertar.TabIndex = 0;
            this.btBiciInsertar.Text = "INSERTAR";
            this.btBiciInsertar.UseVisualStyleBackColor = true;
            this.btBiciInsertar.Click += new System.EventHandler(this.btBiciInsertar_Click);
            // 
            // btBiciEditar
            // 
            this.btBiciEditar.Enabled = false;
            this.btBiciEditar.Location = new System.Drawing.Point(21, 57);
            this.btBiciEditar.Name = "btBiciEditar";
            this.btBiciEditar.Size = new System.Drawing.Size(100, 23);
            this.btBiciEditar.TabIndex = 0;
            this.btBiciEditar.Text = "EDITAR";
            this.btBiciEditar.UseVisualStyleBackColor = true;
            this.btBiciEditar.Click += new System.EventHandler(this.btBiciEditar_Click);
            // 
            // gbBiciPrecios
            // 
            this.gbBiciPrecios.Controls.Add(this.btTipoBiciPrecios);
            this.gbBiciPrecios.Controls.Add(this.label25);
            this.gbBiciPrecios.Controls.Add(this.label16);
            this.gbBiciPrecios.Controls.Add(this.label14);
            this.gbBiciPrecios.Controls.Add(this.tbBiciPrecioTandem);
            this.gbBiciPrecios.Controls.Add(this.tbBiciPrecioAdulto);
            this.gbBiciPrecios.Controls.Add(this.tbBiciPrecioNinyo);
            this.gbBiciPrecios.Location = new System.Drawing.Point(520, 348);
            this.gbBiciPrecios.Name = "gbBiciPrecios";
            this.gbBiciPrecios.Size = new System.Drawing.Size(181, 179);
            this.gbBiciPrecios.TabIndex = 22;
            this.gbBiciPrecios.TabStop = false;
            this.gbBiciPrecios.Text = "Ajuste de Precios (EUROS)";
            // 
            // btTipoBiciPrecios
            // 
            this.btTipoBiciPrecios.Location = new System.Drawing.Point(60, 134);
            this.btTipoBiciPrecios.Name = "btTipoBiciPrecios";
            this.btTipoBiciPrecios.Size = new System.Drawing.Size(100, 23);
            this.btTipoBiciPrecios.TabIndex = 8;
            this.btTipoBiciPrecios.Text = "NUEVO PRECIO";
            this.btTipoBiciPrecios.UseVisualStyleBackColor = true;
            this.btTipoBiciPrecios.Click += new System.EventHandler(this.btTipoBiciPrecios_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(55, 92);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 13);
            this.label25.TabIndex = 7;
            this.label25.Text = "Tándem:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(64, 66);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Adulto:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(72, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Niño:";
            // 
            // tbBiciPrecioTandem
            // 
            this.tbBiciPrecioTandem.Location = new System.Drawing.Point(110, 89);
            this.tbBiciPrecioTandem.Name = "tbBiciPrecioTandem";
            this.tbBiciPrecioTandem.Size = new System.Drawing.Size(50, 20);
            this.tbBiciPrecioTandem.TabIndex = 4;
            // 
            // tbBiciPrecioAdulto
            // 
            this.tbBiciPrecioAdulto.Location = new System.Drawing.Point(110, 63);
            this.tbBiciPrecioAdulto.Name = "tbBiciPrecioAdulto";
            this.tbBiciPrecioAdulto.Size = new System.Drawing.Size(50, 20);
            this.tbBiciPrecioAdulto.TabIndex = 3;
            // 
            // tbBiciPrecioNinyo
            // 
            this.tbBiciPrecioNinyo.Location = new System.Drawing.Point(110, 34);
            this.tbBiciPrecioNinyo.Name = "tbBiciPrecioNinyo";
            this.tbBiciPrecioNinyo.Size = new System.Drawing.Size(50, 20);
            this.tbBiciPrecioNinyo.TabIndex = 2;
            // 
            // gbBiciResultados
            // 
            this.gbBiciResultados.Controls.Add(this.btBiciVolver);
            this.gbBiciResultados.Controls.Add(this.btBiciAsignar);
            this.gbBiciResultados.Controls.Add(this.dgvBiciResultados);
            this.gbBiciResultados.Location = new System.Drawing.Point(32, 139);
            this.gbBiciResultados.Name = "gbBiciResultados";
            this.gbBiciResultados.Size = new System.Drawing.Size(668, 190);
            this.gbBiciResultados.TabIndex = 21;
            this.gbBiciResultados.TabStop = false;
            this.gbBiciResultados.Text = "Resultados de la búsqueda";
            // 
            // btBiciVolver
            // 
            this.btBiciVolver.Location = new System.Drawing.Point(548, 121);
            this.btBiciVolver.Name = "btBiciVolver";
            this.btBiciVolver.Size = new System.Drawing.Size(100, 23);
            this.btBiciVolver.TabIndex = 13;
            this.btBiciVolver.Text = "VOLVER";
            this.btBiciVolver.UseVisualStyleBackColor = true;
            this.btBiciVolver.Visible = false;
            this.btBiciVolver.Click += new System.EventHandler(this.btBiciVolver_Click);
            // 
            // btBiciAsignar
            // 
            this.btBiciAsignar.Location = new System.Drawing.Point(548, 150);
            this.btBiciAsignar.Name = "btBiciAsignar";
            this.btBiciAsignar.Size = new System.Drawing.Size(100, 23);
            this.btBiciAsignar.TabIndex = 12;
            this.btBiciAsignar.Text = "ASIGNAR";
            this.btBiciAsignar.UseVisualStyleBackColor = true;
            this.btBiciAsignar.Visible = false;
            this.btBiciAsignar.Click += new System.EventHandler(this.btBiciAsignar_Click);
            // 
            // dgvBiciResultados
            // 
            this.dgvBiciResultados.AllowUserToAddRows = false;
            this.dgvBiciResultados.AllowUserToDeleteRows = false;
            this.dgvBiciResultados.AllowUserToResizeColumns = false;
            this.dgvBiciResultados.AllowUserToResizeRows = false;
            this.dgvBiciResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBiciResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BiciId,
            this.BiciTipo,
            this.BiciFechaAlta,
            this.BiciPrecio,
            this.BiciEnUso});
            this.dgvBiciResultados.Location = new System.Drawing.Point(44, 28);
            this.dgvBiciResultados.MultiSelect = false;
            this.dgvBiciResultados.Name = "dgvBiciResultados";
            this.dgvBiciResultados.ReadOnly = true;
            this.dgvBiciResultados.Size = new System.Drawing.Size(471, 145);
            this.dgvBiciResultados.TabIndex = 11;
            this.dgvBiciResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvBiciResultados_RowHeaderMouseClick);
            // 
            // BiciId
            // 
            this.BiciId.HeaderText = "ID";
            this.BiciId.Name = "BiciId";
            this.BiciId.ReadOnly = true;
            this.BiciId.Width = 45;
            // 
            // BiciTipo
            // 
            this.BiciTipo.HeaderText = "Tipo";
            this.BiciTipo.Name = "BiciTipo";
            this.BiciTipo.ReadOnly = true;
            // 
            // BiciFechaAlta
            // 
            this.BiciFechaAlta.HeaderText = "Fecha de Alta";
            this.BiciFechaAlta.Name = "BiciFechaAlta";
            this.BiciFechaAlta.ReadOnly = true;
            this.BiciFechaAlta.Width = 150;
            // 
            // BiciPrecio
            // 
            this.BiciPrecio.HeaderText = "Precio";
            this.BiciPrecio.Name = "BiciPrecio";
            this.BiciPrecio.ReadOnly = true;
            // 
            // BiciEnUso
            // 
            this.BiciEnUso.HeaderText = "En Uso";
            this.BiciEnUso.Name = "BiciEnUso";
            this.BiciEnUso.ReadOnly = true;
            this.BiciEnUso.Visible = false;
            // 
            // gbBiciDatos
            // 
            this.gbBiciDatos.Controls.Add(this.label33);
            this.gbBiciDatos.Controls.Add(this.tbBiciID);
            this.gbBiciDatos.Controls.Add(this.checkBiciUso);
            this.gbBiciDatos.Controls.Add(this.dtpBiciFecha);
            this.gbBiciDatos.Controls.Add(this.label15);
            this.gbBiciDatos.Controls.Add(this.cbBiciTipo);
            this.gbBiciDatos.Controls.Add(this.label13);
            this.gbBiciDatos.Location = new System.Drawing.Point(33, 348);
            this.gbBiciDatos.Name = "gbBiciDatos";
            this.gbBiciDatos.Size = new System.Drawing.Size(313, 179);
            this.gbBiciDatos.TabIndex = 12;
            this.gbBiciDatos.TabStop = false;
            this.gbBiciDatos.Text = "Datos de la Bicicleta";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label33.Location = new System.Drawing.Point(245, 140);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(21, 13);
            this.label33.TabIndex = 23;
            this.label33.Text = "ID:";
            this.label33.Visible = false;
            // 
            // tbBiciID
            // 
            this.tbBiciID.Enabled = false;
            this.tbBiciID.Location = new System.Drawing.Point(266, 137);
            this.tbBiciID.Name = "tbBiciID";
            this.tbBiciID.Size = new System.Drawing.Size(31, 20);
            this.tbBiciID.TabIndex = 22;
            this.tbBiciID.Visible = false;
            // 
            // checkBiciUso
            // 
            this.checkBiciUso.AutoSize = true;
            this.checkBiciUso.Location = new System.Drawing.Point(97, 86);
            this.checkBiciUso.Name = "checkBiciUso";
            this.checkBiciUso.Size = new System.Drawing.Size(59, 17);
            this.checkBiciUso.TabIndex = 7;
            this.checkBiciUso.Text = "En uso";
            this.checkBiciUso.UseVisualStyleBackColor = true;
            // 
            // dtpBiciFecha
            // 
            this.dtpBiciFecha.Location = new System.Drawing.Point(97, 60);
            this.dtpBiciFecha.Name = "dtpBiciFecha";
            this.dtpBiciFecha.Size = new System.Drawing.Size(200, 20);
            this.dtpBiciFecha.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Fecha de Alta:";
            // 
            // cbBiciTipo
            // 
            this.cbBiciTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBiciTipo.FormattingEnabled = true;
            this.cbBiciTipo.Items.AddRange(new object[] {
            "Niño",
            "Adulto",
            "Tándem"});
            this.cbBiciTipo.Location = new System.Drawing.Point(97, 33);
            this.cbBiciTipo.Name = "cbBiciTipo";
            this.cbBiciTipo.Size = new System.Drawing.Size(121, 21);
            this.cbBiciTipo.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(60, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tipo:";
            // 
            // gbBiciBusqueda
            // 
            this.gbBiciBusqueda.Controls.Add(this.checkBiciDisponibles);
            this.gbBiciBusqueda.Controls.Add(this.cbBiciFiltro);
            this.gbBiciBusqueda.Controls.Add(this.btBiciBusca);
            this.gbBiciBusqueda.Location = new System.Drawing.Point(417, 29);
            this.gbBiciBusqueda.Name = "gbBiciBusqueda";
            this.gbBiciBusqueda.Size = new System.Drawing.Size(284, 95);
            this.gbBiciBusqueda.TabIndex = 10;
            this.gbBiciBusqueda.TabStop = false;
            this.gbBiciBusqueda.Text = "Filtrado de Bicicletas (por tipo)";
            // 
            // checkBiciDisponibles
            // 
            this.checkBiciDisponibles.AutoSize = true;
            this.checkBiciDisponibles.Checked = true;
            this.checkBiciDisponibles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBiciDisponibles.Location = new System.Drawing.Point(33, 58);
            this.checkBiciDisponibles.Name = "checkBiciDisponibles";
            this.checkBiciDisponibles.Size = new System.Drawing.Size(154, 17);
            this.checkBiciDisponibles.TabIndex = 3;
            this.checkBiciDisponibles.Text = "Mostrar sólo las disponibles";
            this.checkBiciDisponibles.UseVisualStyleBackColor = true;
            // 
            // cbBiciFiltro
            // 
            this.cbBiciFiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBiciFiltro.FormattingEnabled = true;
            this.cbBiciFiltro.Items.AddRange(new object[] {
            "(Ver todas)",
            "Niño",
            "Adulto",
            "Tándem"});
            this.cbBiciFiltro.Location = new System.Drawing.Point(33, 27);
            this.cbBiciFiltro.Name = "cbBiciFiltro";
            this.cbBiciFiltro.Size = new System.Drawing.Size(121, 21);
            this.cbBiciFiltro.TabIndex = 2;
            // 
            // btBiciBusca
            // 
            this.btBiciBusca.Location = new System.Drawing.Point(163, 25);
            this.btBiciBusca.Name = "btBiciBusca";
            this.btBiciBusca.Size = new System.Drawing.Size(100, 23);
            this.btBiciBusca.TabIndex = 1;
            this.btBiciBusca.Text = "BUSCAR";
            this.btBiciBusca.UseVisualStyleBackColor = true;
            this.btBiciBusca.Click += new System.EventHandler(this.btBiciBusca_Click);
            // 
            // gbBiciInfo
            // 
            this.gbBiciInfo.Controls.Add(this.labelBicisUso);
            this.gbBiciInfo.Controls.Add(this.labelBicisTotal);
            this.gbBiciInfo.Controls.Add(this.label11);
            this.gbBiciInfo.Controls.Add(this.label12);
            this.gbBiciInfo.Location = new System.Drawing.Point(33, 29);
            this.gbBiciInfo.Name = "gbBiciInfo";
            this.gbBiciInfo.Size = new System.Drawing.Size(243, 95);
            this.gbBiciInfo.TabIndex = 9;
            this.gbBiciInfo.TabStop = false;
            this.gbBiciInfo.Text = "Información General";
            // 
            // labelBicisUso
            // 
            this.labelBicisUso.AutoSize = true;
            this.labelBicisUso.Location = new System.Drawing.Point(144, 56);
            this.labelBicisUso.Name = "labelBicisUso";
            this.labelBicisUso.Size = new System.Drawing.Size(0, 13);
            this.labelBicisUso.TabIndex = 3;
            // 
            // labelBicisTotal
            // 
            this.labelBicisTotal.AutoSize = true;
            this.labelBicisTotal.Location = new System.Drawing.Point(144, 33);
            this.labelBicisTotal.Name = "labelBicisTotal";
            this.labelBicisTotal.Size = new System.Drawing.Size(0, 13);
            this.labelBicisTotal.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "- Total de Bicicletas:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "- Actualmente en uso:";
            // 
            // tpCaballo
            // 
            this.tpCaballo.Controls.Add(this.gbCaballoOpciones);
            this.tpCaballo.Controls.Add(this.gbCaballoResultados);
            this.tpCaballo.Controls.Add(this.gbCaballoDatos);
            this.tpCaballo.Controls.Add(this.gbCaballoInfo);
            this.tpCaballo.Location = new System.Drawing.Point(4, 22);
            this.tpCaballo.Name = "tpCaballo";
            this.tpCaballo.Size = new System.Drawing.Size(767, 578);
            this.tpCaballo.TabIndex = 2;
            this.tpCaballo.Text = "Paseos a Caballo";
            this.tpCaballo.UseVisualStyleBackColor = true;
            this.tpCaballo.Enter += new System.EventHandler(this.tpCaballo_Enter);
            // 
            // gbCaballoOpciones
            // 
            this.gbCaballoOpciones.Controls.Add(this.btCaballoBorrar);
            this.gbCaballoOpciones.Controls.Add(this.btCaballoReset);
            this.gbCaballoOpciones.Controls.Add(this.btCaballoInsertar);
            this.gbCaballoOpciones.Controls.Add(this.btCaballoEditar);
            this.gbCaballoOpciones.Location = new System.Drawing.Point(577, 290);
            this.gbCaballoOpciones.Name = "gbCaballoOpciones";
            this.gbCaballoOpciones.Size = new System.Drawing.Size(135, 213);
            this.gbCaballoOpciones.TabIndex = 24;
            this.gbCaballoOpciones.TabStop = false;
            this.gbCaballoOpciones.Text = "Opciones";
            // 
            // btCaballoBorrar
            // 
            this.btCaballoBorrar.Enabled = false;
            this.btCaballoBorrar.Location = new System.Drawing.Point(21, 86);
            this.btCaballoBorrar.Name = "btCaballoBorrar";
            this.btCaballoBorrar.Size = new System.Drawing.Size(100, 23);
            this.btCaballoBorrar.TabIndex = 2;
            this.btCaballoBorrar.Text = "BORRAR";
            this.btCaballoBorrar.UseVisualStyleBackColor = true;
            this.btCaballoBorrar.Click += new System.EventHandler(this.btCaballoBorrar_Click);
            // 
            // btCaballoReset
            // 
            this.btCaballoReset.Location = new System.Drawing.Point(21, 134);
            this.btCaballoReset.Name = "btCaballoReset";
            this.btCaballoReset.Size = new System.Drawing.Size(100, 23);
            this.btCaballoReset.TabIndex = 1;
            this.btCaballoReset.Text = "RESET";
            this.btCaballoReset.UseVisualStyleBackColor = true;
            this.btCaballoReset.Click += new System.EventHandler(this.btCaballoReset_Click);
            // 
            // btCaballoInsertar
            // 
            this.btCaballoInsertar.Location = new System.Drawing.Point(21, 28);
            this.btCaballoInsertar.Name = "btCaballoInsertar";
            this.btCaballoInsertar.Size = new System.Drawing.Size(100, 23);
            this.btCaballoInsertar.TabIndex = 0;
            this.btCaballoInsertar.Text = "INSERTAR";
            this.btCaballoInsertar.UseVisualStyleBackColor = true;
            this.btCaballoInsertar.Click += new System.EventHandler(this.btCaballoInsertar_Click);
            // 
            // btCaballoEditar
            // 
            this.btCaballoEditar.Enabled = false;
            this.btCaballoEditar.Location = new System.Drawing.Point(21, 57);
            this.btCaballoEditar.Name = "btCaballoEditar";
            this.btCaballoEditar.Size = new System.Drawing.Size(100, 23);
            this.btCaballoEditar.TabIndex = 0;
            this.btCaballoEditar.Text = "EDITAR";
            this.btCaballoEditar.UseVisualStyleBackColor = true;
            this.btCaballoEditar.Click += new System.EventHandler(this.btCaballoEditar_Click);
            // 
            // gbCaballoResultados
            // 
            this.gbCaballoResultados.Controls.Add(this.btCaballoVolver);
            this.gbCaballoResultados.Controls.Add(this.btCaballoAsignar);
            this.gbCaballoResultados.Controls.Add(this.dgvCaballoResultados);
            this.gbCaballoResultados.Location = new System.Drawing.Point(232, 29);
            this.gbCaballoResultados.Name = "gbCaballoResultados";
            this.gbCaballoResultados.Size = new System.Drawing.Size(480, 240);
            this.gbCaballoResultados.TabIndex = 13;
            this.gbCaballoResultados.TabStop = false;
            this.gbCaballoResultados.Text = "Paseos a Caballo disponibles";
            // 
            // btCaballoVolver
            // 
            this.btCaballoVolver.Location = new System.Drawing.Point(257, 202);
            this.btCaballoVolver.Name = "btCaballoVolver";
            this.btCaballoVolver.Size = new System.Drawing.Size(100, 23);
            this.btCaballoVolver.TabIndex = 14;
            this.btCaballoVolver.Text = "VOLVER";
            this.btCaballoVolver.UseVisualStyleBackColor = true;
            this.btCaballoVolver.Visible = false;
            this.btCaballoVolver.Click += new System.EventHandler(this.btCaballoVolver_Click);
            // 
            // btCaballoAsignar
            // 
            this.btCaballoAsignar.Location = new System.Drawing.Point(363, 202);
            this.btCaballoAsignar.Name = "btCaballoAsignar";
            this.btCaballoAsignar.Size = new System.Drawing.Size(100, 23);
            this.btCaballoAsignar.TabIndex = 13;
            this.btCaballoAsignar.Text = "ASIGNAR";
            this.btCaballoAsignar.UseVisualStyleBackColor = true;
            this.btCaballoAsignar.Visible = false;
            this.btCaballoAsignar.Click += new System.EventHandler(this.btCaballoAsignar_Click);
            // 
            // dgvCaballoResultados
            // 
            this.dgvCaballoResultados.AllowUserToAddRows = false;
            this.dgvCaballoResultados.AllowUserToDeleteRows = false;
            this.dgvCaballoResultados.AllowUserToResizeColumns = false;
            this.dgvCaballoResultados.AllowUserToResizeRows = false;
            this.dgvCaballoResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaballoResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.paseoID,
            this.paseoNombreRuta,
            this.paseoDescripcion,
            this.paseoDuracion,
            this.paseoPrecio,
            this.paseoConPony});
            this.dgvCaballoResultados.Location = new System.Drawing.Point(17, 28);
            this.dgvCaballoResultados.MultiSelect = false;
            this.dgvCaballoResultados.Name = "dgvCaballoResultados";
            this.dgvCaballoResultados.ReadOnly = true;
            this.dgvCaballoResultados.Size = new System.Drawing.Size(446, 160);
            this.dgvCaballoResultados.TabIndex = 12;
            this.dgvCaballoResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCaballoResultados_RowHeaderMouseClick);
            // 
            // paseoID
            // 
            this.paseoID.HeaderText = "ID";
            this.paseoID.Name = "paseoID";
            this.paseoID.ReadOnly = true;
            this.paseoID.Width = 30;
            // 
            // paseoNombreRuta
            // 
            this.paseoNombreRuta.HeaderText = "Nombre Ruta";
            this.paseoNombreRuta.Name = "paseoNombreRuta";
            this.paseoNombreRuta.ReadOnly = true;
            // 
            // paseoDescripcion
            // 
            this.paseoDescripcion.HeaderText = "Descripción";
            this.paseoDescripcion.Name = "paseoDescripcion";
            this.paseoDescripcion.ReadOnly = true;
            // 
            // paseoDuracion
            // 
            this.paseoDuracion.HeaderText = "Duración";
            this.paseoDuracion.Name = "paseoDuracion";
            this.paseoDuracion.ReadOnly = true;
            this.paseoDuracion.Width = 60;
            // 
            // paseoPrecio
            // 
            this.paseoPrecio.HeaderText = "Precio";
            this.paseoPrecio.Name = "paseoPrecio";
            this.paseoPrecio.ReadOnly = true;
            this.paseoPrecio.Width = 60;
            // 
            // paseoConPony
            // 
            this.paseoConPony.HeaderText = "Pony";
            this.paseoConPony.Name = "paseoConPony";
            this.paseoConPony.ReadOnly = true;
            this.paseoConPony.Width = 40;
            // 
            // gbCaballoDatos
            // 
            this.gbCaballoDatos.Controls.Add(this.label39);
            this.gbCaballoDatos.Controls.Add(this.tbPaseoID);
            this.gbCaballoDatos.Controls.Add(this.checkCaballoPony);
            this.gbCaballoDatos.Controls.Add(this.tbCaballoPrecio);
            this.gbCaballoDatos.Controls.Add(this.label24);
            this.gbCaballoDatos.Controls.Add(this.tbCaballoRuta);
            this.gbCaballoDatos.Controls.Add(this.tbCaballoDescripcion);
            this.gbCaballoDatos.Controls.Add(this.label23);
            this.gbCaballoDatos.Controls.Add(this.label22);
            this.gbCaballoDatos.Controls.Add(this.tbCaballoDuracion);
            this.gbCaballoDatos.Controls.Add(this.label21);
            this.gbCaballoDatos.Controls.Add(this.label19);
            this.gbCaballoDatos.Controls.Add(this.label20);
            this.gbCaballoDatos.Location = new System.Drawing.Point(232, 290);
            this.gbCaballoDatos.Name = "gbCaballoDatos";
            this.gbCaballoDatos.Size = new System.Drawing.Size(330, 213);
            this.gbCaballoDatos.TabIndex = 11;
            this.gbCaballoDatos.TabStop = false;
            this.gbCaballoDatos.Text = "Datos del Paseo a Caballo";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label39.Location = new System.Drawing.Point(262, 179);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(21, 13);
            this.label39.TabIndex = 25;
            this.label39.Text = "ID:";
            this.label39.Visible = false;
            // 
            // tbPaseoID
            // 
            this.tbPaseoID.Enabled = false;
            this.tbPaseoID.Location = new System.Drawing.Point(283, 176);
            this.tbPaseoID.Name = "tbPaseoID";
            this.tbPaseoID.Size = new System.Drawing.Size(31, 20);
            this.tbPaseoID.TabIndex = 24;
            this.tbPaseoID.Visible = false;
            // 
            // checkCaballoPony
            // 
            this.checkCaballoPony.AutoSize = true;
            this.checkCaballoPony.Location = new System.Drawing.Point(111, 179);
            this.checkCaballoPony.Name = "checkCaballoPony";
            this.checkCaballoPony.Size = new System.Drawing.Size(15, 14);
            this.checkCaballoPony.TabIndex = 5;
            this.checkCaballoPony.UseVisualStyleBackColor = true;
            // 
            // tbCaballoPrecio
            // 
            this.tbCaballoPrecio.Location = new System.Drawing.Point(111, 153);
            this.tbCaballoPrecio.Name = "tbCaballoPrecio";
            this.tbCaballoPrecio.Size = new System.Drawing.Size(59, 20);
            this.tbCaballoPrecio.TabIndex = 4;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(72, 36);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(33, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Ruta:";
            // 
            // tbCaballoRuta
            // 
            this.tbCaballoRuta.Location = new System.Drawing.Point(111, 33);
            this.tbCaballoRuta.Name = "tbCaballoRuta";
            this.tbCaballoRuta.Size = new System.Drawing.Size(203, 20);
            this.tbCaballoRuta.TabIndex = 1;
            // 
            // tbCaballoDescripcion
            // 
            this.tbCaballoDescripcion.Location = new System.Drawing.Point(111, 61);
            this.tbCaballoDescripcion.Multiline = true;
            this.tbCaballoDescripcion.Name = "tbCaballoDescripcion";
            this.tbCaballoDescripcion.Size = new System.Drawing.Size(203, 60);
            this.tbCaballoDescripcion.TabIndex = 2;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(176, 130);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 13);
            this.label23.TabIndex = 14;
            this.label23.Text = "minutos";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(65, 156);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Precio:";
            // 
            // tbCaballoDuracion
            // 
            this.tbCaballoDuracion.Location = new System.Drawing.Point(111, 127);
            this.tbCaballoDuracion.Name = "tbCaballoDuracion";
            this.tbCaballoDuracion.Size = new System.Drawing.Size(59, 20);
            this.tbCaballoDuracion.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(39, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(66, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Descripción:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(53, 130);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Duración:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(56, 179);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "En pony:";
            // 
            // gbCaballoInfo
            // 
            this.gbCaballoInfo.Controls.Add(this.labelPaseosPony);
            this.gbCaballoInfo.Controls.Add(this.labelPaseosTotal);
            this.gbCaballoInfo.Controls.Add(this.label26);
            this.gbCaballoInfo.Controls.Add(this.label17);
            this.gbCaballoInfo.Location = new System.Drawing.Point(33, 29);
            this.gbCaballoInfo.Name = "gbCaballoInfo";
            this.gbCaballoInfo.Size = new System.Drawing.Size(182, 95);
            this.gbCaballoInfo.TabIndex = 10;
            this.gbCaballoInfo.TabStop = false;
            this.gbCaballoInfo.Text = "Información General";
            // 
            // labelPaseosPony
            // 
            this.labelPaseosPony.Location = new System.Drawing.Point(119, 56);
            this.labelPaseosPony.Name = "labelPaseosPony";
            this.labelPaseosPony.Size = new System.Drawing.Size(41, 13);
            this.labelPaseosPony.TabIndex = 2;
            // 
            // labelPaseosTotal
            // 
            this.labelPaseosTotal.Location = new System.Drawing.Point(119, 33);
            this.labelPaseosTotal.Name = "labelPaseosTotal";
            this.labelPaseosTotal.Size = new System.Drawing.Size(41, 13);
            this.labelPaseosTotal.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(19, 56);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "- Rutas para Niños:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "- Total de Rutas:";
            // 
            // tpArco
            // 
            this.tpArco.Controls.Add(this.gbArcoOpciones);
            this.tpArco.Controls.Add(this.gbArcoResultados);
            this.tpArco.Controls.Add(this.gbArcoDatos);
            this.tpArco.Controls.Add(this.gbArcoInfo);
            this.tpArco.Controls.Add(this.label18);
            this.tpArco.Location = new System.Drawing.Point(4, 22);
            this.tpArco.Name = "tpArco";
            this.tpArco.Size = new System.Drawing.Size(767, 578);
            this.tpArco.TabIndex = 3;
            this.tpArco.Text = "Tiro con Arco";
            this.tpArco.UseVisualStyleBackColor = true;
            this.tpArco.Enter += new System.EventHandler(this.tpArco_Enter);
            // 
            // gbArcoOpciones
            // 
            this.gbArcoOpciones.Controls.Add(this.btArcoBorrar);
            this.gbArcoOpciones.Controls.Add(this.btArcoReset);
            this.gbArcoOpciones.Controls.Add(this.btArcoInsertar);
            this.gbArcoOpciones.Controls.Add(this.btArcoEditar);
            this.gbArcoOpciones.Location = new System.Drawing.Point(577, 290);
            this.gbArcoOpciones.Name = "gbArcoOpciones";
            this.gbArcoOpciones.Size = new System.Drawing.Size(135, 179);
            this.gbArcoOpciones.TabIndex = 25;
            this.gbArcoOpciones.TabStop = false;
            this.gbArcoOpciones.Text = "Opciones";
            // 
            // btArcoBorrar
            // 
            this.btArcoBorrar.Enabled = false;
            this.btArcoBorrar.Location = new System.Drawing.Point(21, 86);
            this.btArcoBorrar.Name = "btArcoBorrar";
            this.btArcoBorrar.Size = new System.Drawing.Size(100, 23);
            this.btArcoBorrar.TabIndex = 2;
            this.btArcoBorrar.Text = "BORRAR";
            this.btArcoBorrar.UseVisualStyleBackColor = true;
            this.btArcoBorrar.Click += new System.EventHandler(this.btArcoBorrar_Click);
            // 
            // btArcoReset
            // 
            this.btArcoReset.Location = new System.Drawing.Point(21, 134);
            this.btArcoReset.Name = "btArcoReset";
            this.btArcoReset.Size = new System.Drawing.Size(100, 23);
            this.btArcoReset.TabIndex = 1;
            this.btArcoReset.Text = "RESET";
            this.btArcoReset.UseVisualStyleBackColor = true;
            this.btArcoReset.Click += new System.EventHandler(this.btArcoReset_Click);
            // 
            // btArcoInsertar
            // 
            this.btArcoInsertar.Location = new System.Drawing.Point(21, 28);
            this.btArcoInsertar.Name = "btArcoInsertar";
            this.btArcoInsertar.Size = new System.Drawing.Size(100, 23);
            this.btArcoInsertar.TabIndex = 0;
            this.btArcoInsertar.Text = "INSERTAR";
            this.btArcoInsertar.UseVisualStyleBackColor = true;
            this.btArcoInsertar.Click += new System.EventHandler(this.btArcoInsertar_Click);
            // 
            // btArcoEditar
            // 
            this.btArcoEditar.Enabled = false;
            this.btArcoEditar.Location = new System.Drawing.Point(21, 57);
            this.btArcoEditar.Name = "btArcoEditar";
            this.btArcoEditar.Size = new System.Drawing.Size(100, 23);
            this.btArcoEditar.TabIndex = 0;
            this.btArcoEditar.Text = "EDITAR";
            this.btArcoEditar.UseVisualStyleBackColor = true;
            this.btArcoEditar.Click += new System.EventHandler(this.btArcoEditar_Click);
            // 
            // gbArcoResultados
            // 
            this.gbArcoResultados.Controls.Add(this.btArcoVolver);
            this.gbArcoResultados.Controls.Add(this.btArcoAsignar);
            this.gbArcoResultados.Controls.Add(this.dgvArcoResultados);
            this.gbArcoResultados.Location = new System.Drawing.Point(232, 29);
            this.gbArcoResultados.Name = "gbArcoResultados";
            this.gbArcoResultados.Size = new System.Drawing.Size(480, 240);
            this.gbArcoResultados.TabIndex = 24;
            this.gbArcoResultados.TabStop = false;
            this.gbArcoResultados.Text = "Ofertas de Tiro con Arco disponibles";
            // 
            // btArcoVolver
            // 
            this.btArcoVolver.Location = new System.Drawing.Point(257, 202);
            this.btArcoVolver.Name = "btArcoVolver";
            this.btArcoVolver.Size = new System.Drawing.Size(100, 23);
            this.btArcoVolver.TabIndex = 14;
            this.btArcoVolver.Text = "VOLVER";
            this.btArcoVolver.UseVisualStyleBackColor = true;
            this.btArcoVolver.Visible = false;
            this.btArcoVolver.Click += new System.EventHandler(this.btArcoVolver_Click);
            // 
            // btArcoAsignar
            // 
            this.btArcoAsignar.Location = new System.Drawing.Point(363, 202);
            this.btArcoAsignar.Name = "btArcoAsignar";
            this.btArcoAsignar.Size = new System.Drawing.Size(100, 23);
            this.btArcoAsignar.TabIndex = 13;
            this.btArcoAsignar.Text = "ASIGNAR";
            this.btArcoAsignar.UseVisualStyleBackColor = true;
            this.btArcoAsignar.Visible = false;
            this.btArcoAsignar.Click += new System.EventHandler(this.btArcoAsignar_Click);
            // 
            // dgvArcoResultados
            // 
            this.dgvArcoResultados.AllowUserToAddRows = false;
            this.dgvArcoResultados.AllowUserToDeleteRows = false;
            this.dgvArcoResultados.AllowUserToResizeColumns = false;
            this.dgvArcoResultados.AllowUserToResizeRows = false;
            this.dgvArcoResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArcoResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tiroID,
            this.tiroNumDianas,
            this.tiroNumFlechas,
            this.tiroPrecio});
            this.dgvArcoResultados.Location = new System.Drawing.Point(17, 28);
            this.dgvArcoResultados.MultiSelect = false;
            this.dgvArcoResultados.Name = "dgvArcoResultados";
            this.dgvArcoResultados.ReadOnly = true;
            this.dgvArcoResultados.Size = new System.Drawing.Size(446, 160);
            this.dgvArcoResultados.TabIndex = 0;
            this.dgvArcoResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvArcoResultados_RowHeaderMouseClick);
            // 
            // tiroID
            // 
            this.tiroID.HeaderText = "ID";
            this.tiroID.Name = "tiroID";
            this.tiroID.ReadOnly = true;
            this.tiroID.Width = 50;
            // 
            // tiroNumDianas
            // 
            this.tiroNumDianas.HeaderText = "Núm. Dianas";
            this.tiroNumDianas.Name = "tiroNumDianas";
            this.tiroNumDianas.ReadOnly = true;
            // 
            // tiroNumFlechas
            // 
            this.tiroNumFlechas.HeaderText = "Núm. Flechas";
            this.tiroNumFlechas.Name = "tiroNumFlechas";
            this.tiroNumFlechas.ReadOnly = true;
            // 
            // tiroPrecio
            // 
            this.tiroPrecio.HeaderText = "Precio";
            this.tiroPrecio.Name = "tiroPrecio";
            this.tiroPrecio.ReadOnly = true;
            // 
            // gbArcoDatos
            // 
            this.gbArcoDatos.Controls.Add(this.label44);
            this.gbArcoDatos.Controls.Add(this.tbArcoID);
            this.gbArcoDatos.Controls.Add(this.label40);
            this.gbArcoDatos.Controls.Add(this.tbArcoPrecio);
            this.gbArcoDatos.Controls.Add(this.tbArcoNumFlechas);
            this.gbArcoDatos.Controls.Add(this.tbArcoNumDianas);
            this.gbArcoDatos.Controls.Add(this.label38);
            this.gbArcoDatos.Controls.Add(this.label37);
            this.gbArcoDatos.Controls.Add(this.label27);
            this.gbArcoDatos.Location = new System.Drawing.Point(232, 290);
            this.gbArcoDatos.Name = "gbArcoDatos";
            this.gbArcoDatos.Size = new System.Drawing.Size(330, 179);
            this.gbArcoDatos.TabIndex = 23;
            this.gbArcoDatos.TabStop = false;
            this.gbArcoDatos.Text = "Datos del Tiro con Arco";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label44.Location = new System.Drawing.Point(253, 140);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(21, 13);
            this.label44.TabIndex = 27;
            this.label44.Text = "ID:";
            this.label44.Visible = false;
            // 
            // tbArcoID
            // 
            this.tbArcoID.Enabled = false;
            this.tbArcoID.Location = new System.Drawing.Point(274, 137);
            this.tbArcoID.Name = "tbArcoID";
            this.tbArcoID.Size = new System.Drawing.Size(31, 20);
            this.tbArcoID.TabIndex = 26;
            this.tbArcoID.Visible = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(196, 97);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(34, 13);
            this.label40.TabIndex = 8;
            this.label40.Text = "Euros";
            // 
            // tbArcoPrecio
            // 
            this.tbArcoPrecio.Location = new System.Drawing.Point(128, 94);
            this.tbArcoPrecio.Name = "tbArcoPrecio";
            this.tbArcoPrecio.Size = new System.Drawing.Size(62, 20);
            this.tbArcoPrecio.TabIndex = 6;
            // 
            // tbArcoNumFlechas
            // 
            this.tbArcoNumFlechas.Location = new System.Drawing.Point(128, 64);
            this.tbArcoNumFlechas.Name = "tbArcoNumFlechas";
            this.tbArcoNumFlechas.Size = new System.Drawing.Size(62, 20);
            this.tbArcoNumFlechas.TabIndex = 5;
            // 
            // tbArcoNumDianas
            // 
            this.tbArcoNumDianas.Location = new System.Drawing.Point(128, 38);
            this.tbArcoNumDianas.Name = "tbArcoNumDianas";
            this.tbArcoNumDianas.Size = new System.Drawing.Size(62, 20);
            this.tbArcoNumDianas.TabIndex = 4;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(82, 97);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(40, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Precio:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(20, 67);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Número de Flechas:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(24, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(98, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Número de Dianas:";
            // 
            // gbArcoInfo
            // 
            this.gbArcoInfo.Controls.Add(this.labelArcoOfertas);
            this.gbArcoInfo.Controls.Add(this.label28);
            this.gbArcoInfo.Location = new System.Drawing.Point(33, 29);
            this.gbArcoInfo.Name = "gbArcoInfo";
            this.gbArcoInfo.Size = new System.Drawing.Size(182, 95);
            this.gbArcoInfo.TabIndex = 22;
            this.gbArcoInfo.TabStop = false;
            this.gbArcoInfo.Text = "Información General";
            // 
            // labelArcoOfertas
            // 
            this.labelArcoOfertas.AutoSize = true;
            this.labelArcoOfertas.Location = new System.Drawing.Point(122, 33);
            this.labelArcoOfertas.Name = "labelArcoOfertas";
            this.labelArcoOfertas.Size = new System.Drawing.Size(0, 13);
            this.labelArcoOfertas.TabIndex = 1;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(28, 33);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(92, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "- Total de Ofertas:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(41, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 0;
            // 
            // tpPiraguismo
            // 
            this.tpPiraguismo.Controls.Add(this.gbPiraguismoOpciones);
            this.tpPiraguismo.Controls.Add(this.gbPiraguismoDatos);
            this.tpPiraguismo.Controls.Add(this.gbPiraguismoResultados);
            this.tpPiraguismo.Controls.Add(this.gbPiraguismoInfo);
            this.tpPiraguismo.Location = new System.Drawing.Point(4, 22);
            this.tpPiraguismo.Name = "tpPiraguismo";
            this.tpPiraguismo.Size = new System.Drawing.Size(767, 578);
            this.tpPiraguismo.TabIndex = 4;
            this.tpPiraguismo.Text = "Piragüismo";
            this.tpPiraguismo.UseVisualStyleBackColor = true;
            this.tpPiraguismo.Enter += new System.EventHandler(this.tpPiraguismo_Enter);
            // 
            // gbPiraguismoOpciones
            // 
            this.gbPiraguismoOpciones.Controls.Add(this.btPiraguismoBorrar);
            this.gbPiraguismoOpciones.Controls.Add(this.btPiraguismoReset);
            this.gbPiraguismoOpciones.Controls.Add(this.btPiraguismoInsertar);
            this.gbPiraguismoOpciones.Controls.Add(this.btPiraguismoEditar);
            this.gbPiraguismoOpciones.Location = new System.Drawing.Point(577, 290);
            this.gbPiraguismoOpciones.Name = "gbPiraguismoOpciones";
            this.gbPiraguismoOpciones.Size = new System.Drawing.Size(135, 179);
            this.gbPiraguismoOpciones.TabIndex = 27;
            this.gbPiraguismoOpciones.TabStop = false;
            this.gbPiraguismoOpciones.Text = "Opciones";
            // 
            // btPiraguismoBorrar
            // 
            this.btPiraguismoBorrar.Enabled = false;
            this.btPiraguismoBorrar.Location = new System.Drawing.Point(21, 86);
            this.btPiraguismoBorrar.Name = "btPiraguismoBorrar";
            this.btPiraguismoBorrar.Size = new System.Drawing.Size(100, 23);
            this.btPiraguismoBorrar.TabIndex = 2;
            this.btPiraguismoBorrar.Text = "BORRAR";
            this.btPiraguismoBorrar.UseVisualStyleBackColor = true;
            this.btPiraguismoBorrar.Click += new System.EventHandler(this.btPiraguismoBorrar_Click);
            // 
            // btPiraguismoReset
            // 
            this.btPiraguismoReset.Location = new System.Drawing.Point(21, 134);
            this.btPiraguismoReset.Name = "btPiraguismoReset";
            this.btPiraguismoReset.Size = new System.Drawing.Size(100, 23);
            this.btPiraguismoReset.TabIndex = 1;
            this.btPiraguismoReset.Text = "RESET";
            this.btPiraguismoReset.UseVisualStyleBackColor = true;
            this.btPiraguismoReset.Click += new System.EventHandler(this.btPiraguismoReset_Click);
            // 
            // btPiraguismoInsertar
            // 
            this.btPiraguismoInsertar.Location = new System.Drawing.Point(21, 28);
            this.btPiraguismoInsertar.Name = "btPiraguismoInsertar";
            this.btPiraguismoInsertar.Size = new System.Drawing.Size(100, 23);
            this.btPiraguismoInsertar.TabIndex = 0;
            this.btPiraguismoInsertar.Text = "INSERTAR";
            this.btPiraguismoInsertar.UseVisualStyleBackColor = true;
            this.btPiraguismoInsertar.Click += new System.EventHandler(this.btPiraguismoInsertar_Click);
            // 
            // btPiraguismoEditar
            // 
            this.btPiraguismoEditar.Enabled = false;
            this.btPiraguismoEditar.Location = new System.Drawing.Point(21, 57);
            this.btPiraguismoEditar.Name = "btPiraguismoEditar";
            this.btPiraguismoEditar.Size = new System.Drawing.Size(100, 23);
            this.btPiraguismoEditar.TabIndex = 0;
            this.btPiraguismoEditar.Text = "EDITAR";
            this.btPiraguismoEditar.UseVisualStyleBackColor = true;
            this.btPiraguismoEditar.Click += new System.EventHandler(this.btPiraguismoEditar_Click);
            // 
            // gbPiraguismoDatos
            // 
            this.gbPiraguismoDatos.Controls.Add(this.label55);
            this.gbPiraguismoDatos.Controls.Add(this.tbPiraguismoID);
            this.gbPiraguismoDatos.Controls.Add(this.label29);
            this.gbPiraguismoDatos.Controls.Add(this.tbPiraguismoPrecio);
            this.gbPiraguismoDatos.Controls.Add(this.tbPiraguismoNumChalecos);
            this.gbPiraguismoDatos.Controls.Add(this.tbPiraguismoPantano);
            this.gbPiraguismoDatos.Controls.Add(this.label35);
            this.gbPiraguismoDatos.Controls.Add(this.label41);
            this.gbPiraguismoDatos.Controls.Add(this.label42);
            this.gbPiraguismoDatos.Location = new System.Drawing.Point(232, 290);
            this.gbPiraguismoDatos.Name = "gbPiraguismoDatos";
            this.gbPiraguismoDatos.Size = new System.Drawing.Size(330, 179);
            this.gbPiraguismoDatos.TabIndex = 26;
            this.gbPiraguismoDatos.TabStop = false;
            this.gbPiraguismoDatos.Text = "Datos del Pantano";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label55.Location = new System.Drawing.Point(253, 140);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(21, 13);
            this.label55.TabIndex = 27;
            this.label55.Text = "ID:";
            this.label55.Visible = false;
            // 
            // tbPiraguismoID
            // 
            this.tbPiraguismoID.Enabled = false;
            this.tbPiraguismoID.Location = new System.Drawing.Point(274, 137);
            this.tbPiraguismoID.Name = "tbPiraguismoID";
            this.tbPiraguismoID.Size = new System.Drawing.Size(31, 20);
            this.tbPiraguismoID.TabIndex = 26;
            this.tbPiraguismoID.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(196, 97);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = "Euros";
            // 
            // tbPiraguismoPrecio
            // 
            this.tbPiraguismoPrecio.Location = new System.Drawing.Point(128, 94);
            this.tbPiraguismoPrecio.Name = "tbPiraguismoPrecio";
            this.tbPiraguismoPrecio.Size = new System.Drawing.Size(62, 20);
            this.tbPiraguismoPrecio.TabIndex = 6;
            // 
            // tbPiraguismoNumChalecos
            // 
            this.tbPiraguismoNumChalecos.Location = new System.Drawing.Point(128, 64);
            this.tbPiraguismoNumChalecos.Name = "tbPiraguismoNumChalecos";
            this.tbPiraguismoNumChalecos.Size = new System.Drawing.Size(62, 20);
            this.tbPiraguismoNumChalecos.TabIndex = 5;
            // 
            // tbPiraguismoPantano
            // 
            this.tbPiraguismoPantano.Location = new System.Drawing.Point(128, 38);
            this.tbPiraguismoPantano.Name = "tbPiraguismoPantano";
            this.tbPiraguismoPantano.Size = new System.Drawing.Size(156, 20);
            this.tbPiraguismoPantano.TabIndex = 4;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(82, 97);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(40, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Precio:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(13, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(109, 13);
            this.label41.TabIndex = 1;
            this.label41.Text = "Número de Chalecos:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(15, 41);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(107, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Nombre del Pantano:";
            // 
            // gbPiraguismoResultados
            // 
            this.gbPiraguismoResultados.Controls.Add(this.btPiraguismoVolver);
            this.gbPiraguismoResultados.Controls.Add(this.btPiraguismoAsignar);
            this.gbPiraguismoResultados.Controls.Add(this.dgvPiraguismoResultados);
            this.gbPiraguismoResultados.Location = new System.Drawing.Point(232, 29);
            this.gbPiraguismoResultados.Name = "gbPiraguismoResultados";
            this.gbPiraguismoResultados.Size = new System.Drawing.Size(480, 240);
            this.gbPiraguismoResultados.TabIndex = 25;
            this.gbPiraguismoResultados.TabStop = false;
            this.gbPiraguismoResultados.Text = "Lugares para practicar Piragüismo (Pantanos)";
            // 
            // btPiraguismoVolver
            // 
            this.btPiraguismoVolver.Location = new System.Drawing.Point(257, 202);
            this.btPiraguismoVolver.Name = "btPiraguismoVolver";
            this.btPiraguismoVolver.Size = new System.Drawing.Size(100, 23);
            this.btPiraguismoVolver.TabIndex = 14;
            this.btPiraguismoVolver.Text = "VOLVER";
            this.btPiraguismoVolver.UseVisualStyleBackColor = true;
            this.btPiraguismoVolver.Visible = false;
            this.btPiraguismoVolver.Click += new System.EventHandler(this.btPiraguismoVolver_Click);
            // 
            // btPiraguismoAsignar
            // 
            this.btPiraguismoAsignar.Location = new System.Drawing.Point(363, 202);
            this.btPiraguismoAsignar.Name = "btPiraguismoAsignar";
            this.btPiraguismoAsignar.Size = new System.Drawing.Size(100, 23);
            this.btPiraguismoAsignar.TabIndex = 13;
            this.btPiraguismoAsignar.Text = "ASIGNAR";
            this.btPiraguismoAsignar.UseVisualStyleBackColor = true;
            this.btPiraguismoAsignar.Visible = false;
            this.btPiraguismoAsignar.Click += new System.EventHandler(this.btPiraguismoAsignar_Click);
            // 
            // dgvPiraguismoResultados
            // 
            this.dgvPiraguismoResultados.AllowUserToAddRows = false;
            this.dgvPiraguismoResultados.AllowUserToDeleteRows = false;
            this.dgvPiraguismoResultados.AllowUserToResizeColumns = false;
            this.dgvPiraguismoResultados.AllowUserToResizeRows = false;
            this.dgvPiraguismoResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPiraguismoResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.piragID,
            this.piragPantano,
            this.piragNumChalecos,
            this.piragPrecio});
            this.dgvPiraguismoResultados.Location = new System.Drawing.Point(17, 28);
            this.dgvPiraguismoResultados.MultiSelect = false;
            this.dgvPiraguismoResultados.Name = "dgvPiraguismoResultados";
            this.dgvPiraguismoResultados.ReadOnly = true;
            this.dgvPiraguismoResultados.Size = new System.Drawing.Size(446, 160);
            this.dgvPiraguismoResultados.TabIndex = 0;
            this.dgvPiraguismoResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPiraguismoResultados_RowHeaderMouseClick);
            // 
            // piragID
            // 
            this.piragID.HeaderText = "ID";
            this.piragID.Name = "piragID";
            this.piragID.ReadOnly = true;
            this.piragID.Width = 30;
            // 
            // piragPantano
            // 
            this.piragPantano.HeaderText = "Pantano";
            this.piragPantano.Name = "piragPantano";
            this.piragPantano.ReadOnly = true;
            this.piragPantano.Width = 150;
            // 
            // piragNumChalecos
            // 
            this.piragNumChalecos.HeaderText = "Núm. Chalecos";
            this.piragNumChalecos.Name = "piragNumChalecos";
            this.piragNumChalecos.ReadOnly = true;
            this.piragNumChalecos.Width = 120;
            // 
            // piragPrecio
            // 
            this.piragPrecio.HeaderText = "Precio";
            this.piragPrecio.Name = "piragPrecio";
            this.piragPrecio.ReadOnly = true;
            this.piragPrecio.Width = 80;
            // 
            // gbPiraguismoInfo
            // 
            this.gbPiraguismoInfo.Controls.Add(this.labelPiragChalecos);
            this.gbPiraguismoInfo.Controls.Add(this.labelPiragPantanos);
            this.gbPiraguismoInfo.Controls.Add(this.label59);
            this.gbPiraguismoInfo.Controls.Add(this.label30);
            this.gbPiraguismoInfo.Location = new System.Drawing.Point(33, 29);
            this.gbPiraguismoInfo.Name = "gbPiraguismoInfo";
            this.gbPiraguismoInfo.Size = new System.Drawing.Size(182, 95);
            this.gbPiraguismoInfo.TabIndex = 11;
            this.gbPiraguismoInfo.TabStop = false;
            this.gbPiraguismoInfo.Text = "Información General";
            // 
            // labelPiragChalecos
            // 
            this.labelPiragChalecos.AutoSize = true;
            this.labelPiragChalecos.Location = new System.Drawing.Point(125, 56);
            this.labelPiragChalecos.Name = "labelPiragChalecos";
            this.labelPiragChalecos.Size = new System.Drawing.Size(0, 13);
            this.labelPiragChalecos.TabIndex = 3;
            // 
            // labelPiragPantanos
            // 
            this.labelPiragPantanos.AutoSize = true;
            this.labelPiragPantanos.Location = new System.Drawing.Point(125, 33);
            this.labelPiragPantanos.Name = "labelPiragPantanos";
            this.labelPiragPantanos.Size = new System.Drawing.Size(0, 13);
            this.labelPiragPantanos.TabIndex = 2;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(21, 56);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(102, 13);
            this.label59.TabIndex = 1;
            this.label59.Text = "- Total de Chalecos:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(20, 33);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "- Total de Pantanos:";
            // 
            // tpRapel
            // 
            this.tpRapel.Controls.Add(this.gbRapelOpciones);
            this.tpRapel.Controls.Add(this.gbRapelDatos);
            this.tpRapel.Controls.Add(this.gbRapelResultados);
            this.tpRapel.Controls.Add(this.gbRapelInfo);
            this.tpRapel.Location = new System.Drawing.Point(4, 22);
            this.tpRapel.Name = "tpRapel";
            this.tpRapel.Size = new System.Drawing.Size(767, 578);
            this.tpRapel.TabIndex = 5;
            this.tpRapel.Text = "Rápel";
            this.tpRapel.UseVisualStyleBackColor = true;
            this.tpRapel.Enter += new System.EventHandler(this.tpRapel_Enter);
            // 
            // gbRapelOpciones
            // 
            this.gbRapelOpciones.Controls.Add(this.btRapelBorrar);
            this.gbRapelOpciones.Controls.Add(this.btRapelReset);
            this.gbRapelOpciones.Controls.Add(this.btRapelInsertar);
            this.gbRapelOpciones.Controls.Add(this.btRapelEditar);
            this.gbRapelOpciones.Location = new System.Drawing.Point(577, 290);
            this.gbRapelOpciones.Name = "gbRapelOpciones";
            this.gbRapelOpciones.Size = new System.Drawing.Size(135, 179);
            this.gbRapelOpciones.TabIndex = 27;
            this.gbRapelOpciones.TabStop = false;
            this.gbRapelOpciones.Text = "Opciones";
            // 
            // btRapelBorrar
            // 
            this.btRapelBorrar.Enabled = false;
            this.btRapelBorrar.Location = new System.Drawing.Point(21, 86);
            this.btRapelBorrar.Name = "btRapelBorrar";
            this.btRapelBorrar.Size = new System.Drawing.Size(100, 23);
            this.btRapelBorrar.TabIndex = 2;
            this.btRapelBorrar.Text = "BORRAR";
            this.btRapelBorrar.UseVisualStyleBackColor = true;
            this.btRapelBorrar.Click += new System.EventHandler(this.btRapelBorrar_Click);
            // 
            // btRapelReset
            // 
            this.btRapelReset.Location = new System.Drawing.Point(21, 134);
            this.btRapelReset.Name = "btRapelReset";
            this.btRapelReset.Size = new System.Drawing.Size(100, 23);
            this.btRapelReset.TabIndex = 1;
            this.btRapelReset.Text = "RESET";
            this.btRapelReset.UseVisualStyleBackColor = true;
            this.btRapelReset.Click += new System.EventHandler(this.btRapelReset_Click);
            // 
            // btRapelInsertar
            // 
            this.btRapelInsertar.Location = new System.Drawing.Point(21, 28);
            this.btRapelInsertar.Name = "btRapelInsertar";
            this.btRapelInsertar.Size = new System.Drawing.Size(100, 23);
            this.btRapelInsertar.TabIndex = 0;
            this.btRapelInsertar.Text = "INSERTAR";
            this.btRapelInsertar.UseVisualStyleBackColor = true;
            this.btRapelInsertar.Click += new System.EventHandler(this.btRapelInsertar_Click);
            // 
            // btRapelEditar
            // 
            this.btRapelEditar.Enabled = false;
            this.btRapelEditar.Location = new System.Drawing.Point(21, 57);
            this.btRapelEditar.Name = "btRapelEditar";
            this.btRapelEditar.Size = new System.Drawing.Size(100, 23);
            this.btRapelEditar.TabIndex = 0;
            this.btRapelEditar.Text = "EDITAR";
            this.btRapelEditar.UseVisualStyleBackColor = true;
            this.btRapelEditar.Click += new System.EventHandler(this.btRapelEditar_Click);
            // 
            // gbRapelDatos
            // 
            this.gbRapelDatos.Controls.Add(this.label73);
            this.gbRapelDatos.Controls.Add(this.tbRapelID);
            this.gbRapelDatos.Controls.Add(this.label31);
            this.gbRapelDatos.Controls.Add(this.label43);
            this.gbRapelDatos.Controls.Add(this.tbRapelNumEquipos);
            this.gbRapelDatos.Controls.Add(this.tbRapelPrecio);
            this.gbRapelDatos.Controls.Add(this.label45);
            this.gbRapelDatos.Controls.Add(this.tbRapelNumCuerdas);
            this.gbRapelDatos.Controls.Add(this.tbRapelLugar);
            this.gbRapelDatos.Controls.Add(this.label46);
            this.gbRapelDatos.Controls.Add(this.label47);
            this.gbRapelDatos.Location = new System.Drawing.Point(232, 290);
            this.gbRapelDatos.Name = "gbRapelDatos";
            this.gbRapelDatos.Size = new System.Drawing.Size(330, 179);
            this.gbRapelDatos.TabIndex = 26;
            this.gbRapelDatos.TabStop = false;
            this.gbRapelDatos.Text = "Datos del Lugar";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label73.Location = new System.Drawing.Point(272, 122);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(21, 13);
            this.label73.TabIndex = 27;
            this.label73.Text = "ID:";
            this.label73.Visible = false;
            // 
            // tbRapelID
            // 
            this.tbRapelID.Enabled = false;
            this.tbRapelID.Location = new System.Drawing.Point(293, 119);
            this.tbRapelID.Name = "tbRapelID";
            this.tbRapelID.Size = new System.Drawing.Size(31, 20);
            this.tbRapelID.TabIndex = 26;
            this.tbRapelID.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(19, 93);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(103, 13);
            this.label31.TabIndex = 11;
            this.label31.Text = "Número de Equipos:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(196, 119);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(34, 13);
            this.label43.TabIndex = 8;
            this.label43.Text = "Euros";
            // 
            // tbRapelNumEquipos
            // 
            this.tbRapelNumEquipos.Location = new System.Drawing.Point(128, 90);
            this.tbRapelNumEquipos.Name = "tbRapelNumEquipos";
            this.tbRapelNumEquipos.Size = new System.Drawing.Size(62, 20);
            this.tbRapelNumEquipos.TabIndex = 3;
            // 
            // tbRapelPrecio
            // 
            this.tbRapelPrecio.Location = new System.Drawing.Point(128, 116);
            this.tbRapelPrecio.Name = "tbRapelPrecio";
            this.tbRapelPrecio.Size = new System.Drawing.Size(62, 20);
            this.tbRapelPrecio.TabIndex = 4;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(82, 119);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(40, 13);
            this.label45.TabIndex = 2;
            this.label45.Text = "Precio:";
            // 
            // tbRapelNumCuerdas
            // 
            this.tbRapelNumCuerdas.Location = new System.Drawing.Point(128, 64);
            this.tbRapelNumCuerdas.Name = "tbRapelNumCuerdas";
            this.tbRapelNumCuerdas.Size = new System.Drawing.Size(62, 20);
            this.tbRapelNumCuerdas.TabIndex = 2;
            // 
            // tbRapelLugar
            // 
            this.tbRapelLugar.Location = new System.Drawing.Point(128, 38);
            this.tbRapelLugar.Name = "tbRapelLugar";
            this.tbRapelLugar.Size = new System.Drawing.Size(156, 20);
            this.tbRapelLugar.TabIndex = 1;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(18, 67);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(104, 13);
            this.label46.TabIndex = 1;
            this.label46.Text = "Número de Cuerdas:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(85, 41);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(37, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "Lugar:";
            // 
            // gbRapelResultados
            // 
            this.gbRapelResultados.Controls.Add(this.btRapelVolver);
            this.gbRapelResultados.Controls.Add(this.btRapelAsignar);
            this.gbRapelResultados.Controls.Add(this.dgvRapelResultados);
            this.gbRapelResultados.Location = new System.Drawing.Point(232, 29);
            this.gbRapelResultados.Name = "gbRapelResultados";
            this.gbRapelResultados.Size = new System.Drawing.Size(480, 240);
            this.gbRapelResultados.TabIndex = 25;
            this.gbRapelResultados.TabStop = false;
            this.gbRapelResultados.Text = "Lugares para la práctica del Rápel";
            // 
            // btRapelVolver
            // 
            this.btRapelVolver.Location = new System.Drawing.Point(257, 202);
            this.btRapelVolver.Name = "btRapelVolver";
            this.btRapelVolver.Size = new System.Drawing.Size(100, 23);
            this.btRapelVolver.TabIndex = 14;
            this.btRapelVolver.Text = "VOLVER";
            this.btRapelVolver.UseVisualStyleBackColor = true;
            this.btRapelVolver.Visible = false;
            this.btRapelVolver.Click += new System.EventHandler(this.btRapelVolver_Click);
            // 
            // btRapelAsignar
            // 
            this.btRapelAsignar.Location = new System.Drawing.Point(363, 202);
            this.btRapelAsignar.Name = "btRapelAsignar";
            this.btRapelAsignar.Size = new System.Drawing.Size(100, 23);
            this.btRapelAsignar.TabIndex = 13;
            this.btRapelAsignar.Text = "ASIGNAR";
            this.btRapelAsignar.UseVisualStyleBackColor = true;
            this.btRapelAsignar.Visible = false;
            this.btRapelAsignar.Click += new System.EventHandler(this.btRapelAsignar_Click);
            // 
            // dgvRapelResultados
            // 
            this.dgvRapelResultados.AllowUserToAddRows = false;
            this.dgvRapelResultados.AllowUserToDeleteRows = false;
            this.dgvRapelResultados.AllowUserToResizeColumns = false;
            this.dgvRapelResultados.AllowUserToResizeRows = false;
            this.dgvRapelResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRapelResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rapelID,
            this.rapelLugar,
            this.rapelCuerdas,
            this.rapelEquipos,
            this.rapelPrecio});
            this.dgvRapelResultados.Location = new System.Drawing.Point(17, 28);
            this.dgvRapelResultados.MultiSelect = false;
            this.dgvRapelResultados.Name = "dgvRapelResultados";
            this.dgvRapelResultados.ReadOnly = true;
            this.dgvRapelResultados.Size = new System.Drawing.Size(446, 160);
            this.dgvRapelResultados.TabIndex = 0;
            this.dgvRapelResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRapelResultados_RowHeaderMouseClick);
            // 
            // rapelID
            // 
            this.rapelID.HeaderText = "ID";
            this.rapelID.Name = "rapelID";
            this.rapelID.ReadOnly = true;
            this.rapelID.Width = 30;
            // 
            // rapelLugar
            // 
            this.rapelLugar.HeaderText = "Lugar";
            this.rapelLugar.Name = "rapelLugar";
            this.rapelLugar.ReadOnly = true;
            this.rapelLugar.Width = 150;
            // 
            // rapelCuerdas
            // 
            this.rapelCuerdas.HeaderText = "Cuerdas";
            this.rapelCuerdas.Name = "rapelCuerdas";
            this.rapelCuerdas.ReadOnly = true;
            this.rapelCuerdas.Width = 70;
            // 
            // rapelEquipos
            // 
            this.rapelEquipos.HeaderText = "Equipos";
            this.rapelEquipos.Name = "rapelEquipos";
            this.rapelEquipos.ReadOnly = true;
            this.rapelEquipos.Width = 70;
            // 
            // rapelPrecio
            // 
            this.rapelPrecio.HeaderText = "Precio";
            this.rapelPrecio.Name = "rapelPrecio";
            this.rapelPrecio.ReadOnly = true;
            this.rapelPrecio.Width = 50;
            // 
            // gbRapelInfo
            // 
            this.gbRapelInfo.Controls.Add(this.labelRapelEquipos);
            this.gbRapelInfo.Controls.Add(this.labelRapelCuerdas);
            this.gbRapelInfo.Controls.Add(this.labelRapelLugares);
            this.gbRapelInfo.Controls.Add(this.label61);
            this.gbRapelInfo.Controls.Add(this.label60);
            this.gbRapelInfo.Controls.Add(this.label32);
            this.gbRapelInfo.Location = new System.Drawing.Point(33, 29);
            this.gbRapelInfo.Name = "gbRapelInfo";
            this.gbRapelInfo.Size = new System.Drawing.Size(182, 110);
            this.gbRapelInfo.TabIndex = 11;
            this.gbRapelInfo.TabStop = false;
            this.gbRapelInfo.Text = "Información General";
            // 
            // labelRapelEquipos
            // 
            this.labelRapelEquipos.AutoSize = true;
            this.labelRapelEquipos.Location = new System.Drawing.Point(120, 79);
            this.labelRapelEquipos.Name = "labelRapelEquipos";
            this.labelRapelEquipos.Size = new System.Drawing.Size(0, 13);
            this.labelRapelEquipos.TabIndex = 5;
            // 
            // labelRapelCuerdas
            // 
            this.labelRapelCuerdas.AutoSize = true;
            this.labelRapelCuerdas.Location = new System.Drawing.Point(120, 56);
            this.labelRapelCuerdas.Name = "labelRapelCuerdas";
            this.labelRapelCuerdas.Size = new System.Drawing.Size(0, 13);
            this.labelRapelCuerdas.TabIndex = 4;
            // 
            // labelRapelLugares
            // 
            this.labelRapelLugares.AutoSize = true;
            this.labelRapelLugares.Location = new System.Drawing.Point(120, 33);
            this.labelRapelLugares.Name = "labelRapelLugares";
            this.labelRapelLugares.Size = new System.Drawing.Size(0, 13);
            this.labelRapelLugares.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 79);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(96, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "- Total de Equipos:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(24, 56);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(97, 13);
            this.label60.TabIndex = 1;
            this.label60.Text = "- Total de Cuerdas:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(25, 33);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(96, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "- Total de Lugares:";
            // 
            // tpParque
            // 
            this.tpParque.Controls.Add(this.gbParqueOpciones);
            this.tpParque.Controls.Add(this.gbParqueDatos);
            this.tpParque.Controls.Add(this.gbParqueResultados);
            this.tpParque.Controls.Add(this.gbParqueInfo);
            this.tpParque.Location = new System.Drawing.Point(4, 22);
            this.tpParque.Name = "tpParque";
            this.tpParque.Size = new System.Drawing.Size(767, 578);
            this.tpParque.TabIndex = 6;
            this.tpParque.Text = "Parques Temáticos";
            this.tpParque.UseVisualStyleBackColor = true;
            this.tpParque.Enter += new System.EventHandler(this.tpParque_Enter);
            // 
            // gbParqueOpciones
            // 
            this.gbParqueOpciones.Controls.Add(this.btParqueBorrar);
            this.gbParqueOpciones.Controls.Add(this.btParqueReset);
            this.gbParqueOpciones.Controls.Add(this.btParqueInsertar);
            this.gbParqueOpciones.Controls.Add(this.btParqueEditar);
            this.gbParqueOpciones.Location = new System.Drawing.Point(577, 290);
            this.gbParqueOpciones.Name = "gbParqueOpciones";
            this.gbParqueOpciones.Size = new System.Drawing.Size(135, 211);
            this.gbParqueOpciones.TabIndex = 27;
            this.gbParqueOpciones.TabStop = false;
            this.gbParqueOpciones.Text = "Opciones";
            // 
            // btParqueBorrar
            // 
            this.btParqueBorrar.Enabled = false;
            this.btParqueBorrar.Location = new System.Drawing.Point(21, 86);
            this.btParqueBorrar.Name = "btParqueBorrar";
            this.btParqueBorrar.Size = new System.Drawing.Size(100, 23);
            this.btParqueBorrar.TabIndex = 2;
            this.btParqueBorrar.Text = "BORRAR";
            this.btParqueBorrar.UseVisualStyleBackColor = true;
            this.btParqueBorrar.Click += new System.EventHandler(this.btParqueBorrar_Click);
            // 
            // btParqueReset
            // 
            this.btParqueReset.Location = new System.Drawing.Point(21, 134);
            this.btParqueReset.Name = "btParqueReset";
            this.btParqueReset.Size = new System.Drawing.Size(100, 23);
            this.btParqueReset.TabIndex = 1;
            this.btParqueReset.Text = "RESET";
            this.btParqueReset.UseVisualStyleBackColor = true;
            this.btParqueReset.Click += new System.EventHandler(this.btParqueReset_Click);
            // 
            // btParqueInsertar
            // 
            this.btParqueInsertar.Location = new System.Drawing.Point(21, 28);
            this.btParqueInsertar.Name = "btParqueInsertar";
            this.btParqueInsertar.Size = new System.Drawing.Size(100, 23);
            this.btParqueInsertar.TabIndex = 0;
            this.btParqueInsertar.Text = "INSERTAR";
            this.btParqueInsertar.UseVisualStyleBackColor = true;
            this.btParqueInsertar.Click += new System.EventHandler(this.btParqueInsertar_Click);
            // 
            // btParqueEditar
            // 
            this.btParqueEditar.Enabled = false;
            this.btParqueEditar.Location = new System.Drawing.Point(21, 57);
            this.btParqueEditar.Name = "btParqueEditar";
            this.btParqueEditar.Size = new System.Drawing.Size(100, 23);
            this.btParqueEditar.TabIndex = 0;
            this.btParqueEditar.Text = "EDITAR";
            this.btParqueEditar.UseVisualStyleBackColor = true;
            this.btParqueEditar.Click += new System.EventHandler(this.btParqueEditar_Click);
            // 
            // gbParqueDatos
            // 
            this.gbParqueDatos.Controls.Add(this.label74);
            this.gbParqueDatos.Controls.Add(this.tbParqueID);
            this.gbParqueDatos.Controls.Add(this.tbParqueDescripcion);
            this.gbParqueDatos.Controls.Add(this.label62);
            this.gbParqueDatos.Controls.Add(this.label48);
            this.gbParqueDatos.Controls.Add(this.tbParquePrecio);
            this.gbParqueDatos.Controls.Add(this.tbParqueLugar);
            this.gbParqueDatos.Controls.Add(this.tbParqueNombre);
            this.gbParqueDatos.Controls.Add(this.label50);
            this.gbParqueDatos.Controls.Add(this.label51);
            this.gbParqueDatos.Controls.Add(this.label52);
            this.gbParqueDatos.Location = new System.Drawing.Point(232, 290);
            this.gbParqueDatos.Name = "gbParqueDatos";
            this.gbParqueDatos.Size = new System.Drawing.Size(330, 211);
            this.gbParqueDatos.TabIndex = 26;
            this.gbParqueDatos.TabStop = false;
            this.gbParqueDatos.Text = "Datos del Parque Temático";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label74.Location = new System.Drawing.Point(253, 175);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(21, 13);
            this.label74.TabIndex = 30;
            this.label74.Text = "ID:";
            this.label74.Visible = false;
            // 
            // tbParqueID
            // 
            this.tbParqueID.Enabled = false;
            this.tbParqueID.Location = new System.Drawing.Point(274, 172);
            this.tbParqueID.Name = "tbParqueID";
            this.tbParqueID.Size = new System.Drawing.Size(31, 20);
            this.tbParqueID.TabIndex = 29;
            this.tbParqueID.Visible = false;
            // 
            // tbParqueDescripcion
            // 
            this.tbParqueDescripcion.Location = new System.Drawing.Point(93, 90);
            this.tbParqueDescripcion.Multiline = true;
            this.tbParqueDescripcion.Name = "tbParqueDescripcion";
            this.tbParqueDescripcion.Size = new System.Drawing.Size(196, 73);
            this.tbParqueDescripcion.TabIndex = 3;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(21, 90);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(66, 13);
            this.label62.TabIndex = 27;
            this.label62.Text = "Descripción:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(161, 172);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 8;
            this.label48.Text = "Euros";
            // 
            // tbParquePrecio
            // 
            this.tbParquePrecio.Location = new System.Drawing.Point(93, 169);
            this.tbParquePrecio.Name = "tbParquePrecio";
            this.tbParquePrecio.Size = new System.Drawing.Size(62, 20);
            this.tbParquePrecio.TabIndex = 4;
            // 
            // tbParqueLugar
            // 
            this.tbParqueLugar.Location = new System.Drawing.Point(93, 64);
            this.tbParqueLugar.Name = "tbParqueLugar";
            this.tbParqueLugar.Size = new System.Drawing.Size(196, 20);
            this.tbParqueLugar.TabIndex = 2;
            // 
            // tbParqueNombre
            // 
            this.tbParqueNombre.Location = new System.Drawing.Point(93, 38);
            this.tbParqueNombre.Name = "tbParqueNombre";
            this.tbParqueNombre.Size = new System.Drawing.Size(196, 20);
            this.tbParqueNombre.TabIndex = 1;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(47, 172);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 13);
            this.label50.TabIndex = 2;
            this.label50.Text = "Precio:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(28, 67);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(56, 13);
            this.label51.TabIndex = 1;
            this.label51.Text = "Localidad:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(37, 41);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(47, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Nombre:";
            // 
            // gbParqueResultados
            // 
            this.gbParqueResultados.Controls.Add(this.btParqueVolver);
            this.gbParqueResultados.Controls.Add(this.btParqueAsignar);
            this.gbParqueResultados.Controls.Add(this.dgvParqueResultados);
            this.gbParqueResultados.Location = new System.Drawing.Point(232, 29);
            this.gbParqueResultados.Name = "gbParqueResultados";
            this.gbParqueResultados.Size = new System.Drawing.Size(480, 240);
            this.gbParqueResultados.TabIndex = 25;
            this.gbParqueResultados.TabStop = false;
            this.gbParqueResultados.Text = "Listado de Parques Temáticos";
            // 
            // btParqueVolver
            // 
            this.btParqueVolver.Location = new System.Drawing.Point(257, 202);
            this.btParqueVolver.Name = "btParqueVolver";
            this.btParqueVolver.Size = new System.Drawing.Size(100, 23);
            this.btParqueVolver.TabIndex = 14;
            this.btParqueVolver.Text = "VOLVER";
            this.btParqueVolver.UseVisualStyleBackColor = true;
            this.btParqueVolver.Visible = false;
            this.btParqueVolver.Click += new System.EventHandler(this.btParqueVolver_Click);
            // 
            // btParqueAsignar
            // 
            this.btParqueAsignar.Location = new System.Drawing.Point(363, 202);
            this.btParqueAsignar.Name = "btParqueAsignar";
            this.btParqueAsignar.Size = new System.Drawing.Size(100, 23);
            this.btParqueAsignar.TabIndex = 13;
            this.btParqueAsignar.Text = "ASIGNAR";
            this.btParqueAsignar.UseVisualStyleBackColor = true;
            this.btParqueAsignar.Visible = false;
            this.btParqueAsignar.Click += new System.EventHandler(this.btParqueAsignar_Click);
            // 
            // dgvParqueResultados
            // 
            this.dgvParqueResultados.AllowUserToAddRows = false;
            this.dgvParqueResultados.AllowUserToDeleteRows = false;
            this.dgvParqueResultados.AllowUserToResizeColumns = false;
            this.dgvParqueResultados.AllowUserToResizeRows = false;
            this.dgvParqueResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParqueResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.parqueID,
            this.nombre,
            this.parqueLocalidad,
            this.parqueDescripcion,
            this.parquePrecio});
            this.dgvParqueResultados.Location = new System.Drawing.Point(17, 28);
            this.dgvParqueResultados.MultiSelect = false;
            this.dgvParqueResultados.Name = "dgvParqueResultados";
            this.dgvParqueResultados.ReadOnly = true;
            this.dgvParqueResultados.Size = new System.Drawing.Size(446, 160);
            this.dgvParqueResultados.TabIndex = 0;
            this.dgvParqueResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvParqueResultados_RowHeaderMouseClick);
            // 
            // parqueID
            // 
            this.parqueID.HeaderText = "ID";
            this.parqueID.Name = "parqueID";
            this.parqueID.ReadOnly = true;
            this.parqueID.Width = 30;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Width = 150;
            // 
            // parqueLocalidad
            // 
            this.parqueLocalidad.HeaderText = "Localidad";
            this.parqueLocalidad.Name = "parqueLocalidad";
            this.parqueLocalidad.ReadOnly = true;
            this.parqueLocalidad.Width = 120;
            // 
            // parqueDescripcion
            // 
            this.parqueDescripcion.HeaderText = "Descripción";
            this.parqueDescripcion.Name = "parqueDescripcion";
            this.parqueDescripcion.ReadOnly = true;
            this.parqueDescripcion.Visible = false;
            // 
            // parquePrecio
            // 
            this.parquePrecio.HeaderText = "Precio";
            this.parquePrecio.Name = "parquePrecio";
            this.parquePrecio.ReadOnly = true;
            this.parquePrecio.Width = 70;
            // 
            // gbParqueInfo
            // 
            this.gbParqueInfo.Controls.Add(this.labelParquesTotal);
            this.gbParqueInfo.Controls.Add(this.label34);
            this.gbParqueInfo.Location = new System.Drawing.Point(33, 29);
            this.gbParqueInfo.Name = "gbParqueInfo";
            this.gbParqueInfo.Size = new System.Drawing.Size(182, 95);
            this.gbParqueInfo.TabIndex = 11;
            this.gbParqueInfo.TabStop = false;
            this.gbParqueInfo.Text = "Información General";
            // 
            // labelParquesTotal
            // 
            this.labelParquesTotal.AutoSize = true;
            this.labelParquesTotal.Location = new System.Drawing.Point(120, 33);
            this.labelParquesTotal.Name = "labelParquesTotal";
            this.labelParquesTotal.Size = new System.Drawing.Size(0, 13);
            this.labelParquesTotal.TabIndex = 1;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(23, 33);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(97, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "- Total de Parques:";
            // 
            // tpLocalidad
            // 
            this.tpLocalidad.BackColor = System.Drawing.Color.White;
            this.tpLocalidad.Controls.Add(this.gbLocalidadOpciones);
            this.tpLocalidad.Controls.Add(this.gbLocalidadDatos);
            this.tpLocalidad.Controls.Add(this.gbLocalidadResultados);
            this.tpLocalidad.Controls.Add(this.gbLocalidadInfo);
            this.tpLocalidad.Location = new System.Drawing.Point(4, 22);
            this.tpLocalidad.Name = "tpLocalidad";
            this.tpLocalidad.Size = new System.Drawing.Size(767, 578);
            this.tpLocalidad.TabIndex = 7;
            this.tpLocalidad.Text = "Localidades de Interés";
            this.tpLocalidad.Enter += new System.EventHandler(this.tpLocalidad_Enter);
            // 
            // gbLocalidadOpciones
            // 
            this.gbLocalidadOpciones.Controls.Add(this.btLocalidadBorrar);
            this.gbLocalidadOpciones.Controls.Add(this.btLocalidadReset);
            this.gbLocalidadOpciones.Controls.Add(this.btLocalidadInsertar);
            this.gbLocalidadOpciones.Controls.Add(this.btLocalidadEditar);
            this.gbLocalidadOpciones.Location = new System.Drawing.Point(577, 290);
            this.gbLocalidadOpciones.Name = "gbLocalidadOpciones";
            this.gbLocalidadOpciones.Size = new System.Drawing.Size(135, 202);
            this.gbLocalidadOpciones.TabIndex = 28;
            this.gbLocalidadOpciones.TabStop = false;
            this.gbLocalidadOpciones.Text = "Opciones";
            // 
            // btLocalidadBorrar
            // 
            this.btLocalidadBorrar.Enabled = false;
            this.btLocalidadBorrar.Location = new System.Drawing.Point(21, 86);
            this.btLocalidadBorrar.Name = "btLocalidadBorrar";
            this.btLocalidadBorrar.Size = new System.Drawing.Size(100, 23);
            this.btLocalidadBorrar.TabIndex = 2;
            this.btLocalidadBorrar.Text = "BORRAR";
            this.btLocalidadBorrar.UseVisualStyleBackColor = true;
            this.btLocalidadBorrar.Click += new System.EventHandler(this.btLocalidadBorrar_Click);
            // 
            // btLocalidadReset
            // 
            this.btLocalidadReset.Location = new System.Drawing.Point(21, 134);
            this.btLocalidadReset.Name = "btLocalidadReset";
            this.btLocalidadReset.Size = new System.Drawing.Size(100, 23);
            this.btLocalidadReset.TabIndex = 1;
            this.btLocalidadReset.Text = "RESET";
            this.btLocalidadReset.UseVisualStyleBackColor = true;
            this.btLocalidadReset.Click += new System.EventHandler(this.btLocalidadReset_Click);
            // 
            // btLocalidadInsertar
            // 
            this.btLocalidadInsertar.Location = new System.Drawing.Point(21, 28);
            this.btLocalidadInsertar.Name = "btLocalidadInsertar";
            this.btLocalidadInsertar.Size = new System.Drawing.Size(100, 23);
            this.btLocalidadInsertar.TabIndex = 0;
            this.btLocalidadInsertar.Text = "INSERTAR";
            this.btLocalidadInsertar.UseVisualStyleBackColor = true;
            this.btLocalidadInsertar.Click += new System.EventHandler(this.btLocalidadInsertar_Click);
            // 
            // btLocalidadEditar
            // 
            this.btLocalidadEditar.Enabled = false;
            this.btLocalidadEditar.Location = new System.Drawing.Point(21, 57);
            this.btLocalidadEditar.Name = "btLocalidadEditar";
            this.btLocalidadEditar.Size = new System.Drawing.Size(100, 23);
            this.btLocalidadEditar.TabIndex = 0;
            this.btLocalidadEditar.Text = "EDITAR";
            this.btLocalidadEditar.UseVisualStyleBackColor = true;
            this.btLocalidadEditar.Click += new System.EventHandler(this.btLocalidadEditar_Click);
            // 
            // gbLocalidadDatos
            // 
            this.gbLocalidadDatos.Controls.Add(this.label75);
            this.gbLocalidadDatos.Controls.Add(this.tbLocalidadID);
            this.gbLocalidadDatos.Controls.Add(this.label63);
            this.gbLocalidadDatos.Controls.Add(this.tbLocalidadDistancia);
            this.gbLocalidadDatos.Controls.Add(this.label49);
            this.gbLocalidadDatos.Controls.Add(this.tbLocalidadDescripcion);
            this.gbLocalidadDatos.Controls.Add(this.label53);
            this.gbLocalidadDatos.Controls.Add(this.label54);
            this.gbLocalidadDatos.Controls.Add(this.tbLocalidadPrecio);
            this.gbLocalidadDatos.Controls.Add(this.tbLocalidadLugar);
            this.gbLocalidadDatos.Controls.Add(this.label56);
            this.gbLocalidadDatos.Controls.Add(this.label57);
            this.gbLocalidadDatos.Location = new System.Drawing.Point(232, 287);
            this.gbLocalidadDatos.Name = "gbLocalidadDatos";
            this.gbLocalidadDatos.Size = new System.Drawing.Size(330, 202);
            this.gbLocalidadDatos.TabIndex = 27;
            this.gbLocalidadDatos.TabStop = false;
            this.gbLocalidadDatos.Text = "Datos de la Localidad";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label75.Location = new System.Drawing.Point(256, 170);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(21, 13);
            this.label75.TabIndex = 33;
            this.label75.Text = "ID:";
            this.label75.Visible = false;
            // 
            // tbLocalidadID
            // 
            this.tbLocalidadID.Enabled = false;
            this.tbLocalidadID.Location = new System.Drawing.Point(277, 167);
            this.tbLocalidadID.Name = "tbLocalidadID";
            this.tbLocalidadID.Size = new System.Drawing.Size(31, 20);
            this.tbLocalidadID.TabIndex = 32;
            this.tbLocalidadID.Visible = false;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(189, 141);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(119, 13);
            this.label63.TabIndex = 31;
            this.label63.Text = "Km. (desde el complejo)";
            // 
            // tbLocalidadDistancia
            // 
            this.tbLocalidadDistancia.Location = new System.Drawing.Point(121, 138);
            this.tbLocalidadDistancia.Name = "tbLocalidadDistancia";
            this.tbLocalidadDistancia.Size = new System.Drawing.Size(62, 20);
            this.tbLocalidadDistancia.TabIndex = 3;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(61, 141);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(54, 13);
            this.label49.TabIndex = 29;
            this.label49.Text = "Distancia:";
            // 
            // tbLocalidadDescripcion
            // 
            this.tbLocalidadDescripcion.Location = new System.Drawing.Point(121, 59);
            this.tbLocalidadDescripcion.Multiline = true;
            this.tbLocalidadDescripcion.Name = "tbLocalidadDescripcion";
            this.tbLocalidadDescripcion.Size = new System.Drawing.Size(187, 73);
            this.tbLocalidadDescripcion.TabIndex = 2;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(49, 59);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(66, 13);
            this.label53.TabIndex = 27;
            this.label53.Text = "Descripción:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(189, 167);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 13);
            this.label54.TabIndex = 8;
            this.label54.Text = "Euros";
            // 
            // tbLocalidadPrecio
            // 
            this.tbLocalidadPrecio.Location = new System.Drawing.Point(121, 164);
            this.tbLocalidadPrecio.Name = "tbLocalidadPrecio";
            this.tbLocalidadPrecio.Size = new System.Drawing.Size(62, 20);
            this.tbLocalidadPrecio.TabIndex = 4;
            // 
            // tbLocalidadLugar
            // 
            this.tbLocalidadLugar.Location = new System.Drawing.Point(121, 33);
            this.tbLocalidadLugar.Name = "tbLocalidadLugar";
            this.tbLocalidadLugar.Size = new System.Drawing.Size(187, 20);
            this.tbLocalidadLugar.TabIndex = 1;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(75, 167);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(40, 13);
            this.label56.TabIndex = 2;
            this.label56.Text = "Precio:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(68, 36);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(47, 13);
            this.label57.TabIndex = 1;
            this.label57.Text = "Nombre:";
            // 
            // gbLocalidadResultados
            // 
            this.gbLocalidadResultados.Controls.Add(this.btLocalidadVolver);
            this.gbLocalidadResultados.Controls.Add(this.btLocalidadAsignar);
            this.gbLocalidadResultados.Controls.Add(this.dgvLocalidadResultados);
            this.gbLocalidadResultados.Location = new System.Drawing.Point(232, 29);
            this.gbLocalidadResultados.Name = "gbLocalidadResultados";
            this.gbLocalidadResultados.Size = new System.Drawing.Size(480, 240);
            this.gbLocalidadResultados.TabIndex = 25;
            this.gbLocalidadResultados.TabStop = false;
            this.gbLocalidadResultados.Text = "Listado de Localidades de Interés";
            // 
            // btLocalidadVolver
            // 
            this.btLocalidadVolver.Location = new System.Drawing.Point(257, 202);
            this.btLocalidadVolver.Name = "btLocalidadVolver";
            this.btLocalidadVolver.Size = new System.Drawing.Size(100, 23);
            this.btLocalidadVolver.TabIndex = 14;
            this.btLocalidadVolver.Text = "VOLVER";
            this.btLocalidadVolver.UseVisualStyleBackColor = true;
            this.btLocalidadVolver.Visible = false;
            this.btLocalidadVolver.Click += new System.EventHandler(this.btLocalidadVolver_Click);
            // 
            // btLocalidadAsignar
            // 
            this.btLocalidadAsignar.Location = new System.Drawing.Point(363, 202);
            this.btLocalidadAsignar.Name = "btLocalidadAsignar";
            this.btLocalidadAsignar.Size = new System.Drawing.Size(100, 23);
            this.btLocalidadAsignar.TabIndex = 13;
            this.btLocalidadAsignar.Text = "ASIGNAR";
            this.btLocalidadAsignar.UseVisualStyleBackColor = true;
            this.btLocalidadAsignar.Visible = false;
            this.btLocalidadAsignar.Click += new System.EventHandler(this.btLocalidadAsignar_Click);
            // 
            // dgvLocalidadResultados
            // 
            this.dgvLocalidadResultados.AllowUserToAddRows = false;
            this.dgvLocalidadResultados.AllowUserToDeleteRows = false;
            this.dgvLocalidadResultados.AllowUserToResizeColumns = false;
            this.dgvLocalidadResultados.AllowUserToResizeRows = false;
            this.dgvLocalidadResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocalidadResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.localidadID,
            this.localidadNombre,
            this.localidadDescripcion,
            this.localidadDistancia,
            this.localidadPrecio});
            this.dgvLocalidadResultados.Location = new System.Drawing.Point(17, 28);
            this.dgvLocalidadResultados.MultiSelect = false;
            this.dgvLocalidadResultados.Name = "dgvLocalidadResultados";
            this.dgvLocalidadResultados.ReadOnly = true;
            this.dgvLocalidadResultados.Size = new System.Drawing.Size(446, 160);
            this.dgvLocalidadResultados.TabIndex = 0;
            this.dgvLocalidadResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvLocalidadResultados_RowHeaderMouseClick);
            // 
            // localidadID
            // 
            this.localidadID.HeaderText = "ID";
            this.localidadID.Name = "localidadID";
            this.localidadID.ReadOnly = true;
            this.localidadID.Width = 30;
            // 
            // localidadNombre
            // 
            this.localidadNombre.HeaderText = "Nombre";
            this.localidadNombre.Name = "localidadNombre";
            this.localidadNombre.ReadOnly = true;
            // 
            // localidadDescripcion
            // 
            this.localidadDescripcion.HeaderText = "Descripción";
            this.localidadDescripcion.Name = "localidadDescripcion";
            this.localidadDescripcion.ReadOnly = true;
            this.localidadDescripcion.Width = 120;
            // 
            // localidadDistancia
            // 
            this.localidadDistancia.HeaderText = "Distancia";
            this.localidadDistancia.Name = "localidadDistancia";
            this.localidadDistancia.ReadOnly = true;
            this.localidadDistancia.Width = 60;
            // 
            // localidadPrecio
            // 
            this.localidadPrecio.HeaderText = "Precio";
            this.localidadPrecio.Name = "localidadPrecio";
            this.localidadPrecio.ReadOnly = true;
            this.localidadPrecio.Width = 60;
            // 
            // gbLocalidadInfo
            // 
            this.gbLocalidadInfo.Controls.Add(this.labelLocalidadTotal);
            this.gbLocalidadInfo.Controls.Add(this.label36);
            this.gbLocalidadInfo.Location = new System.Drawing.Point(33, 29);
            this.gbLocalidadInfo.Name = "gbLocalidadInfo";
            this.gbLocalidadInfo.Size = new System.Drawing.Size(182, 95);
            this.gbLocalidadInfo.TabIndex = 11;
            this.gbLocalidadInfo.TabStop = false;
            this.gbLocalidadInfo.Text = "Información General";
            // 
            // labelLocalidadTotal
            // 
            this.labelLocalidadTotal.AutoSize = true;
            this.labelLocalidadTotal.Location = new System.Drawing.Point(125, 33);
            this.labelLocalidadTotal.Name = "labelLocalidadTotal";
            this.labelLocalidadTotal.Size = new System.Drawing.Size(0, 13);
            this.labelLocalidadTotal.TabIndex = 1;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(11, 33);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(115, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "- Total de Localidades:";
            // 
            // panelActIni
            // 
            this.panelActIni.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panelActIni.Controls.Add(this.btIncidencias);
            this.panelActIni.Controls.Add(this.btActividades);
            this.panelActIni.Location = new System.Drawing.Point(12, 15);
            this.panelActIni.Name = "panelActIni";
            this.panelActIni.Size = new System.Drawing.Size(160, 222);
            this.panelActIni.TabIndex = 1;
            // 
            // btIncidencias
            // 
            this.btIncidencias.Location = new System.Drawing.Point(15, 57);
            this.btIncidencias.Name = "btIncidencias";
            this.btIncidencias.Size = new System.Drawing.Size(130, 30);
            this.btIncidencias.TabIndex = 1;
            this.btIncidencias.Text = "Incidencias";
            this.btIncidencias.UseVisualStyleBackColor = true;
            this.btIncidencias.Click += new System.EventHandler(this.btIncidencias_Click);
            // 
            // btActividades
            // 
            this.btActividades.Location = new System.Drawing.Point(15, 16);
            this.btActividades.Name = "btActividades";
            this.btActividades.Size = new System.Drawing.Size(130, 30);
            this.btActividades.TabIndex = 0;
            this.btActividades.Text = "Gestionar Actividad";
            this.btActividades.UseVisualStyleBackColor = true;
            this.btActividades.Click += new System.EventHandler(this.btActividades_Click);
            // 
            // panelActividades
            // 
            this.panelActividades.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panelActividades.Controls.Add(this.tcActividades);
            this.panelActividades.Location = new System.Drawing.Point(178, 11);
            this.panelActividades.Name = "panelActividades";
            this.panelActividades.Size = new System.Drawing.Size(781, 759);
            this.panelActividades.TabIndex = 2;
            // 
            // panelIncidencias
            // 
            this.panelIncidencias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panelIncidencias.Controls.Add(this.gbIncidenciaOpciones);
            this.panelIncidencias.Controls.Add(this.gbIncidenciaResultados);
            this.panelIncidencias.Controls.Add(this.gbIncidenciaDatos);
            this.panelIncidencias.Location = new System.Drawing.Point(176, 15);
            this.panelIncidencias.Name = "panelIncidencias";
            this.panelIncidencias.Size = new System.Drawing.Size(765, 831);
            this.panelIncidencias.TabIndex = 28;
            this.panelIncidencias.Visible = false;
            // 
            // gbIncidenciaOpciones
            // 
            this.gbIncidenciaOpciones.Controls.Add(this.btIncidenciaBorrar);
            this.gbIncidenciaOpciones.Controls.Add(this.btIncidenciaReset);
            this.gbIncidenciaOpciones.Controls.Add(this.btIncidenciaInsertar);
            this.gbIncidenciaOpciones.Controls.Add(this.btIncidenciaEditar);
            this.gbIncidenciaOpciones.Location = new System.Drawing.Point(524, 352);
            this.gbIncidenciaOpciones.Name = "gbIncidenciaOpciones";
            this.gbIncidenciaOpciones.Size = new System.Drawing.Size(135, 191);
            this.gbIncidenciaOpciones.TabIndex = 32;
            this.gbIncidenciaOpciones.TabStop = false;
            this.gbIncidenciaOpciones.Text = "Opciones";
            // 
            // btIncidenciaBorrar
            // 
            this.btIncidenciaBorrar.Enabled = false;
            this.btIncidenciaBorrar.Location = new System.Drawing.Point(21, 86);
            this.btIncidenciaBorrar.Name = "btIncidenciaBorrar";
            this.btIncidenciaBorrar.Size = new System.Drawing.Size(100, 23);
            this.btIncidenciaBorrar.TabIndex = 2;
            this.btIncidenciaBorrar.Text = "BORRAR";
            this.btIncidenciaBorrar.UseVisualStyleBackColor = true;
            this.btIncidenciaBorrar.Click += new System.EventHandler(this.btIncidenciaBorrar_Click);
            // 
            // btIncidenciaReset
            // 
            this.btIncidenciaReset.Location = new System.Drawing.Point(21, 134);
            this.btIncidenciaReset.Name = "btIncidenciaReset";
            this.btIncidenciaReset.Size = new System.Drawing.Size(100, 23);
            this.btIncidenciaReset.TabIndex = 1;
            this.btIncidenciaReset.Text = "RESET";
            this.btIncidenciaReset.UseVisualStyleBackColor = true;
            this.btIncidenciaReset.Click += new System.EventHandler(this.btIncidenciaReset_Click);
            // 
            // btIncidenciaInsertar
            // 
            this.btIncidenciaInsertar.Location = new System.Drawing.Point(21, 28);
            this.btIncidenciaInsertar.Name = "btIncidenciaInsertar";
            this.btIncidenciaInsertar.Size = new System.Drawing.Size(100, 23);
            this.btIncidenciaInsertar.TabIndex = 0;
            this.btIncidenciaInsertar.Text = "INSERTAR";
            this.btIncidenciaInsertar.UseVisualStyleBackColor = true;
            this.btIncidenciaInsertar.Click += new System.EventHandler(this.btIncidenciaInsertar_Click);
            // 
            // btIncidenciaEditar
            // 
            this.btIncidenciaEditar.Enabled = false;
            this.btIncidenciaEditar.Location = new System.Drawing.Point(21, 57);
            this.btIncidenciaEditar.Name = "btIncidenciaEditar";
            this.btIncidenciaEditar.Size = new System.Drawing.Size(100, 23);
            this.btIncidenciaEditar.TabIndex = 0;
            this.btIncidenciaEditar.Text = "EDITAR";
            this.btIncidenciaEditar.UseVisualStyleBackColor = true;
            this.btIncidenciaEditar.Click += new System.EventHandler(this.btIncidenciaEditar_Click);
            // 
            // gbIncidenciaResultados
            // 
            this.gbIncidenciaResultados.Controls.Add(this.rbIncidenciaTodas);
            this.gbIncidenciaResultados.Controls.Add(this.rbIncidenciaPendientes);
            this.gbIncidenciaResultados.Controls.Add(this.dgvIncidenciaResultados);
            this.gbIncidenciaResultados.Controls.Add(this.gbIncidenciaInfo);
            this.gbIncidenciaResultados.Location = new System.Drawing.Point(16, 16);
            this.gbIncidenciaResultados.Name = "gbIncidenciaResultados";
            this.gbIncidenciaResultados.Size = new System.Drawing.Size(739, 317);
            this.gbIncidenciaResultados.TabIndex = 31;
            this.gbIncidenciaResultados.TabStop = false;
            this.gbIncidenciaResultados.Text = "Listado de Incidencias";
            // 
            // rbIncidenciaTodas
            // 
            this.rbIncidenciaTodas.AutoSize = true;
            this.rbIncidenciaTodas.Location = new System.Drawing.Point(153, 26);
            this.rbIncidenciaTodas.Name = "rbIncidenciaTodas";
            this.rbIncidenciaTodas.Size = new System.Drawing.Size(89, 17);
            this.rbIncidenciaTodas.TabIndex = 31;
            this.rbIncidenciaTodas.Text = "Mostrar todas";
            this.rbIncidenciaTodas.UseVisualStyleBackColor = true;
            this.rbIncidenciaTodas.Click += new System.EventHandler(this.rbIncidenciaTodas_Click);
            // 
            // rbIncidenciaPendientes
            // 
            this.rbIncidenciaPendientes.AutoSize = true;
            this.rbIncidenciaPendientes.Checked = true;
            this.rbIncidenciaPendientes.Location = new System.Drawing.Point(15, 26);
            this.rbIncidenciaPendientes.Name = "rbIncidenciaPendientes";
            this.rbIncidenciaPendientes.Size = new System.Drawing.Size(132, 17);
            this.rbIncidenciaPendientes.TabIndex = 30;
            this.rbIncidenciaPendientes.TabStop = true;
            this.rbIncidenciaPendientes.Text = "Pendientes de Revisar";
            this.rbIncidenciaPendientes.UseVisualStyleBackColor = true;
            this.rbIncidenciaPendientes.Click += new System.EventHandler(this.rbIncidenciaPendientes_Click);
            // 
            // dgvIncidenciaResultados
            // 
            this.dgvIncidenciaResultados.AllowUserToAddRows = false;
            this.dgvIncidenciaResultados.AllowUserToDeleteRows = false;
            this.dgvIncidenciaResultados.AllowUserToResizeColumns = false;
            this.dgvIncidenciaResultados.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncidenciaResultados.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvIncidenciaResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIncidenciaResultados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.incidenciaID,
            this.incidenciaActividad,
            this.incidenciaFecha,
            this.incidenciaDescripcion,
            this.incidenciaRevisada});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvIncidenciaResultados.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvIncidenciaResultados.Location = new System.Drawing.Point(6, 55);
            this.dgvIncidenciaResultados.MultiSelect = false;
            this.dgvIncidenciaResultados.Name = "dgvIncidenciaResultados";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIncidenciaResultados.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvIncidenciaResultados.Size = new System.Drawing.Size(526, 235);
            this.dgvIncidenciaResultados.TabIndex = 29;
            this.dgvIncidenciaResultados.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvIncidenciaResultados_RowHeaderMouseClick);
            // 
            // incidenciaID
            // 
            this.incidenciaID.HeaderText = "ID";
            this.incidenciaID.Name = "incidenciaID";
            this.incidenciaID.Width = 30;
            // 
            // incidenciaActividad
            // 
            this.incidenciaActividad.HeaderText = "Actividad";
            this.incidenciaActividad.Name = "incidenciaActividad";
            this.incidenciaActividad.Width = 120;
            // 
            // incidenciaFecha
            // 
            this.incidenciaFecha.HeaderText = "Fecha";
            this.incidenciaFecha.Name = "incidenciaFecha";
            this.incidenciaFecha.Width = 70;
            // 
            // incidenciaDescripcion
            // 
            this.incidenciaDescripcion.HeaderText = "Descripción";
            this.incidenciaDescripcion.Name = "incidenciaDescripcion";
            this.incidenciaDescripcion.Width = 170;
            // 
            // incidenciaRevisada
            // 
            this.incidenciaRevisada.HeaderText = "Revisada";
            this.incidenciaRevisada.Name = "incidenciaRevisada";
            this.incidenciaRevisada.Width = 70;
            // 
            // gbIncidenciaInfo
            // 
            this.gbIncidenciaInfo.Controls.Add(this.labelIncidenciaPendientes);
            this.gbIncidenciaInfo.Controls.Add(this.labelIncidenciaTotal);
            this.gbIncidenciaInfo.Controls.Add(this.label66);
            this.gbIncidenciaInfo.Controls.Add(this.label67);
            this.gbIncidenciaInfo.Controls.Add(this.label68);
            this.gbIncidenciaInfo.Location = new System.Drawing.Point(548, 55);
            this.gbIncidenciaInfo.Name = "gbIncidenciaInfo";
            this.gbIncidenciaInfo.Size = new System.Drawing.Size(179, 81);
            this.gbIncidenciaInfo.TabIndex = 12;
            this.gbIncidenciaInfo.TabStop = false;
            this.gbIncidenciaInfo.Text = "RESUMEN";
            // 
            // labelIncidenciaPendientes
            // 
            this.labelIncidenciaPendientes.AutoSize = true;
            this.labelIncidenciaPendientes.Location = new System.Drawing.Point(130, 56);
            this.labelIncidenciaPendientes.Name = "labelIncidenciaPendientes";
            this.labelIncidenciaPendientes.Size = new System.Drawing.Size(0, 13);
            this.labelIncidenciaPendientes.TabIndex = 4;
            // 
            // labelIncidenciaTotal
            // 
            this.labelIncidenciaTotal.AutoSize = true;
            this.labelIncidenciaTotal.Location = new System.Drawing.Point(130, 33);
            this.labelIncidenciaTotal.Name = "labelIncidenciaTotal";
            this.labelIncidenciaTotal.Size = new System.Drawing.Size(0, 13);
            this.labelIncidenciaTotal.TabIndex = 3;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(31, 79);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(0, 13);
            this.label66.TabIndex = 2;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(10, 56);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(118, 13);
            this.label67.TabIndex = 1;
            this.label67.Text = "- Pendientes de revisar:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(16, 33);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(112, 13);
            this.label68.TabIndex = 0;
            this.label68.Text = "- Total de Incidencias:";
            // 
            // gbIncidenciaDatos
            // 
            this.gbIncidenciaDatos.Controls.Add(this.label58);
            this.gbIncidenciaDatos.Controls.Add(this.tbIncidenciaID);
            this.gbIncidenciaDatos.Controls.Add(this.checkIncidenciaRevisada);
            this.gbIncidenciaDatos.Controls.Add(this.cbIncidenciaActividad);
            this.gbIncidenciaDatos.Controls.Add(this.dtpIncidenciaFecha);
            this.gbIncidenciaDatos.Controls.Add(this.label65);
            this.gbIncidenciaDatos.Controls.Add(this.label64);
            this.gbIncidenciaDatos.Controls.Add(this.label9);
            this.gbIncidenciaDatos.Controls.Add(this.tbIncidenciaDescripcion);
            this.gbIncidenciaDatos.Location = new System.Drawing.Point(7, 352);
            this.gbIncidenciaDatos.Name = "gbIncidenciaDatos";
            this.gbIncidenciaDatos.Size = new System.Drawing.Size(511, 191);
            this.gbIncidenciaDatos.TabIndex = 0;
            this.gbIncidenciaDatos.TabStop = false;
            this.gbIncidenciaDatos.Text = "Datos de Incidencia";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label58.Location = new System.Drawing.Point(453, 168);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(21, 13);
            this.label58.TabIndex = 23;
            this.label58.Text = "ID:";
            this.label58.Visible = false;
            // 
            // tbIncidenciaID
            // 
            this.tbIncidenciaID.Enabled = false;
            this.tbIncidenciaID.Location = new System.Drawing.Point(474, 165);
            this.tbIncidenciaID.Name = "tbIncidenciaID";
            this.tbIncidenciaID.Size = new System.Drawing.Size(31, 20);
            this.tbIncidenciaID.TabIndex = 22;
            this.tbIncidenciaID.Visible = false;
            // 
            // checkIncidenciaRevisada
            // 
            this.checkIncidenciaRevisada.AutoSize = true;
            this.checkIncidenciaRevisada.Enabled = false;
            this.checkIncidenciaRevisada.Location = new System.Drawing.Point(130, 154);
            this.checkIncidenciaRevisada.Name = "checkIncidenciaRevisada";
            this.checkIncidenciaRevisada.Size = new System.Drawing.Size(71, 17);
            this.checkIncidenciaRevisada.TabIndex = 5;
            this.checkIncidenciaRevisada.Text = "Revisada";
            this.checkIncidenciaRevisada.UseVisualStyleBackColor = true;
            // 
            // cbIncidenciaActividad
            // 
            this.cbIncidenciaActividad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIncidenciaActividad.FormattingEnabled = true;
            this.cbIncidenciaActividad.Items.AddRange(new object[] {
            "(Seleccione una de la lista)",
            "Quad",
            "Bicicleta",
            "Paseo a Caballo",
            "Tiro con Arco",
            "Piragüismo",
            "Rápel",
            "Parque Temático",
            "Localidad de Interés"});
            this.cbIncidenciaActividad.Location = new System.Drawing.Point(130, 51);
            this.cbIncidenciaActividad.Name = "cbIncidenciaActividad";
            this.cbIncidenciaActividad.Size = new System.Drawing.Size(200, 21);
            this.cbIncidenciaActividad.TabIndex = 3;
            // 
            // dtpIncidenciaFecha
            // 
            this.dtpIncidenciaFecha.Location = new System.Drawing.Point(130, 25);
            this.dtpIncidenciaFecha.Name = "dtpIncidenciaFecha";
            this.dtpIncidenciaFecha.Size = new System.Drawing.Size(200, 20);
            this.dtpIncidenciaFecha.TabIndex = 4;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(70, 54);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(54, 13);
            this.label65.TabIndex = 3;
            this.label65.Text = "Actividad:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(84, 31);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(40, 13);
            this.label64.TabIndex = 2;
            this.label64.Text = "Fecha:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Descripción / Motivo:";
            // 
            // tbIncidenciaDescripcion
            // 
            this.tbIncidenciaDescripcion.Location = new System.Drawing.Point(130, 78);
            this.tbIncidenciaDescripcion.Multiline = true;
            this.tbIncidenciaDescripcion.Name = "tbIncidenciaDescripcion";
            this.tbIncidenciaDescripcion.Size = new System.Drawing.Size(301, 70);
            this.tbIncidenciaDescripcion.TabIndex = 0;
            // 
            // pbActividad
            // 
            this.pbActividad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbActividad.InitialImage = null;
            this.pbActividad.Location = new System.Drawing.Point(12, 143);
            this.pbActividad.Name = "pbActividad";
            this.pbActividad.Size = new System.Drawing.Size(158, 400);
            this.pbActividad.TabIndex = 29;
            this.pbActividad.TabStop = false;
            // 
            // FormActividades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 762);
            this.Controls.Add(this.pbActividad);
            this.Controls.Add(this.panelActividades);
            this.Controls.Add(this.panelActIni);
            this.Controls.Add(this.panelIncidencias);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormActividades";
            this.Text = "TORDICAN S.L. - Actividades";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormActividades_FormClosed);
            this.Load += new System.EventHandler(this.FormActividades_Load);
            this.tcActividades.ResumeLayout(false);
            this.tpQuad.ResumeLayout(false);
            this.tpQuad.PerformLayout();
            this.gbQuadResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuadResultados)).EndInit();
            this.gbQuadOpciones.ResumeLayout(false);
            this.gbQuadDatos.ResumeLayout(false);
            this.gbQuadDatos.PerformLayout();
            this.gbQuadBusqueda.ResumeLayout(false);
            this.gbQuadBusqueda.PerformLayout();
            this.gbQuadInfo.ResumeLayout(false);
            this.gbQuadInfo.PerformLayout();
            this.tpBicicleta.ResumeLayout(false);
            this.tpBicicleta.PerformLayout();
            this.gbBiciOpciones.ResumeLayout(false);
            this.gbBiciPrecios.ResumeLayout(false);
            this.gbBiciPrecios.PerformLayout();
            this.gbBiciResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBiciResultados)).EndInit();
            this.gbBiciDatos.ResumeLayout(false);
            this.gbBiciDatos.PerformLayout();
            this.gbBiciBusqueda.ResumeLayout(false);
            this.gbBiciBusqueda.PerformLayout();
            this.gbBiciInfo.ResumeLayout(false);
            this.gbBiciInfo.PerformLayout();
            this.tpCaballo.ResumeLayout(false);
            this.gbCaballoOpciones.ResumeLayout(false);
            this.gbCaballoResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaballoResultados)).EndInit();
            this.gbCaballoDatos.ResumeLayout(false);
            this.gbCaballoDatos.PerformLayout();
            this.gbCaballoInfo.ResumeLayout(false);
            this.gbCaballoInfo.PerformLayout();
            this.tpArco.ResumeLayout(false);
            this.tpArco.PerformLayout();
            this.gbArcoOpciones.ResumeLayout(false);
            this.gbArcoResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArcoResultados)).EndInit();
            this.gbArcoDatos.ResumeLayout(false);
            this.gbArcoDatos.PerformLayout();
            this.gbArcoInfo.ResumeLayout(false);
            this.gbArcoInfo.PerformLayout();
            this.tpPiraguismo.ResumeLayout(false);
            this.gbPiraguismoOpciones.ResumeLayout(false);
            this.gbPiraguismoDatos.ResumeLayout(false);
            this.gbPiraguismoDatos.PerformLayout();
            this.gbPiraguismoResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiraguismoResultados)).EndInit();
            this.gbPiraguismoInfo.ResumeLayout(false);
            this.gbPiraguismoInfo.PerformLayout();
            this.tpRapel.ResumeLayout(false);
            this.gbRapelOpciones.ResumeLayout(false);
            this.gbRapelDatos.ResumeLayout(false);
            this.gbRapelDatos.PerformLayout();
            this.gbRapelResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRapelResultados)).EndInit();
            this.gbRapelInfo.ResumeLayout(false);
            this.gbRapelInfo.PerformLayout();
            this.tpParque.ResumeLayout(false);
            this.gbParqueOpciones.ResumeLayout(false);
            this.gbParqueDatos.ResumeLayout(false);
            this.gbParqueDatos.PerformLayout();
            this.gbParqueResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvParqueResultados)).EndInit();
            this.gbParqueInfo.ResumeLayout(false);
            this.gbParqueInfo.PerformLayout();
            this.tpLocalidad.ResumeLayout(false);
            this.gbLocalidadOpciones.ResumeLayout(false);
            this.gbLocalidadDatos.ResumeLayout(false);
            this.gbLocalidadDatos.PerformLayout();
            this.gbLocalidadResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalidadResultados)).EndInit();
            this.gbLocalidadInfo.ResumeLayout(false);
            this.gbLocalidadInfo.PerformLayout();
            this.panelActIni.ResumeLayout(false);
            this.panelActividades.ResumeLayout(false);
            this.panelIncidencias.ResumeLayout(false);
            this.gbIncidenciaOpciones.ResumeLayout(false);
            this.gbIncidenciaResultados.ResumeLayout(false);
            this.gbIncidenciaResultados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncidenciaResultados)).EndInit();
            this.gbIncidenciaInfo.ResumeLayout(false);
            this.gbIncidenciaInfo.PerformLayout();
            this.gbIncidenciaDatos.ResumeLayout(false);
            this.gbIncidenciaDatos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbActividad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcActividades;
        private System.Windows.Forms.TabPage tpQuad;
        private System.Windows.Forms.TabPage tpBicicleta;
        private System.Windows.Forms.TabPage tpCaballo;
        private System.Windows.Forms.TabPage tpArco;
        private System.Windows.Forms.TabPage tpPiraguismo;
        private System.Windows.Forms.TabPage tpRapel;
        private System.Windows.Forms.TabPage tpParque;
        private System.Windows.Forms.TabPage tpLocalidad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvQuadResultados;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbQuadPotencia;
        private System.Windows.Forms.TextBox tbQuadModelo;
        private System.Windows.Forms.TextBox tbQuadMarca;
        private System.Windows.Forms.TextBox tbQuadMatricula;
        private System.Windows.Forms.GroupBox gbQuadInfo;
        private System.Windows.Forms.GroupBox gbQuadBusqueda;
        private System.Windows.Forms.Panel panelActIni;
        private System.Windows.Forms.Panel panelActividades;
        private System.Windows.Forms.Button btQuadBusca;
        private System.Windows.Forms.TextBox tbQuadBusca;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkQuadDisponibles;
        private System.Windows.Forms.ComboBox cbQuadFiltro;
        private System.Windows.Forms.Button btIncidencias;
        private System.Windows.Forms.Button btActividades;
        private System.Windows.Forms.GroupBox gbQuadDatos;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbQuadPeso;
        private System.Windows.Forms.DateTimePicker dtpQuadFecha;
        private System.Windows.Forms.TextBox tbQuadPrecio;
        private System.Windows.Forms.GroupBox gbBiciBusqueda;
        private System.Windows.Forms.CheckBox checkBiciDisponibles;
        private System.Windows.Forms.ComboBox cbBiciFiltro;
        private System.Windows.Forms.Button btBiciBusca;
        private System.Windows.Forms.GroupBox gbBiciInfo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgvBiciResultados;
        private System.Windows.Forms.GroupBox gbBiciDatos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btQuadEditar;
        private System.Windows.Forms.Button btQuadInsertar;
        private System.Windows.Forms.ComboBox cbBiciTipo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox checkBiciUso;
        private System.Windows.Forms.DateTimePicker dtpBiciFecha;
        private System.Windows.Forms.CheckBox checkQuadUso;
        private System.Windows.Forms.GroupBox gbCaballoInfo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox gbCaballoDatos;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbCaballoDescripcion;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbCaballoDuracion;
        private System.Windows.Forms.GroupBox gbQuadOpciones;
        private System.Windows.Forms.DataGridView dgvCaballoResultados;
        private System.Windows.Forms.GroupBox gbBiciResultados;
        private System.Windows.Forms.GroupBox gbCaballoResultados;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbCaballoRuta;
        private System.Windows.Forms.TextBox tbCaballoPrecio;
        private System.Windows.Forms.CheckBox checkCaballoPony;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox gbArcoInfo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox gbPiraguismoInfo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox gbRapelInfo;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox gbParqueInfo;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox gbLocalidadInfo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox gbArcoResultados;
        private System.Windows.Forms.GroupBox gbArcoDatos;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbArcoPrecio;
        private System.Windows.Forms.TextBox tbArcoNumFlechas;
        private System.Windows.Forms.TextBox tbArcoNumDianas;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView dgvArcoResultados;
        private System.Windows.Forms.GroupBox gbPiraguismoDatos;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbPiraguismoPrecio;
        private System.Windows.Forms.TextBox tbPiraguismoNumChalecos;
        private System.Windows.Forms.TextBox tbPiraguismoPantano;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.GroupBox gbPiraguismoResultados;
        private System.Windows.Forms.DataGridView dgvPiraguismoResultados;
        private System.Windows.Forms.GroupBox gbRapelDatos;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbRapelPrecio;
        private System.Windows.Forms.TextBox tbRapelNumCuerdas;
        private System.Windows.Forms.TextBox tbRapelLugar;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox gbRapelResultados;
        private System.Windows.Forms.DataGridView dgvRapelResultados;
        private System.Windows.Forms.GroupBox gbParqueDatos;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox tbParquePrecio;
        private System.Windows.Forms.TextBox tbParqueLugar;
        private System.Windows.Forms.TextBox tbParqueNombre;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox gbParqueResultados;
        private System.Windows.Forms.DataGridView dgvParqueResultados;
        private System.Windows.Forms.GroupBox gbLocalidadResultados;
        private System.Windows.Forms.DataGridView dgvLocalidadResultados;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbRapelNumEquipos;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox tbParqueDescripcion;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox gbLocalidadDatos;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox tbLocalidadDistancia;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox tbLocalidadDescripcion;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox tbLocalidadPrecio;
        private System.Windows.Forms.TextBox tbLocalidadLugar;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox gbQuadResultados;
        private System.Windows.Forms.Panel panelIncidencias;
        private System.Windows.Forms.GroupBox gbIncidenciaInfo;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox gbIncidenciaDatos;
        private System.Windows.Forms.DateTimePicker dtpIncidenciaFecha;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbIncidenciaDescripcion;
        private System.Windows.Forms.ComboBox cbIncidenciaActividad;
        private System.Windows.Forms.DataGridView dgvIncidenciaResultados;
        private System.Windows.Forms.CheckBox checkIncidenciaRevisada;
        private System.Windows.Forms.GroupBox gbIncidenciaResultados;
        private System.Windows.Forms.Button btQuadAsignar;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadId;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadMatricula;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadMarca;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadModelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadPotencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadPeso;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadFechaAlta;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuadEnUso;
        private System.Windows.Forms.Button btQuadReset;
        private System.Windows.Forms.Label labelQuadsEnUso;
        private System.Windows.Forms.Label labelQuadsTotal;
        private System.Windows.Forms.PictureBox pbActividad;
        private System.Windows.Forms.TextBox tbQuadID;
        private System.Windows.Forms.Label labelQuadID;
        private System.Windows.Forms.Button btQuadBorrar;
        private System.Windows.Forms.Button btBiciAsignar;
        private System.Windows.Forms.GroupBox gbBiciPrecios;
        private System.Windows.Forms.GroupBox gbBiciOpciones;
        private System.Windows.Forms.Button btBiciBorrar;
        private System.Windows.Forms.Button btBiciReset;
        private System.Windows.Forms.Button btBiciInsertar;
        private System.Windows.Forms.Button btBiciEditar;
        private System.Windows.Forms.GroupBox gbCaballoOpciones;
        private System.Windows.Forms.Button btCaballoBorrar;
        private System.Windows.Forms.Button btCaballoReset;
        private System.Windows.Forms.Button btCaballoInsertar;
        private System.Windows.Forms.Button btCaballoEditar;
        private System.Windows.Forms.GroupBox gbArcoOpciones;
        private System.Windows.Forms.Button btArcoBorrar;
        private System.Windows.Forms.Button btArcoReset;
        private System.Windows.Forms.Button btArcoInsertar;
        private System.Windows.Forms.Button btArcoEditar;
        private System.Windows.Forms.GroupBox gbPiraguismoOpciones;
        private System.Windows.Forms.Button btPiraguismoBorrar;
        private System.Windows.Forms.Button btPiraguismoReset;
        private System.Windows.Forms.Button btPiraguismoInsertar;
        private System.Windows.Forms.Button btPiraguismoEditar;
        private System.Windows.Forms.GroupBox gbRapelOpciones;
        private System.Windows.Forms.Button btRapelBorrar;
        private System.Windows.Forms.Button btRapelReset;
        private System.Windows.Forms.Button btRapelInsertar;
        private System.Windows.Forms.Button btRapelEditar;
        private System.Windows.Forms.GroupBox gbParqueOpciones;
        private System.Windows.Forms.Button btParqueBorrar;
        private System.Windows.Forms.Button btParqueReset;
        private System.Windows.Forms.Button btParqueInsertar;
        private System.Windows.Forms.Button btParqueEditar;
        private System.Windows.Forms.GroupBox gbLocalidadOpciones;
        private System.Windows.Forms.Button btLocalidadBorrar;
        private System.Windows.Forms.Button btLocalidadReset;
        private System.Windows.Forms.Button btLocalidadInsertar;
        private System.Windows.Forms.Button btLocalidadEditar;
        private System.Windows.Forms.Button btTipoBiciPrecios;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbBiciPrecioTandem;
        private System.Windows.Forms.TextBox tbBiciPrecioAdulto;
        private System.Windows.Forms.TextBox tbBiciPrecioNinyo;
        private System.Windows.Forms.Button btCaballoAsignar;
        private System.Windows.Forms.Button btArcoAsignar;
        private System.Windows.Forms.Button btPiraguismoAsignar;
        private System.Windows.Forms.Button btRapelAsignar;
        private System.Windows.Forms.Button btParqueAsignar;
        private System.Windows.Forms.Button btLocalidadAsignar;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbBiciID;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbPaseoID;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbArcoID;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox tbPiraguismoID;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox tbRapelID;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox tbParqueID;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox tbLocalidadID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BiciId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BiciTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BiciFechaAlta;
        private System.Windows.Forms.DataGridViewTextBoxColumn BiciPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn BiciEnUso;
        private System.Windows.Forms.Label labelBicisUso;
        private System.Windows.Forms.Label labelBicisTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn paseoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn paseoNombreRuta;
        private System.Windows.Forms.DataGridViewTextBoxColumn paseoDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn paseoDuracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn paseoPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn paseoConPony;
        private System.Windows.Forms.Label labelPaseosPony;
        private System.Windows.Forms.Label labelPaseosTotal;
        private System.Windows.Forms.Label labelArcoOfertas;
        private System.Windows.Forms.DataGridViewTextBoxColumn tiroID;
        private System.Windows.Forms.DataGridViewTextBoxColumn tiroNumDianas;
        private System.Windows.Forms.DataGridViewTextBoxColumn tiroNumFlechas;
        private System.Windows.Forms.DataGridViewTextBoxColumn tiroPrecio;
        private System.Windows.Forms.Label labelPiragChalecos;
        private System.Windows.Forms.Label labelPiragPantanos;
        private System.Windows.Forms.DataGridViewTextBoxColumn piragID;
        private System.Windows.Forms.DataGridViewTextBoxColumn piragPantano;
        private System.Windows.Forms.DataGridViewTextBoxColumn piragNumChalecos;
        private System.Windows.Forms.DataGridViewTextBoxColumn piragPrecio;
        private System.Windows.Forms.Label labelRapelEquipos;
        private System.Windows.Forms.Label labelRapelCuerdas;
        private System.Windows.Forms.Label labelRapelLugares;
        private System.Windows.Forms.DataGridViewTextBoxColumn parqueID;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn parqueLocalidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn parqueDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn parquePrecio;
        private System.Windows.Forms.Label labelParquesTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn rapelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn rapelLugar;
        private System.Windows.Forms.DataGridViewTextBoxColumn rapelCuerdas;
        private System.Windows.Forms.DataGridViewTextBoxColumn rapelEquipos;
        private System.Windows.Forms.DataGridViewTextBoxColumn rapelPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn localidadID;
        private System.Windows.Forms.DataGridViewTextBoxColumn localidadNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn localidadDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn localidadDistancia;
        private System.Windows.Forms.DataGridViewTextBoxColumn localidadPrecio;
        private System.Windows.Forms.Label labelLocalidadTotal;
        private System.Windows.Forms.GroupBox gbIncidenciaOpciones;
        private System.Windows.Forms.Button btIncidenciaBorrar;
        private System.Windows.Forms.Button btIncidenciaReset;
        private System.Windows.Forms.Button btIncidenciaInsertar;
        private System.Windows.Forms.Button btIncidenciaEditar;
        private System.Windows.Forms.RadioButton rbIncidenciaTodas;
        private System.Windows.Forms.RadioButton rbIncidenciaPendientes;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox tbIncidenciaID;
        private System.Windows.Forms.Label labelIncidenciaPendientes;
        private System.Windows.Forms.Label labelIncidenciaTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn incidenciaID;
        private System.Windows.Forms.DataGridViewTextBoxColumn incidenciaActividad;
        private System.Windows.Forms.DataGridViewTextBoxColumn incidenciaFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn incidenciaDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn incidenciaRevisada;
        private System.Windows.Forms.Label labelQuadNota;
        private System.Windows.Forms.Button btQuadVolver;
        private System.Windows.Forms.Button btBiciVolver;
        private System.Windows.Forms.Button btCaballoVolver;
        private System.Windows.Forms.Button btArcoVolver;
        private System.Windows.Forms.Button btPiraguismoVolver;
        private System.Windows.Forms.Button btRapelVolver;
        private System.Windows.Forms.Button btParqueVolver;
        private System.Windows.Forms.Button btLocalidadVolver;
        private System.Windows.Forms.Label labelBiciNota;
    }
}