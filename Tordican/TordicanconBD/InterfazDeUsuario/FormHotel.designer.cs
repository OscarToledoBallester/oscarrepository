﻿namespace InterfazDeUsuario
{
    partial class FormHotel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHotel));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.gbParkingRemolques = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gbParkingTurismos = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btInabilitar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbHabitacion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPlanta = new System.Windows.Forms.TextBox();
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.gbPlanta3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gbPlanta2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpHotel = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tpRestaurante = new System.Windows.Forms.TabPage();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tpEntorno = new System.Windows.Forms.TabPage();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.tabPage3.SuspendLayout();
            this.gbParkingRemolques.SuspendLayout();
            this.gbParkingTurismos.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gb1.SuspendLayout();
            this.gbPlanta3.SuspendLayout();
            this.gbPlanta2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpHotel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tpRestaurante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tpEntorno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.textBox57);
            this.tabPage3.Controls.Add(this.textBox58);
            this.tabPage3.Controls.Add(this.gbParkingRemolques);
            this.tabPage3.Controls.Add(this.gbParkingTurismos);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1276, 703);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Parking";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(412, 244);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Ocupada";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(412, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Libre";
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.Color.Red;
            this.textBox57.Location = new System.Drawing.Point(369, 244);
            this.textBox57.Multiline = true;
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(15, 15);
            this.textBox57.TabIndex = 25;
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.Color.Lime;
            this.textBox58.Location = new System.Drawing.Point(369, 213);
            this.textBox58.Multiline = true;
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(15, 15);
            this.textBox58.TabIndex = 24;
            // 
            // gbParkingRemolques
            // 
            this.gbParkingRemolques.Controls.Add(this.label10);
            this.gbParkingRemolques.Location = new System.Drawing.Point(16, 167);
            this.gbParkingRemolques.Name = "gbParkingRemolques";
            this.gbParkingRemolques.Size = new System.Drawing.Size(337, 120);
            this.gbParkingRemolques.TabIndex = 22;
            this.gbParkingRemolques.TabStop = false;
            this.gbParkingRemolques.Text = "Parking Remolques";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 26);
            this.label10.TabIndex = 0;
            this.label10.Text = "Plazas\r\nRemolques";
            // 
            // gbParkingTurismos
            // 
            this.gbParkingTurismos.Controls.Add(this.label11);
            this.gbParkingTurismos.Location = new System.Drawing.Point(16, 25);
            this.gbParkingTurismos.Name = "gbParkingTurismos";
            this.gbParkingTurismos.Size = new System.Drawing.Size(470, 120);
            this.gbParkingTurismos.TabIndex = 1;
            this.gbParkingTurismos.TabStop = false;
            this.gbParkingTurismos.Text = "Parking Turismos";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 26);
            this.label11.TabIndex = 0;
            this.label11.Text = "Plazas\r\nVehículos";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.gb1);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.textBox36);
            this.tabPage1.Controls.Add(this.textBox35);
            this.tabPage1.Controls.Add(this.textBox34);
            this.tabPage1.Controls.Add(this.gbPlanta3);
            this.tabPage1.Controls.Add(this.gbPlanta2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(955, 609);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Habitaciones";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btInabilitar);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbHabitacion);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbPlanta);
            this.groupBox1.Location = new System.Drawing.Point(525, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(174, 173);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mantenimiento de Habitaciones";
            // 
            // btInabilitar
            // 
            this.btInabilitar.Location = new System.Drawing.Point(34, 98);
            this.btInabilitar.Name = "btInabilitar";
            this.btInabilitar.Size = new System.Drawing.Size(98, 34);
            this.btInabilitar.TabIndex = 27;
            this.btInabilitar.Text = "Inabilitar";
            this.btInabilitar.UseVisualStyleBackColor = true;
            this.btInabilitar.Click += new System.EventHandler(this.btInabilitar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Habitación";
            // 
            // tbHabitacion
            // 
            this.tbHabitacion.Location = new System.Drawing.Point(101, 58);
            this.tbHabitacion.Name = "tbHabitacion";
            this.tbHabitacion.Size = new System.Drawing.Size(33, 20);
            this.tbHabitacion.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Planta";
            // 
            // tbPlanta
            // 
            this.tbPlanta.Location = new System.Drawing.Point(101, 27);
            this.tbPlanta.Name = "tbPlanta";
            this.tbPlanta.Size = new System.Drawing.Size(31, 20);
            this.tbPlanta.TabIndex = 28;
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.label1);
            this.gb1.Location = new System.Drawing.Point(16, 25);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(489, 120);
            this.gb1.TabIndex = 25;
            this.gb1.TabStop = false;
            this.gb1.Text = "Planta1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Individuales";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(428, 363);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Por Revisar";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(426, 332);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Ocupada";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(426, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Libre";
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.Color.Fuchsia;
            this.textBox36.Location = new System.Drawing.Point(383, 363);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(15, 15);
            this.textBox36.TabIndex = 21;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.Color.Red;
            this.textBox35.Location = new System.Drawing.Point(383, 332);
            this.textBox35.Multiline = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(15, 15);
            this.textBox35.TabIndex = 20;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.Color.Lime;
            this.textBox34.Location = new System.Drawing.Point(383, 301);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(15, 15);
            this.textBox34.TabIndex = 19;
            // 
            // gbPlanta3
            // 
            this.gbPlanta3.Controls.Add(this.label6);
            this.gbPlanta3.Controls.Add(this.label5);
            this.gbPlanta3.Location = new System.Drawing.Point(16, 278);
            this.gbPlanta3.Name = "gbPlanta3";
            this.gbPlanta3.Size = new System.Drawing.Size(337, 120);
            this.gbPlanta3.TabIndex = 18;
            this.gbPlanta3.TabStop = false;
            this.gbPlanta3.Text = "Planta3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(171, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Suite 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Suite 1";
            // 
            // gbPlanta2
            // 
            this.gbPlanta2.Controls.Add(this.label4);
            this.gbPlanta2.Location = new System.Drawing.Point(16, 152);
            this.gbPlanta2.Name = "gbPlanta2";
            this.gbPlanta2.Size = new System.Drawing.Size(489, 120);
            this.gbPlanta2.TabIndex = 17;
            this.gbPlanta2.TabStop = false;
            this.gbPlanta2.Text = "Planta2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Dobles";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tpHotel);
            this.tabControl1.Controls.Add(this.tpRestaurante);
            this.tabControl1.Controls.Add(this.tpEntorno);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(963, 635);
            this.tabControl1.TabIndex = 2;
            // 
            // tpHotel
            // 
            this.tpHotel.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.tpHotel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tpHotel.Controls.Add(this.label14);
            this.tpHotel.Controls.Add(this.richTextBox1);
            this.tpHotel.Controls.Add(this.pictureBox1);
            this.tpHotel.Location = new System.Drawing.Point(4, 22);
            this.tpHotel.Name = "tpHotel";
            this.tpHotel.Size = new System.Drawing.Size(1276, 703);
            this.tpHotel.TabIndex = 3;
            this.tpHotel.Text = "Hotel";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(404, 531);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(589, 55);
            this.label14.TabIndex = 3;
            this.label14.Text = "Un lugar pensado para tí.";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Black;
            this.richTextBox1.Location = new System.Drawing.Point(143, 429);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox1.Size = new System.Drawing.Size(792, 131);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DimGray;
            this.pictureBox1.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.CAA7W56N11;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(102, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(877, 409);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tpRestaurante
            // 
            this.tpRestaurante.BackColor = System.Drawing.Color.Khaki;
            this.tpRestaurante.Controls.Add(this.richTextBox2);
            this.tpRestaurante.Controls.Add(this.label15);
            this.tpRestaurante.Controls.Add(this.pictureBox2);
            this.tpRestaurante.Controls.Add(this.pictureBox3);
            this.tpRestaurante.Location = new System.Drawing.Point(4, 22);
            this.tpRestaurante.Name = "tpRestaurante";
            this.tpRestaurante.Size = new System.Drawing.Size(1276, 703);
            this.tpRestaurante.TabIndex = 4;
            this.tpRestaurante.Text = "Restaurante";
            // 
            // richTextBox2
            // 
            this.richTextBox2.AutoWordSelection = true;
            this.richTextBox2.BackColor = System.Drawing.Color.Khaki;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(85, 348);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox2.Size = new System.Drawing.Size(475, 241);
            this.richTextBox2.TabIndex = 2;
            this.richTextBox2.Text = resources.GetString("richTextBox2.Text");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Maroon;
            this.label15.Location = new System.Drawing.Point(606, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(466, 219);
            this.label15.TabIndex = 3;
            this.label15.Text = "Un placer para\r\ntodos los\r\nsentidos";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(85, 36);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(509, 306);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(566, 280);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(426, 290);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // tpEntorno
            // 
            this.tpEntorno.BackColor = System.Drawing.Color.DarkBlue;
            this.tpEntorno.Controls.Add(this.pictureBox10);
            this.tpEntorno.Controls.Add(this.pictureBox5);
            this.tpEntorno.Controls.Add(this.pictureBox13);
            this.tpEntorno.Controls.Add(this.pictureBox11);
            this.tpEntorno.Controls.Add(this.pictureBox8);
            this.tpEntorno.Controls.Add(this.pictureBox7);
            this.tpEntorno.Controls.Add(this.pictureBox4);
            this.tpEntorno.Controls.Add(this.richTextBox3);
            this.tpEntorno.Controls.Add(this.label16);
            this.tpEntorno.Controls.Add(this.pictureBox9);
            this.tpEntorno.Location = new System.Drawing.Point(4, 22);
            this.tpEntorno.Name = "tpEntorno";
            this.tpEntorno.Size = new System.Drawing.Size(1276, 703);
            this.tpEntorno.TabIndex = 5;
            this.tpEntorno.Text = "Entorno";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.rapel;
            this.pictureBox10.Location = new System.Drawing.Point(109, 285);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(172, 166);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 6;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.PIRAGÜISMO;
            this.pictureBox5.Location = new System.Drawing.Point(750, 291);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(169, 160);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 1;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.caza;
            this.pictureBox13.Location = new System.Drawing.Point(356, 419);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(260, 169);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 9;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.quads;
            this.pictureBox11.Location = new System.Drawing.Point(613, 419);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(171, 169);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 7;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.bici2;
            this.pictureBox8.Location = new System.Drawing.Point(196, 419);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(165, 169);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 4;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.caballo;
            this.pictureBox7.Location = new System.Drawing.Point(750, 141);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(169, 162);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 3;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.tiroconarco;
            this.pictureBox4.Location = new System.Drawing.Point(109, 140);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(172, 163);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.Color.DarkBlue;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.ForeColor = System.Drawing.Color.White;
            this.richTextBox3.Location = new System.Drawing.Point(280, 193);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox3.Size = new System.Drawing.Size(486, 233);
            this.richTextBox3.TabIndex = 10;
            this.richTextBox3.Text = resources.GetString("richTextBox3.Text");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(252, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(568, 55);
            this.label16.TabIndex = 11;
            this.label16.Text = "Belleza de la Naturaleza";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = global::InterfazDeUsuario.Properties.Resources.guadalest;
            this.pictureBox9.Location = new System.Drawing.Point(213, 51);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(571, 174);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 5;
            this.pictureBox9.TabStop = false;
            // 
            // FormHotel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 635);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormHotel";
            this.Text = "FormHotel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormHotel_FormClosed);
            this.Load += new System.EventHandler(this.FormHotel_Load);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.gbParkingRemolques.ResumeLayout(false);
            this.gbParkingRemolques.PerformLayout();
            this.gbParkingTurismos.ResumeLayout(false);
            this.gbParkingTurismos.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gb1.ResumeLayout(false);
            this.gb1.PerformLayout();
            this.gbPlanta3.ResumeLayout(false);
            this.gbPlanta3.PerformLayout();
            this.gbPlanta2.ResumeLayout(false);
            this.gbPlanta2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpHotel.ResumeLayout(false);
            this.tpHotel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tpRestaurante.ResumeLayout(false);
            this.tpRestaurante.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tpEntorno.ResumeLayout(false);
            this.tpEntorno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.GroupBox gbParkingRemolques;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gbParkingTurismos;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.GroupBox gbPlanta3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox gbPlanta2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button btInabilitar;
        private System.Windows.Forms.TextBox tbHabitacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPlanta;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tpHotel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tpRestaurante;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tpEntorno;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label16;
    }
}