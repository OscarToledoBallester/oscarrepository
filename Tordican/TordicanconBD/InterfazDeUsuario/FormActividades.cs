﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using ENLogicaDeNegocio;
using Persistencia;


namespace InterfazDeUsuario
{
    public partial class FormActividades : Form
    {
        public FormActividades()
        {
            // Constructor.
            InitializeComponent();
            cambiarImagenes(1);
        }
        public FormActividades(string pestanya)
        {
            InitializeComponent();

            // Constructor sobrecargado (recibe el parametro "pestanya" para activar un TabPage concreto).
            // IMPORTANTE: Los botones "ASIGNAR" y "VOLVER" solamente los mostramos cuando accedemos
            // desde RESERVAS utilizando este constructor sobrecargado.
            btQuadAsignar.Visible = true;
            btQuadVolver.Visible = true;
            btBiciAsignar.Visible = true;
            btBiciVolver.Visible = true;
            btCaballoAsignar.Visible = true;
            btCaballoVolver.Visible = true;
            btArcoAsignar.Visible = true;
            btArcoVolver.Visible = true;
            btPiraguismoAsignar.Visible = true;
            btPiraguismoVolver.Visible = true;
            btRapelAsignar.Visible = true;
            btRapelVolver.Visible = true;
            btParqueAsignar.Visible = true;
            btParqueVolver.Visible = true;
            btLocalidadAsignar.Visible = true;
            btLocalidadVolver.Visible = true;

            switch (pestanya)
            {
                case "tpQuad":
                    tcActividades.SelectedTab = tpQuad;
                    cambiarImagenes(1);
                    break;
                case "tpBicicleta":
                    tcActividades.SelectedTab = tpBicicleta;
                    cambiarImagenes(2);
                    break;
                case "tpCaballo":
                    tcActividades.SelectedTab = tpCaballo;
                    cambiarImagenes(3);
                    break;
                case "tpArco":
                    tcActividades.SelectedTab = tpArco;
                    cambiarImagenes(4);
                    break;
                case "tpPiraguismo":
                    tcActividades.SelectedTab = tpPiraguismo;
                    cambiarImagenes(5);
                    break;
                case "tpRapel":
                    tcActividades.SelectedTab = tpRapel;
                    cambiarImagenes(6);
                    break;
                case "tpParque":
                    tcActividades.SelectedTab = tpParque;
                    cambiarImagenes(7);
                    break;
                case "tpLocalidad":
                    tcActividades.SelectedTab = tpLocalidad;
                    cambiarImagenes(8);
                    break;
            }
        }      
        private void FormActividades_Load(object sender, EventArgs e)
        {
            // CARGA DE OPCIONES CONDICIONADA AL TIPO DE USUARIO DEL SISTEMA
            // -------------------------------------------------------------
            // - PARA EL GERENTE ---------> Principal.tipoEmp = 0;
            // - PARA EL RECEPCIONISTA ---> Principal.tipoEmp = 1;

            if (Principal.tipoEmp == 0)
            {
                // GERENTE:
                //  - Le podemos bloquear los botones de "ASIGNAR" y "VOLVER", pero NO lo hacemos!
                /*
                btQuadAsignar.Enabled = false;
                btQuadVolver.Enabled = false;
                btBiciAsignar.Enabled = false;
                btBiciVolver.Enabled = false;
                btCaballoAsignar.Enabled = false;
                btCaballoVolver.Enabled = false;
                btArcoAsignar.Enabled = false;
                btArcoVolver.Enabled = false;
                btPiraguismoAsignar.Enabled = false;
                btPiraguismoVolver.Enabled = false;
                btRapelAsignar.Enabled = false;
                btRapelVolver.Enabled = false;
                btParqueAsignar.Enabled = false;
                btParqueVolver.Enabled = false;
                btLocalidadAsignar.Enabled = false;
                btLocalidadVolver.Enabled = false;
                */
            }
            else
            {
                // RECEPCIONISTA:
                //  - Le ocultamos las opciones para insertar, editar y borrar actividades.
                //  - Le inhabilitamos el GroupBox con los datos de la actividad.
                gbQuadDatos.Enabled = false;
                gbQuadOpciones.Visible = false;
                checkQuadDisponibles.Enabled = false; // Evitamos que vea los NO disponibles.
                gbBiciDatos.Enabled = false;
                gbBiciOpciones.Visible = false;
                checkBiciDisponibles.Enabled = false; // Evitamos que vea las NO disponibles.
                gbBiciPrecios.Visible = false;
                gbCaballoDatos.Enabled = false;
                gbCaballoOpciones.Visible = false;
                gbArcoDatos.Enabled = false;
                gbArcoOpciones.Visible = false;
                gbPiraguismoDatos.Enabled = false;
                gbPiraguismoOpciones.Visible = false;
                gbRapelDatos.Enabled = false;
                gbRapelOpciones.Visible = false;
                gbParqueDatos.Enabled = false;
                gbParqueOpciones.Visible = false;
                gbLocalidadDatos.Enabled = false;
                gbLocalidadOpciones.Visible = false;
                // Al recepcionista le ocultamos el boton de Incidencias.
                btIncidencias.Visible = false;
                btActividades.Visible = false;
                labelQuadNota.Visible = false; // Ocultacion de notas.
                labelBiciNota.Visible = false;
            }

            btActividades.Enabled = false; // Boton deshabilitado.

            // QUADS.
            getTodosVehiculoQuad(true, "todos", ""); // Cargamos solo los disponibles.
            cbQuadFiltro.SelectedIndex = 0; // Sin filtro.
            mostrarInfoVehiculosQuad();
       //     cambiarImagenes(1);

            // BIBICLETAS.
            getTodasBicicleta(0, true); // Cargamos solo los disponibles.
            cbBiciFiltro.SelectedIndex = 0; // Ver todas.
            cbBiciTipo.SelectedIndex = 1; // Adulto.
            mostrarInfoBicicletas();
            cargarPreciosBici();

            // PASEOS A CABALLO.
            getTodosPaseosCaballo();
            mostrarInfoPaseosCaballo();

            // TIRO CON ARCO.
            getTodosTiroArco();

            // PIRAGUISMO.
            getTodosPiraguismo();
            mostrarInfoPiraguismo();
 
            // RAPEL.
            getTodosRapel();
            mostrarInfoRapel();

            // PARQUES TEMATICOS.
            getTodosParques();

            // LOCALIDADES DE INTERES.
            getTodasLocalidades();

            // INCIDENCIAS
            getTodasIncidencia(true);
            mostrarInfoIncidencia();
            cbIncidenciaActividad.SelectedIndex = 0;
            
        }
        private void FormActividades_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Acciones a realizar cuando se cierra el formulario.
            Principal.ActivarDesactivarBotones(false);
            Principal.formActividades = null;
        }
        private void btActividades_Click(object sender, EventArgs e)
        {
            // Botones laterales (izquierda): Actividades.

            // Realizamos la muestra/ocultacion de paneles y habilitacion/bloqueo de botones.
            panelIncidencias.Visible = false;
            panelActividades.Visible = true;
            btActividades.Enabled = false;
            btIncidencias.Enabled = true;
        }
        private void btIncidencias_Click(object sender, EventArgs e)
        {
            // Botones laterales (izquierda): Incidencias.

            // Realizamos la muestra/ocultacion de paneles y habilitacion/bloqueo de botones.
            panelActividades.Visible = false;
            panelIncidencias.Visible = true;
            btIncidencias.Enabled = false;
            btActividades.Enabled = true;
            cambiarImagenes(9);
        }
        private void cambiarImagenes(int indice)
        {
            switch (indice)
            {
                case 1:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_quad;
                    break;
                case 2:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_bicicleta;
                    break;
                case 3:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_caballo;
                    break;
                case 4:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_tiro_arco;
                    break;
                case 5:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_piraguismo;
                    break;
                case 6:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_rapel;
                    break;
                case 7:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_parque_tematico;
                    break;
                case 8:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_localidad;
                    break;
                case 9:
                    pbActividad.Image = InterfazDeUsuario.Properties.Resources.actividad_incidencia;
                    break;
                default:
                    pbActividad.Image = null;
                    break;
            }
            pbActividad.Refresh(); // Recarga la imagen.
        }


        /* ===================================================================================================================
         * VALIDACIONES NECESARIAS PARA ACTIVIDADES (Expresiones regulares)
         * ===================================================================================================================
         */
        private bool esMatricula(string cad) // Validacion de matriculas
        {
            //  \D – Representa cualquier carácter que no sea un dígito del 0 al 9.
            //  \d – Representa un dígito del 0 al 9.
            //  \w – Representa cualquier carácter alfanumérico.
            string patEuropea = @"^\d{4}-[\D\w]{3}$"; // Ejemplo: 0123-ABC
            string patProvincial = @"^[\D\w]{1,2}-\d{4}-[\D\w]{1,2}$"; // Ejemplos: AB-0123-CD, A-1234-B, MU-5678-D, S-0987-DE
            return (Regex.Match(cad, patEuropea).Success || Regex.Match(cad, patProvincial).Success) ? true : false;
        }
        private bool esDecimal(string cad) // Validacion de numero decimal no negativo
        {
            string patDecimal = @"^(\d+)\,(\d+)$|^(\d+)$"; // Ejemplos: 12,5 / 5 / 200,99 / 1,65 / 28
            return (Regex.Match(cad, patDecimal).Success) ? true : false;
        }
        private bool esNatural(string cad) // Validacion de numero natural
        {
            string patEntero = @"^\d+$";
            return (Regex.Match(cad, patEntero).Success) ? true : false;
        }
        public string limpiarCadena(string cadena)
        {
            // List of characters handled:
            // \000 null
            // \010 backspace
            // \011 horizontal tab
            // \012 new line
            // \015 carriage return
            // \032 substitute
            // \042 double quote
            // \047 single quote
            // \134 backslash
            // \140 grave accent

            string resultado = cadena;
            try
            {
                resultado = System.Text.RegularExpressions.Regex.Replace(cadena, @"[\000\010\011\012\015\032\042\047\134\140]", "");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            return resultado;
        }


        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "QUADS"
         * ===================================================================================================================
         */
        private void btQuadInsertar_Click(object sender, EventArgs e)
        {
            // Inserta un vehiculo quad en la tabla "VehiculoQuad" de la BD.
            ENVehiculoQuad quad = new ENVehiculoQuad();
            string matriculaS = limpiarCadena(tbQuadMatricula.Text.Trim().ToUpper()); // Pasamos a mayusculas.
            string marcaS = limpiarCadena(tbQuadMarca.Text.Trim());
            string modeloS = limpiarCadena(tbQuadModelo.Text.Trim());
            string potenciaS = limpiarCadena(tbQuadPotencia.Text.Trim());
            string pesoS = limpiarCadena(tbQuadPeso.Text.Trim());
            string precioS = limpiarCadena(tbQuadPrecio.Text.Trim());
            DateTime fechaAltaDT = dtpQuadFecha.Value;
            bool enUsoB = checkQuadUso.Checked;
            string texto = "", caption = "";

            if (comprobarCamposQuad(matriculaS, marcaS, modeloS, potenciaS, pesoS, precioS)) // Todos OK
            {
                float potenciaF = float.Parse(potenciaS);
                float pesoF = float.Parse(pesoS);
                float precioF = float.Parse(precioS);

                CADVehiculoQuad quadCAD = new CADVehiculoQuad();
                if (quadCAD.existeMatricula(matriculaS))
                {
                    texto = "- ¡Matrícula ya registrada previamente!\n";
                    texto += "- No pueden existir duplicados de matrícula.";
                    caption = "ERROR AL INSERTAR QUAD";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    bool insertado = quad.insertar(matriculaS, marcaS, modeloS, potenciaF, pesoF, precioF, fechaAltaDT, enUsoB);
                    if (insertado)
                    {
                        texto = "- El vehículo quad se ha insertado correctamente.";
                        caption = "QUAD INSERTADO";
                        MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // Limpiamos los campos.
                        limpiarCamposQuad();

                        // Volvemos a cargar mostrar todos los quads (incluido el insertado) en el DGV.
                        getTodosVehiculoQuad(true, "todos", "");
                        checkQuadDisponibles.Checked = true;
                        cbQuadFiltro.SelectedIndex = 0;
                        mostrarInfoVehiculosQuad();
                    }
                    else
                    {
                        texto = "- Hubo algún problema al insertar el vehículo quad.\n";
                        texto += "- Revise que no existan comillas ni caracteres extraños.\n\n";
                        texto += "(¿Intento de SQL Injection?)";
                        caption = "ERROR AL INSERTAR QUAD";
                        MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private bool comprobarCamposQuad(string matricula, string marca, string modelo, string potencia, string peso, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (!esMatricula(matricula))
                msjError += "- La 'matrícula' no tiene un patrón correcto (Europea o Provincial).\n";
            if (marca == "")
                msjError += "- La 'marca' no contiene datos.\n";
            if (modelo == "")
                msjError += "- El 'modelo' no contiene datos.\n";
            if (!esDecimal(potencia))
                msjError += "- El valor para 'potencia' no es correcto.\n";
            if (!esDecimal(peso))
                msjError += "- El valor para 'peso' no es correcto.\n";
            if (!esDecimal(precio))
                msjError += "- El valor para 'precio' no es correcto.\n";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposQuad()
        {
            // Hace un reset a todos los campos del Quad (textbox, checkbox, ...)

            tbQuadID.Text = "";
            tbQuadMatricula.Text = "";
            tbQuadMatricula.Enabled = true; // Por si lo habiamos bloqueado al seleccionad fila en DGV.
            tbQuadMarca.Text = "";
            tbQuadModelo.Text = "";
            tbQuadPotencia.Text = "";
            tbQuadPeso.Text = "";
            tbQuadPrecio.Text = "";
            dtpQuadFecha.Value = DateTime.Today;
            checkQuadUso.Checked = false;

            // Finalmente, habilitamos el boton "Insertar".
            btQuadInsertar.Enabled = true;
            btQuadEditar.Enabled = false;
            btQuadBorrar.Enabled = false;

        }
        private void mostrarInfoVehiculosQuad()
        {
            // Muestra total de quads y quads en uso.
        
            int total = 0;
            int enUso = 0;
            ENVehiculoQuad quad = new ENVehiculoQuad();

            // Obtenemos las sumas de la tabla VehiculoQuad.
            total = quad.getNumTotal(false);
            enUso = quad.getNumTotal(true);

            // Mostramos los resultados en los label correspondientes.
            labelQuadsTotal.Text = total.ToString();
            labelQuadsEnUso.Text = enUso.ToString();
        }
        private void btQuadModificar_Click(object sender, EventArgs e)
        {
            // Modifica los atributos de un quad en la tabla "VehiculoQuad" de la BD.
            ENVehiculoQuad quad = new ENVehiculoQuad();

            int idQuadI = int.Parse(tbQuadID.Text.ToString());
            string matriculaS = tbQuadMatricula.Text.Trim();
            string marcaS = limpiarCadena(tbQuadMarca.Text.Trim());
            string modeloS = limpiarCadena(tbQuadModelo.Text.Trim());
            string potenciaS = limpiarCadena(tbQuadPotencia.Text.Trim());
            string pesoS = limpiarCadena(tbQuadPeso.Text.Trim());
            string precioS = limpiarCadena(tbQuadPrecio.Text.Trim());
            DateTime fechaAltaDT = dtpQuadFecha.Value;
            bool enUsoB = checkQuadUso.Checked;

            if (comprobarCamposQuad(matriculaS, marcaS, modeloS, potenciaS, pesoS, precioS))
            {
                float potenciaF = float.Parse(potenciaS);
                float pesoF = float.Parse(pesoS);
                float precioF = float.Parse(precioS);

                bool modificado = quad.modificar(idQuadI, matriculaS, marcaS, modeloS, potenciaF, pesoF, precioF, fechaAltaDT, enUsoB);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "QUAD MODIFICADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposQuad();

                    // Volvemos a cargar mostrar todos los quads (incluido el insertado) en el DGV.
                    getTodosVehiculoQuad(true, "todos", "");
                    checkQuadDisponibles.Checked = true;
                    cbQuadFiltro.SelectedIndex = 0;
                    mostrarInfoVehiculosQuad();
                }
                else
                {
                    texto = "- Hubo algún problema al modificar el vehículo quad.\n";
                    texto += "- Revise que no existan comillas ni caracteres extraños.\n\n";
                    texto += "(¿Intento de SQL Injection?)";
                    caption = "ERROR AL INSERTAR QUAD";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void getTodosVehiculoQuad(bool disponibles, string filtro, string cadenaBuscar)
        {
            // Muestra en el DataGridView los quads. 

            // Borra el contenido del DataGrid cada vez que hacemos una busqueda.
            dgvQuadResultados.Rows.Clear();

            ENVehiculoQuad quad = new ENVehiculoQuad();
            DataSet todosQuads = null;
            int numFilas = 0;
            string idQuad, matricula, marca, modelo, potencia, peso, precio, fechaAlta, enUso;

            // Llamada a la EN que nos devuelve el DataSet.
            todosQuads = quad.getTodos(disponibles, filtro, cadenaBuscar);
            numFilas = todosQuads.Tables[0].Rows.Count;

            if (todosQuads != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idQuad = todosQuads.Tables[0].Rows[i].ItemArray[0].ToString();
                    matricula = todosQuads.Tables[0].Rows[i].ItemArray[1].ToString();
                    marca = todosQuads.Tables[0].Rows[i].ItemArray[2].ToString();
                    modelo = todosQuads.Tables[0].Rows[i].ItemArray[3].ToString();
                    potencia = todosQuads.Tables[0].Rows[i].ItemArray[4].ToString();
                    peso = todosQuads.Tables[0].Rows[i].ItemArray[5].ToString();
                    precio = todosQuads.Tables[0].Rows[i].ItemArray[6].ToString();
                    fechaAlta = todosQuads.Tables[0].Rows[i].ItemArray[7].ToString();
                    enUso = todosQuads.Tables[0].Rows[i].ItemArray[8].ToString();

                    // Añadimos la linea al DataGridView
                    dgvQuadResultados.Rows.Add(idQuad, matricula, marca, modelo, potencia, peso, precio, fechaAlta, enUso);
                }
            }
            else
            {
                string texto = "- ¡No se han encontrado resultados!";
                string caption = "RESULTADOS DE BÚSQUEDA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
        private void btQuadBusca_Click(object sender, EventArgs e)
        {
            // Gestion de las distintas busquedas, segun filtros.

            string filtro = cbQuadFiltro.Text;
            string cadenaBuscar = limpiarCadena(tbQuadBusca.Text.Trim());
            //MessageBox.Show(busqueda);

            // Busqueda segun el criterio seleccionado.
            switch (filtro)
            {
                case "Marca":
                    getTodosVehiculoQuad(checkQuadDisponibles.Checked, "marca", cadenaBuscar);
                    break;

                case "Matricula": // Al ser la Clave Primaria de la tabla solo tendremos una fila. Mostramos tambien en TextBox.
                    getTodosVehiculoQuad(checkQuadDisponibles.Checked, "matricula", cadenaBuscar);
                    break;

                default: // Sin filtros.
                    getTodosVehiculoQuad(checkQuadDisponibles.Checked, "todos", cadenaBuscar);
                    break;
            }
            mostrarInfoVehiculosQuad();
        }
        private void btQuadReset_Click(object sender, EventArgs e)
        {
            // Limpia y resetea los campos.
            limpiarCamposQuad();
        }
        private void btQuadBorrar_Click(object sender, EventArgs e)
        {
            // Realiza el borrado del quad seleccionado.

            ENVehiculoQuad quad = new ENVehiculoQuad();
            int idQuad = int.Parse(tbQuadID.Text.ToString());
            string marca = tbQuadMarca.Text.ToString();
            string modelo = tbQuadModelo.Text.ToString();
            string matricula = tbQuadMatricula.Text.ToString();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar el quad " + marca + " " + modelo + " con matrícula " + matricula + " de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = quad.borrar(idQuad);
                if (borrado == true)
                {
                    // Limpiamos los campos.
                    limpiarCamposQuad();

                    // Volvemos a cargar mostrar todos los quads (incluido el insertado) en el DGV.
                    getTodosVehiculoQuad(true, "todos", "");
                    checkQuadDisponibles.Checked = true;
                    cbQuadFiltro.SelectedIndex = 0;
                    mostrarInfoVehiculosQuad();
                }
                else
                {
                    texto = "- Se produjo un problema al borrar el vehículo quad.";
                    caption = "ERROR AL BORRAR QUAD";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK);
                }
            }
        }
        private void cbQuadFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Habilita y desabilita el TextBox de busqueda segun el filtro seleccionado.
            if (cbQuadFiltro.Text != "(Sin filtro)")
            {
                tbQuadBusca.Enabled = true;
                tbQuadBusca.Focus();
            }
            else
            {
                tbQuadBusca.Enabled = false;
                tbQuadBusca.Text = "";
            }
        }
        private void dgvQuadResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Carga los campos de la fila seleccionada del DataGridView en los TextBox de abajo.
            foreach (DataGridViewRow fila in dgvQuadResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbQuadID.Text = fila.Cells[0].Value.ToString();
                    tbQuadMatricula.Text = fila.Cells[1].Value.ToString();
                    tbQuadMarca.Text = fila.Cells[2].Value.ToString();
                    tbQuadModelo.Text = fila.Cells[3].Value.ToString();
                    tbQuadPotencia.Text = fila.Cells[4].Value.ToString();
                    tbQuadPeso.Text = fila.Cells[5].Value.ToString();
                    tbQuadPrecio.Text = fila.Cells[6].Value.ToString();
                    dtpQuadFecha.Value = DateTime.Parse(fila.Cells[7].Value.ToString());
                    checkQuadUso.Checked = bool.Parse(fila.Cells[8].Value.ToString());
                }
            }
            // Inhabilitamos el boton insertar, porque cuando seleccionamos un registro es SOLO para modificaciones.
            tbQuadMatricula.Enabled = false;
            btQuadInsertar.Enabled = false;
            btQuadEditar.Enabled = true;
            // Solo podemos borrar los quads que NO ESTAN EN USO!!!
            btQuadBorrar.Enabled = (checkQuadUso.Checked == false) ? true : false; // Solo podemos borrar los que NO ESTAN EN USO!!!
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "BICICLETAS"
         * ===================================================================================================================
         */
        private bool comprobarCamposBiciPrecios(string precioNino, string precioAdulto, string precioTandem)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (!esDecimal(precioNino))
                msjError += "- El precio para la bicicleta de 'niño' no es correcto.\n";
            if (!esDecimal(precioAdulto))
                msjError += "- El precio para la bicicleta de 'adulto' no es correcto.\n";
            if (!esDecimal(precioTandem))
                msjError += "- El precio para la bicicleta 'tándem' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void btBiciInsertar_Click(object sender, EventArgs e)
        {
            // Inserta una bicicleta en la BD.

            ENBicicleta bici = new ENBicicleta();

            int tipoI = 0;
            switch (cbBiciTipo.Text)
            {
                case "Niño":
                    tipoI = 1;
                    break;
                case "Adulto":
                    tipoI = 2;
                    break;
                case "Tándem":
                    tipoI = 3;
                    break;
            }
            DateTime fechaAltaD = dtpBiciFecha.Value;
            bool enUsoB = checkBiciUso.Checked;
            bool insertado = bici.insertar(tipoI, fechaAltaD, enUsoB);
            string texto = "";
            string caption = "";

            if (insertado)
            {
                texto = "- La bicicleta se ha insertado correctamente.";
                caption = "BICICLETA INSERTADA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Limpiamos los campos.
                limpiarCamposBici();

                getTodasBicicleta(0, true);
                cbBiciFiltro.SelectedIndex = 0;
                checkBiciDisponibles.Checked = true;
                mostrarInfoBicicletas();
            }
            else
            {
                texto = "- Se produjo un error al insertar la bicicleta.";
                caption = "ERROR AL INSERTAR BICICLETA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
       
        }
        private void btBiciBusca_Click(object sender, EventArgs e)
        {
            string filtro = cbBiciFiltro.Text;
            bool disponibles = checkBiciDisponibles.Checked;

            switch (filtro)
            {
                case "(Ver todas)":
                    getTodasBicicleta(0, disponibles);
                    break;
                case "Niño":
                    getTodasBicicleta(1, disponibles);
                    break;
                case "Adulto":
                    getTodasBicicleta(2, disponibles);
                    break;
                case "Tándem":
                    getTodasBicicleta(3, disponibles);
                    break;
            }
        }
        private void getTodasBicicleta(int filtro, bool disponibles)
        {
            // Muestra en el DataGridView las bicicletas.

            // Borramos el contenido anterior del DataGridView.
            dgvBiciResultados.Rows.Clear();

            ENBicicleta bici = new ENBicicleta();
            DataSet todasBicis = null;
            int numFilas = 0;
            string idBici, tipo, fechaAltaDia, precio, enUso;
            DateTime fechaAlta;

            // Llamada que nos devuelve el DataSet.
            todasBicis = bici.getTodas(filtro, disponibles);
            numFilas = todasBicis.Tables[0].Rows.Count;

            if (todasBicis != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idBici = todasBicis.Tables[0].Rows[i].ItemArray[0].ToString();
                    tipo = todasBicis.Tables[0].Rows[i].ItemArray[1].ToString();
                    fechaAlta = DateTime.Parse(todasBicis.Tables[0].Rows[i].ItemArray[2].ToString());
                    fechaAltaDia = fechaAlta.ToShortDateString().ToString();
                    precio = todasBicis.Tables[0].Rows[i].ItemArray[3].ToString();
                    enUso = todasBicis.Tables[0].Rows[i].ItemArray[4].ToString();

                    // Añadimos linea al DataGridView.
                    dgvBiciResultados.Rows.Add(idBici, tipo, fechaAltaDia, precio, enUso);
                }
            }
        }
        private void cargarPreciosBici()
        {
            ENBicicleta bici = new ENBicicleta();
            float precioNino = 0, precioAdulto = 0, precioTandem = 0;

            bici.getPrecios(ref precioNino, ref precioAdulto, ref precioTandem);
            tbBiciPrecioNinyo.Text = precioNino.ToString();
            tbBiciPrecioAdulto.Text = precioAdulto.ToString();
            tbBiciPrecioTandem.Text = precioTandem.ToString();
        }
        private void limpiarCamposBici()
        {
            cbBiciTipo.SelectedIndex = 1; // Adulto.
            dtpBiciFecha.Value = DateTime.Today;
            checkBiciUso.Checked = false;
            tbBiciID.Text = "";

            // Habilitamos insertar.
            btBiciInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btBiciEditar.Enabled = false;
            btBiciBorrar.Enabled = false;
        }
        private void mostrarInfoBicicletas()
        {
            int total = 0;
            int enUso = 0;
            ENBicicleta bici = new ENBicicleta();

            // Obtenemos sumas de la tabla Bicicleta.
            total = bici.getNumTotal(false);
            enUso = bici.getNumTotal(true);

            // Mostrar resultados en labels.
            labelBicisTotal.Text = total.ToString();
            labelBicisUso.Text = enUso.ToString();
        }
        private void dgvBiciResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvBiciResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbBiciID.Text = fila.Cells[0].Value.ToString();
                    switch (fila.Cells[1].Value.ToString())
                    {
                        case "Niño":
                            cbBiciTipo.SelectedIndex = 0;
                            break;

                        case "Adulto":
                            cbBiciTipo.SelectedIndex = 1;
                            break;

                        case "Tándem":
                            cbBiciTipo.SelectedIndex = 2;
                            break;
                    }
                    dtpBiciFecha.Value = DateTime.Parse(fila.Cells[2].Value.ToString());
                    checkBiciUso.Checked = bool.Parse(fila.Cells[4].Value.ToString());
                }
            }
            btBiciInsertar.Enabled = false;
            btBiciEditar.Enabled = true;
            // Solo borramos las bicis que no estan en uso.
            btBiciBorrar.Enabled = (checkBiciUso.Checked == false) ? true : false;
        }
        private void btBiciEditar_Click(object sender, EventArgs e)
        {
            ENVerificacion comprobaciones = new ENVerificacion();
            // REALIZAR COMPROBACIONES DE CAMPOS CON CLASE "VALIDACION" ANTES DE CREAR LA ENTIDAD DE NEGOCIO.

            ENBicicleta bici = new ENBicicleta();

            int idBiciI = int.Parse(tbBiciID.Text.ToString());
            int tipoI = 0;
            switch (cbBiciTipo.Text)
            {
                case "Niño":
                    tipoI = 1;
                    break;
                case "Adulto":
                    tipoI = 2;
                    break;
                case "Tándem":
                    tipoI = 3;
                    break;
            }
            DateTime fechaAltaD = dtpBiciFecha.Value;
            bool enUsoB = checkBiciUso.Checked;
            bool insertado = bici.modificar(idBiciI, tipoI, fechaAltaD, enUsoB);
            string texto = "";
            string caption = "";

            if (insertado)
            {
                texto = "- Se han guardado los cambios realizados.";
                caption = "BICICLETA MODIFICADA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Limpiamos los campos.
                limpiarCamposBici();

                // Volvemos a cargar mostrar todos los quads (incluido el insertado) en el DGV.
                getTodasBicicleta(0, true);
                cbBiciFiltro.SelectedIndex = 0;
                checkBiciDisponibles.Checked = true;
                mostrarInfoBicicletas();
            }
            else
            {
                texto = "- Se produjo un error al modificar la bicicleta.";
                caption = "ERROR AL MODIFICAR BICICLETA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btBiciBorrar_Click(object sender, EventArgs e)
        {
            ENBicicleta bici = new ENBicicleta();
            int idBici = int.Parse(tbBiciID.Text.ToString());
            string tipo = cbBiciTipo.Text.ToString();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar la bicicleta " + idBici + " de tipo " + tipo + " de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = bici.borrar(idBici);
                if (borrado == true)
                {
                    limpiarCamposBici();
                    getTodasBicicleta(0, true);
                    cbBiciFiltro.SelectedIndex = 0;
                    checkBiciDisponibles.Checked = true;
                    mostrarInfoBicicletas();
                }
                else
                {
                    texto = "- Se produjo un error al borrar la bicicleta.";
                    caption = "ERROR AL BORRAR BICICLETA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }
        private void btBiciReset_Click(object sender, EventArgs e)
        {
            limpiarCamposBici();
        }
        private void btTipoBiciPrecios_Click(object sender, EventArgs e)
        {
            ENBicicleta bici = new ENBicicleta();
            string precioNinyoS = tbBiciPrecioNinyo.Text;
            string precioAdultoS = tbBiciPrecioAdulto.Text;
            string precioTandemS = tbBiciPrecioTandem.Text;

            bool modificados = false;
            string texto;
            string caption;

            if (comprobarCamposBiciPrecios(precioNinyoS, precioAdultoS, precioTandemS))
            {
                float precioNinyo = float.Parse(precioNinyoS);
                float precioAdulto = float.Parse(precioAdultoS);
                float precioTandem = float.Parse(precioTandemS);

                modificados = bici.setPrecios(precioNinyo, precioAdulto, precioTandem);

                if (modificados)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "PRECIOS ACTUALIZADOS";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Cargamos los nuevos precios.
                    cargarPreciosBici();

                    // Volvemos a cargar mostrar todas las bicis (con precios actualizados) en el DGV.
                    getTodasBicicleta(0, false);
                }
                else
                {
                    texto = "- Se produjo un problema al modificar los precios.";
                    caption = "ERROR AL MODIFICAR PRECIOS";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "PASEOS A CABALLO"
         * ===================================================================================================================
         */
        private bool comprobarCamposPaseoCaballo(string nombreRuta, string descripcion, string duracion, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (nombreRuta == "")
                msjError += "- La 'ruta' no contiene datos.\n";
            if (descripcion == "")
                msjError += "- La 'descripción' no contiene datos.\n";
            if (!esNatural(duracion))
                msjError += "- La valor de 'duración' no es correcto.\n";
            else if (esNatural(duracion) && int.Parse(duracion) < 10)
                msjError += "- La 'duración' mínima son 10 minutos.\n";
            if (!esDecimal(precio))
                msjError += "- El valor de 'precio' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposPaseoCaballo()
        {
            tbCaballoRuta.Text = "";
            tbCaballoDescripcion.Text = "";
            tbCaballoDuracion.Text = "";
            tbCaballoPrecio.Text = "";
            checkCaballoPony.Checked = false;
            tbPaseoID.Text = "";

            // Habilitamos insertar.
            btCaballoInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btCaballoEditar.Enabled = false;
            btCaballoBorrar.Enabled = false;
        }
        private void getTodosPaseosCaballo()
        {
            dgvCaballoResultados.Rows.Clear(); // Borra DGV.
            ENPaseoCaballo paseo = new ENPaseoCaballo();
            DataSet todosPaseos = null;
            int numFilas = 0;
            string idPaseo, nombreRuta, descripcion, duracion, precio, conPony;

            todosPaseos = paseo.getTodos();
            numFilas = todosPaseos.Tables[0].Rows.Count;

            if (todosPaseos != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idPaseo = todosPaseos.Tables[0].Rows[i].ItemArray[0].ToString();
                    nombreRuta = todosPaseos.Tables[0].Rows[i].ItemArray[1].ToString();
                    descripcion = todosPaseos.Tables[0].Rows[i].ItemArray[2].ToString();
                    duracion = todosPaseos.Tables[0].Rows[i].ItemArray[3].ToString();
                    precio = todosPaseos.Tables[0].Rows[i].ItemArray[4].ToString();
                    conPony = todosPaseos.Tables[0].Rows[i].ItemArray[5].ToString();

                    dgvCaballoResultados.Rows.Add(idPaseo, nombreRuta, descripcion, duracion, precio, conPony);
                }
            }
        }
        private void mostrarInfoPaseosCaballo()
        {
            int total = 0;
            int conPony = 0;
            ENPaseoCaballo paseo = new ENPaseoCaballo();

            // Obtenemos sumas de la tabla Bicicleta.
            total = paseo.getNumTotal(false);
            conPony = paseo.getNumTotal(true);

            // Mostrar resultados en labels.
            labelPaseosTotal.Text = total.ToString();
            labelPaseosPony.Text = conPony.ToString();
        }
        private void btCaballoInsertar_Click(object sender, EventArgs e)
        {
            ENPaseoCaballo paseo = new ENPaseoCaballo();

            string nombreRuta = limpiarCadena(tbCaballoRuta.Text.Trim());
            string descripcion = limpiarCadena(tbCaballoDescripcion.Text.Trim());
            string duracionS = limpiarCadena(tbCaballoDuracion.Text.Trim());
            string precioS = limpiarCadena(tbCaballoPrecio.Text.Trim());
            bool conPony = checkCaballoPony.Checked;

            if (comprobarCamposPaseoCaballo(nombreRuta, descripcion, duracionS, precioS))
            {
                int duracion = int.Parse(duracionS);
                float precio = float.Parse(precioS);

                bool insertado = paseo.insertar(nombreRuta, descripcion, duracion, precio, conPony);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- El paseo a caballo se ha insertado correctamente.";
                    caption = "PASEO A CABALLO INSERTADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposPaseoCaballo();

                    getTodosPaseosCaballo();
                    mostrarInfoPaseosCaballo();
                }
                else
                {
                    texto = "- Se produjo un error al insertar la bicicleta.";
                    caption = "ERROR AL INSERTAR BICICLETA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }
        private void btCaballoReset_Click(object sender, EventArgs e)
        {
            limpiarCamposPaseoCaballo();
        }
        private void dgvCaballoResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvCaballoResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbPaseoID.Text = fila.Cells[0].Value.ToString();
                    tbCaballoRuta.Text = fila.Cells[1].Value.ToString();
                    tbCaballoDescripcion.Text = fila.Cells[2].Value.ToString();
                    tbCaballoDuracion.Text = fila.Cells[3].Value.ToString();
                    tbCaballoPrecio.Text = fila.Cells[4].Value.ToString();
                    checkCaballoPony.Checked = bool.Parse(fila.Cells[5].Value.ToString());
                }
            }
            btCaballoInsertar.Enabled = false;
            btCaballoEditar.Enabled = true;
            btCaballoBorrar.Enabled = true;
        }
        private void btCaballoEditar_Click(object sender, EventArgs e)
        {
            ENPaseoCaballo paseo = new ENPaseoCaballo();

            int idPaseo = int.Parse(tbPaseoID.Text);
            string nombreRuta = limpiarCadena(tbCaballoRuta.Text.Trim());
            string descripcion = limpiarCadena(tbCaballoDescripcion.Text.Trim());
            string duracionS = limpiarCadena(tbCaballoDuracion.Text.Trim());
            string precioS = limpiarCadena(tbCaballoPrecio.Text.Trim());
            bool conPony = checkCaballoPony.Checked;

            if (comprobarCamposPaseoCaballo(nombreRuta, descripcion, duracionS, precioS))
            {
                int duracion = int.Parse(duracionS);
                float precio = float.Parse(precioS);

                bool modificado = paseo.modificar(idPaseo, nombreRuta, descripcion, duracion, precio, conPony);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "PASEO A CABALLO MODIFICADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposPaseoCaballo();

                    // Volvemos a cargar mostrar todos los quads (incluido el insertado) en el DGV.
                    getTodosPaseosCaballo();
                    mostrarInfoPaseosCaballo();
                }
                else
                {
                    texto = "- Se produjo un error al modificar el paseo a caballo.";
                    caption = "ERROR AL MODIFICAR EL PASEO A CABALLO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btCaballoBorrar_Click(object sender, EventArgs e)
        {
            ENPaseoCaballo paseo = new ENPaseoCaballo();
            int idPaseo = int.Parse(tbPaseoID.Text);
            string nombreRuta = tbCaballoRuta.Text;
            bool borrado = false;

            string texto = "¿Seguro que desea borrar el paseo a caballo (" + idPaseo + ") '" + nombreRuta + "' de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = paseo.borrar(idPaseo);
                if (borrado == true)
                {
                    limpiarCamposPaseoCaballo();
                    getTodosPaseosCaballo();
                    mostrarInfoPaseosCaballo();
                }
                else
                {
                    texto = "- Se produjo un error al borrar el paseo a caballo.";
                    caption = "ERROR AL BORRAR EL PASEO A CABALLO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "TIRO CON ARCO"
         * ===================================================================================================================
         */
        bool comprobarCamposTiroArco(string numDianas, string numFlechas, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (!esNatural(numDianas))
                msjError += "- El valor de 'número de dianas' no es correcto.\n";
            else if (esNatural(numDianas) && int.Parse(numDianas) < 1)
                msjError += "- El valor mínimo de 'número de dianas' es uno.\n";
            if (!esNatural(numFlechas))
                msjError += "- El valor de 'número de flechas' no es correcto.\n";
            else if (esNatural(numFlechas) && int.Parse(numFlechas) < 1)
                msjError += "- El valor mínimo de 'número de flechas' es uno.\n";
            if (!esDecimal(precio))
                msjError += "- El valor de 'precio' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposTiroArco()
        {
            tbArcoID.Text = "";
            tbArcoNumDianas.Text = "";
            tbArcoNumFlechas.Text = "";
            tbArcoPrecio.Text = "";

            // Habilitamos insertar.
            btArcoInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btArcoEditar.Enabled = false;
            btArcoBorrar.Enabled = false;
        }
        private void getTodosTiroArco()
        {
            dgvArcoResultados.Rows.Clear(); // Borra DGV.
            ENTiroArco tiro = new ENTiroArco();
            DataSet todosTiros = null;
            int numFilas = 0;
            string idTiro, numFlechas, numDianas, precio;

            todosTiros = tiro.getTodos();
            numFilas = todosTiros.Tables[0].Rows.Count;

            if (todosTiros != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idTiro = todosTiros.Tables[0].Rows[i].ItemArray[0].ToString();
                    numDianas = todosTiros.Tables[0].Rows[i].ItemArray[1].ToString();
                    numFlechas = todosTiros.Tables[0].Rows[i].ItemArray[2].ToString();
                    precio = todosTiros.Tables[0].Rows[i].ItemArray[3].ToString();

                    dgvArcoResultados.Rows.Add(idTiro, numDianas, numFlechas, precio);
                }
            }
            labelArcoOfertas.Text = numFilas.ToString();
        }
        private void btArcoInsertar_Click(object sender, EventArgs e)
        {
            ENTiroArco tiro = new ENTiroArco();

            string numDianasS = tbArcoNumDianas.Text.Trim();
            string numFlechasS = tbArcoNumFlechas.Text.Trim();
            string precioS = tbArcoPrecio.Text.Trim();

            if (comprobarCamposTiroArco(numDianasS, numFlechasS, precioS))
            {
                int numDianas = int.Parse(numDianasS);
                int numFlechas = int.Parse(numFlechasS);
                float precio = float.Parse(precioS);

                bool insertado = tiro.insertar(numDianas, numFlechas, precio);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- La oferta de tiro con arco se ha insertado correctamente.";
                    caption = "TIRO CON ARCO INSERTADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposTiroArco();

                    getTodosTiroArco();
                }
                else
                {
                    texto = "- Se produjo un error al insertar la oferta de tiro.";
                    caption = "ERROR AL INSERTAR OFERTA DE TIRO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btArcoEditar_Click(object sender, EventArgs e)
        {
            ENTiroArco tiro = new ENTiroArco();

            int idTiro = int.Parse(tbArcoID.Text);
            string numDianasS = tbArcoNumDianas.Text.Trim();
            string numFlechasS = tbArcoNumFlechas.Text.Trim();
            string precioS = tbArcoPrecio.Text.Trim();

            if (comprobarCamposTiroArco(numDianasS, numFlechasS, precioS))
            {
                int numDianas = int.Parse(numDianasS);
                int numFlechas = int.Parse(numFlechasS);
                float precio = float.Parse(precioS);

                bool modificado = tiro.modificar(idTiro, numDianas, numFlechas, precio);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "OFERTA DE TIRO CON ARCO MODIFICADA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposTiroArco();

                    // Volvemos a cargar mostrar todos los quads (incluido el insertado) en el DGV.
                    getTodosTiroArco();
                }
                else
                {
                    texto = "- Se produjo un error al modificar la oferta de tiro.";
                    caption = "ERROR AL MODIFICAR OFERTA DE TIRO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btArcoBorrar_Click(object sender, EventArgs e)
        {
            ENTiroArco tiro = new ENTiroArco();

            int idTiro = int.Parse(tbArcoID.Text);
            int numDianas = int.Parse(tbArcoNumDianas.Text);
            bool borrado = false;

            string texto = "¿Seguro que desea borrar la oferta de tiro con arco (" + idTiro + ") con " + numDianas + " dianas de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = tiro.borrar(idTiro);
                if (borrado == true)
                {
                    limpiarCamposTiroArco();
                    getTodosTiroArco();
                }
                else
                {
                    texto = "- Se produjo un error al borrar la oferta de tiro.";
                    caption = "ERROR AL BORRAR LA OFERTA DE TIRO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btArcoReset_Click(object sender, EventArgs e)
        {
            limpiarCamposTiroArco();
        }
        private void dgvArcoResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvArcoResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbArcoID.Text = fila.Cells[0].Value.ToString();
                    tbArcoNumDianas.Text = fila.Cells[1].Value.ToString();
                    tbArcoNumFlechas.Text = fila.Cells[2].Value.ToString();
                    tbArcoPrecio.Text = fila.Cells[3].Value.ToString();
                }
            }
            btArcoInsertar.Enabled = false;
            btArcoEditar.Enabled = true;
            btArcoBorrar.Enabled = true;
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "PIRAGUISMO"
         * ===================================================================================================================
         */
        private bool comprobarCamposPiraguismo(string pantano, string numChalecos, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (pantano == "")
                msjError += "- El 'pantano' no contiene datos.\n";
            if (!esNatural(numChalecos))
                msjError += "- El valor de 'número de chalecos' no es correcto.\n";
            else if (esNatural(numChalecos) && int.Parse(numChalecos) < 1)
                msjError += "- El valor mínimo de 'número de chalecos' es uno.\n";
            if (!esDecimal(precio))
                msjError += "- El valor de 'precio' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposPiraguismo()
        {
            tbPiraguismoID.Text = "";
            tbPiraguismoPantano.Text = "";
            tbPiraguismoNumChalecos.Text = "";
            tbPiraguismoPrecio.Text = "";

            // Habilitamos insertar.
            btPiraguismoInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btPiraguismoEditar.Enabled = false;
            btPiraguismoBorrar.Enabled = false;
        }
        private void getTodosPiraguismo()
        {
            dgvPiraguismoResultados.Rows.Clear(); // Borra DGV.
            ENPiraguismo pira = new ENPiraguismo();
            DataSet todosPira = null;
            int numFilas = 0;
            string idPiraguismo, pantano, numChalecos, precio;

            todosPira = pira.getTodos();
            numFilas = todosPira.Tables[0].Rows.Count;

            if (todosPira != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idPiraguismo = todosPira.Tables[0].Rows[i].ItemArray[0].ToString();
                    pantano = todosPira.Tables[0].Rows[i].ItemArray[1].ToString();
                    numChalecos = todosPira.Tables[0].Rows[i].ItemArray[2].ToString();
                    precio = todosPira.Tables[0].Rows[i].ItemArray[3].ToString();

                    dgvPiraguismoResultados.Rows.Add(idPiraguismo, pantano, numChalecos, precio);
                }
            }
        }
        private void mostrarInfoPiraguismo()
        {
            int numPantanos = 0;
            int numChalecos = 0;
            ENPiraguismo pira = new ENPiraguismo();

            // Obtenemos sumas de la tabla Bicicleta.
            numPantanos = pira.getNumTotalPantanos();
            numChalecos = pira.getNumTotalChalecos();

            // Mostrar resultados en labels.
            labelPiragPantanos.Text = numPantanos.ToString();
            labelPiragChalecos.Text = numChalecos.ToString();
        }
        private void btPiraguismoInsertar_Click(object sender, EventArgs e)
        {
            ENPiraguismo pira = new ENPiraguismo();

            string pantano = limpiarCadena(tbPiraguismoPantano.Text.Trim());
            string numChalecosS = tbPiraguismoNumChalecos.Text.Trim();
            string precioS = tbPiraguismoPrecio.Text.Trim();

            if (comprobarCamposPiraguismo(pantano, numChalecosS, precioS))
            {
                int numChalecos = int.Parse(numChalecosS);
                float precio = float.Parse(precioS);

                bool insertado = pira.insertar(pantano, numChalecos, precio);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- El pantano (para piragüismo) se ha insertado correctamente.";
                    caption = "PANTANO INSERTADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposPiraguismo();
                    getTodosPiraguismo();
                    mostrarInfoPiraguismo();
                }
                else
                {
                    texto = "- Se produjo un error al insertar el pantano (para piragüismo).";
                    caption = "ERROR AL INSERTAR PANTANO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btPiraguismoEditar_Click(object sender, EventArgs e)
        {
            ENPiraguismo pira = new ENPiraguismo();

            int idPiraguismo = int.Parse(tbPiraguismoID.Text);
            string pantano = limpiarCadena(tbPiraguismoPantano.Text.Trim());
            string numChalecosS = tbPiraguismoNumChalecos.Text.Trim();
            string precioS = tbPiraguismoPrecio.Text.Trim();

            if (comprobarCamposPiraguismo(pantano, numChalecosS, precioS))
            {
                int numChalecos = int.Parse(numChalecosS);
                float precio = float.Parse(precioS);
                bool modificado = pira.modificar(idPiraguismo, pantano, numChalecos, precio);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "PANTANO MODIFICADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposPiraguismo();
                    getTodosPiraguismo();
                    mostrarInfoPiraguismo();
                }
                else
                {
                    texto = "- Se produjo un error al modificar el pantano.";
                    caption = "ERROR AL MODIFICAR EL PANTANO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btPiraguismoBorrar_Click(object sender, EventArgs e)
        {
            ENPiraguismo pira = new ENPiraguismo();

            int idPiraguismo = int.Parse(tbPiraguismoID.Text);
            string pantano = tbPiraguismoPantano.Text.Trim();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar el pantano (" + idPiraguismo + ") con nombre '" + pantano + "' de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = pira.borrar(idPiraguismo);
                if (borrado == true)
                {
                    limpiarCamposPiraguismo();
                    getTodosPiraguismo();
                    mostrarInfoPiraguismo();
                }
                else
                {
                    texto = "- Se produjo un error al borrar el pantano.";
                    caption = "ERROR AL BORRAR EL PANTANO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btPiraguismoReset_Click(object sender, EventArgs e)
        {
            limpiarCamposPiraguismo();
        }
        private void dgvPiraguismoResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvPiraguismoResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbPiraguismoID.Text = fila.Cells[0].Value.ToString();
                    tbPiraguismoPantano.Text = fila.Cells[1].Value.ToString();
                    tbPiraguismoNumChalecos.Text = fila.Cells[2].Value.ToString();
                    tbPiraguismoPrecio.Text = fila.Cells[3].Value.ToString();
                }
            }
            btPiraguismoInsertar.Enabled = false;
            btPiraguismoEditar.Enabled = true;
            btPiraguismoBorrar.Enabled = true;
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "RAPEL"
         * ===================================================================================================================
         */
        private bool comprobarCamposRapel(string lugar, string numCuerdas, string numEquipos, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (lugar == "")
                msjError += "- El 'lugar' (pantano) no contiene datos.\n";
            if (!esNatural(numCuerdas))
                msjError += "- El valor de 'número de cuerdas' no es correcto.\n";
            else if (esNatural(numCuerdas) && int.Parse(numCuerdas) < 1)
                msjError += "- El valor mínimo de 'número de cuerdas' es uno.\n";
            if (!esNatural(numEquipos))
                msjError += "- El valor de 'número de equipos' no es correcto.\n";
            else if (esNatural(numEquipos) && int.Parse(numEquipos) < 1)
                msjError += "- El valor mínimo de 'número de equipos' es uno.\n";
            if (!esDecimal(precio))
                msjError += "- El valor de 'precio' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposRapel()
        {
            tbRapelID.Text = "";
            tbRapelLugar.Text = "";
            tbRapelNumCuerdas.Text = "";
            tbRapelNumEquipos.Text = "";
            tbRapelPrecio.Text = "";

            // Habilitamos insertar.
            btRapelInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btRapelEditar.Enabled = false;
            btRapelBorrar.Enabled = false;
        }
        private void getTodosRapel()
        {
            dgvRapelResultados.Rows.Clear(); // Borra DGV.
            ENRapel rapel = new ENRapel();
            DataSet todosRapel = null;
            int numFilas = 0;
            string idRapel, lugar, numCuerdas, numEquipos, precio;

            todosRapel = rapel.getTodos();
            numFilas = todosRapel.Tables[0].Rows.Count;

            if (todosRapel != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idRapel = todosRapel.Tables[0].Rows[i].ItemArray[0].ToString();
                    lugar = todosRapel.Tables[0].Rows[i].ItemArray[1].ToString();
                    numCuerdas = todosRapel.Tables[0].Rows[i].ItemArray[2].ToString();
                    numEquipos = todosRapel.Tables[0].Rows[i].ItemArray[3].ToString();
                    precio = todosRapel.Tables[0].Rows[i].ItemArray[4].ToString();

                    dgvRapelResultados.Rows.Add(idRapel, lugar, numCuerdas, numEquipos, precio);
                }
            }
        }
        private void mostrarInfoRapel()
        {
            int numLugares = 0;
            int numCuerdas = 0;
            int numEquipos = 0;
            ENRapel rapel = new ENRapel();

            // Obtenemos sumas de la tabla Bicicleta.
            numLugares = rapel.getNumTotalLugares();
            numCuerdas = rapel.getNumTotalCuerdas();
            numEquipos = rapel.getNumTotalEquipos();

            // Mostrar resultados en labels.
            labelRapelLugares.Text = numLugares.ToString();
            labelRapelCuerdas.Text = numCuerdas.ToString();
            labelRapelEquipos.Text = numEquipos.ToString();
        }
        private void btRapelInsertar_Click(object sender, EventArgs e)
        {
            ENRapel rapel = new ENRapel();

            string lugar = limpiarCadena(tbRapelLugar.Text.Trim());
            string numCuerdasS = tbRapelNumCuerdas.Text.Trim();
            string numEquiposS = tbRapelNumEquipos.Text.Trim();
            string precioS = tbRapelPrecio.Text.Trim();

            if (comprobarCamposRapel(lugar, numCuerdasS, numEquiposS, precioS))
            {
                int numCuerdas = int.Parse(numCuerdasS);
                int numEquipos = int.Parse(numEquiposS);
                float precio = float.Parse(precioS);

                bool insertado = rapel.insertar(lugar, numCuerdas, numEquipos, precio);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- El lugar (para rápel) se ha insertado correctamente.";
                    caption = "LUGAR INSERTADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposRapel();
                    getTodosRapel();
                    mostrarInfoRapel();
                }
                else
                {
                    texto = "- Se produjo un error al insertar el lugar (para rápel).";
                    caption = "ERROR AL INSERTAR LUGAR";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btRapelEditar_Click(object sender, EventArgs e)
        {
            ENRapel rapel = new ENRapel();

            int idRapel = int.Parse(tbRapelID.Text);
            string lugar = limpiarCadena(tbRapelLugar.Text.Trim());
            string numCuerdasS = tbRapelNumCuerdas.Text.Trim();
            string numEquiposS = tbRapelNumEquipos.Text.Trim();
            string precioS = tbRapelPrecio.Text.Trim();

            if (comprobarCamposRapel(lugar, numCuerdasS, numEquiposS, precioS))
            {
                int numCuerdas = int.Parse(numCuerdasS);
                int numEquipos = int.Parse(numEquiposS);
                float precio = float.Parse(precioS);

                bool modificado = rapel.modificar(idRapel, lugar, numCuerdas, numEquipos, precio);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "LUGAR MODIFICADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposRapel();
                    getTodosRapel();
                    mostrarInfoRapel();
                }
                else
                {
                    texto = "- Se produjo un error al modificar el lugar.";
                    caption = "ERROR AL MODIFICAR EL LUGAR";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btRapelBorrar_Click(object sender, EventArgs e)
        {
            ENRapel rapel = new ENRapel();
            int idRapel = int.Parse(tbRapelID.Text);
            string lugar = tbRapelLugar.Text.Trim();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar el lugar (" + idRapel + ") con nombre '" + lugar + "' de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = rapel.borrar(idRapel);
                if (borrado == true)
                {
                    limpiarCamposRapel();
                    getTodosRapel();
                    mostrarInfoRapel();
                }
                else
                {
                    texto = "- Se produjo un error al borrar el lugar.";
                    caption = "ERROR AL BORRAR EL LUGAR";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btRapelReset_Click(object sender, EventArgs e)
        {
            limpiarCamposRapel();
        }
        private void dgvRapelResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvRapelResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbRapelID.Text = fila.Cells[0].Value.ToString();
                    tbRapelLugar.Text = fila.Cells[1].Value.ToString();
                    tbRapelNumEquipos.Text = fila.Cells[2].Value.ToString();
                    tbRapelNumCuerdas.Text = fila.Cells[3].Value.ToString();
                    tbRapelPrecio.Text = fila.Cells[4].Value.ToString();
                }
            }
            btRapelInsertar.Enabled = false;
            btRapelEditar.Enabled = true;
            btRapelBorrar.Enabled = true;
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "PARQUES TEMATICOS"
         * ===================================================================================================================
         */
        private bool comprobarCamposParque(string nombre, string localidad, string descripcion, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (nombre == "")
                msjError += "- El 'nombre' no contiene datos.\n";
            if (localidad == "")
                msjError += "- La 'localidad' no contiene datos.\n";
            if (descripcion == "")
                msjError += "- La 'descripción' no contiene datos.\n";
            if (!esDecimal(precio))
                msjError += "- El valor de 'precio' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposParque()
        {
            tbParqueID.Text = "";
            tbParqueNombre.Text = "";
            tbParqueLugar.Text = "";
            tbParqueDescripcion.Text = "";
            tbParquePrecio.Text = "";

            // Habilitamos insertar.
            btParqueInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btParqueEditar.Enabled = false;
            btParqueBorrar.Enabled = false;
        }
        private void getTodosParques()
        {
            dgvParqueResultados.Rows.Clear(); // Borra DGV.
            ENParqueTematico parque = new ENParqueTematico();
            DataSet todosParques = null;
            int numFilas = 0;
            string idParque, nombre, localidad, descripcion, precio;

            todosParques = parque.getTodos();
            numFilas = todosParques.Tables[0].Rows.Count;

            if (todosParques != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idParque = todosParques.Tables[0].Rows[i].ItemArray[0].ToString();
                    nombre = todosParques.Tables[0].Rows[i].ItemArray[1].ToString();
                    localidad = todosParques.Tables[0].Rows[i].ItemArray[2].ToString();
                    descripcion = todosParques.Tables[0].Rows[i].ItemArray[3].ToString();
                    precio = todosParques.Tables[0].Rows[i].ItemArray[4].ToString();

                    dgvParqueResultados.Rows.Add(idParque, nombre, localidad, descripcion, precio);
                }
            }
            labelParquesTotal.Text = numFilas.ToString();
        }
        private void btParqueInsertar_Click(object sender, EventArgs e)
        {
            ENParqueTematico parque = new ENParqueTematico();
            string nombre = limpiarCadena(tbParqueNombre.Text.Trim());
            string localidad = limpiarCadena(tbParqueLugar.Text.Trim());
            string descripcion = limpiarCadena(tbParqueDescripcion.Text.Trim());
            string precioS = tbParquePrecio.Text.Trim();

            if (comprobarCamposParque(nombre, localidad, descripcion, precioS))
            {
                float precio = float.Parse(precioS);

                bool insertado = parque.insertar(nombre, localidad, descripcion, precio);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- El parque temático se ha insertado correctamente.";
                    caption = "PARQUE TEMÁTICO INSERTADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposParque();
                    getTodosParques();
                }
                else
                {
                    texto = "- ¿Había introducido este mismo parque anteriormente?\n";
                    texto += "- ¡No pueden haber 2 parques temáticos con el mismo nombre!";
                    caption = "ERROR AL INSERTAR PARQUE TEMÁTICO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btParqueEditar_Click(object sender, EventArgs e)
        {
            ENParqueTematico parque = new ENParqueTematico();
            int idParque = int.Parse(tbParqueID.Text);
            string nombre = limpiarCadena(tbParqueNombre.Text.Trim());
            string localidad = limpiarCadena(tbParqueLugar.Text.Trim());
            string descripcion = limpiarCadena(tbParqueDescripcion.Text.Trim());
            string precioS = tbParquePrecio.Text.Trim();

            if (comprobarCamposParque(nombre, localidad, descripcion, precioS))
            {
                float precio = float.Parse(precioS);
                bool modificado = parque.modificar(idParque, nombre, localidad, descripcion, precio);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "PARQUE TEMÁTICO MODIFICADO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposParque();
                    getTodosParques();
                }
                else
                {
                    texto = "- ¿Había introducido este mismo parque anteriormente?\n";
                    texto += "- ¡No pueden haber 2 parques temáticos con el mismo nombre!";
                    caption = "ERROR AL MODIFICAR EL PARQUE TEMÁTICO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btParqueBorrar_Click(object sender, EventArgs e)
        {
            ENParqueTematico parque = new ENParqueTematico();
            int idParque = int.Parse(tbParqueID.Text);
            string nombre = tbParqueNombre.Text.Trim();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar el parque temático (" + idParque + ") '" + nombre + "' de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = parque.borrar(idParque);
                if (borrado == true)
                {
                    limpiarCamposParque();
                    getTodosParques();
                }
                else
                {
                    texto = "- Se produjo un error al borrar el parque temático.";
                    caption = "ERROR AL BORRAR EL PARQUE TEMÁTICO";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btParqueReset_Click(object sender, EventArgs e)
        {
            limpiarCamposParque();
        }
        private void dgvParqueResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvParqueResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbParqueID.Text = fila.Cells[0].Value.ToString();
                    tbParqueNombre.Text = fila.Cells[1].Value.ToString();
                    tbParqueLugar.Text = fila.Cells[2].Value.ToString();
                    tbParqueDescripcion.Text = fila.Cells[3].Value.ToString();
                    tbParquePrecio.Text = fila.Cells[4].Value.ToString();
                }
            }
            btParqueInsertar.Enabled = false;
            btParqueEditar.Enabled = true;
            btParqueBorrar.Enabled = true;
        }

        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "LOCALIDADES DE INTERES"
         * ===================================================================================================================
         */
        private bool comprobarCamposLocalidad(string nombre, string descripcion, string distancia, string precio)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (nombre == "")
                msjError += "- El 'nombre' no contiene datos.\n";
            if (descripcion == "")
                msjError += "- La 'descripción' no contiene datos.\n";
            if (!esDecimal(distancia))
                msjError += "- El valor de 'distancia' no es correcto.\n";
            else if (esDecimal(distancia) && float.Parse(distancia) < 0)
                msjError += "- La 'distancia' no puede ser cero.\n";
            if (!esDecimal(precio))
                msjError += "- El valor de 'precio' no es correcto.";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void limpiarCamposLocalidad()
        {
            tbLocalidadID.Text = "";
            tbLocalidadLugar.Text = "";
            tbLocalidadDescripcion.Text = "";
            tbLocalidadDistancia.Text = "";
            tbLocalidadPrecio.Text = "";

            // Habilitamos insertar.
            btLocalidadInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btLocalidadEditar.Enabled = false;
            btLocalidadBorrar.Enabled = false;
        }
        private void getTodasLocalidades()
        {
            dgvLocalidadResultados.Rows.Clear(); // Borra DGV.
            ENLocalidadInteres locali = new ENLocalidadInteres();
            DataSet todasLocali = null;
            int numFilas = 0;
            string idLocalidad, nombre, descripcion, distancia, precio;
           
            todasLocali = locali.getTodos();
            numFilas = todasLocali.Tables[0].Rows.Count;

            if (todasLocali != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idLocalidad = todasLocali.Tables[0].Rows[i].ItemArray[0].ToString();
                    nombre = todasLocali.Tables[0].Rows[i].ItemArray[1].ToString();
                    descripcion = todasLocali.Tables[0].Rows[i].ItemArray[2].ToString();
                    distancia = todasLocali.Tables[0].Rows[i].ItemArray[3].ToString();
                    precio = todasLocali.Tables[0].Rows[i].ItemArray[4].ToString();

                    dgvLocalidadResultados.Rows.Add(idLocalidad, nombre, descripcion, distancia, precio);
                }
            }
            labelLocalidadTotal.Text = numFilas.ToString();
        }
        private void btLocalidadInsertar_Click(object sender, EventArgs e)
        {
            ENLocalidadInteres locali = new ENLocalidadInteres();
            string nombre = limpiarCadena(tbLocalidadLugar.Text.Trim());
            string descripcion = limpiarCadena(tbLocalidadDescripcion.Text.Trim());
            string distanciaS = tbLocalidadDistancia.Text.Trim();
            string precioS = tbLocalidadPrecio.Text.Trim();

            if (comprobarCamposLocalidad(nombre, descripcion, distanciaS, precioS))
            {
                float distancia = float.Parse(distanciaS);
                float precio = float.Parse(precioS);
                bool insertado = locali.insertar(nombre, descripcion, distancia, precio);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- La localidad de interés se ha insertado correctamente.";
                    caption = "LOCALIDAD DE INTERÉS INSERTADA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposLocalidad();
                    getTodasLocalidades();
                }
                else
                {
                    texto = "- ¿Había introducido esta misma localidad anteriormente?\n";
                    texto += "- ¡No pueden haber 2 localidades con el mismo nombre!";
                    caption = "ERROR AL INSERTAR LOCALIDAD DE INTERÉS";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btLocalidadEditar_Click(object sender, EventArgs e)
        {
            ENLocalidadInteres locali = new ENLocalidadInteres();
            int idLocalidad = int.Parse(tbLocalidadID.Text);
            string nombre = limpiarCadena(tbLocalidadLugar.Text.Trim());
            string descripcion = limpiarCadena(tbLocalidadDescripcion.Text.Trim());
            string distanciaS = tbLocalidadDistancia.Text.Trim();
            string precioS = tbLocalidadPrecio.Text.Trim();

            if (comprobarCamposLocalidad(nombre, descripcion, distanciaS, precioS))
            {
                float distancia = float.Parse(distanciaS);
                float precio = float.Parse(precioS);
                bool modificado = locali.modificar(idLocalidad, nombre, descripcion, distancia, precio);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- Se han guardado los cambios realizados.";
                    caption = "LOCALIDAD DE INTERÉS MODIFICADA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposLocalidad();
                    getTodasLocalidades();
                }
                else
                {
                    texto = "- ¿Había introducido esta misma localidad anteriormente?\n";
                    texto += "- ¡No pueden haber 2 localidades con el mismo nombre!";
                    caption = "ERROR AL MODIFICAR LA LOCALIDAD DE INTERÉS";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btLocalidadBorrar_Click(object sender, EventArgs e)
        {
            ENLocalidadInteres locali = new ENLocalidadInteres();
            int idLocalidad = int.Parse(tbLocalidadID.Text);
            string nombre = tbLocalidadLugar.Text.Trim();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar la localidad de interés (" + idLocalidad + ") '" + nombre + "' de la Base de Datos?";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = locali.borrar(idLocalidad);
                if (borrado == true)
                {
                    limpiarCamposLocalidad();
                    getTodasLocalidades();
                }
                else
                {
                    texto = "- Se produjo un error al borrar la localidad de interés.";
                    caption = "ERROR AL BORRAR LA LOCALIDAD DE INTERÉS";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btLocalidadReset_Click(object sender, EventArgs e)
        {
            limpiarCamposLocalidad();
        }
        private void dgvLocalidadResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dgvLocalidadResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbLocalidadID.Text = fila.Cells[0].Value.ToString();
                    tbLocalidadLugar.Text = fila.Cells[1].Value.ToString();
                    tbLocalidadDescripcion.Text = fila.Cells[2].Value.ToString();
                    tbLocalidadDistancia.Text = fila.Cells[3].Value.ToString();
                    tbLocalidadPrecio.Text = fila.Cells[4].Value.ToString();
                }
            }
            btLocalidadInsertar.Enabled = false;
            btLocalidadEditar.Enabled = true;
            btLocalidadBorrar.Enabled = true;
        }


        /* ===================================================================================================================
         * FUNCIONALIDADES DE LA PESTAÑA "INCIDENCIAS"
         * ===================================================================================================================
         */
        private void limpiarCamposIncidencia()
        {
            tbIncidenciaID.Text = "";
            tbIncidenciaDescripcion.Text = "";
            cbIncidenciaActividad.SelectedIndex = 0;
            dtpIncidenciaFecha.Value = DateTime.Today;
            checkIncidenciaRevisada.Checked = false;
            checkIncidenciaRevisada.Enabled = false; // Evitamos de este modo que se inserten incidencias ya revisadas.

            // Habilitamos insertar
            btIncidenciaInsertar.Enabled = true;
            // Desabilitamos editar y borrar.
            btIncidenciaEditar.Enabled = false;
            btIncidenciaBorrar.Enabled = false;

        }
        private void getTodasIncidencia(bool pendientesRevisar)
        {
            // Borramos el contenido anterior del DataGridView.
            dgvIncidenciaResultados.Rows.Clear();

            ENIncidencia inci = new ENIncidencia();
            DataSet todasIncidencias = null;
            int numFilas = 0;
            string idIncidencia, descripcion, revisada, actividadS = "";
            int actividad = 0;
            DateTime fecha;
            string fechaDia;

            // Llamada que nos devuelve el DataSet.
            todasIncidencias = inci.getTodas(pendientesRevisar);
            numFilas = todasIncidencias.Tables[0].Rows.Count;

            if (todasIncidencias != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    idIncidencia = todasIncidencias.Tables[0].Rows[i].ItemArray[0].ToString();
                    descripcion = todasIncidencias.Tables[0].Rows[i].ItemArray[1].ToString();
                    fecha = DateTime.Parse(todasIncidencias.Tables[0].Rows[i].ItemArray[2].ToString());
                    fechaDia = fecha.ToShortDateString().ToString(); // Obtenemos solamente el dia (sin la hora).
                    revisada = todasIncidencias.Tables[0].Rows[i].ItemArray[3].ToString();
                    revisada = (bool.Parse(revisada)) ? "Sí" : "No";
                    actividad = int.Parse(todasIncidencias.Tables[0].Rows[i].ItemArray[4].ToString());
                    switch (actividad) // Conversion para el DGV, asi mostramos la actividad en vez del entero.
                    {
                        case 1:
                            actividadS = "Quad";
                            break;
                        case 2:
                            actividadS = "Bicicleta";
                            break;
                        case 3:
                            actividadS = "Paseo a Caballo";
                            break;
                        case 4:
                            actividadS = "Tiro con Arco";
                            break;
                        case 5:
                            actividadS = "Piragüismo";
                            break;
                        case 6:
                            actividadS = "Rápel";
                            break;
                        case 7:
                            actividadS = "Parque Temático";
                            break;
                        case 8:
                            actividadS = "Localidad de Interés";
                            break;
                    }

                    // Añadimos linea al DataGridView.
                    dgvIncidenciaResultados.Rows.Add(idIncidencia, actividadS, fechaDia, descripcion, revisada);
                }
            }
        }
        private void mostrarInfoIncidencia()
        {
            int numTotal = 0;
            int numPendientes = 0;
            ENIncidencia inci = new ENIncidencia();

            // Obtenemos sumas de la tabla Bicicleta.
            numTotal = inci.getNumTotal(false);
            numPendientes = inci.getNumTotal(true);

            // Mostrar resultados en labels.
            labelIncidenciaTotal.Text = numTotal.ToString();
            labelIncidenciaPendientes.Text = numPendientes.ToString();
        }
        private bool comprobarCamposIncidencia(string descripcion, int actividad)
        {
            // Comprueba que todos los campos sensibles a errores esten rellenos correctamente.
            string msjError = "";

            if (actividad == 0)
                msjError += "- No ha seleccionado ninguna actividad.\n";
            if (descripcion == "" || descripcion.Length < 10)
                msjError += "- Descripción vacía o demasiado corta (mín. 10 caracteres).\n";

            // Comprobacion final.
            if (msjError != "") // Hay errores.
            {
                MessageBox.Show(msjError, "ERROR: REVISE LOS CAMPOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }
        private void btIncidenciaInsertar_Click(object sender, EventArgs e)
        {
            ENIncidencia inci = new ENIncidencia();
            string descripcion = limpiarCadena(tbIncidenciaDescripcion.Text.Trim());
            DateTime fecha = dtpIncidenciaFecha.Value;
            bool revisada = checkIncidenciaRevisada.Checked;
            int actividad = cbIncidenciaActividad.SelectedIndex;

            if (comprobarCamposIncidencia(descripcion, actividad)) // Todo OK
            {
                bool insertado = inci.insertar(descripcion, fecha, revisada, actividad);
                string texto = "";
                string caption = "";

                if (insertado)
                {
                    texto = "- La incidencia se ha insertado correctamente.";
                    caption = "INCIDENCIA INSERTADA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposIncidencia();
                    getTodasIncidencia(true); // Las pendientes.
                    mostrarInfoIncidencia();
                    rbIncidenciaPendientes.Checked = true;
                }
                else
                {
                    texto = "- Se produjo un error al insertar la incidencia.";
                    caption = "ERROR AL INSERTAR INCIDENCIA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btIncidenciaReset_Click(object sender, EventArgs e)
        {
            limpiarCamposIncidencia();
        }
        private void btIncidenciaEditar_Click(object sender, EventArgs e)
        {
            ENIncidencia inci = new ENIncidencia();
            int idIncidencia = int.Parse(tbIncidenciaID.Text);
            string descripcion = limpiarCadena(tbIncidenciaDescripcion.Text.Trim());
            DateTime fecha = dtpIncidenciaFecha.Value;
            bool revisada = checkIncidenciaRevisada.Checked;
            int actividad = cbIncidenciaActividad.SelectedIndex;

            if (comprobarCamposIncidencia(descripcion, actividad)) // Todo OK
            {
                bool modificado = inci.modificar(idIncidencia, descripcion, fecha, revisada, actividad);
                string texto = "";
                string caption = "";

                if (modificado)
                {
                    texto = "- La incidencia se ha modificado correctamente.";
                    caption = "INCIDENCIA MODIFICADA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Limpiamos los campos.
                    limpiarCamposIncidencia();
                    getTodasIncidencia(true); // Las pendientes.
                    mostrarInfoIncidencia();
                    rbIncidenciaPendientes.Checked = true;
                }
                else
                {
                    texto = "- Se produjo un error al modificar la incidencia.";
                    caption = "ERROR AL MODIFICAR INCIDENCIA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btIncidenciaBorrar_Click(object sender, EventArgs e)
        {
            ENIncidencia inci = new ENIncidencia();
            int idIncidencia = int.Parse(tbIncidenciaID.Text);
            string descripcion = tbIncidenciaDescripcion.Text.Trim();
            bool borrado = false;

            string texto = "¿Seguro que desea borrar la incidencia (" + idIncidencia + ") con la siguiente descripción de la Base de Datos?:\n\n'" + descripcion + "'";
            string caption = "CONFIRMACIÓN DE BORRADO";

            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                borrado = inci.borrar(idIncidencia);
                if (borrado == true)
                {
                    limpiarCamposIncidencia();
                    getTodasIncidencia(true); // Las pendientes.
                    mostrarInfoIncidencia();
                    rbIncidenciaPendientes.Checked = true;
                }
                else
                {
                    texto = "- Se produjo un error al borrar la incidencia.";
                    caption = "ERROR AL BORRAR LA INCIDENCIA";
                    MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void dgvIncidenciaResultados_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bool revisada = false;
            string revisadaS = "";
            foreach (DataGridViewRow fila in dgvIncidenciaResultados.Rows)
            {
                if (fila.Selected == true)
                {
                    tbIncidenciaID.Text = fila.Cells[0].Value.ToString();
                    tbIncidenciaDescripcion.Text = fila.Cells[3].Value.ToString();
                    dtpIncidenciaFecha.Value = DateTime.Parse(fila.Cells[2].Value.ToString());
                    revisadaS = fila.Cells[4].Value.ToString();
                    revisada = (revisadaS == "No") ? false : true;
                    checkIncidenciaRevisada.Checked = revisada;

                    switch (fila.Cells[1].Value.ToString()) // Actividad: Del texto a ComboBox.
                    {
                        case "Quad":
                            cbIncidenciaActividad.SelectedIndex = 1;
                            break;
                        case "Bicicleta":
                            cbIncidenciaActividad.SelectedIndex = 2;
                            break;
                        case "Paseo a Caballo":
                            cbIncidenciaActividad.SelectedIndex = 3;
                            break;
                        case "Tiro con Arco":
                            cbIncidenciaActividad.SelectedIndex = 4;
                            break;
                        case "Piragüismo":
                            cbIncidenciaActividad.SelectedIndex = 5;
                            break;
                        case "Rápel":
                            cbIncidenciaActividad.SelectedIndex = 6;
                            break;
                        case "Parque Temático":
                            cbIncidenciaActividad.SelectedIndex = 7;
                            break;
                        case "Localidad de Interés":
                            cbIncidenciaActividad.SelectedIndex = 8;
                            break;
                        default:
                            cbIncidenciaActividad.SelectedIndex = 0;
                            break;
                    }
                    tbLocalidadID.Text = fila.Cells[0].Value.ToString();
                    tbLocalidadLugar.Text = fila.Cells[1].Value.ToString();
                    tbLocalidadDescripcion.Text = fila.Cells[2].Value.ToString();
                    tbLocalidadDistancia.Text = fila.Cells[3].Value.ToString();
                    tbLocalidadPrecio.Text = fila.Cells[4].Value.ToString();
                    // Habilitamos el Checkbox "revisada" para poder marcarla como tal.
                    checkIncidenciaRevisada.Enabled = true;
                }
            }
            btIncidenciaInsertar.Enabled = false;
            btIncidenciaEditar.Enabled = true;
            // Solo podemos borrar las incidencias que hayan sido revisadas.
            btIncidenciaBorrar.Enabled = (revisada) ? true : false;
        }
        private void rbIncidenciaPendientes_Click(object sender, EventArgs e)
        {
            getTodasIncidencia(true);
        }
        private void rbIncidenciaTodas_Click(object sender, EventArgs e)
        {
            getTodasIncidencia(false);
        }

        /* ===================================================================================================================
         * CAMBIOS DE IMAGEN DEL PICTUREBOX SEGUN PESTAÑA
         * ===================================================================================================================
         */
        private void tpQuad_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(1);
        }
        private void tpBicicleta_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(2);
        }
        private void tpCaballo_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(3);
        }
        private void tpArco_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(4);
        }
        private void tpPiraguismo_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(5);
        }
        private void tpRapel_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(6);
        }
        private void tpParque_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(7);
        }
        private void tpLocalidad_Enter(object sender, EventArgs e)
        {
            cambiarImagenes(8);
        }

        /* ===================================================================================================================
         * ASIGNACION DE ACTIVIDADES => ENVIO DE PARAMETROS AL FORMULARIO DE RESERVA.
         * ===================================================================================================================
         */
        private void btQuadAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idQuad = 0;
            if (tbQuadID.Text != "")
            {
                idQuad = int.Parse(tbQuadID.Text);
                FormReservas.idAct = idQuad; // Escribimos en el formulario de Reserva el ID y la Actividad.
                FormReservas.actividad = "aQuad";
                texto = "- ¡Ha seleccionado el vehículo quad!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "QUAD SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ningún vehículo quad.\n";
                texto += "- (Es posible que no queden quads disponibles).";
                caption = "QUAD NO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btQuadVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btBiciAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idBici = 0;
            if (tbBiciID.Text != "")
            {
                idBici = int.Parse(tbBiciID.Text);
                FormReservas.idAct = idBici;
                FormReservas.actividad = "aBicicleta"; 
                texto = "- ¡Ha seleccionado la bicicleta!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "BICICLETA SELECCIONADA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ninguna bicicleta.\n";
                texto += "- (Es posible que no queden bicicletas disponibles).";
                caption = "BICICLETA NO SELECCIONADA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btBiciVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btCaballoAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idPaseo = 0;
            if (tbPaseoID.Text != "")
            {
                idPaseo = int.Parse(tbPaseoID.Text);
                FormReservas.idAct = idPaseo;
                FormReservas.actividad = "aPaseo";
                texto = "- ¡Ha seleccionado el paseo a caballo!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "PASEO A CABALLO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ningún paseo a caballo.";
                caption = "PASEO A CABALLO NO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btCaballoVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btArcoAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idArco = 0;
            if (tbArcoID.Text != "")
            {
                idArco = int.Parse(tbArcoID.Text);
                FormReservas.idAct = idArco;
                FormReservas.actividad = "aTiro";
                texto = "- ¡Ha seleccionado la oferta de tiro con arco!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "TIRO CON ARCO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ninguna oferta de tiro con arco.";
                caption = "TIRO CON ARCO NO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btArcoVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btPiraguismoAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idPiraguismo = 0;
            if (tbPiraguismoID.Text != "")
            {
                idPiraguismo = int.Parse(tbPiraguismoID.Text);
                FormReservas.idAct = idPiraguismo;
                FormReservas.actividad = "aPiraguismo";
                texto = "- ¡Ha seleccionado el pantano para piragüismo!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "PIRAGÜISMO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ningún pantano para piragüismo.";
                caption = "PIRAGÜISMO NO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btPiraguismoVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btRapelAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idRapel = 0;
            if (tbRapelID.Text != "")
            {
                idRapel = int.Parse(tbRapelID.Text);
                FormReservas.idAct = idRapel;
                FormReservas.actividad = "aRapel";
                texto = "- ¡Ha seleccionado el barranco para rápel!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "RÁPEL SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ningún barranco para rápel.";
                caption = "RÁPEL NO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btRapelVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btParqueAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idParque = 0;
            if (tbParqueID.Text != "")
            {
                idParque = int.Parse(tbParqueID.Text);
                FormReservas.idAct = idParque;
                FormReservas.actividad = "aParque";
                texto = "- ¡Ha seleccionado el parque temático!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "PARQUE TEMÁTICO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ningún parque temático.";
                caption = "PARQUE TEMÁTICO NO SELECCIONADO";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btParqueVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }
        private void btLocalidadAsignar_Click(object sender, EventArgs e)
        {
            string texto = "";
            string caption = "";

            int idLocalidad = 0;
            if (tbLocalidadID.Text != "")
            {
                idLocalidad = int.Parse(tbLocalidadID.Text);
                FormReservas.idAct = idLocalidad;
                FormReservas.actividad = "aLocalidad";
                texto = "- ¡Ha seleccionado la localidad de interés!\n";
                texto += "- (Volvemos al formulario de Reserva)";
                caption = "LOCALIDAD DE INTERÉS SELECCIONADA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                texto = "- No ha seleccionado ninguna localidad de interés.";
                caption = "LOCALIDAD DE INTERÉS NO SELECCIONADA";
                MessageBox.Show(texto, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void btLocalidadVolver_Click(object sender, EventArgs e)
        {
            string texto = "¿Desea volver a RESERVAS sin contratar actividad?";
            string caption = "CANCELAR CONTRATACIÓN";
            if (MessageBox.Show(texto, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                Close();
        }


        /* ===================================================================================================================
         * FUNCION PARA DEVOLVER PRECIOS AL FORMULARIO DE RESERVAS.
         * ===================================================================================================================
         */
        public static float DevuelvePrecioAreserva(string actividad, int id)
        {
            float pr = 0;
            switch (actividad)
            {
                case "Quad":
                    CADVehiculoQuad ca = new CADVehiculoQuad();
                    pr = ca.getPrecio(id);
                    break;
                case "Bicicleta":
                    CADBicicleta cb = new CADBicicleta();
                    pr = cb.getPrecio(id);
                    break;
                case "Paseo":
                    CADPaseoCaballo pc = new CADPaseoCaballo();
                    pr = pc.getPrecio(id);
                    break;
                case "Tiro":
                    CADTiroArco ta = new CADTiroArco();
                    pr = ta.getPrecio(id);
                    break;
                case "Piraguismo":
                    CADPiraguismo pi = new CADPiraguismo();
                    pr = pi.getPrecio(id);
                    break;
                case "Rapel":
                    CADRapel ra = new CADRapel();
                    break;
                case "Parque":
                    CADParqueTematico pk = new CADParqueTematico();
                    pr = pk.getPrecio(id);
                    break;
                case "Localidad":
                    CADLocalidadInteres lo = new CADLocalidadInteres();
                    lo.getPrecio(id);
                    break;
            }
            return pr;
        }
    }
}