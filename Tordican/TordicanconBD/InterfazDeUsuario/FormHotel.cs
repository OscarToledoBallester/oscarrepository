﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using ENLogicaDeNegocio;
using Persistencia;


namespace InterfazDeUsuario
{
    public partial class FormHotel : Form
    {
        private Button[] arrayText;
        private Button[] arrayTextTur;
        private Button[] arrayTextRem;
        private Button tb;
        

        //private GroupBox gp1;
        public FormHotel()
        {
            InitializeComponent();
        }

        private void FormHotel_FormClosed(object sender, FormClosedEventArgs e)
        {
            Principal.ActivarDesactivarBotones(false);
        }

        private void FormHotel_Load(object sender, EventArgs e)
        {
            CargarBotonesHab(1);
            CargarBotonesHab(2);
            CargarBotonesHab(3);
            CargarBotonesParkTurismos();
            CargarBotonesParkRemolques();
        }

        public void CargarBotonesHab(int p)
        {
            CADHabitaciones hab = new CADHabitaciones();
            DataSet ocupada = new DataSet();

            //Creacion de botones
            int anch;
            int alt;
            int i = 0;
            int j = 0;
            int k = 0;
            int x = 0;
            int y = 0;
            if (p == 1)
            {
                anch = alt = 30;
                x = 90;
                y = 30;
                k = 0;
                i = 20;
                arrayText = new Button[20];

            }
            else if (p == 2)
            {
                anch = alt = 30;
                x = 90;
                y = 30;
                k = 0;
                i = 20;
                arrayText = new Button[20];

            }
            else
            {
                anch = alt = 50;
                x = 90;
                y = 35;
                k = 0;
                i = 2;

                arrayText = new Button[2];
            }

            for (int m = 0; m < i; m++)
            {
                tb = new Button();
                //tb.Multiline = true;
                tb.Location = new Point(x + k, y + j);
                tb.Size = new Size(anch, alt);
                if (p == 1 || p == 2)
                    k += 35;
                else
                    k += 150;

                //Comprobamos si la habitacion esta ocupada i le cambiamos el color de fondo al textbox

                ocupada = hab.consultarDisponibilidadHabCAD(m + 1, p);
                if (ocupada.Tables[0].Rows.Count > 0)
                {
                    tb.BackColor = System.Drawing.Color.Red;

                }
                else
                    tb.BackColor = System.Drawing.Color.Lime;

                //saltamos a la fila 2
                if (m == 9)
                {
                    j = 40;
                    k = 0;
                }
                if (p == 1)
                {
                    tb.Name = "BInd" + p + m;
                    m = m + 1;
                    tb.Text = "" + m;
                    m = m - 1;
                    arrayText[m] = tb;
                    gb1.Controls.Add(arrayText[m]);
                    

                }
                else if (p == 2)
                {
                    tb.Name = "BDoble" + p + m;
                    m = m + 1;
                    tb.Text = "" + m;
                    m = m - 1;
                    arrayText[m] = tb;
                    gbPlanta2.Controls.Add(arrayText[m]);
                    
                }
                else
                {
                    tb.Name = "BSuite" + p + m;
                    m = m + 1;
                    tb.Text = "" + m;
                    m = m - 1;
                    arrayText[m] = tb;
                    gbPlanta3.Controls.Add(arrayText[m]);
                    
                }

                //arrayText[m].Click += new EventHandler(FormHotel_Click);
                
            }
        }
        /*void FormHotel_Click(object sender, EventArgs e)
        {
            bool parar = false;
            for (int i = 0; i < arrayText.Length && !parar; i++)
            {
                if (arrayText[i] != null)
                {
                    if (arrayText[i].Capture)
                    {
                        parar = true;
                        numH = int.Parse(arrayText[i].Text);
                        tbInabilitar.Text = "hola";

                    }
                }
            }
        }*/
        /*void FormReservas_Click(object sender, EventArgs e)
        {
            bool parar = false;
            for (int l = 0; l < arrayhabs.Length && !parar; l++)
            {
                if (arrayhabs[l] != null)
                {
                    if (arrayhabs[l].Capture)
                    {
                        parar = true;
                        numH = int.Parse(arrayhabs[l].Text);
                        MessageBox.Show(arrayhabs[l].Text);
                        arrayhabs[l].BackColor = System.Drawing.Color.Aqua;
                        //buthab.BackColor = System.Drawing.Color.Lime;
                    }
                }
            }
        }*/
        public void CargarBotonesParkTurismos()
        {
            int anch, alt;
            int i = 0;
            int j = 0;
            int k = 0;
            int x = 0;
            int y = 0;

            CADParking park = new CADParking();
            DataSet plazas = new DataSet();




            //propiedades de los textbox de los turismos
            anch = alt = 30;
            x = 90;
            y = 30;
            k = 0;
            i = 20;
            arrayTextTur = new Button[20];

            for (int m = 0; m < i; m++)
            {
                tb = new Button();
                //tb.Multiline = true;
                tb.Location = new Point(x + k, y + j);
                tb.Size = new Size(anch, alt);
                k += 35;

                if (m == 9)
                {
                    k = 0;
                    j += 40;
                }

                //Pintamos el textbox
                plazas = park.comprobarOcupaciónPlazaTur(m + 1);
                if (plazas.Tables[0].Rows.Count > 0)
                {
                    tb.BackColor = System.Drawing.Color.Red;

                }
                else
                    tb.BackColor = System.Drawing.Color.Lime;


                tb.Name = "BTur" + m;
                m = m + 1;
                tb.Text = "P" + m;
                m = m - 1;
                arrayTextTur[m] = tb;
                gbParkingTurismos.Controls.Add(arrayTextTur[m]);
            }


        }

        public void CargarBotonesParkRemolques()
        {
            int anch, alt;
            int i = 0;
            int j = 0;
            int k = 0;
            int x = 0;
            int y = 0;

            CADParking park = new CADParking();
            DataSet plazas = new DataSet();




            //propiedades de los textbox de los turismos
            anch = alt = 40;
            x = 95;
            y = 20;
            k = 0;
            i = 10;
            arrayTextRem = new Button[10];

            for (int m = 0; m < i; m++)
            {
                tb = new Button();
                //tb.Multiline = true;
                tb.Location = new Point(x + k, y + j);
                tb.Size = new Size(anch, alt);
                k += 45;

                if (m == 4)
                {
                    k = 0;
                    j += 45;
                }

                //Pintamos el textbox
                plazas = park.comprobarOcupaciónPlazaRem(m + 1);
                if (plazas.Tables[0].Rows.Count > 0)
                {
                    tb.BackColor = System.Drawing.Color.Red;

                }
                else
                    tb.BackColor = System.Drawing.Color.Lime;


                tb.Name = "BRem" + m;
                m = m + 1;
                tb.Text = "PR" + m;
                m = m - 1;
                arrayTextRem[m] = tb;
                gbParkingRemolques.Controls.Add(arrayTextRem[m]);
            }


        }

        private void btInabilitar_Click(object sender, EventArgs e)
        {
            int numP = -1;
            int numH = -1;
            CADHabitaciones hab;

            string mens1 = "";
            
            if (tbPlanta.Text == "")
            {
                mens1 += "- Introduce una planta.\n";
            }
            else
            {
                int.TryParse(tbPlanta.Text, out numP);
                if(numP < 1 ||numP >3)
                    mens1 += "-La planta introducida no es correcta \n";            
            }
            if (tbHabitacion.Text == "")
            {
                mens1 += "- Introduce una habitación. \n";
            }
            else
            {
                int.TryParse(tbHabitacion.Text, out numH);
                if(numH < 1 || numH > 20)
                    mens1 += "-La habitación introducida no es correcta \n";    

            }
            if (mens1 != "")
                MessageBox.Show(mens1, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            { 
                //Vamos a Inabilitar la habitación seleccionada
                hab = new CADHabitaciones();
                DataSet habitacion;
                habitacion = hab.consultarDisponibilidadHabCAD(numH, numP);
                if(habitacion.Tables[0].Rows.Count > 0)
                {
                    if (habitacion.Tables[0].Rows[0].ItemArray[0].ToString() == "False")
                        MessageBox.Show("No puedes Inabilitar esta habitación porque está ocupada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    hab.reservarHabitacionCAD(numH, numP);
                    hab.cambiarDisopnibilidadAOcupada(numH, numP);
                    if (numP == 1)
                        CargarBotonesHab(1);
                    else if (numP == 2)
                        CargarBotonesHab(2);
                    else
                        CargarBotonesHab(3);
                
                }
               

            }
            
        }

      

   
        

        

        


    }  
    
}

