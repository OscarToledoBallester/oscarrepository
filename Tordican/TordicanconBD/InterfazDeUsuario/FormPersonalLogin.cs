﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ENLogicaDeNegocio;
using Persistencia;

namespace InterfazDeUsuario
{
    public partial class FormPersonalLogin : Form
    {
        private string dni;
        ENPersonal p;
        ENVerificacion v;
        
        public FormPersonalLogin(string idni)
        {
            InitializeComponent();
            p = new ENPersonal();
            dni = idni;
            DataSet mostrar = p.busquedaUC(idni);
            tbUsuario.Text = mostrar.Tables[0].Rows[0].ItemArray[0].ToString().Trim();
            tbContraseña.Text = mostrar.Tables[0].Rows[0].ItemArray[1].ToString().Trim();
        }

        private void btModificar_Click(object sender, EventArgs e)
        {
            v = new ENVerificacion();
            if (tbContraseña.Text == "" || tbUsuario.Text == "" || v.noHack(tbContraseña.Text) == false || v.noHack(tbUsuario.Text) == false)
            {
                MessageBox.Show("Usuario o Contraseña sin asignar, recuerde no usar comillas ni dejar ningún campo vacio.", "Error");
            }
            else
            {
                string nusuario = tbUsuario.Text;
                string npass = tbContraseña.Text;
                p = new ENPersonal();
                DataSet d = p.busquedaPersonalUsuario(nusuario);
                if (d.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Usuario ya registrado", "Error");
                }
                else
                {
                    bool modifica = p.modificarUC(dni, nusuario, npass);
                    if (modifica == true)
                    {
                        MessageBox.Show("Usuario/Contraseña modificados", "Correcto");
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Usuario/Contraseña no se han podido modificar", "Error");
                    }
                }
            }
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            Close();
        }

 
    }
}
