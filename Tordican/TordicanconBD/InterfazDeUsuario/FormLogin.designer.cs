﻿namespace InterfazDeUsuario
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.lbUsuario = new System.Windows.Forms.Label();
            this.lbPassw = new System.Windows.Forms.Label();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.tbPassw = new System.Windows.Forms.TextBox();
            this.btEntrar = new System.Windows.Forms.Button();
            this.btSalir = new System.Windows.Forms.Button();
            this.chMcarac = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lbUsuario
            // 
            resources.ApplyResources(this.lbUsuario, "lbUsuario");
            this.lbUsuario.Name = "lbUsuario";
            // 
            // lbPassw
            // 
            resources.ApplyResources(this.lbPassw, "lbPassw");
            this.lbPassw.Name = "lbPassw";
            // 
            // tbLogin
            // 
            resources.ApplyResources(this.tbLogin, "tbLogin");
            this.tbLogin.Name = "tbLogin";
            // 
            // tbPassw
            // 
            resources.ApplyResources(this.tbPassw, "tbPassw");
            this.tbPassw.Name = "tbPassw";
            // 
            // btEntrar
            // 
            resources.ApplyResources(this.btEntrar, "btEntrar");
            this.btEntrar.Name = "btEntrar";
            this.btEntrar.UseVisualStyleBackColor = true;
            this.btEntrar.Click += new System.EventHandler(this.btEntrar_Click);
            // 
            // btSalir
            // 
            this.btSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btSalir, "btSalir");
            this.btSalir.Name = "btSalir";
            this.btSalir.UseVisualStyleBackColor = true;
            this.btSalir.Click += new System.EventHandler(this.btSalir_Click);
            // 
            // chMcarac
            // 
            resources.ApplyResources(this.chMcarac, "chMcarac");
            this.chMcarac.Name = "chMcarac";
            this.chMcarac.UseVisualStyleBackColor = true;
            this.chMcarac.CheckedChanged += new System.EventHandler(this.chMcarac_CheckedChanged);
            // 
            // FormLogin
            // 
            this.AcceptButton = this.btEntrar;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btSalir;
            this.Controls.Add(this.chMcarac);
            this.Controls.Add(this.btSalir);
            this.Controls.Add(this.btEntrar);
            this.Controls.Add(this.tbPassw);
            this.Controls.Add(this.tbLogin);
            this.Controls.Add(this.lbPassw);
            this.Controls.Add(this.lbUsuario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbUsuario;
        private System.Windows.Forms.Label lbPassw;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.TextBox tbPassw;
        private System.Windows.Forms.Button btEntrar;
        private System.Windows.Forms.Button btSalir;
        private System.Windows.Forms.CheckBox chMcarac;
    }
}