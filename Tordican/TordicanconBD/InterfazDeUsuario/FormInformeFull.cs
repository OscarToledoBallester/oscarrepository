﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Persistencia;
using CrystalDecisions.Windows.Forms;

namespace InterfazDeUsuario
{
    public partial class FormInformeFull : Form
    {

        public FormInformeFull()
        {
            InitializeComponent();
        }

        public void setInforme(object informe)
        {
            crVisor.ReportSource = informe;
            crVisor.Refresh();
        }
    }
}
