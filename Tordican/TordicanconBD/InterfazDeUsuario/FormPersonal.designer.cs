﻿namespace InterfazDeUsuario
{
    partial class FormPersonal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panelPersonalConsultar = new System.Windows.Forms.Panel();
            this.groupOpcionesDeBusqueda = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lb3 = new System.Windows.Forms.Label();
            this.cbTipoPersonal = new System.Windows.Forms.ComboBox();
            this.cbFiltroPersonal = new System.Windows.Forms.ComboBox();
            this.tbBusquedaPersonal = new System.Windows.Forms.TextBox();
            this.btPersonalBuscar = new System.Windows.Forms.Button();
            this.dataGridPersonal = new System.Windows.Forms.DataGridView();
            this.ColumnaDNIPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaNombrePersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaPrimerApellidoPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaSegundoApellidoPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaDireccionPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Provincia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaTelefonoPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaCorreoElectronicoPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaPuestoPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnaNumSeguridadSocPersonal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbPuestoPersonal = new System.Windows.Forms.GroupBox();
            this.rbEmpleadoPersonal = new System.Windows.Forms.RadioButton();
            this.rbAdministracionPersonal = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btLimpiar = new System.Windows.Forms.Button();
            this.btContraseña = new System.Windows.Forms.Button();
            this.btModificarPersonal = new System.Windows.Forms.Button();
            this.btAltaPersonal = new System.Windows.Forms.Button();
            this.btBajaPersonal = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbCorreoEPersonal = new System.Windows.Forms.TextBox();
            this.tbTelefonoPersonal = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbProvinciaPersonal = new System.Windows.Forms.TextBox();
            this.tbPaisPersonal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbDniPersonal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbDireccionPersonal = new System.Windows.Forms.TextBox();
            this.tbNumSSPersonal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbSegundoAPersonal = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbNombrePersonal = new System.Windows.Forms.TextBox();
            this.tbPrimerAPersonal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panelPersonalConsultar.SuspendLayout();
            this.groupOpcionesDeBusqueda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPersonal)).BeginInit();
            this.gbPuestoPersonal.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1046, 778);
            this.tabControl1.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.panelPersonalConsultar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1038, 752);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Gestión";
            // 
            // panelPersonalConsultar
            // 
            this.panelPersonalConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPersonalConsultar.Controls.Add(this.label4);
            this.panelPersonalConsultar.Controls.Add(this.groupOpcionesDeBusqueda);
            this.panelPersonalConsultar.Controls.Add(this.dataGridPersonal);
            this.panelPersonalConsultar.Controls.Add(this.gbPuestoPersonal);
            this.panelPersonalConsultar.Controls.Add(this.groupBox6);
            this.panelPersonalConsultar.Controls.Add(this.groupBox7);
            this.panelPersonalConsultar.Controls.Add(this.groupBox8);
            this.panelPersonalConsultar.Location = new System.Drawing.Point(81, 6);
            this.panelPersonalConsultar.Name = "panelPersonalConsultar";
            this.panelPersonalConsultar.Size = new System.Drawing.Size(855, 723);
            this.panelPersonalConsultar.TabIndex = 22;
            // 
            // groupOpcionesDeBusqueda
            // 
            this.groupOpcionesDeBusqueda.Controls.Add(this.label3);
            this.groupOpcionesDeBusqueda.Controls.Add(this.lb3);
            this.groupOpcionesDeBusqueda.Controls.Add(this.cbTipoPersonal);
            this.groupOpcionesDeBusqueda.Controls.Add(this.cbFiltroPersonal);
            this.groupOpcionesDeBusqueda.Controls.Add(this.tbBusquedaPersonal);
            this.groupOpcionesDeBusqueda.Controls.Add(this.btPersonalBuscar);
            this.groupOpcionesDeBusqueda.Location = new System.Drawing.Point(23, 10);
            this.groupOpcionesDeBusqueda.Name = "groupOpcionesDeBusqueda";
            this.groupOpcionesDeBusqueda.Size = new System.Drawing.Size(648, 54);
            this.groupOpcionesDeBusqueda.TabIndex = 46;
            this.groupOpcionesDeBusqueda.TabStop = false;
            this.groupOpcionesDeBusqueda.Text = "Opciones Búsqueda";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(162, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Filtro Tipo";
            // 
            // lb3
            // 
            this.lb3.AutoSize = true;
            this.lb3.Location = new System.Drawing.Point(291, 8);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(65, 13);
            this.lb3.TabIndex = 5;
            this.lb3.Text = "Filtro Campo";
            // 
            // cbTipoPersonal
            // 
            this.cbTipoPersonal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoPersonal.FormattingEnabled = true;
            this.cbTipoPersonal.Items.AddRange(new object[] {
            "Todos",
            "Empleado",
            "Administración"});
            this.cbTipoPersonal.Location = new System.Drawing.Point(162, 27);
            this.cbTipoPersonal.Name = "cbTipoPersonal";
            this.cbTipoPersonal.Size = new System.Drawing.Size(123, 21);
            this.cbTipoPersonal.TabIndex = 2;
            // 
            // cbFiltroPersonal
            // 
            this.cbFiltroPersonal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroPersonal.FormattingEnabled = true;
            this.cbFiltroPersonal.Items.AddRange(new object[] {
            "DNI",
            "Nombre",
            "Primer Apellido",
            "Segundo Apellido",
            "Teléfono",
            "Correo Electrónico",
            "País",
            "Provincia",
            "Tipo"});
            this.cbFiltroPersonal.Location = new System.Drawing.Point(291, 27);
            this.cbFiltroPersonal.Name = "cbFiltroPersonal";
            this.cbFiltroPersonal.Size = new System.Drawing.Size(143, 21);
            this.cbFiltroPersonal.TabIndex = 3;
            this.cbFiltroPersonal.SelectedIndexChanged += new System.EventHandler(this.cbFiltroPersonal_SelectedIndexChanged);
            // 
            // tbBusquedaPersonal
            // 
            this.tbBusquedaPersonal.Location = new System.Drawing.Point(6, 28);
            this.tbBusquedaPersonal.Name = "tbBusquedaPersonal";
            this.tbBusquedaPersonal.Size = new System.Drawing.Size(142, 20);
            this.tbBusquedaPersonal.TabIndex = 1;
            // 
            // btPersonalBuscar
            // 
            this.btPersonalBuscar.Location = new System.Drawing.Point(454, 25);
            this.btPersonalBuscar.Name = "btPersonalBuscar";
            this.btPersonalBuscar.Size = new System.Drawing.Size(187, 23);
            this.btPersonalBuscar.TabIndex = 4;
            this.btPersonalBuscar.Text = "Buscar";
            this.btPersonalBuscar.UseVisualStyleBackColor = true;
            this.btPersonalBuscar.Click += new System.EventHandler(this.btPersonalBuscar_Click_1);
            // 
            // dataGridPersonal
            // 
            this.dataGridPersonal.AllowUserToAddRows = false;
            this.dataGridPersonal.AllowUserToDeleteRows = false;
            this.dataGridPersonal.AllowUserToResizeColumns = false;
            this.dataGridPersonal.AllowUserToResizeRows = false;
            this.dataGridPersonal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPersonal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnaDNIPersonal,
            this.ColumnaNombrePersonal,
            this.ColumnaPrimerApellidoPersonal,
            this.ColumnaSegundoApellidoPersonal,
            this.ColumnaDireccionPersonal,
            this.Pais,
            this.Provincia,
            this.ColumnaTelefonoPersonal,
            this.ColumnaCorreoElectronicoPersonal,
            this.ColumnaPuestoPersonal,
            this.ColumnaNumSeguridadSocPersonal});
            this.dataGridPersonal.Location = new System.Drawing.Point(23, 383);
            this.dataGridPersonal.MultiSelect = false;
            this.dataGridPersonal.Name = "dataGridPersonal";
            this.dataGridPersonal.ReadOnly = true;
            this.dataGridPersonal.Size = new System.Drawing.Size(648, 195);
            this.dataGridPersonal.TabIndex = 45;
            this.dataGridPersonal.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridPersonal_RowHeaderMouseClick_1);
            // 
            // ColumnaDNIPersonal
            // 
            this.ColumnaDNIPersonal.HeaderText = "DNI";
            this.ColumnaDNIPersonal.Name = "ColumnaDNIPersonal";
            this.ColumnaDNIPersonal.ReadOnly = true;
            // 
            // ColumnaNombrePersonal
            // 
            this.ColumnaNombrePersonal.HeaderText = "Nombre";
            this.ColumnaNombrePersonal.Name = "ColumnaNombrePersonal";
            this.ColumnaNombrePersonal.ReadOnly = true;
            // 
            // ColumnaPrimerApellidoPersonal
            // 
            this.ColumnaPrimerApellidoPersonal.HeaderText = "Primer Apellido";
            this.ColumnaPrimerApellidoPersonal.Name = "ColumnaPrimerApellidoPersonal";
            this.ColumnaPrimerApellidoPersonal.ReadOnly = true;
            // 
            // ColumnaSegundoApellidoPersonal
            // 
            this.ColumnaSegundoApellidoPersonal.HeaderText = "Segundo Apellido";
            this.ColumnaSegundoApellidoPersonal.Name = "ColumnaSegundoApellidoPersonal";
            this.ColumnaSegundoApellidoPersonal.ReadOnly = true;
            // 
            // ColumnaDireccionPersonal
            // 
            this.ColumnaDireccionPersonal.HeaderText = "Dirección";
            this.ColumnaDireccionPersonal.Name = "ColumnaDireccionPersonal";
            this.ColumnaDireccionPersonal.ReadOnly = true;
            // 
            // Pais
            // 
            this.Pais.HeaderText = "Pais";
            this.Pais.Name = "Pais";
            this.Pais.ReadOnly = true;
            // 
            // Provincia
            // 
            this.Provincia.HeaderText = "Provincia";
            this.Provincia.Name = "Provincia";
            this.Provincia.ReadOnly = true;
            // 
            // ColumnaTelefonoPersonal
            // 
            this.ColumnaTelefonoPersonal.HeaderText = "Teléfono";
            this.ColumnaTelefonoPersonal.Name = "ColumnaTelefonoPersonal";
            this.ColumnaTelefonoPersonal.ReadOnly = true;
            // 
            // ColumnaCorreoElectronicoPersonal
            // 
            this.ColumnaCorreoElectronicoPersonal.HeaderText = "Correo Electrónico";
            this.ColumnaCorreoElectronicoPersonal.Name = "ColumnaCorreoElectronicoPersonal";
            this.ColumnaCorreoElectronicoPersonal.ReadOnly = true;
            // 
            // ColumnaPuestoPersonal
            // 
            this.ColumnaPuestoPersonal.HeaderText = "Tipo Empleado";
            this.ColumnaPuestoPersonal.Name = "ColumnaPuestoPersonal";
            this.ColumnaPuestoPersonal.ReadOnly = true;
            // 
            // ColumnaNumSeguridadSocPersonal
            // 
            this.ColumnaNumSeguridadSocPersonal.HeaderText = "Nº Seguridad Social";
            this.ColumnaNumSeguridadSocPersonal.Name = "ColumnaNumSeguridadSocPersonal";
            this.ColumnaNumSeguridadSocPersonal.ReadOnly = true;
            // 
            // gbPuestoPersonal
            // 
            this.gbPuestoPersonal.Controls.Add(this.rbEmpleadoPersonal);
            this.gbPuestoPersonal.Controls.Add(this.rbAdministracionPersonal);
            this.gbPuestoPersonal.Location = new System.Drawing.Point(249, 224);
            this.gbPuestoPersonal.Name = "gbPuestoPersonal";
            this.gbPuestoPersonal.Size = new System.Drawing.Size(193, 110);
            this.gbPuestoPersonal.TabIndex = 41;
            this.gbPuestoPersonal.TabStop = false;
            this.gbPuestoPersonal.Text = "Puesto*";
            // 
            // rbEmpleadoPersonal
            // 
            this.rbEmpleadoPersonal.AutoSize = true;
            this.rbEmpleadoPersonal.Location = new System.Drawing.Point(6, 48);
            this.rbEmpleadoPersonal.Name = "rbEmpleadoPersonal";
            this.rbEmpleadoPersonal.Size = new System.Drawing.Size(72, 17);
            this.rbEmpleadoPersonal.TabIndex = 16;
            this.rbEmpleadoPersonal.TabStop = true;
            this.rbEmpleadoPersonal.Text = "Empleado";
            this.rbEmpleadoPersonal.UseVisualStyleBackColor = true;
            // 
            // rbAdministracionPersonal
            // 
            this.rbAdministracionPersonal.AutoSize = true;
            this.rbAdministracionPersonal.Location = new System.Drawing.Point(6, 19);
            this.rbAdministracionPersonal.Name = "rbAdministracionPersonal";
            this.rbAdministracionPersonal.Size = new System.Drawing.Size(93, 17);
            this.rbAdministracionPersonal.TabIndex = 15;
            this.rbAdministracionPersonal.TabStop = true;
            this.rbAdministracionPersonal.Text = "Administración";
            this.rbAdministracionPersonal.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btLimpiar);
            this.groupBox6.Controls.Add(this.btContraseña);
            this.groupBox6.Controls.Add(this.btModificarPersonal);
            this.groupBox6.Controls.Add(this.btAltaPersonal);
            this.groupBox6.Controls.Add(this.btBajaPersonal);
            this.groupBox6.Location = new System.Drawing.Point(448, 224);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(223, 153);
            this.groupBox6.TabIndex = 40;
            this.groupBox6.TabStop = false;
            // 
            // btLimpiar
            // 
            this.btLimpiar.Location = new System.Drawing.Point(29, 127);
            this.btLimpiar.Name = "btLimpiar";
            this.btLimpiar.Size = new System.Drawing.Size(188, 23);
            this.btLimpiar.TabIndex = 21;
            this.btLimpiar.Text = "Limpiar";
            this.btLimpiar.UseVisualStyleBackColor = true;
            this.btLimpiar.Click += new System.EventHandler(this.btLimpiar_Click_1);
            // 
            // btContraseña
            // 
            this.btContraseña.Location = new System.Drawing.Point(29, 98);
            this.btContraseña.Name = "btContraseña";
            this.btContraseña.Size = new System.Drawing.Size(188, 23);
            this.btContraseña.TabIndex = 20;
            this.btContraseña.Text = "Usuario/Contraseña";
            this.btContraseña.UseVisualStyleBackColor = true;
            this.btContraseña.Click += new System.EventHandler(this.btContraseña_Click_1);
            // 
            // btModificarPersonal
            // 
            this.btModificarPersonal.Location = new System.Drawing.Point(29, 69);
            this.btModificarPersonal.Name = "btModificarPersonal";
            this.btModificarPersonal.Size = new System.Drawing.Size(188, 23);
            this.btModificarPersonal.TabIndex = 19;
            this.btModificarPersonal.Text = "Modificar";
            this.btModificarPersonal.UseVisualStyleBackColor = true;
            this.btModificarPersonal.Click += new System.EventHandler(this.btModificarPersonal_Click_1);
            // 
            // btAltaPersonal
            // 
            this.btAltaPersonal.Location = new System.Drawing.Point(29, 11);
            this.btAltaPersonal.Name = "btAltaPersonal";
            this.btAltaPersonal.Size = new System.Drawing.Size(188, 23);
            this.btAltaPersonal.TabIndex = 17;
            this.btAltaPersonal.Text = "Alta";
            this.btAltaPersonal.UseVisualStyleBackColor = true;
            this.btAltaPersonal.Click += new System.EventHandler(this.btAltaPersonal_Click);
            // 
            // btBajaPersonal
            // 
            this.btBajaPersonal.Location = new System.Drawing.Point(29, 40);
            this.btBajaPersonal.Name = "btBajaPersonal";
            this.btBajaPersonal.Size = new System.Drawing.Size(188, 23);
            this.btBajaPersonal.TabIndex = 18;
            this.btBajaPersonal.Text = "Baja";
            this.btBajaPersonal.UseVisualStyleBackColor = true;
            this.btBajaPersonal.Click += new System.EventHandler(this.btBajaPersonal_Click_1);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this.tbCorreoEPersonal);
            this.groupBox7.Controls.Add(this.tbTelefonoPersonal);
            this.groupBox7.Location = new System.Drawing.Point(23, 224);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(220, 110);
            this.groupBox7.TabIndex = 39;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Datos Contacto";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Correo Electrónico";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Teléfono*";
            // 
            // tbCorreoEPersonal
            // 
            this.tbCorreoEPersonal.Location = new System.Drawing.Point(12, 74);
            this.tbCorreoEPersonal.Name = "tbCorreoEPersonal";
            this.tbCorreoEPersonal.Size = new System.Drawing.Size(199, 20);
            this.tbCorreoEPersonal.TabIndex = 14;
            // 
            // tbTelefonoPersonal
            // 
            this.tbTelefonoPersonal.Location = new System.Drawing.Point(12, 34);
            this.tbTelefonoPersonal.Name = "tbTelefonoPersonal";
            this.tbTelefonoPersonal.Size = new System.Drawing.Size(199, 20);
            this.tbTelefonoPersonal.TabIndex = 13;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label1);
            this.groupBox8.Controls.Add(this.label2);
            this.groupBox8.Controls.Add(this.tbProvinciaPersonal);
            this.groupBox8.Controls.Add(this.tbPaisPersonal);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.tbDniPersonal);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.tbDireccionPersonal);
            this.groupBox8.Controls.Add(this.tbNumSSPersonal);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.tbSegundoAPersonal);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.tbNombrePersonal);
            this.groupBox8.Controls.Add(this.tbPrimerAPersonal);
            this.groupBox8.Location = new System.Drawing.Point(23, 66);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(648, 152);
            this.groupBox8.TabIndex = 38;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Datos Personales";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(348, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Provincia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "País";
            // 
            // tbProvinciaPersonal
            // 
            this.tbProvinciaPersonal.Location = new System.Drawing.Point(348, 115);
            this.tbProvinciaPersonal.Name = "tbProvinciaPersonal";
            this.tbProvinciaPersonal.Size = new System.Drawing.Size(294, 20);
            this.tbProvinciaPersonal.TabIndex = 12;
            // 
            // tbPaisPersonal
            // 
            this.tbPaisPersonal.Location = new System.Drawing.Point(6, 115);
            this.tbPaisPersonal.Name = "tbPaisPersonal";
            this.tbPaisPersonal.Size = new System.Drawing.Size(336, 20);
            this.tbPaisPersonal.TabIndex = 11;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "DNI*";
            // 
            // tbDniPersonal
            // 
            this.tbDniPersonal.Location = new System.Drawing.Point(6, 32);
            this.tbDniPersonal.Name = "tbDniPersonal";
            this.tbDniPersonal.Size = new System.Drawing.Size(142, 20);
            this.tbDniPersonal.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(348, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Dirección";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Nº Seguridad Social*";
            // 
            // tbDireccionPersonal
            // 
            this.tbDireccionPersonal.Location = new System.Drawing.Point(348, 73);
            this.tbDireccionPersonal.Name = "tbDireccionPersonal";
            this.tbDireccionPersonal.Size = new System.Drawing.Size(294, 20);
            this.tbDireccionPersonal.TabIndex = 10;
            // 
            // tbNumSSPersonal
            // 
            this.tbNumSSPersonal.Location = new System.Drawing.Point(6, 73);
            this.tbNumSSPersonal.Name = "tbNumSSPersonal";
            this.tbNumSSPersonal.Size = new System.Drawing.Size(336, 20);
            this.tbNumSSPersonal.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(511, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Segundo Apellido*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(375, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Primer Apellido*";
            // 
            // tbSegundoAPersonal
            // 
            this.tbSegundoAPersonal.Location = new System.Drawing.Point(514, 31);
            this.tbSegundoAPersonal.Name = "tbSegundoAPersonal";
            this.tbSegundoAPersonal.Size = new System.Drawing.Size(130, 20);
            this.tbSegundoAPersonal.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(162, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Nombre*";
            // 
            // tbNombrePersonal
            // 
            this.tbNombrePersonal.Location = new System.Drawing.Point(162, 32);
            this.tbNombrePersonal.Name = "tbNombrePersonal";
            this.tbNombrePersonal.Size = new System.Drawing.Size(207, 20);
            this.tbNombrePersonal.TabIndex = 6;
            // 
            // tbPrimerAPersonal
            // 
            this.tbPrimerAPersonal.Location = new System.Drawing.Point(378, 31);
            this.tbPrimerAPersonal.Name = "tbPrimerAPersonal";
            this.tbPrimerAPersonal.Size = new System.Drawing.Size(130, 20);
            this.tbPrimerAPersonal.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 337);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "Campo Obligatorio*";
            // 
            // FormPersonal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 778);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormPersonal";
            this.Text = "Personal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPersonal_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panelPersonalConsultar.ResumeLayout(false);
            this.panelPersonalConsultar.PerformLayout();
            this.groupOpcionesDeBusqueda.ResumeLayout(false);
            this.groupOpcionesDeBusqueda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPersonal)).EndInit();
            this.gbPuestoPersonal.ResumeLayout(false);
            this.gbPuestoPersonal.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panelPersonalConsultar;
        private System.Windows.Forms.GroupBox groupOpcionesDeBusqueda;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lb3;
        private System.Windows.Forms.ComboBox cbTipoPersonal;
        private System.Windows.Forms.ComboBox cbFiltroPersonal;
        private System.Windows.Forms.TextBox tbBusquedaPersonal;
        private System.Windows.Forms.Button btPersonalBuscar;
        private System.Windows.Forms.DataGridView dataGridPersonal;
        private System.Windows.Forms.GroupBox gbPuestoPersonal;
        private System.Windows.Forms.RadioButton rbEmpleadoPersonal;
        private System.Windows.Forms.RadioButton rbAdministracionPersonal;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btLimpiar;
        private System.Windows.Forms.Button btContraseña;
        private System.Windows.Forms.Button btModificarPersonal;
        private System.Windows.Forms.Button btAltaPersonal;
        private System.Windows.Forms.Button btBajaPersonal;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbCorreoEPersonal;
        private System.Windows.Forms.TextBox tbTelefonoPersonal;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbProvinciaPersonal;
        private System.Windows.Forms.TextBox tbPaisPersonal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbDniPersonal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbDireccionPersonal;
        private System.Windows.Forms.TextBox tbNumSSPersonal;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbSegundoAPersonal;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbNombrePersonal;
        private System.Windows.Forms.TextBox tbPrimerAPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaDNIPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaNombrePersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaPrimerApellidoPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaSegundoApellidoPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaDireccionPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pais;
        private System.Windows.Forms.DataGridViewTextBoxColumn Provincia;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaTelefonoPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaCorreoElectronicoPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaPuestoPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnaNumSeguridadSocPersonal;
        private System.Windows.Forms.Label label4;


    }
}