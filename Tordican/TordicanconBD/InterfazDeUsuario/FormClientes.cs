﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ENLogicaDeNegocio;

namespace InterfazDeUsuario
{

    public partial class FormClientes : Form
    {

        ENClientes cliente;
        public FormClientes()
        {
            InitializeComponent();
            gbClientes.Visible = true;
            dataGridClientes.Visible = true;
            rellenarDataGrid();
        }

        public FormClientes(string dni)
        {
            InitializeComponent();
            gbClientes.Visible = true;
            tbDniCliente.Text = dni;
            dataGridClientes.Visible = true;
        }

        private void FormClientes_Load(object sender, EventArgs e)
        {
            cbFiltroCliente.SelectedIndex = 0;
        }

        private void rellenarDataGrid()
        {
            gbClientes.Visible = true;
            dataGridClientes.Visible = true;

            //Borrar el contenido del dataGridClientes cada vez que los mostramos todos
            int filas = dataGridClientes.Rows.Count;
            if (dataGridClientes.RowCount > 0)
                for (int i = 0; i < filas; i++)
                {
                    dataGridClientes.Rows.Clear();
                }

            cliente = new ENClientes();
            DataSet todos;
            int numFilas;
            todos = cliente.mostrarTodos();

            numFilas = todos.Tables[0].Rows.Count;
            if (todos != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    dataGridClientes.Rows.Add(todos.Tables[0].Rows[i].ItemArray[0].ToString(), todos.Tables[0].Rows[i].ItemArray[1].ToString(), todos.Tables[0].Rows[i].ItemArray[2].ToString(), todos.Tables[0].Rows[i].ItemArray[3].ToString(), todos.Tables[0].Rows[i].ItemArray[4].ToString(), todos.Tables[0].Rows[i].ItemArray[5].ToString(), todos.Tables[0].Rows[i].ItemArray[6].ToString(), todos.Tables[0].Rows[i].ItemArray[7].ToString(), todos.Tables[0].Rows[i].ItemArray[8].ToString());

                }

            }

        }

        private void borrarTextBox()
        {
            tbDniCliente.Clear();
            tbNombreCliente.Clear();
            tbApe1Cliente.Clear();
            tbApe2Cliente.Clear();
            tbDirCliente.Clear();
            tbPaisCliente.Clear();
            tbProvinciaCliente.Clear();
            tbTlfnCliente.Clear();
            tbEmailCliente.Clear();
        }

        bool comprobarComillas(string dni, string nom, string ape1, string ape2, string dir, string pais, string prov, string tel, string email)
        {
            bool error = false;
            ENVerificacion verifico = new ENVerificacion();

            if (!verifico.noHack(dni) || !verifico.noHack(nom) || !verifico.noHack(ape1) || !verifico.noHack(ape2) || !verifico.noHack(dir) || !verifico.noHack(pais) || !verifico.noHack(prov) || !verifico.noHack(tel) || !verifico.noHack(email))
                error = true;

            return error;
        }
        bool ComprobarCampos(string dni, string nom, string ape1, string ape2, string tel, string email)
        {
            bool correcto = true;
            string mensjError = "";
            //Compruebo que nunguncampo tenga caracterees que desborden la BD
            if (comprobarComillas(dni, nom, ape1, ape2, tbDirCliente.Text, tbPaisCliente.Text, tbProvinciaCliente.Text, tel, email))
                mensjError += "Recuerda que no usar comillas en ninguno de los campos solicitados. \n";
            ENVerificacion verifico = new ENVerificacion();

            //Comprubo que el dni no sea vacio
            if (dni == "")
            {
                mensjError += "-El campo Dni no puede ser vacio \n";
            }
            else
            {
                //Verifico DNI
                if (!verifico.dni(dni))
                {
                    mensjError += "-El campo Dni no es correcto (ejemplo: 00000000T).\n";
                }
            }
            //Comprubo que el nombre no sea vacio
            if (nom == "")
            {
                mensjError += "-El campo nombre no puede ser vacio \n";
            }
            //Comprubo que el nombre no sea vacio
            if (ape1 == "")
            {
                mensjError += "-El campo Apellido no puede ser vacio \n";
            }
            
            //Verifico Email
            if (email != "")
            {
                if (!verifico.email(email))
                {
                    mensjError += "El campo Email no es correcto (formato: ejemplo@uno.com).\n";

                }
            }
            //Verifico Telefono
            if (tel != "")
            {
                if (!verifico.telefono(tel))
                {
                    mensjError += "El campo Telefono no es correcto (formato: 9 dígitos).\n";

                }
            }

            if (mensjError != "")
            {
                MessageBox.Show(mensjError, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                correcto = false;
            }

            return correcto;

        }

        private void btAltaCliente_Click(object sender, EventArgs e)
        {
            cliente = new ENClientes();

            bool camposCorrectos;

            camposCorrectos = ComprobarCampos(tbDniCliente.Text.Trim(), tbNombreCliente.Text.Trim(), tbApe1Cliente.Text.Trim(), tbApe2Cliente.Text.Trim(), tbTlfnCliente.Text.Trim(), tbEmailCliente.Text.Trim());



            if (camposCorrectos)
            {
                //Doy de alta el cliente

                if (cliente.nuevoCliente(tbDniCliente.Text.Trim(), tbNombreCliente.Text.Trim(), tbApe1Cliente.Text.Trim(), tbApe2Cliente.Text.Trim(), tbDirCliente.Text.Trim(), tbPaisCliente.Text.Trim(), tbProvinciaCliente.Text.Trim(), tbTlfnCliente.Text.Trim(), tbEmailCliente.Text.Trim(), "", ""))
                {

                    dataGridClientes.Rows.Clear();
                    //Mostraremos la nueva alta en el datagrid
                    rellenarDataGrid();


                    MessageBox.Show("Cliente insertado correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tbDniCliente.Clear();
                    tbNombreCliente.Clear();
                    tbApe1Cliente.Clear();
                    tbApe2Cliente.Clear();
                    tbDirCliente.Clear();
                    tbPaisCliente.Clear();
                    tbProvinciaCliente.Clear();
                    tbTlfnCliente.Clear();
                    tbEmailCliente.Clear();



                }
                else
                {
                    MessageBox.Show("El Cliente ya pertenece a nuestra Base de Datos", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }


        }

        private void btBuscarCliente_Click(object sender, EventArgs e)
        {
            ENVerificacion verif = new ENVerificacion();
            //Borrar el contenido del dataGridClientes cada vez que hacemos una busqueda
            dataGridClientes.Rows.Clear();


            DataSet mostrar;
            int numFilas;
            cliente = new ENClientes();
            string busqueda = "";
            if (tbBusquedaCliente.Text == "")
            {
                MessageBox.Show("Introduce una ocurrencia de busqueda", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rellenarDataGrid();
            }
            else
            {
                if (!verif.noHack(tbBusquedaCliente.Text.Trim()))
                {
                    MessageBox.Show("Recuerda que no usar comillas en ninguno de los campos solicitados.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    rellenarDataGrid();
                }
                else
                    busqueda = cbFiltroCliente.Text;
                //Buscar por Criterio de Busqueda
                if (busqueda == "Dni")
                {
                    if (verif.dni(tbBusquedaCliente.Text.Trim()))
                    {
                        mostrar = cliente.obtenerClientePorDni(tbBusquedaCliente.Text.Trim());
                        numFilas = mostrar.Tables[0].Rows.Count;
                        if (mostrar != null && numFilas > 0)
                        {
                            for (int i = 0; i < numFilas; i++)
                            {
                                dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());

                                //Como solo nos muestra un cliente mostraremos tambien los datos en el textbox
                                tbDniCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[0].ToString();
                                tbNombreCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[1].ToString();
                                tbApe1Cliente.Text = mostrar.Tables[0].Rows[i].ItemArray[2].ToString();
                                tbApe2Cliente.Text = mostrar.Tables[0].Rows[i].ItemArray[3].ToString();
                                tbDirCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[4].ToString();
                                tbPaisCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[5].ToString();
                                tbProvinciaCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[6].ToString();
                                tbTlfnCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[7].ToString();
                                tbEmailCliente.Text = mostrar.Tables[0].Rows[i].ItemArray[8].ToString();

                            }
                            rellenarDataGrid();
                        }
                        else
                        {
                            MessageBox.Show("No existe ningún Cliente con el Dni introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            rellenarDataGrid();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El DNI introducido es incorrecto (formato: 00000000T).", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }

                }
                if (busqueda == "Nombre")
                {
                    mostrar = cliente.obtenerClientePorNombre(tbBusquedaCliente.Text);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (mostrar != null && numFilas > 0)
                    {
                        for (int i = 0; i < numFilas; i++)
                        {
                            dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                            //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                        borrarTextBox();
                    }
                    else
                    {
                        MessageBox.Show("No existe ningún Cliente con el Nombre introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "Primer Apellido")
                {
                    mostrar = cliente.obtenerClientePorApe1(tbBusquedaCliente.Text);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (mostrar != null && numFilas > 0)
                    {
                        for (int i = 0; i < numFilas; i++)
                        {
                            dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                            //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                        borrarTextBox();
                    }
                    else
                    {
                        MessageBox.Show("No existe ningún Cliente con el Primer Apellido introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "Segundo Apellido")
                {
                    mostrar = cliente.obtenerClientePorApe2(tbBusquedaCliente.Text);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (mostrar != null && numFilas > 0)
                    {
                        for (int i = 0; i < numFilas; i++)
                        {
                            dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                            //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                        borrarTextBox();
                    }
                    else
                    {
                        MessageBox.Show("No existe ningún Cliente con el Segundo Apellido introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "Dirección")
                {
                    mostrar = cliente.obtenerClientePorDireccion(tbBusquedaCliente.Text);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (mostrar != null && numFilas > 0)
                    {
                        for (int i = 0; i < numFilas; i++)
                        {
                            dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                            //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                        borrarTextBox();
                    }
                    else
                    {
                        MessageBox.Show("No existe ningún Cliente con la Dirección introducida", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "País")
                {
                    mostrar = cliente.obtenerClientePorPais(tbBusquedaCliente.Text);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (mostrar != null && numFilas > 0)
                    {
                        for (int i = 0; i < numFilas; i++)
                        {
                            dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                            //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                        borrarTextBox();
                    }
                    else
                    {
                        MessageBox.Show("No existe ningún Cliente con el País introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "Provincia")
                {
                    mostrar = cliente.obtenerClientePorProvincia(tbBusquedaCliente.Text);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (mostrar != null && numFilas > 0)
                    {
                        for (int i = 0; i < numFilas; i++)
                        {
                            dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                            //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                        }
                        borrarTextBox();
                    }
                    else
                    {
                        MessageBox.Show("No existe ningún Cliente de la Provincia introducida", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "Teléfono")
                {
                    if (verif.telefono(tbBusquedaCliente.Text.Trim()))
                    {
                        mostrar = cliente.obtenerClientePorTelefono(tbBusquedaCliente.Text);
                        numFilas = mostrar.Tables[0].Rows.Count;
                        if (mostrar != null && numFilas > 0)
                        {
                            for (int i = 0; i < numFilas; i++)
                            {
                                dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                                //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                            }
                            borrarTextBox();
                        }
                        else
                        {
                            MessageBox.Show("No existe ningún Cliente con el Teléfono introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            rellenarDataGrid();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El Teléfono introducido no es correcto (formato: 9 dígitos)", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
                if (busqueda == "Correo Electrónico")
                {
                    if (verif.email(tbBusquedaCliente.Text.Trim()))
                    {
                        mostrar = cliente.obtenerClientePorEmail(tbBusquedaCliente.Text);
                        numFilas = mostrar.Tables[0].Rows.Count;
                        if (mostrar != null && numFilas > 0)
                        {
                            for (int i = 0; i < numFilas; i++)
                            {
                                dataGridClientes.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString());
                                //MessageBox.Show(mostrar.Tables[0].Rows[i].ItemArray[0].ToString());
                            }
                            borrarTextBox();
                        }
                        else
                        {
                            MessageBox.Show("No existe ningún Cliente con Correo Electrónico introducido", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            rellenarDataGrid();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El Email introducion no es correcto (formato: ejemplo@uno.com)", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rellenarDataGrid();
                    }
                }
            }
        }

        private void btBajaCliente_Click(object sender, EventArgs e)
        {
            cliente = new ENClientes();
            ENVerificacion ver = new ENVerificacion();
            //bool res = false;

            if (tbDniCliente.Text != "" && ver.dni(tbDniCliente.Text.Trim()))
            {
                if (cliente.obtenerClientePorDni(tbDniCliente.Text.Trim()).Tables[0].Rows.Count == 0)
                    MessageBox.Show("El cliente " + tbDniCliente.Text + " no pertenece a nuestra Base de Datos", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    if (MessageBox.Show("Esta seguro que quiere eliminar el Cliente " + tbDniCliente.Text, "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                    {
                        if (cliente.eliminarCliente(tbDniCliente.Text))
                        {
                            MessageBox.Show("Cliente " + tbDniCliente.Text + " eliminado correctamente", "", MessageBoxButtons.OK);
                            tbBusquedaCliente.Clear();
                            tbDniCliente.Clear();
                            tbNombreCliente.Clear();
                            tbApe1Cliente.Clear();
                            tbApe2Cliente.Clear();
                            tbDirCliente.Clear();
                            tbPaisCliente.Clear();
                            tbProvinciaCliente.Clear();
                            tbTlfnCliente.Clear();
                            tbEmailCliente.Clear();

                            rellenarDataGrid();
                        }
                        else
                        {
                            MessageBox.Show("El cliente " + tbDniCliente.Text + " no se puede eliminar porque tiene alguna reserva realizada", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }  
                
                }



               

            }
            else
            {
                MessageBox.Show("Introduce un DNI correcto (formato: 00000000T)", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rellenarDataGrid();
            }

            


        }

        private void dataGridClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridClientes_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dataGridClientes.Rows)
            {
                if (fila.Selected == true)
                {
                    tbDniCliente.Text = fila.Cells[0].Value.ToString();
                    tbNombreCliente.Text = fila.Cells[1].Value.ToString();
                    tbApe1Cliente.Text = fila.Cells[2].Value.ToString();
                    tbApe2Cliente.Text = fila.Cells[3].Value.ToString();
                    tbDirCliente.Text = fila.Cells[4].Value.ToString();
                    tbPaisCliente.Text = fila.Cells[5].Value.ToString();
                    tbProvinciaCliente.Text = fila.Cells[6].Value.ToString();
                    tbTlfnCliente.Text = fila.Cells[7].Value.ToString();
                    tbEmailCliente.Text = fila.Cells[8].Value.ToString();

                }
            }
        }

        private void btModificarCliente_Click(object sender, EventArgs e)
        {
            cliente = new ENClientes();
            bool camposCorrectos;

            camposCorrectos = ComprobarCampos(tbDniCliente.Text.Trim(), tbNombreCliente.Text.Trim(), tbApe1Cliente.Text.Trim(), tbApe2Cliente.Text.Trim(), tbTlfnCliente.Text.Trim(), tbEmailCliente.Text.Trim());

            if (camposCorrectos)//Modifico el cliente
            {
                if (MessageBox.Show("Esta seguro que quiere modificar los datos del Cliente " + tbDniCliente.Text, "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    if (cliente.actualizarCliente(tbDniCliente.Text.Trim(), tbNombreCliente.Text.Trim(), tbApe1Cliente.Text.Trim(), tbApe2Cliente.Text.Trim(), tbDirCliente.Text.Trim(), tbPaisCliente.Text.Trim(), tbProvinciaCliente.Text.Trim(), tbTlfnCliente.Text.Trim(), tbEmailCliente.Text.Trim()))
                    {

                        dataGridClientes.Rows.Clear();
                        //Mostraremos la nueva alta en el datagrid
                        rellenarDataGrid();


                        MessageBox.Show("Cliente se ha modificado correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        /*tbDniCliente.Clear();
                          tbNombreCliente.Clear();
                          tbApe1Cliente.Clear();
                          tbApe2Cliente.Clear();
                          tbDirCliente.Clear();
                          tbPaisCliente.Clear();
                          tbProvinciaCliente.Clear();
                          tbTlfnCliente.Clear();
                          tbEmailCliente.Clear();*/



                    }
                    else
                    {
                        MessageBox.Show("El Cliente con este dni ya pertenece a nuestra Base de Datos", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }

        }

        private void FormClientes_FormClosed(object sender, FormClosedEventArgs e)
        {
            Principal.ActivarDesactivarBotones(false);
            Principal.formClientes = null;

        }

        private void btSalirCli_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btLimpiar_Click(object sender, EventArgs e)
        {
            tbDniCliente.Clear();
            tbNombreCliente.Clear();
            tbApe1Cliente.Clear();
            tbApe2Cliente.Clear();
            tbDirCliente.Clear();
            tbPaisCliente.Clear();
            tbProvinciaCliente.Clear();
            tbTlfnCliente.Clear();
            tbEmailCliente.Clear();
            tbBusquedaCliente.Clear();
            rellenarDataGrid();



        }
    }
}


