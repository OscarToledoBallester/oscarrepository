﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ENLogicaDeNegocio;
using Persistencia;

namespace InterfazDeUsuario
{
    public partial class FormReservas : Form
    {
        private enum Actividad {Quad = 1,Bicicleta = 2,Paseo = 3,Tiro = 4,Piraguismo = 5,Rapel = 6,Parque = 7,Localidades = 8};
        private ENClientes cl;
        private ENReservas re;
        private ENVerificacion verificar;
        private int fechas = 0;
        
        //Si es true ha elegido la de ingreso,false la de salida
        //Para Actividades 
        public static string actividad;
        public static int idAct;
        //Para reservas
        private Button buthab;
        private Button[] arrayhabs;
        private GroupBox gbPlanta;
        private int numP;
        private int numH;
        private int rs;
        private bool dado = false;
        private bool unaHab = false;
        private DateTime feliminar = DateTime.Today;
        private int diasEliminar = 0;
        private bool cambioPlanta = false;
        //Para eliminar Reservas caza
        private int cazaElimina = 0;

        public FormReservas()
        {
            InitializeComponent();
            gbPlanta = new GroupBox();
            gbPlanta.Location = new Point(64, 257);
            gbPlanta.Size = new Size(727, 182);
            this.Controls.Add(gbPlanta);
        }

        private void FormReservas_Load(object sender, EventArgs e)
        {
            dataGridReservas.Visible = false;
            gbPlanta.Visible = false;
            lbLibres.Visible = false;
            lbOcupadas.Visible = false;
            tblibre.Visible = false;
            tbOcupadas.Visible = false;
            tbTotal.Visible = false;
        }

        private void btElegirRes_Click(object sender, EventArgs e)
        {
            ElegirMes.Visible = true;
            fechas = 0;
        }

        private void btFechaIng_Click(object sender, EventArgs e)
        {
            ElegirMes.Visible = true;
            fechas = 1;
        }

        private void btFechaSal_Click(object sender, EventArgs e)
        {
            ElegirMes.Visible = true;
            fechas = 2;
        }

        private void ElegirMes_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (fechas == 0)
            {
                tbFechaRes.Text = ElegirMes.SelectionEnd.ToString();
            }
            else if (fechas == 1)
            {
                tbFechaIng.Text = ElegirMes.SelectionEnd.ToString();
            }
            else
            {
                tbFechaSal.Text = ElegirMes.SelectionEnd.ToString();
            }
            
            ElegirMes.Visible = false;
        }

        private void LimpiarTodosLosCampos()
        {
            tbNif.Clear();
            tbNombre.Clear();
            tbApellidos.Clear();
            tbFechaRes.Clear();
            tbFechaIng.Clear();
            tbFechaSal.Clear();
            cbHabitacion.Text = "";
            cbPension.Text = "";
            gbPlanta.Visible = false;
            tblibre.Visible = false;
            tbOcupadas.Visible = false;
            lbLibres.Visible = false;
            lbOcupadas.Visible = false;
            dataGridReservas.Rows.Clear();
            dataGridReservas.ClearSelection();
            dataGridReservas.Visible = false;
            dgvCaza.Rows.Clear();
            dgvCaza.ClearSelection();
            panelCaza.Visible = false;
            tbTotal.Visible = false;
            unaHab = false;
            ElegirMes.Visible = false;
            cbPlazaParking.Checked = false;
            btNueva.Enabled = true;
            btActividades.Enabled = true;
            tbResCaza.Enabled = true;
            btBuscar.Enabled = true;
            BorrarArrayDeBotones();           
            tbResCaza.Enabled = true;
            
        }

        private void BorrarArrayDeBotones()
        {
            if (arrayhabs != null)
            {
                for (int i = 0; i < arrayhabs.Length; i++)
                {
                    arrayhabs[i].Dispose();
                }
                arrayhabs = null;
            }
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            LimpiarTodosLosCampos();
        }

        private void btNueva_Click(object sender, EventArgs e)
        {
            bool nueva;
            bool aParking = false;
            if (tbNif.Text == "")
            {
                MessageBox.Show("El campo DNI es obligatorio", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                if (tbFechaRes.Text.Length == 6 || tbFechaIng.Text.Length ==  6 || tbFechaSal.Text.Length == 6)
                {
                    MessageBox.Show("Fechas Incorrectas", "ERROR!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    re = new ENReservas();
                    try
                    {

                        if (cbPlazaParking.Checked == true)
                            aParking = true;
                        nueva = re.NuevaReserva(DateTime.Parse(tbFechaRes.Text), DateTime.Parse(tbFechaIng.Text), DateTime.Parse(tbFechaSal.Text), tbNif.Text, actividad, idAct, numH, numP, aParking);
                        if (nueva)
                        {
                            DespuesDeReserva(numH, true);
                            MessageBox.Show("Reserva realizada satisfactoriamente", "BIEN!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LimpiarTodosLosCampos();
                        }
                        else
                            DespuesDeReserva(numH, false);
                    }
                    catch (ModelException exc)
                    {
                        MessageBox.Show(exc.Message, "ERROR!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btModificar_Click(object sender, EventArgs e)
        {
            if (tbNif.Text != "")
            {
                if (verificar.dni(tbNif.Text))
                {
                    if (dataGridReservas.Rows.Count < 1)
                    {
                       
                        MessageBox.Show("Eliga una fila para hacer Cambios", "Advertencia!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("DNI INCORRECTO", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe introducir un DNI para modificar la reserva", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void btElimiar_Click(object sender, EventArgs e)
        {
            if (btElimiar.Text == "Eliminar Caza")
            {
                if (cazaElimina == 0)
                    MessageBox.Show("Antes debe seleccionar la reserva de caza que quiere eliminar","ATENCION!!!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                else
                {
                    if (MessageBox.Show("¿Esta seguro de eliminar esta reserva?", "ATENCION!!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        EliminarCaza();
                }
            }
            else
            {
                re = new ENReservas();
                bool borra = false;
                if (tbNif.Text != "")
                {
                    if (verificar.dni(tbNif.Text))
                    {
                        if (dataGridReservas.Visible == false)
                        {
                            dataGridReservas.Visible = true;
                            CargarResevasCliente(tbNif.Text, "");
                        }

                        if (dataGridReservas.Rows.Count >= 1)
                        {
                            if (!dado)
                            {
                                borra = false;
                                MessageBox.Show("Eliga una fila", "Advertencia!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                borra = true;
                            }
                        }
                        else
                        {
                            rs = numP = numH = diasEliminar = -1;
                            borra = true;
                        }

                        if (borra)
                        {
                            try
                            {
                                if (re.EliminarReserva(tbNif.Text, numP, numH, rs, feliminar, diasEliminar))
                                {
                                    MessageBox.Show("Reserva Eliminada correctamente");
                                    if (dataGridReservas.Rows.Count > 0)
                                    {
                                        dataGridReservas.ClearSelection();
                                        dataGridReservas.Rows.Clear();
                                        dataGridReservas.Visible = false;
                                        btNueva.Enabled = true;
                                        btBuscar.Enabled = true;
                                        btActividades.Enabled = true;
                                        tbResCaza.Enabled = true;
                                        tbTotal.Visible = false;
                                    }
                                    //LimpiarTodosLosCampos();
                                }
                            }
                            catch (ModelException exc)
                            {
                                MessageBox.Show(exc.Message, "ERROR!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("El DNI es incorrecto", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Debe introducir un DNI para eliminar la reserva", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void EliminarCaza()
        {
            re = new ENReservas();
            try
            {
                if (re.EliminarReservaCaza(cazaElimina))
                {
                    MessageBox.Show("Reserva Eliminada correctamente");
                    dgvCaza.ClearSelection();
                    panelCaza.Visible = false;
                    if (dataGridReservas.Rows.Count > 0)
                    {
                        dataGridReservas.Visible = true;
                        dgvCaza.Rows.Clear();
                    }
                    else
                        dataGridReservas.Visible = false;
                }
            }
            catch(ModelException exc)
            {
                MessageBox.Show(exc.Message,"ERROR!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            verificar = new ENVerificacion();
            re = new ENReservas();

            if (tbNif.Text == "" && tbFechaRes.Text.Length == 6 && tbFechaIng.Text.Length == 6 && tbFechaSal.Text.Length == 6)
            {
                MessageBox.Show("Para buscar debe introducir un NIF obligatoriamente y las fechas opcional", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                if (tbNif.Text != "")
                {
                    if (verificar.dni(tbNif.Text))
                    {
                        btNueva.Enabled = false;
                        btActividades.Enabled = false;
                        tbResCaza.Enabled = false;
                        try
                        {
                            if (tbFechaSal.Text.Length > 6 && tbFechaIng.Text.Length > 6 && tbFechaRes.Text.Length > 6)
                            {
                                if (re.ComprobarFechas(DateTime.Parse(tbFechaRes.Text), DateTime.Parse(tbFechaIng.Text), DateTime.Parse(tbFechaSal.Text)))
                                    CargarResevasCliente(tbNif.Text, tbFechaIng.Text);
                            }
                            else if (tbFechaSal.Text.Length > 6 || tbFechaIng.Text.Length > 6 || tbFechaRes.Text.Length > 6)
                            {
                                if (tbFechaIng.Text.Length > 6)
                                    CargarResevasCliente(tbNif.Text, tbFechaIng.Text);
                                else
                                    CargarResevasCliente(tbNif.Text, "");
                            }
                            else
                                CargarResevasCliente(tbNif.Text, "");

                            

                        }
                        catch (ModelException exc)
                        {
                            throw exc;
                        }
                    }
                    else
                        MessageBox.Show("El DNI introducido para la busqueda es erroneo","ERROR!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
        }

        private void BtBuscarDNI_Click(object sender, EventArgs e)
        {
            verificar = new ENVerificacion();
            cl = new ENClientes();
            DataSet datosCli = new DataSet();
            if (tbNif.Text != "")
            {

                if (verificar.dni(tbNif.Text))
                {
                    datosCli = cl.obtenerClientePorDni(tbNif.Text);
                    if (datosCli != null && datosCli.Tables[0].Rows.Count > 0)
                    {
                        tbNombre.Text = datosCli.Tables[0].Rows[0].ItemArray[1].ToString();
                        tbApellidos.Text = datosCli.Tables[0].Rows[0].ItemArray[2].ToString() + ' ' + datosCli.Tables[0].Rows[0].ItemArray[3].ToString();
                    }
                    else
                    {
                        if ((MessageBox.Show("El DNI no existe,¿DESEA DARLO DE ALTA?", "AVISO!!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.Yes))
                        {
                            FormClientes fcl = new FormClientes(tbNif.Text);
                            fcl.MdiParent = this.MdiParent;
                            fcl.Show();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("EL DNI ES ERRONEO","ERROR!!",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    tbNif.Clear();
                }
            }
            else
            {
                MessageBox.Show("Debe introducir un DNI", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void CargarResevasCliente(string nif,string fecha)
        {
            CADReserva cargo = new CADReserva();
            CADParking par = new CADParking();
            DataSet re = new DataSet();
            
            string act = "";
            int k = 0;
            int id = -1;
            float preciAct = 0;
            float prHab = 0;
            string total = "";
            int idenum = 0;
            float parking = 0;

            float todasRes = 0;

            

            re = cargo.ConsultarReservas(nif,fecha);
            if (re.Tables[0].Rows.Count > 0)
            {
                if (dataGridReservas.Visible == false)
                {
                    gbPlanta.Visible = false;
                    dataGridReservas.Visible = true;
                    btBuscar.Enabled = false;

                }
                for (int i = 0; i < re.Tables[0].Rows.Count; i++)
                {
                    for (int j = 0; j < re.Tables[0].Columns.Count; j++)
                    {
                        if (j >= 6)
                        {
                            if (re.Tables[0].Rows[i].ItemArray[j].ToString() == "")
                            {
                                k++;
                            }
                            else
                            {
                                idenum = k;
                                id = int.Parse(re.Tables[0].Rows[i].ItemArray[j].ToString());
                            }
                        }
                    }
                    prHab = float.Parse(re.Tables[0].Rows[i].ItemArray[4].ToString()) * float.Parse(re.Tables[0].Rows[i].ItemArray[5].ToString());

                    if (id != -1)
                    {
                        act = Enum.GetName(typeof(Actividad), idenum + 1);
                        preciAct = FormActividades.DevuelvePrecioAreserva(act, id);
                    }
                    else
                    {
                        act = "";
                        preciAct = DineroCazaReserva(nif, int.Parse(re.Tables[0].Rows[i].ItemArray[4].ToString()), DateTime.Parse(re.Tables[0].Rows[i].ItemArray[1].ToString()));
                        if (preciAct != 0)
                        {
                            act = "CAZA";
                            btVerDetalleCaza.Visible = true;
                        }
                    }
                    parking = par.PrecioPlaza(int.Parse(re.Tables[0].Rows[i].ItemArray[0].ToString()),0);
                    
                    total = (Math.Round((prHab + preciAct + parking),2)).ToString() + " €";
                    dataGridReservas.Rows.Add(re.Tables[0].Rows[i].ItemArray[0].ToString(), re.Tables[0].Rows[i].ItemArray[1].ToString(), re.Tables[0].Rows[i].ItemArray[2].ToString(), re.Tables[0].Rows[i].ItemArray[3].ToString(), re.Tables[0].Rows[i].ItemArray[4].ToString(), re.Tables[0].Rows[i].ItemArray[5].ToString(), act,preciAct,parking,total);
                    todasRes += (prHab + preciAct + parking);
                    if(act == "CAZA")
                         dgvCaza.ClearSelection();
                    total = act = "";
                    idenum = k = 0;
                    preciAct = 0;
                    id = -1;
                   
                }
                tbTotal.Visible = true;
                tbTotal.Text = todasRes.ToString() + " €";
          
            }

            else 
            {
                preciAct = DineroCaza(nif, fecha);
                if (preciAct == 0)
                {
                    if (fecha != "")
                        MessageBox.Show("El Sr. D." + tbNombre.Text + " no tiene reservas para la fechar introducida", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else
                        MessageBox.Show("El Sr. D." + tbNombre.Text + " no tiene reservas", "ATENCION!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    panelCaza.Visible = true;
                    dataGridReservas.Visible = false;
                    //tbTotal.Visible = true;
                    tbTotal.Text = preciAct.ToString() + " €";
                }
            }
        }

        private float DineroCazaReserva(string nif, int dias,DateTime fec)
        {
            CADReserva cadR = new CADReserva();
            DataSet caza = new DataSet();
            CADTipoCaza tpc;
            CADArea area;
            CADPerrera perro;
            CADParking park;
            float total = 0;
            float precioA = 0;
            float precioTipo = 0;
            float precioCom = 0;
            string desCom = "";
            string descaza = "";
            float subT = 0;
            float precioPerro = 0;
            int numPerro = 0;
            float prparking;

            DateTime f2 = new DateTime();
            f2 = fec.AddDays(dias);
            caza = cadR.TieneCaza(nif, fec, f2);

            if (caza.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < caza.Tables[0].Rows.Count; i++)
                {
                    park = new CADParking();

                    tpc = new CADTipoCaza();
                    area = new CADArea();
                    perro = new CADPerrera();
                    precioA = area.CADGetPrecio(int.Parse(caza.Tables[0].Rows[i].ItemArray[2].ToString()));
                    precioTipo = tpc.CADObtenerPrecio(int.Parse(caza.Tables[0].Rows[i].ItemArray[3].ToString()));
                    descaza = tpc.CADObtenerDescripcion(int.Parse(caza.Tables[0].Rows[i].ItemArray[3].ToString()));
                    precioCom = area.CADGetPrecioTipoComida(int.Parse(caza.Tables[0].Rows[i].ItemArray[4].ToString()));
                    desCom = area.CADGetDescripcionComida(int.Parse(caza.Tables[0].Rows[i].ItemArray[4].ToString()));
                    numPerro = cadR.Perrera(int.Parse(caza.Tables[0].Rows[i].ItemArray[0].ToString()));
                    if(numPerro > 0) precioPerro = perro.CADObtenerPrecioPerreras(numPerro);
                    prparking = park.PrecioPlaza(int.Parse(caza.Tables[0].Rows[i].ItemArray[0].ToString()), 1);
                    subT = precioA + precioTipo + precioCom + precioPerro;
                    dgvCaza.Rows.Add(caza.Tables[0].Rows[i].ItemArray[0].ToString(), caza.Tables[0].Rows[i].ItemArray[1].ToString(), caza.Tables[0].Rows[i].ItemArray[2].ToString(),precioA, descaza,precioTipo,desCom,precioCom, caza.Tables[0].Rows[i].ItemArray[5].ToString(),precioPerro,prparking,subT);
                    total += subT;
                    precioA = precioCom = precioTipo = precioPerro = subT = prparking= 0;
                    descaza = desCom = "";
                }
                tbSubCaza.Text = total.ToString() + " €";
                
            }
            return total;
        }

        private float DineroCaza(string nif, string fec)
        {
            CADReserva cadR = new CADReserva();
            DataSet caza = new DataSet();
            CADTipoCaza tpc;
            CADArea area;
            CADPerrera perro;
            CADParking park;

            float total = 0;
            float precioA = 0;
            float precioTipo = 0;
            float precioCom = 0;
            string desCom = "";
            string descaza = "";
            float subT = 0;
            float precioPerro = 0;
            int numPerro = 0;
            float prparking = 0;

            caza = cadR.CazaDNI(nif,fec);

            if (caza.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < caza.Tables[0].Rows.Count; i++)
                {
                    tpc = new CADTipoCaza();
                    area = new CADArea();
                    perro = new CADPerrera();
                    park = new CADParking();
                    precioA = area.CADGetPrecio(int.Parse(caza.Tables[0].Rows[i].ItemArray[2].ToString()));
                    precioTipo = tpc.CADObtenerPrecio(int.Parse(caza.Tables[0].Rows[i].ItemArray[3].ToString()));
                    descaza = tpc.CADObtenerDescripcion(int.Parse(caza.Tables[0].Rows[i].ItemArray[3].ToString()));
                    precioCom = area.CADGetPrecioTipoComida(int.Parse(caza.Tables[0].Rows[i].ItemArray[4].ToString()));
                    desCom = area.CADGetDescripcionComida(int.Parse(caza.Tables[0].Rows[i].ItemArray[4].ToString()));
                    numPerro = cadR.Perrera(int.Parse(caza.Tables[0].Rows[i].ItemArray[0].ToString()));
                    if (numPerro > 0) precioPerro = perro.CADObtenerPrecioPerreras(numPerro);
                    prparking = park.PrecioPlaza(int.Parse(caza.Tables[0].Rows[i].ItemArray[0].ToString()), 1);
                    subT = precioA + precioTipo + precioCom + precioPerro;
                    dgvCaza.Rows.Add(caza.Tables[0].Rows[i].ItemArray[0].ToString(), caza.Tables[0].Rows[i].ItemArray[1].ToString(), caza.Tables[0].Rows[i].ItemArray[2].ToString(), precioA, descaza, precioTipo, desCom, precioCom, caza.Tables[0].Rows[i].ItemArray[5].ToString(), precioPerro, prparking,subT);
                    total += subT;
                    precioA = precioCom = precioTipo = precioPerro = prparking =subT = 0;
                    descaza = desCom = "";
                }
                tbSubCaza.Text = total.ToString() + " €";
            }
            return total;
        }

   

        private void btActividades_Click(object sender, EventArgs e)
        {
            if(tbNif.Text == "")
                MessageBox.Show("Debe introducir un DNI antes", "ADVERTENCIA!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            else if (!rbQuad.Checked && !rbBici.Checked && !rbPasCab.Checked && !rbTiro.Checked && !rbPirag.Checked && !rbRapel.Checked && !rbParque.Checked && !rbLocalidad.Checked)
                MessageBox.Show("Debes elegir una actividad","ERROR!!",MessageBoxButtons.YesNo,MessageBoxIcon.Error);
            else
            {
                tbResCaza.Enabled = false;
                FormActividades act = new FormActividades(actividad);
                act.MdiParent = this.MdiParent;
                act.Show();
            }
        }

        private void FormReservas_FormClosed(object sender, FormClosedEventArgs e)
        {
            Principal.ActivarDesactivarBotones(false);
            Principal.formReservas = null;
            
        }

        private void rbQuad_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpQuad";
        }

        private void rbBici_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpBicicleta";
        }

        private void rbPasCab_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpCaballo";
        }

        private void rbTiro_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpArco";
        }

        private void rbPirag_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpPiraguismo";
        }

        private void rbRapel_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpRapel";
        }

        private void rbParque_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpParque";
        }

        private void rbLocalidad_CheckedChanged(object sender, EventArgs e)
        {
            actividad = "tpLocalidad";
        }

        private void cbHabitacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dataGridReservas.Visible == true)
            {
                dataGridReservas.Visible = false;
                gbPlanta.Visible = true;
                btNueva.Enabled = true;
                btElimiar.Enabled = true;
                btBuscar.Enabled = true;
            }

            if (arrayhabs != null)
            {
                BorrarArrayDeBotones();
                cambioPlanta = true;
            }

            int j = 0;
            int k = 0;   
            int l = 1;
            int nha = 2;
            bool disponible = false;
            gbPlanta.Visible = true;
            lbLibres.Visible = true;
            lbOcupadas.Visible = true;
            tblibre.Visible = true;
            tbOcupadas.Visible = true;


            if (cbHabitacion.SelectedIndex == 1)
            {
                gbPlanta.Text = "Planta 1";
                numP = 1;
                nha = 20;
                arrayhabs = new Button[20];
                
            }
            else if (cbHabitacion.SelectedIndex == 2)
            {
                gbPlanta.Text = "Planta 2";
                numP = 2;
                nha = 20;
                arrayhabs = new Button[20];
            }
            else if (cbHabitacion.SelectedIndex == 3)
            {
                gbPlanta.Text = "Planta 3";
                numP = 3;
                nha = 2;
                arrayhabs = new Button[2];
            }

            for (int i = 0; i < nha; i++)
            {
                l = l + i;
                buthab = new Button();
                if (numP == 1)
                    buthab.Name = "buthabInd" + l;
                else if (numP == 2)
                    buthab.Name = "buthabDoble" + l;
                else
                    buthab.Name = "buthabSuite" + l;

                if (i == 10) { k = 50; j = 0; }

                if (numP == 3)
                {
                    buthab.Location = new Point(75 + j, 40);
                    buthab.Size = new Size(200, 100);
                }
                else
                {
                    buthab.Location = new Point(15 + j, 20 + k);
                    buthab.Size = new Size(45, 35);
                }
                buthab.Text = l.ToString();

                disponible = ConsultarDisponibilidad(i + 1,numP);
                if (disponible)
                    buthab.BackColor = System.Drawing.Color.Lime;
                else
                {
                    buthab.BackColor = System.Drawing.Color.Red;
                    buthab.Enabled = false;
                }
                arrayhabs[i] = buthab;
                if (numP == 3)
                    j += 350;
                else
                    j += 70;
                gbPlanta.Controls.Add(arrayhabs[i]);
                arrayhabs[i].Click += new EventHandler(FormReservas_Click);
                l = 1;
            }
        }

        private bool ConsultarDisponibilidad(int hab,int num)
        {
            bool ok = true;
            CADHabitaciones cadhab = new CADHabitaciones();
            DataSet res = new DataSet();

            res = cadhab.consultarDisponibilidadHabCAD(hab,numP);
            if (res != null)
            {
                int nf = res.Tables[0].Rows.Count;
                if (nf > 0)
                {
                    if (bool.Parse(res.Tables[0].Rows[0].ItemArray[0].ToString()) == false)
                        ok = false;
                }
            }
            return ok;
        }

        private void DespuesDeReserva(int h, bool es)
        {
            bool parar = false;
            for (int l = 0; l < arrayhabs.Length && !parar; l++)
            {
                if (arrayhabs[l] != null)
                {
                    if (l == h)
                    {
                        parar = true;
                        if (es)
                            arrayhabs[l].BackColor = System.Drawing.Color.Red;
                        else
                            arrayhabs[l].BackColor = System.Drawing.Color.Lime;
                    }
                }
            }
        }

        void FormReservas_Click(object sender, EventArgs e)
        {
            bool parar = false;

            for (int l = 0; l < arrayhabs.Length && !parar; l++)
            {
                if (arrayhabs[l] != null)
                {
                    if (arrayhabs[l].Capture)
                    {
                         if(numH > 0 && !cambioPlanta)
                            arrayhabs[numH - 1].BackColor = System.Drawing.Color.Lime;
                        if (unaHab == false)
                        {
                            unaHab = true;
                            parar = true;
                            numH = int.Parse(arrayhabs[l].Text);
                            arrayhabs[l].BackColor = System.Drawing.Color.Aqua;
                            cambioPlanta = false;
                        }
                        else
                        {
                            unaHab = true;
                            parar = true;
                            numH = int.Parse(arrayhabs[l].Text);
                            arrayhabs[l].BackColor = System.Drawing.Color.Aqua;
                            cambioPlanta = false;
                        }
                    }
                }
            }
        }

        private void dataGridReservas_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
            foreach (DataGridViewRow fila in dataGridReservas.Rows)
            {
                if (fila.Selected == true)
                {
                    if(fila.Cells[0].Value.ToString() != "")
                    {
                        dado = true;
                        numP = int.Parse(fila.Cells[2].Value.ToString());
                        rs = int.Parse(fila.Cells[0].Value.ToString());
                        numH = int.Parse(fila.Cells[3].Value.ToString());
                        feliminar = DateTime.Parse(fila.Cells[1].Value.ToString());
                        diasEliminar = int.Parse(fila.Cells[4].Value.ToString());

                    }
                }
            }
            
        }

        private void btSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btVerDetalleCaza_Click(object sender, EventArgs e)
        {
            if (dado == false)
            {
                MessageBox.Show("Debe elegir una linea antes","AVISO!!",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else
            {
                DineroCazaReserva(tbNif.Text, diasEliminar, feliminar);
                panelCaza.Visible = true;
                btVerDetalleCaza.Visible = false;
                if (dataGridReservas.Visible == true)
                {
                    dataGridReservas.Visible = false;
                    gbPlanta.Visible = false;
                    btBuscar.Enabled = false;
                }
                btElimiar.Text = "Eliminar Caza";
            }
        }

        private void btcerrarCaza_Click(object sender, EventArgs e)
        {
            panelCaza.Visible = false;
            btVerDetalleCaza.Visible = false;
            if (dataGridReservas.Visible == false)
            {
                dataGridReservas.Visible = true;
                gbPlanta.Visible = false;
                btBuscar.Enabled = false;
            }
            btElimiar.Text = "Eliminar";
        }

        private void dgvCaza_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            cazaElimina = 0;
            foreach (DataGridViewRow fila in dgvCaza.Rows)
            {
                if (fila.Selected == true)
                {
                    if (fila.Cells[0].Value.ToString() != "")
                    {
                        cazaElimina = int.Parse(fila.Cells[0].Value.ToString());
                        if (MessageBox.Show("¿Esta seguro de eliminar esta reserva?", "ATENCION!!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                            EliminarCaza();

                    }
                }
            }
        }

    }
}
