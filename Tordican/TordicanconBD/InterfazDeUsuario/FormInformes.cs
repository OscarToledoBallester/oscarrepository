﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using ENLogicaDeNegocio;
using Persistencia;


namespace InterfazDeUsuario
{
    public partial class FormInformes : Form
    {
        public FormInformes()
        {
            // Constructor.
            InitializeComponent();

            cbActividades.SelectedIndex = 0;
            cbRevisadas.SelectedIndex = 0;
            cbCotoPiezas.SelectedIndex = 0;
        }

        private void FormInformes_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Acciones a realizar cuando se cierra el formulario.
            Principal.ActivarDesactivarBotones(false);
            Principal.formInformes = null;
        }


        /* ===================================================================================================================
         * CONTROLES DE INFORMES
         * ===================================================================================================================
         */

        /* -------------------------------------------------------------------------------------------------------------------
         * PANEL DE CLIENTES
         * -------------------------------------------------------------------------------------------------------------------
         */
        private void cbClientesEnableFechas_CheckedChanged(object sender, EventArgs e)
        {
            dtClientesDesde.Enabled = dtClientesHasta.Enabled = cbClientesEnableFechas.Checked;
        }

        private void btClientesCrear_Click(object sender, EventArgs e)
        {
            // Comprobamos que las fechas estén bien introducidas, que hasta no sea menor que desde
            if (cbClientesEnableFechas.Checked && dtClientesDesde.Value.Date > dtClientesHasta.Value.Date)
                epTimeTravel.SetError(this.dtClientesDesde, "De momento no se puede viajar en el tiempo");
            else
            {
                ENInformes en = new ENInformes();
                DataSet ds = null;
                epTimeTravel.SetError(this.dtClientesDesde, "");
                if (cbClientesEnableFechas.Checked) ds = en.ObtenerClientesPorFechas(dtClientesDesde.Value, dtClientesHasta.Value);
                else ds = en.ObtenerClientes();
                CRClientes report = new CRClientes();
                report.SetDataSource(ds);
                FormInformeFull informe = new FormInformeFull();
                informe.setInforme(report);
                informe.Show();
            }
        }


        /* -------------------------------------------------------------------------------------------------------------------
         * PANEL DE ACTIVIDADES
         * -------------------------------------------------------------------------------------------------------------------
         */
        private void rbActividadesRealizadas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbActividadesRealizadas.Checked)
            {
                gbIncidencias.Visible = false;
            }
        }

        private void rbActividadesIncidencias_CheckedChanged(object sender, EventArgs e)
        {
            if (rbActividadesIncidencias.Checked)
            {
                gbIncidencias.Visible = true;
            }
        }

        private void cbActividadesEnableFechas_CheckedChanged(object sender, EventArgs e)
        {
            if (cbActividadesEnableFechas.Checked)
            {
                dtActividadesDesde.Enabled = true;
                dtActividadesHasta.Enabled = true;
            }
            else
            {
                dtActividadesDesde.Enabled = false;
                dtActividadesHasta.Enabled = false;
            }
        }

        private void btActividadesCrear_Click(object sender, EventArgs e)
        {
            // Comprobamos que las feches estén bien introducidas, que hasta no sea menor que desde
            if (cbActividadesEnableFechas.Checked && dtActividadesDesde.Value.Date > dtActividadesHasta.Value.Date)
                epTimeTravel.SetError(this.dtActividadesDesde, "De momento no se puede viajar en el tiempo");
            else
            {
                epTimeTravel.SetError(this.dtActividadesDesde, "");
                ENInformes en = new ENInformes();
                DataSet ds = null;

                FormInformeFull informe = new FormInformeFull();

                // Informes de incidencias
                if (rbActividadesIncidencias.Checked)
                {
                    // Si se han seleccionado todos los tipos de actividades
                    if (cbActividades.SelectedIndex == 0)
                    {
                        // Si se filtra por fechas
                        if (cbActividadesEnableFechas.Checked) ds = en.ObtenerIncidenciasPorFechas(cbRevisadas.SelectedIndex, dtActividadesDesde.Value, dtActividadesHasta.Value);
                        else ds = en.ObtenerIncidencias(cbRevisadas.SelectedIndex);
                        CRIncidencias report = new CRIncidencias();
                        report.SetDataSource(ds);
                        informe.setInforme(report);
                    }
                    // Para una actividad específica cambiamos de informe
                    else
                    {
                        // Si se filtra por fechas
                        if (cbActividadesEnableFechas.Checked) ds = en.ObtenerIncidenciasporTipoyFechas(cbRevisadas.SelectedIndex, cbActividades.SelectedIndex, dtActividadesDesde.Value, dtActividadesHasta.Value);
                        else ds = en.ObtenerIncidenciasPorTipo(cbRevisadas.SelectedIndex, cbActividades.SelectedIndex);
                        CRIncidenciaTipo report = new CRIncidenciaTipo();
                        report.SetDataSource(ds);
                        informe.setInforme(report);
                    }
                }
                // Informes de reservas de actividades
                else
                {
                    switch (cbActividades.SelectedIndex)
                    {
                        // Todas
                        case 0:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividades();
                            CRActividades report = new CRActividades();
                            report.SetDataSource(ds);
                            informe.setInforme(report);
                            break;
                        // Quads
                        case 1:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesQuadPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesQuad();
                            CRActividadQuad reportQ = new CRActividadQuad();
                            reportQ.SetDataSource(ds);
                            informe.setInforme(reportQ);
                            break;
                        // Bicicleta
                        case 2:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesBiciPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesBici();
                            CRActividadBici reportB = new CRActividadBici();
                            reportB.SetDataSource(ds);
                            informe.setInforme(reportB);
                            break;
                        // Paseos a caballo
                        case 3:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesPaseoPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesPaseo();
                            CRActividadCaballo reportC = new CRActividadCaballo();
                            reportC.SetDataSource(ds);
                            informe.setInforme(reportC);
                            break;
                        // Tiro con arco
                        case 4:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesTiroPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesTiro();
                            CRActividadTiro reportA = new CRActividadTiro();
                            reportA.SetDataSource(ds);
                            informe.setInforme(reportA);
                            break;
                        // Piragüismo
                        case 5:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesPiraguaPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesPiragua();
                            CRActividadPiragua reportP = new CRActividadPiragua();
                            reportP.SetDataSource(ds);
                            informe.setInforme(reportP);
                            break;
                        // Rápel
                        case 6:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesRapelPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesRapel();
                            CRActividadRapel reportR = new CRActividadRapel();
                            reportR.SetDataSource(ds);
                            informe.setInforme(reportR);
                            break;
                        // Parque temático
                        case 7:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesParquePorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesParque();
                            CRActividadParque reportT = new CRActividadParque();
                            reportT.SetDataSource(ds);
                            informe.setInforme(reportT);
                            break;
                        // Localidad de interés
                        case 8:
                            if (cbActividadesEnableFechas.Checked) ds = en.ObtenerActividadesLocalidadPorFechas(dtActividadesDesde.Value, dtActividadesHasta.Value);
                            else ds = en.ObtenerActividadesLocalidad();
                            CRActividadLocalidad reportL = new CRActividadLocalidad();
                            reportL.SetDataSource(ds);
                            informe.setInforme(reportL);
                            break;
                    }
                }
                informe.Show();
            }
        }


        /* -------------------------------------------------------------------------------------------------------------------
         * PANEL DE ACTIVIDADES
         * -------------------------------------------------------------------------------------------------------------------
         */

        private void cbCotoEnableFechas_CheckedChanged(object sender, EventArgs e)
        {
            dtCotoDesde.Enabled = dtCotoHasta.Enabled = cbCotoEnableFechas.Checked;
        }

        private void rbCotoPiezas_CheckedChanged(object sender, EventArgs e)
        {
            gbCotoPiezas.Visible = rbCotoPiezas.Checked;
        }

        private void rbCotoAreas_CheckedChanged(object sender, EventArgs e)
        {
            gbCotoPiezas.Visible = rbCotoPiezas.Checked;
        }

        private void rbCotoReservas_CheckedChanged(object sender, EventArgs e)
        {
            gbCotoPiezas.Visible = rbCotoPiezas.Checked;
        }

        private void btCotoCrear_Click(object sender, EventArgs e)
        {
            // Comprobamos que las fechas estén bien introducidas, que hasta no sea menor que desde
            if (cbCotoEnableFechas.Checked && dtCotoDesde.Value > dtCotoHasta.Value)
                epTimeTravel.SetError(this.dtCotoDesde, "De momento no se puede viajar en el tiempo");
            else
            {
                FormInformeFull informe = new FormInformeFull();

                ENInformes en = new ENInformes();
                DataSet ds = null;
                epTimeTravel.SetError(this.dtCotoDesde, "");
                // Informe de piezas cazadas
                if (rbCotoPiezas.Checked)
                {
                    if (cbCotoEnableFechas.Checked)
                        ds = en.ObtenerPiezasPorTipoYFechas(cbCotoPiezas.SelectedIndex, dtCotoDesde.Value, dtCotoHasta.Value);
                    else
                        ds = en.ObtenerPiezasPorTipo(cbCotoPiezas.SelectedIndex);

                    CRPiezasCaza report = new CRPiezasCaza();
                    report.SetDataSource(ds.Tables[0]);
                    informe.setInforme(report);
                }
                // Informe de las veces que se ha contratado el coto
                else if (rbCotoAreas.Checked)
                {
                    if (cbCotoEnableFechas.Checked) ds = en.ObtenerReservasCotoPorFechas(dtCotoDesde.Value, dtCotoHasta.Value);
                    else ds = en.ObtenerReservasCoto();
                    CRContratoCotoArea report = new CRContratoCotoArea();
                    report.SetDataSource(ds);
                    informe.setInforme(report);
                }
                // Informe de las veces que se ha contratado por año
                else
                {
                    if (cbCotoEnableFechas.Checked) ds = en.ObtenerReservasCotoPorFechas(dtCotoDesde.Value, dtCotoHasta.Value);
                    else ds = en.ObtenerReservasCoto();
                    CRContratoCoto report = new CRContratoCoto();
                    report.SetDataSource(ds);
                    informe.setInforme(report);
                }
                informe.Show();
            }
        }
    }
}


