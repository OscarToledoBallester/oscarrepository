﻿namespace InterfazDeUsuario
{
    partial class FormInformes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.epTimeTravel = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabInformes = new System.Windows.Forms.TabControl();
            this.tabClientes = new System.Windows.Forms.TabPage();
            this.cbClientesEnableFechas = new System.Windows.Forms.CheckBox();
            this.dtClientesHasta = new System.Windows.Forms.DateTimePicker();
            this.dtClientesDesde = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btClientesCrear = new System.Windows.Forms.Button();
            this.tabActividades = new System.Windows.Forms.TabPage();
            this.gbIncidencias = new System.Windows.Forms.GroupBox();
            this.cbRevisadas = new System.Windows.Forms.ComboBox();
            this.gbActividadesAsunto = new System.Windows.Forms.GroupBox();
            this.rbActividadesIncidencias = new System.Windows.Forms.RadioButton();
            this.rbActividadesRealizadas = new System.Windows.Forms.RadioButton();
            this.btActividadesCrear = new System.Windows.Forms.Button();
            this.gbActividadesFechas = new System.Windows.Forms.GroupBox();
            this.cbActividadesEnableFechas = new System.Windows.Forms.CheckBox();
            this.dtActividadesHasta = new System.Windows.Forms.DateTimePicker();
            this.dtActividadesDesde = new System.Windows.Forms.DateTimePicker();
            this.label69 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.gbActividades = new System.Windows.Forms.GroupBox();
            this.cbActividades = new System.Windows.Forms.ComboBox();
            this.tabCoto = new System.Windows.Forms.TabPage();
            this.btCotoCrear = new System.Windows.Forms.Button();
            this.gbCotoPiezas = new System.Windows.Forms.GroupBox();
            this.cbCotoPiezas = new System.Windows.Forms.ComboBox();
            this.gbCotoFechas = new System.Windows.Forms.GroupBox();
            this.cbCotoEnableFechas = new System.Windows.Forms.CheckBox();
            this.dtCotoHasta = new System.Windows.Forms.DateTimePicker();
            this.dtCotoDesde = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gbCotoAsunto = new System.Windows.Forms.GroupBox();
            this.rbCotoReservas = new System.Windows.Forms.RadioButton();
            this.rbCotoAreas = new System.Windows.Forms.RadioButton();
            this.rbCotoPiezas = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.epTimeTravel)).BeginInit();
            this.tabInformes.SuspendLayout();
            this.tabClientes.SuspendLayout();
            this.tabActividades.SuspendLayout();
            this.gbIncidencias.SuspendLayout();
            this.gbActividadesAsunto.SuspendLayout();
            this.gbActividadesFechas.SuspendLayout();
            this.gbActividades.SuspendLayout();
            this.tabCoto.SuspendLayout();
            this.gbCotoPiezas.SuspendLayout();
            this.gbCotoFechas.SuspendLayout();
            this.gbCotoAsunto.SuspendLayout();
            this.SuspendLayout();
            // 
            // epTimeTravel
            // 
            this.epTimeTravel.ContainerControl = this;
            // 
            // tabInformes
            // 
            this.tabInformes.Controls.Add(this.tabClientes);
            this.tabInformes.Controls.Add(this.tabActividades);
            this.tabInformes.Controls.Add(this.tabCoto);
            this.tabInformes.Location = new System.Drawing.Point(10, 38);
            this.tabInformes.Name = "tabInformes";
            this.tabInformes.SelectedIndex = 0;
            this.tabInformes.Size = new System.Drawing.Size(878, 627);
            this.tabInformes.TabIndex = 13;
            // 
            // tabClientes
            // 
            this.tabClientes.Controls.Add(this.cbClientesEnableFechas);
            this.tabClientes.Controls.Add(this.dtClientesHasta);
            this.tabClientes.Controls.Add(this.dtClientesDesde);
            this.tabClientes.Controls.Add(this.label13);
            this.tabClientes.Controls.Add(this.label2);
            this.tabClientes.Controls.Add(this.btClientesCrear);
            this.tabClientes.Location = new System.Drawing.Point(4, 22);
            this.tabClientes.Name = "tabClientes";
            this.tabClientes.Padding = new System.Windows.Forms.Padding(3);
            this.tabClientes.Size = new System.Drawing.Size(870, 601);
            this.tabClientes.TabIndex = 0;
            this.tabClientes.Text = "Clientes";
            this.tabClientes.UseVisualStyleBackColor = true;
            // 
            // cbClientesEnableFechas
            // 
            this.cbClientesEnableFechas.AutoSize = true;
            this.cbClientesEnableFechas.Location = new System.Drawing.Point(340, 28);
            this.cbClientesEnableFechas.Name = "cbClientesEnableFechas";
            this.cbClientesEnableFechas.Size = new System.Drawing.Size(15, 14);
            this.cbClientesEnableFechas.TabIndex = 27;
            this.cbClientesEnableFechas.UseVisualStyleBackColor = true;
            this.cbClientesEnableFechas.CheckedChanged += new System.EventHandler(this.cbClientesEnableFechas_CheckedChanged);
            // 
            // dtClientesHasta
            // 
            this.dtClientesHasta.Enabled = false;
            this.dtClientesHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtClientesHasta.Location = new System.Drawing.Point(221, 25);
            this.dtClientesHasta.Name = "dtClientesHasta";
            this.dtClientesHasta.Size = new System.Drawing.Size(95, 20);
            this.dtClientesHasta.TabIndex = 26;
            // 
            // dtClientesDesde
            // 
            this.dtClientesDesde.Enabled = false;
            this.dtClientesDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtClientesDesde.Location = new System.Drawing.Point(66, 26);
            this.dtClientesDesde.Name = "dtClientesDesde";
            this.dtClientesDesde.Size = new System.Drawing.Size(95, 20);
            this.dtClientesDesde.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(180, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Hasta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Desde";
            // 
            // btClientesCrear
            // 
            this.btClientesCrear.Location = new System.Drawing.Point(221, 51);
            this.btClientesCrear.Name = "btClientesCrear";
            this.btClientesCrear.Size = new System.Drawing.Size(135, 26);
            this.btClientesCrear.TabIndex = 22;
            this.btClientesCrear.Text = "Generar Informe";
            this.btClientesCrear.UseVisualStyleBackColor = true;
            this.btClientesCrear.Click += new System.EventHandler(this.btClientesCrear_Click);
            // 
            // tabActividades
            // 
            this.tabActividades.Controls.Add(this.gbIncidencias);
            this.tabActividades.Controls.Add(this.gbActividadesAsunto);
            this.tabActividades.Controls.Add(this.btActividadesCrear);
            this.tabActividades.Controls.Add(this.gbActividadesFechas);
            this.tabActividades.Controls.Add(this.gbActividades);
            this.tabActividades.Location = new System.Drawing.Point(4, 22);
            this.tabActividades.Name = "tabActividades";
            this.tabActividades.Padding = new System.Windows.Forms.Padding(3);
            this.tabActividades.Size = new System.Drawing.Size(963, 736);
            this.tabActividades.TabIndex = 1;
            this.tabActividades.Text = "Actividades";
            this.tabActividades.UseVisualStyleBackColor = true;
            // 
            // gbIncidencias
            // 
            this.gbIncidencias.Controls.Add(this.cbRevisadas);
            this.gbIncidencias.Location = new System.Drawing.Point(177, 153);
            this.gbIncidencias.Name = "gbIncidencias";
            this.gbIncidencias.Size = new System.Drawing.Size(105, 51);
            this.gbIncidencias.TabIndex = 17;
            this.gbIncidencias.TabStop = false;
            this.gbIncidencias.Text = "Revisadas";
            this.gbIncidencias.Visible = false;
            // 
            // cbRevisadas
            // 
            this.cbRevisadas.FormattingEnabled = true;
            this.cbRevisadas.Items.AddRange(new object[] {
            "Todas",
            "Revisadas",
            "No revisadas"});
            this.cbRevisadas.Location = new System.Drawing.Point(6, 19);
            this.cbRevisadas.Name = "cbRevisadas";
            this.cbRevisadas.Size = new System.Drawing.Size(93, 21);
            this.cbRevisadas.TabIndex = 0;
            // 
            // gbActividadesAsunto
            // 
            this.gbActividadesAsunto.Controls.Add(this.rbActividadesIncidencias);
            this.gbActividadesAsunto.Controls.Add(this.rbActividadesRealizadas);
            this.gbActividadesAsunto.Location = new System.Drawing.Point(23, 18);
            this.gbActividadesAsunto.Name = "gbActividadesAsunto";
            this.gbActividadesAsunto.Size = new System.Drawing.Size(373, 52);
            this.gbActividadesAsunto.TabIndex = 13;
            this.gbActividadesAsunto.TabStop = false;
            this.gbActividadesAsunto.Text = "Asunto";
            // 
            // rbActividadesIncidencias
            // 
            this.rbActividadesIncidencias.AutoSize = true;
            this.rbActividadesIncidencias.Location = new System.Drawing.Point(234, 19);
            this.rbActividadesIncidencias.Name = "rbActividadesIncidencias";
            this.rbActividadesIncidencias.Size = new System.Drawing.Size(79, 17);
            this.rbActividadesIncidencias.TabIndex = 1;
            this.rbActividadesIncidencias.TabStop = true;
            this.rbActividadesIncidencias.Text = "Incidencias";
            this.rbActividadesIncidencias.UseVisualStyleBackColor = true;
            this.rbActividadesIncidencias.CheckedChanged += new System.EventHandler(this.rbActividadesIncidencias_CheckedChanged);
            // 
            // rbActividadesRealizadas
            // 
            this.rbActividadesRealizadas.AutoSize = true;
            this.rbActividadesRealizadas.Checked = true;
            this.rbActividadesRealizadas.Location = new System.Drawing.Point(16, 19);
            this.rbActividadesRealizadas.Name = "rbActividadesRealizadas";
            this.rbActividadesRealizadas.Size = new System.Drawing.Size(135, 17);
            this.rbActividadesRealizadas.TabIndex = 0;
            this.rbActividadesRealizadas.TabStop = true;
            this.rbActividadesRealizadas.Text = "Actividades Realizadas";
            this.rbActividadesRealizadas.UseVisualStyleBackColor = true;
            this.rbActividadesRealizadas.CheckedChanged += new System.EventHandler(this.rbActividadesRealizadas_CheckedChanged);
            // 
            // btActividadesCrear
            // 
            this.btActividadesCrear.Location = new System.Drawing.Point(288, 167);
            this.btActividadesCrear.Name = "btActividadesCrear";
            this.btActividadesCrear.Size = new System.Drawing.Size(108, 26);
            this.btActividadesCrear.TabIndex = 16;
            this.btActividadesCrear.Text = "Generar Informe";
            this.btActividadesCrear.UseVisualStyleBackColor = true;
            this.btActividadesCrear.Click += new System.EventHandler(this.btActividadesCrear_Click);
            // 
            // gbActividadesFechas
            // 
            this.gbActividadesFechas.Controls.Add(this.cbActividadesEnableFechas);
            this.gbActividadesFechas.Controls.Add(this.dtActividadesHasta);
            this.gbActividadesFechas.Controls.Add(this.dtActividadesDesde);
            this.gbActividadesFechas.Controls.Add(this.label69);
            this.gbActividadesFechas.Controls.Add(this.label76);
            this.gbActividadesFechas.Location = new System.Drawing.Point(23, 80);
            this.gbActividadesFechas.Name = "gbActividadesFechas";
            this.gbActividadesFechas.Size = new System.Drawing.Size(373, 67);
            this.gbActividadesFechas.TabIndex = 14;
            this.gbActividadesFechas.TabStop = false;
            this.gbActividadesFechas.Text = "Fechas";
            // 
            // cbActividadesEnableFechas
            // 
            this.cbActividadesEnableFechas.AutoSize = true;
            this.cbActividadesEnableFechas.Location = new System.Drawing.Point(337, 29);
            this.cbActividadesEnableFechas.Name = "cbActividadesEnableFechas";
            this.cbActividadesEnableFechas.Size = new System.Drawing.Size(15, 14);
            this.cbActividadesEnableFechas.TabIndex = 4;
            this.cbActividadesEnableFechas.UseVisualStyleBackColor = true;
            this.cbActividadesEnableFechas.CheckedChanged += new System.EventHandler(this.cbActividadesEnableFechas_CheckedChanged);
            // 
            // dtActividadesHasta
            // 
            this.dtActividadesHasta.Enabled = false;
            this.dtActividadesHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtActividadesHasta.Location = new System.Drawing.Point(218, 26);
            this.dtActividadesHasta.Name = "dtActividadesHasta";
            this.dtActividadesHasta.Size = new System.Drawing.Size(95, 20);
            this.dtActividadesHasta.TabIndex = 3;
            // 
            // dtActividadesDesde
            // 
            this.dtActividadesDesde.Enabled = false;
            this.dtActividadesDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtActividadesDesde.Location = new System.Drawing.Point(63, 27);
            this.dtActividadesDesde.Name = "dtActividadesDesde";
            this.dtActividadesDesde.Size = new System.Drawing.Size(95, 20);
            this.dtActividadesDesde.TabIndex = 2;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(177, 29);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 1;
            this.label69.Text = "Hasta";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(19, 29);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(38, 13);
            this.label76.TabIndex = 0;
            this.label76.Text = "Desde";
            // 
            // gbActividades
            // 
            this.gbActividades.Controls.Add(this.cbActividades);
            this.gbActividades.Location = new System.Drawing.Point(23, 153);
            this.gbActividades.Name = "gbActividades";
            this.gbActividades.Size = new System.Drawing.Size(147, 52);
            this.gbActividades.TabIndex = 15;
            this.gbActividades.TabStop = false;
            this.gbActividades.Text = "Tipos de Actividades";
            // 
            // cbActividades
            // 
            this.cbActividades.FormattingEnabled = true;
            this.cbActividades.Items.AddRange(new object[] {
            "Todas",
            "Quad",
            "Bicicleta",
            "Paseo a caballo",
            "Tiro con arco",
            "Piragüismo",
            "Rápel",
            "Parq. Temático",
            "Loc. de interés"});
            this.cbActividades.Location = new System.Drawing.Point(12, 19);
            this.cbActividades.Name = "cbActividades";
            this.cbActividades.Size = new System.Drawing.Size(121, 21);
            this.cbActividades.TabIndex = 0;
            // 
            // tabCoto
            // 
            this.tabCoto.Controls.Add(this.btCotoCrear);
            this.tabCoto.Controls.Add(this.gbCotoPiezas);
            this.tabCoto.Controls.Add(this.gbCotoFechas);
            this.tabCoto.Controls.Add(this.gbCotoAsunto);
            this.tabCoto.Location = new System.Drawing.Point(4, 22);
            this.tabCoto.Name = "tabCoto";
            this.tabCoto.Size = new System.Drawing.Size(963, 736);
            this.tabCoto.TabIndex = 2;
            this.tabCoto.Text = "Coto";
            this.tabCoto.UseVisualStyleBackColor = true;
            // 
            // btCotoCrear
            // 
            this.btCotoCrear.Location = new System.Drawing.Point(257, 167);
            this.btCotoCrear.Name = "btCotoCrear";
            this.btCotoCrear.Size = new System.Drawing.Size(135, 26);
            this.btCotoCrear.TabIndex = 15;
            this.btCotoCrear.Text = "Generar Informe";
            this.btCotoCrear.UseVisualStyleBackColor = true;
            this.btCotoCrear.Click += new System.EventHandler(this.btCotoCrear_Click);
            // 
            // gbCotoPiezas
            // 
            this.gbCotoPiezas.Controls.Add(this.cbCotoPiezas);
            this.gbCotoPiezas.Location = new System.Drawing.Point(23, 153);
            this.gbCotoPiezas.Name = "gbCotoPiezas";
            this.gbCotoPiezas.Size = new System.Drawing.Size(147, 52);
            this.gbCotoPiezas.TabIndex = 14;
            this.gbCotoPiezas.TabStop = false;
            this.gbCotoPiezas.Text = "Tipos de piezas";
            // 
            // cbCotoPiezas
            // 
            this.cbCotoPiezas.FormattingEnabled = true;
            this.cbCotoPiezas.Items.AddRange(new object[] {
            "Todos",
            "Tordos",
            "Torcaz",
            "Bravia",
            "Perdiz Roja",
            "Cone/Lie",
            "Faisan",
            "Codorniz",
            "Becada"});
            this.cbCotoPiezas.Location = new System.Drawing.Point(12, 19);
            this.cbCotoPiezas.Name = "cbCotoPiezas";
            this.cbCotoPiezas.Size = new System.Drawing.Size(121, 21);
            this.cbCotoPiezas.TabIndex = 0;
            // 
            // gbCotoFechas
            // 
            this.gbCotoFechas.Controls.Add(this.cbCotoEnableFechas);
            this.gbCotoFechas.Controls.Add(this.dtCotoHasta);
            this.gbCotoFechas.Controls.Add(this.dtCotoDesde);
            this.gbCotoFechas.Controls.Add(this.label1);
            this.gbCotoFechas.Controls.Add(this.label12);
            this.gbCotoFechas.Location = new System.Drawing.Point(23, 80);
            this.gbCotoFechas.Name = "gbCotoFechas";
            this.gbCotoFechas.Size = new System.Drawing.Size(373, 67);
            this.gbCotoFechas.TabIndex = 13;
            this.gbCotoFechas.TabStop = false;
            this.gbCotoFechas.Text = "Fechas";
            // 
            // cbCotoEnableFechas
            // 
            this.cbCotoEnableFechas.AutoSize = true;
            this.cbCotoEnableFechas.Location = new System.Drawing.Point(337, 29);
            this.cbCotoEnableFechas.Name = "cbCotoEnableFechas";
            this.cbCotoEnableFechas.Size = new System.Drawing.Size(15, 14);
            this.cbCotoEnableFechas.TabIndex = 4;
            this.cbCotoEnableFechas.UseVisualStyleBackColor = true;
            this.cbCotoEnableFechas.CheckedChanged += new System.EventHandler(this.cbCotoEnableFechas_CheckedChanged);
            // 
            // dtCotoHasta
            // 
            this.dtCotoHasta.Enabled = false;
            this.dtCotoHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCotoHasta.Location = new System.Drawing.Point(218, 26);
            this.dtCotoHasta.Name = "dtCotoHasta";
            this.dtCotoHasta.Size = new System.Drawing.Size(95, 20);
            this.dtCotoHasta.TabIndex = 3;
            // 
            // dtCotoDesde
            // 
            this.dtCotoDesde.Enabled = false;
            this.dtCotoDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCotoDesde.Location = new System.Drawing.Point(63, 27);
            this.dtCotoDesde.Name = "dtCotoDesde";
            this.dtCotoDesde.Size = new System.Drawing.Size(95, 20);
            this.dtCotoDesde.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hasta";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Desde";
            // 
            // gbCotoAsunto
            // 
            this.gbCotoAsunto.Controls.Add(this.rbCotoReservas);
            this.gbCotoAsunto.Controls.Add(this.rbCotoAreas);
            this.gbCotoAsunto.Controls.Add(this.rbCotoPiezas);
            this.gbCotoAsunto.Location = new System.Drawing.Point(23, 18);
            this.gbCotoAsunto.Name = "gbCotoAsunto";
            this.gbCotoAsunto.Size = new System.Drawing.Size(373, 52);
            this.gbCotoAsunto.TabIndex = 12;
            this.gbCotoAsunto.TabStop = false;
            this.gbCotoAsunto.Text = "Asunto";
            // 
            // rbCotoReservas
            // 
            this.rbCotoReservas.AutoSize = true;
            this.rbCotoReservas.Location = new System.Drawing.Point(267, 19);
            this.rbCotoReservas.Name = "rbCotoReservas";
            this.rbCotoReservas.Size = new System.Drawing.Size(70, 17);
            this.rbCotoReservas.TabIndex = 2;
            this.rbCotoReservas.TabStop = true;
            this.rbCotoReservas.Text = "Reservas";
            this.rbCotoReservas.UseVisualStyleBackColor = true;
            this.rbCotoReservas.CheckedChanged += new System.EventHandler(this.rbCotoReservas_CheckedChanged);
            // 
            // rbCotoAreas
            // 
            this.rbCotoAreas.AutoSize = true;
            this.rbCotoAreas.Location = new System.Drawing.Point(160, 19);
            this.rbCotoAreas.Name = "rbCotoAreas";
            this.rbCotoAreas.Size = new System.Drawing.Size(52, 17);
            this.rbCotoAreas.TabIndex = 1;
            this.rbCotoAreas.TabStop = true;
            this.rbCotoAreas.Text = "Areas";
            this.rbCotoAreas.UseVisualStyleBackColor = true;
            this.rbCotoAreas.CheckedChanged += new System.EventHandler(this.rbCotoAreas_CheckedChanged);
            // 
            // rbCotoPiezas
            // 
            this.rbCotoPiezas.AutoSize = true;
            this.rbCotoPiezas.Checked = true;
            this.rbCotoPiezas.Location = new System.Drawing.Point(16, 19);
            this.rbCotoPiezas.Name = "rbCotoPiezas";
            this.rbCotoPiezas.Size = new System.Drawing.Size(99, 17);
            this.rbCotoPiezas.TabIndex = 0;
            this.rbCotoPiezas.TabStop = true;
            this.rbCotoPiezas.Text = "Piezas cazadas";
            this.rbCotoPiezas.UseVisualStyleBackColor = true;
            this.rbCotoPiezas.CheckedChanged += new System.EventHandler(this.rbCotoPiezas_CheckedChanged);
            // 
            // FormInformes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 762);
            this.Controls.Add(this.tabInformes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(300, 600);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInformes";
            this.Text = "TORDICAN S.L. - Informes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormInformes_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.epTimeTravel)).EndInit();
            this.tabInformes.ResumeLayout(false);
            this.tabClientes.ResumeLayout(false);
            this.tabClientes.PerformLayout();
            this.tabActividades.ResumeLayout(false);
            this.gbIncidencias.ResumeLayout(false);
            this.gbActividadesAsunto.ResumeLayout(false);
            this.gbActividadesAsunto.PerformLayout();
            this.gbActividadesFechas.ResumeLayout(false);
            this.gbActividadesFechas.PerformLayout();
            this.gbActividades.ResumeLayout(false);
            this.tabCoto.ResumeLayout(false);
            this.gbCotoPiezas.ResumeLayout(false);
            this.gbCotoFechas.ResumeLayout(false);
            this.gbCotoFechas.PerformLayout();
            this.gbCotoAsunto.ResumeLayout(false);
            this.gbCotoAsunto.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ErrorProvider epTimeTravel;
        private System.Windows.Forms.TabControl tabInformes;
        private System.Windows.Forms.TabPage tabClientes;
        private System.Windows.Forms.CheckBox cbClientesEnableFechas;
        private System.Windows.Forms.DateTimePicker dtClientesHasta;
        private System.Windows.Forms.DateTimePicker dtClientesDesde;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btClientesCrear;
        private System.Windows.Forms.TabPage tabActividades;
        private System.Windows.Forms.GroupBox gbIncidencias;
        private System.Windows.Forms.ComboBox cbRevisadas;
        private System.Windows.Forms.GroupBox gbActividadesAsunto;
        private System.Windows.Forms.RadioButton rbActividadesIncidencias;
        private System.Windows.Forms.RadioButton rbActividadesRealizadas;
        private System.Windows.Forms.Button btActividadesCrear;
        private System.Windows.Forms.GroupBox gbActividadesFechas;
        private System.Windows.Forms.CheckBox cbActividadesEnableFechas;
        private System.Windows.Forms.DateTimePicker dtActividadesHasta;
        private System.Windows.Forms.DateTimePicker dtActividadesDesde;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.GroupBox gbActividades;
        private System.Windows.Forms.ComboBox cbActividades;
        private System.Windows.Forms.TabPage tabCoto;
        private System.Windows.Forms.Button btCotoCrear;
        private System.Windows.Forms.GroupBox gbCotoPiezas;
        private System.Windows.Forms.ComboBox cbCotoPiezas;
        private System.Windows.Forms.GroupBox gbCotoFechas;
        private System.Windows.Forms.CheckBox cbCotoEnableFechas;
        private System.Windows.Forms.DateTimePicker dtCotoHasta;
        private System.Windows.Forms.DateTimePicker dtCotoDesde;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox gbCotoAsunto;
        private System.Windows.Forms.RadioButton rbCotoReservas;
        private System.Windows.Forms.RadioButton rbCotoAreas;
        private System.Windows.Forms.RadioButton rbCotoPiezas;
    }
}