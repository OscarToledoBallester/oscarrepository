﻿namespace InterfazDeUsuario
{
    partial class FormReservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReservas));
            this.lbdni = new System.Windows.Forms.Label();
            this.lbNombre = new System.Windows.Forms.Label();
            this.lbApellidos = new System.Windows.Forms.Label();
            this.lbReservas = new System.Windows.Forms.Label();
            this.tbNif = new System.Windows.Forms.TextBox();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.tbApellidos = new System.Windows.Forms.TextBox();
            this.lbFIngreso = new System.Windows.Forms.Label();
            this.lFSalida = new System.Windows.Forms.Label();
            this.ElegirMes = new System.Windows.Forms.MonthCalendar();
            this.tbFechaIng = new System.Windows.Forms.MaskedTextBox();
            this.tbFechaSal = new System.Windows.Forms.MaskedTextBox();
            this.btFechaIng = new System.Windows.Forms.Button();
            this.btFechaSal = new System.Windows.Forms.Button();
            this.tbFechaRes = new System.Windows.Forms.MaskedTextBox();
            this.btElegirRes = new System.Windows.Forms.Button();
            this.btNueva = new System.Windows.Forms.Button();
            this.btBuscar = new System.Windows.Forms.Button();
            this.btElimiar = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.BtBuscarDNI = new System.Windows.Forms.Button();
            this.dataGridReservas = new System.Windows.Forms.DataGridView();
            this.Reserva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ingreso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Planta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Habitacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumDias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioAct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbHabitacion = new System.Windows.Forms.ComboBox();
            this.cbPension = new System.Windows.Forms.ComboBox();
            this.labhab = new System.Windows.Forms.Label();
            this.labPens = new System.Windows.Forms.Label();
            this.tbResCaza = new System.Windows.Forms.Button();
            this.btActividades = new System.Windows.Forms.Button();
            this.gbElAct = new System.Windows.Forms.GroupBox();
            this.rbLocalidad = new System.Windows.Forms.RadioButton();
            this.rbParque = new System.Windows.Forms.RadioButton();
            this.rbRapel = new System.Windows.Forms.RadioButton();
            this.rbPirag = new System.Windows.Forms.RadioButton();
            this.rbTiro = new System.Windows.Forms.RadioButton();
            this.rbPasCab = new System.Windows.Forms.RadioButton();
            this.rbBici = new System.Windows.Forms.RadioButton();
            this.rbQuad = new System.Windows.Forms.RadioButton();
            this.tblibre = new System.Windows.Forms.TextBox();
            this.lbLibres = new System.Windows.Forms.Label();
            this.tbOcupadas = new System.Windows.Forms.TextBox();
            this.lbOcupadas = new System.Windows.Forms.Label();
            this.cbPlazaParking = new System.Windows.Forms.CheckBox();
            this.gpOpcReserva = new System.Windows.Forms.GroupBox();
            this.gbCaza = new System.Windows.Forms.GroupBox();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.panelCaza = new System.Windows.Forms.Panel();
            this.lbCaza = new System.Windows.Forms.Label();
            this.tbSubCaza = new System.Windows.Forms.TextBox();
            this.btcerrarCaza = new System.Windows.Forms.Button();
            this.dgvCaza = new System.Windows.Forms.DataGridView();
            this.btVerDetalleCaza = new System.Windows.Forms.Button();
            this.AReserva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aFecReserva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TCaza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrTipoCaza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tComida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrComida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numEsc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioPerrera = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.parkingRemolque = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridReservas)).BeginInit();
            this.gbElAct.SuspendLayout();
            this.gpOpcReserva.SuspendLayout();
            this.gbCaza.SuspendLayout();
            this.panelCaza.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaza)).BeginInit();
            this.SuspendLayout();
            // 
            // lbdni
            // 
            this.lbdni.AutoSize = true;
            this.lbdni.Location = new System.Drawing.Point(65, 40);
            this.lbdni.Name = "lbdni";
            this.lbdni.Size = new System.Drawing.Size(27, 13);
            this.lbdni.TabIndex = 0;
            this.lbdni.Text = "NIF:";
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Location = new System.Drawing.Point(45, 79);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(47, 13);
            this.lbNombre.TabIndex = 1;
            this.lbNombre.Text = "Nombre:";
            // 
            // lbApellidos
            // 
            this.lbApellidos.AutoSize = true;
            this.lbApellidos.Location = new System.Drawing.Point(37, 118);
            this.lbApellidos.Name = "lbApellidos";
            this.lbApellidos.Size = new System.Drawing.Size(55, 13);
            this.lbApellidos.TabIndex = 2;
            this.lbApellidos.Text = "Apellidos: ";
            // 
            // lbReservas
            // 
            this.lbReservas.AutoSize = true;
            this.lbReservas.Location = new System.Drawing.Point(339, 40);
            this.lbReservas.Name = "lbReservas";
            this.lbReservas.Size = new System.Drawing.Size(83, 13);
            this.lbReservas.TabIndex = 3;
            this.lbReservas.Text = "Fecha Reserva:";
            // 
            // tbNif
            // 
            this.tbNif.Location = new System.Drawing.Point(100, 37);
            this.tbNif.Name = "tbNif";
            this.tbNif.Size = new System.Drawing.Size(109, 20);
            this.tbNif.TabIndex = 0;
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(100, 76);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.ReadOnly = true;
            this.tbNombre.Size = new System.Drawing.Size(160, 20);
            this.tbNombre.TabIndex = 1;
            // 
            // tbApellidos
            // 
            this.tbApellidos.Location = new System.Drawing.Point(100, 115);
            this.tbApellidos.Name = "tbApellidos";
            this.tbApellidos.ReadOnly = true;
            this.tbApellidos.Size = new System.Drawing.Size(200, 20);
            this.tbApellidos.TabIndex = 2;
            // 
            // lbFIngreso
            // 
            this.lbFIngreso.AutoSize = true;
            this.lbFIngreso.Location = new System.Drawing.Point(344, 79);
            this.lbFIngreso.Name = "lbFIngreso";
            this.lbFIngreso.Size = new System.Drawing.Size(78, 13);
            this.lbFIngreso.TabIndex = 10;
            this.lbFIngreso.Text = "Fecha Ingreso:";
            // 
            // lFSalida
            // 
            this.lFSalida.AutoSize = true;
            this.lFSalida.Location = new System.Drawing.Point(350, 118);
            this.lFSalida.Name = "lFSalida";
            this.lFSalida.Size = new System.Drawing.Size(72, 13);
            this.lFSalida.TabIndex = 11;
            this.lFSalida.Text = "Fecha Salida:";
            // 
            // ElegirMes
            // 
            this.ElegirMes.Location = new System.Drawing.Point(571, 2);
            this.ElegirMes.Name = "ElegirMes";
            this.ElegirMes.ShowToday = false;
            this.ElegirMes.TabIndex = 14;
            this.ElegirMes.TabStop = false;
            this.ElegirMes.Visible = false;
            this.ElegirMes.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.ElegirMes_DateSelected);
            // 
            // tbFechaIng
            // 
            this.tbFechaIng.Location = new System.Drawing.Point(439, 76);
            this.tbFechaIng.Mask = "00/00/0000";
            this.tbFechaIng.Name = "tbFechaIng";
            this.tbFechaIng.ReadOnly = true;
            this.tbFechaIng.Size = new System.Drawing.Size(91, 20);
            this.tbFechaIng.TabIndex = 4;
            this.tbFechaIng.ValidatingType = typeof(System.DateTime);
            // 
            // tbFechaSal
            // 
            this.tbFechaSal.Location = new System.Drawing.Point(439, 115);
            this.tbFechaSal.Mask = "00/00/0000";
            this.tbFechaSal.Name = "tbFechaSal";
            this.tbFechaSal.ReadOnly = true;
            this.tbFechaSal.Size = new System.Drawing.Size(91, 20);
            this.tbFechaSal.TabIndex = 5;
            this.tbFechaSal.ValidatingType = typeof(System.DateTime);
            // 
            // btFechaIng
            // 
            this.btFechaIng.Location = new System.Drawing.Point(533, 76);
            this.btFechaIng.Name = "btFechaIng";
            this.btFechaIng.Size = new System.Drawing.Size(25, 20);
            this.btFechaIng.TabIndex = 17;
            this.btFechaIng.Text = "...";
            this.btFechaIng.UseVisualStyleBackColor = true;
            this.btFechaIng.Click += new System.EventHandler(this.btFechaIng_Click);
            // 
            // btFechaSal
            // 
            this.btFechaSal.Location = new System.Drawing.Point(533, 114);
            this.btFechaSal.Name = "btFechaSal";
            this.btFechaSal.Size = new System.Drawing.Size(25, 20);
            this.btFechaSal.TabIndex = 18;
            this.btFechaSal.Text = "...";
            this.btFechaSal.UseVisualStyleBackColor = true;
            this.btFechaSal.Click += new System.EventHandler(this.btFechaSal_Click);
            // 
            // tbFechaRes
            // 
            this.tbFechaRes.Location = new System.Drawing.Point(439, 37);
            this.tbFechaRes.Mask = "00/00/0000";
            this.tbFechaRes.Name = "tbFechaRes";
            this.tbFechaRes.ReadOnly = true;
            this.tbFechaRes.Size = new System.Drawing.Size(91, 20);
            this.tbFechaRes.TabIndex = 3;
            // 
            // btElegirRes
            // 
            this.btElegirRes.Location = new System.Drawing.Point(533, 36);
            this.btElegirRes.Name = "btElegirRes";
            this.btElegirRes.Size = new System.Drawing.Size(25, 20);
            this.btElegirRes.TabIndex = 20;
            this.btElegirRes.Text = "...";
            this.btElegirRes.UseVisualStyleBackColor = true;
            this.btElegirRes.Click += new System.EventHandler(this.btElegirRes_Click);
            // 
            // btNueva
            // 
            this.btNueva.BackColor = System.Drawing.SystemColors.Control;
            this.btNueva.Location = new System.Drawing.Point(16, 19);
            this.btNueva.Name = "btNueva";
            this.btNueva.Size = new System.Drawing.Size(120, 45);
            this.btNueva.TabIndex = 21;
            this.btNueva.Text = "Nueva";
            this.btNueva.UseVisualStyleBackColor = false;
            this.btNueva.Click += new System.EventHandler(this.btNueva_Click);
            // 
            // btBuscar
            // 
            this.btBuscar.BackColor = System.Drawing.SystemColors.Control;
            this.btBuscar.Location = new System.Drawing.Point(16, 88);
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(120, 45);
            this.btBuscar.TabIndex = 23;
            this.btBuscar.Text = "Buscar";
            this.btBuscar.UseVisualStyleBackColor = false;
            this.btBuscar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // btElimiar
            // 
            this.btElimiar.BackColor = System.Drawing.SystemColors.Control;
            this.btElimiar.Location = new System.Drawing.Point(16, 165);
            this.btElimiar.Name = "btElimiar";
            this.btElimiar.Size = new System.Drawing.Size(120, 45);
            this.btElimiar.TabIndex = 24;
            this.btElimiar.Text = "Eliminar";
            this.btElimiar.UseVisualStyleBackColor = false;
            this.btElimiar.Click += new System.EventHandler(this.btElimiar_Click);
            // 
            // btCancelar
            // 
            this.btCancelar.Location = new System.Drawing.Point(16, 247);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(120, 45);
            this.btCancelar.TabIndex = 25;
            this.btCancelar.Text = "Cancelar";
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // BtBuscarDNI
            // 
            this.BtBuscarDNI.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtBuscarDNI.BackgroundImage")));
            this.BtBuscarDNI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtBuscarDNI.Location = new System.Drawing.Point(225, 30);
            this.BtBuscarDNI.Name = "BtBuscarDNI";
            this.BtBuscarDNI.Size = new System.Drawing.Size(35, 32);
            this.BtBuscarDNI.TabIndex = 26;
            this.BtBuscarDNI.UseVisualStyleBackColor = true;
            this.BtBuscarDNI.Click += new System.EventHandler(this.BtBuscarDNI_Click);
            // 
            // dataGridReservas
            // 
            this.dataGridReservas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridReservas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Reserva,
            this.Ingreso,
            this.Planta,
            this.Habitacion,
            this.NumDias,
            this.PrecioH,
            this.Act,
            this.PrecioAct,
            this.Parking,
            this.Total});
            this.dataGridReservas.Location = new System.Drawing.Point(58, 194);
            this.dataGridReservas.Name = "dataGridReservas";
            this.dataGridReservas.Size = new System.Drawing.Size(740, 181);
            this.dataGridReservas.TabIndex = 27;
            this.dataGridReservas.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridReservas_RowHeaderMouseClick);
            // 
            // Reserva
            // 
            this.Reserva.HeaderText = "Reserva";
            this.Reserva.Name = "Reserva";
            this.Reserva.ReadOnly = true;
            this.Reserva.Width = 55;
            // 
            // Ingreso
            // 
            this.Ingreso.HeaderText = "Ingreso";
            this.Ingreso.Name = "Ingreso";
            this.Ingreso.ReadOnly = true;
            this.Ingreso.Width = 80;
            // 
            // Planta
            // 
            this.Planta.HeaderText = "Planta";
            this.Planta.Name = "Planta";
            this.Planta.ReadOnly = true;
            this.Planta.Width = 60;
            // 
            // Habitacion
            // 
            this.Habitacion.HeaderText = "Habitacion";
            this.Habitacion.Name = "Habitacion";
            this.Habitacion.ReadOnly = true;
            this.Habitacion.Width = 65;
            // 
            // NumDias
            // 
            this.NumDias.HeaderText = "Dias";
            this.NumDias.Name = "NumDias";
            this.NumDias.ReadOnly = true;
            this.NumDias.Width = 60;
            // 
            // PrecioH
            // 
            this.PrecioH.HeaderText = "Precio";
            this.PrecioH.Name = "PrecioH";
            this.PrecioH.ReadOnly = true;
            this.PrecioH.Width = 60;
            // 
            // Act
            // 
            this.Act.HeaderText = "Actividad";
            this.Act.Name = "Act";
            this.Act.ReadOnly = true;
            this.Act.Width = 80;
            // 
            // PrecioAct
            // 
            this.PrecioAct.HeaderText = "Precio";
            this.PrecioAct.Name = "PrecioAct";
            this.PrecioAct.ReadOnly = true;
            this.PrecioAct.Width = 65;
            // 
            // Parking
            // 
            this.Parking.HeaderText = "Parking";
            this.Parking.Name = "Parking";
            this.Parking.Width = 65;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 70;
            // 
            // cbHabitacion
            // 
            this.cbHabitacion.FormattingEnabled = true;
            this.cbHabitacion.Items.AddRange(new object[] {
            "Ninguna",
            "Individuales",
            "Dobles",
            "Suites"});
            this.cbHabitacion.Location = new System.Drawing.Point(100, 154);
            this.cbHabitacion.Name = "cbHabitacion";
            this.cbHabitacion.Size = new System.Drawing.Size(197, 21);
            this.cbHabitacion.TabIndex = 28;
            this.cbHabitacion.SelectedIndexChanged += new System.EventHandler(this.cbHabitacion_SelectedIndexChanged);
            // 
            // cbPension
            // 
            this.cbPension.FormattingEnabled = true;
            this.cbPension.Items.AddRange(new object[] {
            "Ninguna",
            "Media Pensión",
            "Pensión Completa",
            "Ben Marica"});
            this.cbPension.Location = new System.Drawing.Point(362, 154);
            this.cbPension.Name = "cbPension";
            this.cbPension.Size = new System.Drawing.Size(197, 21);
            this.cbPension.TabIndex = 29;
            // 
            // labhab
            // 
            this.labhab.AutoSize = true;
            this.labhab.Location = new System.Drawing.Point(31, 157);
            this.labhab.Name = "labhab";
            this.labhab.Size = new System.Drawing.Size(61, 13);
            this.labhab.TabIndex = 32;
            this.labhab.Text = "Habitación:";
            // 
            // labPens
            // 
            this.labPens.AutoSize = true;
            this.labPens.Location = new System.Drawing.Point(306, 157);
            this.labPens.Name = "labPens";
            this.labPens.Size = new System.Drawing.Size(48, 13);
            this.labPens.TabIndex = 34;
            this.labPens.Text = "Pension:";
            // 
            // tbResCaza
            // 
            this.tbResCaza.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tbResCaza.BackgroundImage")));
            this.tbResCaza.Location = new System.Drawing.Point(20, 23);
            this.tbResCaza.Name = "tbResCaza";
            this.tbResCaza.Size = new System.Drawing.Size(260, 125);
            this.tbResCaza.TabIndex = 36;
            this.tbResCaza.UseVisualStyleBackColor = true;
 
            // 
            // btActividades
            // 
            this.btActividades.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btActividades.Location = new System.Drawing.Point(342, 515);
            this.btActividades.Name = "btActividades";
            this.btActividades.Size = new System.Drawing.Size(99, 39);
            this.btActividades.TabIndex = 37;
            this.btActividades.Text = "ACTIVIDADES";
            this.btActividades.UseVisualStyleBackColor = true;
            this.btActividades.Click += new System.EventHandler(this.btActividades_Click);
            // 
            // gbElAct
            // 
            this.gbElAct.Controls.Add(this.rbLocalidad);
            this.gbElAct.Controls.Add(this.rbParque);
            this.gbElAct.Controls.Add(this.rbRapel);
            this.gbElAct.Controls.Add(this.rbPirag);
            this.gbElAct.Controls.Add(this.rbTiro);
            this.gbElAct.Controls.Add(this.rbPasCab);
            this.gbElAct.Controls.Add(this.rbBici);
            this.gbElAct.Controls.Add(this.rbQuad);
            this.gbElAct.Location = new System.Drawing.Point(64, 464);
            this.gbElAct.Name = "gbElAct";
            this.gbElAct.Size = new System.Drawing.Size(272, 131);
            this.gbElAct.TabIndex = 38;
            this.gbElAct.TabStop = false;
            this.gbElAct.Text = "Elegir Actividades";
            // 
            // rbLocalidad
            // 
            this.rbLocalidad.AutoSize = true;
            this.rbLocalidad.Location = new System.Drawing.Point(134, 97);
            this.rbLocalidad.Name = "rbLocalidad";
            this.rbLocalidad.Size = new System.Drawing.Size(132, 17);
            this.rbLocalidad.TabIndex = 7;
            this.rbLocalidad.TabStop = true;
            this.rbLocalidad.Text = "Localidades de Interés";
            this.rbLocalidad.UseVisualStyleBackColor = true;
            this.rbLocalidad.CheckedChanged += new System.EventHandler(this.rbLocalidad_CheckedChanged);
            // 
            // rbParque
            // 
            this.rbParque.AutoSize = true;
            this.rbParque.Location = new System.Drawing.Point(134, 74);
            this.rbParque.Name = "rbParque";
            this.rbParque.Size = new System.Drawing.Size(116, 17);
            this.rbParque.TabIndex = 6;
            this.rbParque.TabStop = true;
            this.rbParque.Text = "Parques Temáticos";
            this.rbParque.UseVisualStyleBackColor = true;
            this.rbParque.CheckedChanged += new System.EventHandler(this.rbParque_CheckedChanged);
            // 
            // rbRapel
            // 
            this.rbRapel.AutoSize = true;
            this.rbRapel.Location = new System.Drawing.Point(134, 51);
            this.rbRapel.Name = "rbRapel";
            this.rbRapel.Size = new System.Drawing.Size(53, 17);
            this.rbRapel.TabIndex = 5;
            this.rbRapel.TabStop = true;
            this.rbRapel.Text = "Rápel";
            this.rbRapel.UseVisualStyleBackColor = true;
            this.rbRapel.CheckedChanged += new System.EventHandler(this.rbRapel_CheckedChanged);
            // 
            // rbPirag
            // 
            this.rbPirag.AutoSize = true;
            this.rbPirag.Location = new System.Drawing.Point(134, 27);
            this.rbPirag.Name = "rbPirag";
            this.rbPirag.Size = new System.Drawing.Size(76, 17);
            this.rbPirag.TabIndex = 4;
            this.rbPirag.TabStop = true;
            this.rbPirag.Text = "Piragüismo";
            this.rbPirag.UseVisualStyleBackColor = true;
            this.rbPirag.CheckedChanged += new System.EventHandler(this.rbPirag_CheckedChanged);
            // 
            // rbTiro
            // 
            this.rbTiro.AutoSize = true;
            this.rbTiro.Location = new System.Drawing.Point(12, 96);
            this.rbTiro.Name = "rbTiro";
            this.rbTiro.Size = new System.Drawing.Size(89, 17);
            this.rbTiro.TabIndex = 3;
            this.rbTiro.TabStop = true;
            this.rbTiro.Text = "Tiro con Arco";
            this.rbTiro.UseVisualStyleBackColor = true;
            this.rbTiro.CheckedChanged += new System.EventHandler(this.rbTiro_CheckedChanged);
            // 
            // rbPasCab
            // 
            this.rbPasCab.AutoSize = true;
            this.rbPasCab.Location = new System.Drawing.Point(12, 73);
            this.rbPasCab.Name = "rbPasCab";
            this.rbPasCab.Size = new System.Drawing.Size(107, 17);
            this.rbPasCab.TabIndex = 2;
            this.rbPasCab.TabStop = true;
            this.rbPasCab.Text = "Paseos a Caballo";
            this.rbPasCab.UseVisualStyleBackColor = true;
            this.rbPasCab.CheckedChanged += new System.EventHandler(this.rbPasCab_CheckedChanged);
            // 
            // rbBici
            // 
            this.rbBici.AutoSize = true;
            this.rbBici.Location = new System.Drawing.Point(12, 50);
            this.rbBici.Name = "rbBici";
            this.rbBici.Size = new System.Drawing.Size(70, 17);
            this.rbBici.TabIndex = 1;
            this.rbBici.TabStop = true;
            this.rbBici.Text = "Bicicletas";
            this.rbBici.UseVisualStyleBackColor = true;
            this.rbBici.CheckedChanged += new System.EventHandler(this.rbBici_CheckedChanged);
            // 
            // rbQuad
            // 
            this.rbQuad.AutoSize = true;
            this.rbQuad.Location = new System.Drawing.Point(12, 27);
            this.rbQuad.Name = "rbQuad";
            this.rbQuad.Size = new System.Drawing.Size(56, 17);
            this.rbQuad.TabIndex = 0;
            this.rbQuad.TabStop = true;
            this.rbQuad.Text = "Quads";
            this.rbQuad.UseVisualStyleBackColor = true;
            this.rbQuad.CheckedChanged += new System.EventHandler(this.rbQuad_CheckedChanged);
            // 
            // tblibre
            // 
            this.tblibre.BackColor = System.Drawing.Color.Lime;
            this.tblibre.Location = new System.Drawing.Point(124, 418);
            this.tblibre.Multiline = true;
            this.tblibre.Name = "tblibre";
            this.tblibre.Size = new System.Drawing.Size(15, 15);
            this.tblibre.TabIndex = 39;
            // 
            // lbLibres
            // 
            this.lbLibres.AutoSize = true;
            this.lbLibres.Location = new System.Drawing.Point(145, 420);
            this.lbLibres.Name = "lbLibres";
            this.lbLibres.Size = new System.Drawing.Size(35, 13);
            this.lbLibres.TabIndex = 40;
            this.lbLibres.Text = "Libres";
            // 
            // tbOcupadas
            // 
            this.tbOcupadas.BackColor = System.Drawing.Color.Red;
            this.tbOcupadas.Location = new System.Drawing.Point(216, 419);
            this.tbOcupadas.Multiline = true;
            this.tbOcupadas.Name = "tbOcupadas";
            this.tbOcupadas.Size = new System.Drawing.Size(15, 15);
            this.tbOcupadas.TabIndex = 41;
            // 
            // lbOcupadas
            // 
            this.lbOcupadas.AutoSize = true;
            this.lbOcupadas.Location = new System.Drawing.Point(237, 420);
            this.lbOcupadas.Name = "lbOcupadas";
            this.lbOcupadas.Size = new System.Drawing.Size(56, 13);
            this.lbOcupadas.TabIndex = 43;
            this.lbOcupadas.Text = "Ocupadas";
            // 
            // cbPlazaParking
            // 
            this.cbPlazaParking.AutoSize = true;
            this.cbPlazaParking.Location = new System.Drawing.Point(571, 158);
            this.cbPlazaParking.Name = "cbPlazaParking";
            this.cbPlazaParking.Size = new System.Drawing.Size(91, 17);
            this.cbPlazaParking.TabIndex = 44;
            this.cbPlazaParking.Text = "Plaza Parking";
            this.cbPlazaParking.UseVisualStyleBackColor = true;
            // 
            // gpOpcReserva
            // 
            this.gpOpcReserva.BackColor = System.Drawing.SystemColors.Control;
            this.gpOpcReserva.Controls.Add(this.btElimiar);
            this.gpOpcReserva.Controls.Add(this.btBuscar);
            this.gpOpcReserva.Controls.Add(this.btNueva);
            this.gpOpcReserva.Controls.Add(this.btCancelar);
            this.gpOpcReserva.Location = new System.Drawing.Point(830, 12);
            this.gpOpcReserva.Name = "gpOpcReserva";
            this.gpOpcReserva.Size = new System.Drawing.Size(159, 298);
            this.gpOpcReserva.TabIndex = 47;
            this.gpOpcReserva.TabStop = false;
            this.gpOpcReserva.Text = "Opciones";
            // 
            // gbCaza
            // 
            this.gbCaza.Controls.Add(this.tbResCaza);
            this.gbCaza.Location = new System.Drawing.Point(499, 454);
            this.gbCaza.Name = "gbCaza";
            this.gbCaza.Size = new System.Drawing.Size(299, 158);
            this.gbCaza.TabIndex = 48;
            this.gbCaza.TabStop = false;
            this.gbCaza.Text = "Reservar Caza";
            // 
            // tbTotal
            // 
            this.tbTotal.Location = new System.Drawing.Point(704, 381);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.ReadOnly = true;
            this.tbTotal.Size = new System.Drawing.Size(94, 20);
            this.tbTotal.TabIndex = 49;
            // 
            // panelCaza
            // 
            this.panelCaza.Controls.Add(this.lbCaza);
            this.panelCaza.Controls.Add(this.tbSubCaza);
            this.panelCaza.Controls.Add(this.btcerrarCaza);
            this.panelCaza.Controls.Add(this.dgvCaza);
            this.panelCaza.Location = new System.Drawing.Point(58, 194);
            this.panelCaza.Name = "panelCaza";
            this.panelCaza.Size = new System.Drawing.Size(740, 240);
            this.panelCaza.TabIndex = 50;
            this.panelCaza.Visible = false;
            // 
            // lbCaza
            // 
            this.lbCaza.AutoSize = true;
            this.lbCaza.Location = new System.Drawing.Point(476, 216);
            this.lbCaza.Name = "lbCaza";
            this.lbCaza.Size = new System.Drawing.Size(61, 13);
            this.lbCaza.TabIndex = 3;
            this.lbCaza.Text = "Total Caza:";
            // 
            // tbSubCaza
            // 
            this.tbSubCaza.Location = new System.Drawing.Point(543, 212);
            this.tbSubCaza.Name = "tbSubCaza";
            this.tbSubCaza.ReadOnly = true;
            this.tbSubCaza.Size = new System.Drawing.Size(91, 20);
            this.tbSubCaza.TabIndex = 2;
            // 
            // btcerrarCaza
            // 
            this.btcerrarCaza.Location = new System.Drawing.Point(640, 208);
            this.btcerrarCaza.Name = "btcerrarCaza";
            this.btcerrarCaza.Size = new System.Drawing.Size(81, 27);
            this.btcerrarCaza.TabIndex = 1;
            this.btcerrarCaza.Text = "Cerrar";
            this.btcerrarCaza.UseVisualStyleBackColor = true;
            this.btcerrarCaza.Click += new System.EventHandler(this.btcerrarCaza_Click);
            // 
            // dgvCaza
            // 
            this.dgvCaza.AllowUserToDeleteRows = false;
            this.dgvCaza.AllowUserToOrderColumns = true;
            this.dgvCaza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AReserva,
            this.aFecReserva,
            this.TArea,
            this.PrArea,
            this.TCaza,
            this.PrTipoCaza,
            this.tComida,
            this.PrComida,
            this.numEsc,
            this.precioPerrera,
            this.parkingRemolque,
            this.SubTotal});
            this.dgvCaza.Location = new System.Drawing.Point(0, 3);
            this.dgvCaza.Name = "dgvCaza";
            this.dgvCaza.ReadOnly = true;
            this.dgvCaza.Size = new System.Drawing.Size(740, 189);
            this.dgvCaza.TabIndex = 0;
            this.dgvCaza.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCaza_RowHeaderMouseClick);
            // 
            // btVerDetalleCaza
            // 
            this.btVerDetalleCaza.Location = new System.Drawing.Point(704, 407);
            this.btVerDetalleCaza.Name = "btVerDetalleCaza";
            this.btVerDetalleCaza.Size = new System.Drawing.Size(94, 22);
            this.btVerDetalleCaza.TabIndex = 51;
            this.btVerDetalleCaza.Text = "Ver Detalle";
            this.btVerDetalleCaza.UseVisualStyleBackColor = true;
            this.btVerDetalleCaza.Visible = false;
            this.btVerDetalleCaza.Click += new System.EventHandler(this.btVerDetalleCaza_Click);
            // 
            // AReserva
            // 
            this.AReserva.HeaderText = "Reserva";
            this.AReserva.Name = "AReserva";
            this.AReserva.ReadOnly = true;
            this.AReserva.Width = 55;
            // 
            // aFecReserva
            // 
            this.aFecReserva.HeaderText = "Fecha";
            this.aFecReserva.Name = "aFecReserva";
            this.aFecReserva.ReadOnly = true;
            this.aFecReserva.Width = 70;
            // 
            // TArea
            // 
            this.TArea.HeaderText = "Area";
            this.TArea.Name = "TArea";
            this.TArea.ReadOnly = true;
            this.TArea.Width = 45;
            // 
            // PrArea
            // 
            this.PrArea.HeaderText = "Precio";
            this.PrArea.Name = "PrArea";
            this.PrArea.ReadOnly = true;
            this.PrArea.Width = 45;
            // 
            // TCaza
            // 
            this.TCaza.HeaderText = "Tipo";
            this.TCaza.Name = "TCaza";
            this.TCaza.ReadOnly = true;
            this.TCaza.Width = 70;
            // 
            // PrTipoCaza
            // 
            this.PrTipoCaza.HeaderText = "Precio";
            this.PrTipoCaza.Name = "PrTipoCaza";
            this.PrTipoCaza.ReadOnly = true;
            this.PrTipoCaza.Width = 45;
            // 
            // tComida
            // 
            this.tComida.HeaderText = "Comida";
            this.tComida.Name = "tComida";
            this.tComida.ReadOnly = true;
            this.tComida.Width = 85;
            // 
            // PrComida
            // 
            this.PrComida.HeaderText = "Precio";
            this.PrComida.Name = "PrComida";
            this.PrComida.ReadOnly = true;
            this.PrComida.Width = 45;
            // 
            // numEsc
            // 
            this.numEsc.HeaderText = "Armas";
            this.numEsc.Name = "numEsc";
            this.numEsc.ReadOnly = true;
            this.numEsc.Width = 50;
            // 
            // precioPerrera
            // 
            this.precioPerrera.HeaderText = "Perrera";
            this.precioPerrera.Name = "precioPerrera";
            this.precioPerrera.ReadOnly = true;
            this.precioPerrera.Width = 50;
            // 
            // parkingRemolque
            // 
            this.parkingRemolque.HeaderText = "Remolque";
            this.parkingRemolque.Name = "parkingRemolque";
            this.parkingRemolque.ReadOnly = true;
            this.parkingRemolque.Width = 60;
            // 
            // SubTotal
            // 
            this.SubTotal.HeaderText = "SubTotal";
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            this.SubTotal.Width = 60;
            // 
            // FormReservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 684);
            this.Controls.Add(this.btVerDetalleCaza);
            this.Controls.Add(this.panelCaza);
            this.Controls.Add(this.tbTotal);
            this.Controls.Add(this.gbCaza);
            this.Controls.Add(this.gpOpcReserva);
            this.Controls.Add(this.cbPlazaParking);
            this.Controls.Add(this.lbOcupadas);
            this.Controls.Add(this.tbOcupadas);
            this.Controls.Add(this.lbLibres);
            this.Controls.Add(this.tblibre);
            this.Controls.Add(this.gbElAct);
            this.Controls.Add(this.btActividades);
            this.Controls.Add(this.labPens);
            this.Controls.Add(this.labhab);
            this.Controls.Add(this.cbPension);
            this.Controls.Add(this.cbHabitacion);
            this.Controls.Add(this.dataGridReservas);
            this.Controls.Add(this.BtBuscarDNI);
            this.Controls.Add(this.btElegirRes);
            this.Controls.Add(this.tbFechaRes);
            this.Controls.Add(this.btFechaSal);
            this.Controls.Add(this.btFechaIng);
            this.Controls.Add(this.tbFechaSal);
            this.Controls.Add(this.tbFechaIng);
            this.Controls.Add(this.ElegirMes);
            this.Controls.Add(this.lFSalida);
            this.Controls.Add(this.lbFIngreso);
            this.Controls.Add(this.tbApellidos);
            this.Controls.Add(this.tbNombre);
            this.Controls.Add(this.tbNif);
            this.Controls.Add(this.lbReservas);
            this.Controls.Add(this.lbApellidos);
            this.Controls.Add(this.lbNombre);
            this.Controls.Add(this.lbdni);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormReservas";
            this.Text = "Reservas- TORDICAN S.L.";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormReservas_FormClosed);
            this.Load += new System.EventHandler(this.FormReservas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridReservas)).EndInit();
            this.gbElAct.ResumeLayout(false);
            this.gbElAct.PerformLayout();
            this.gpOpcReserva.ResumeLayout(false);
            this.gbCaza.ResumeLayout(false);
            this.panelCaza.ResumeLayout(false);
            this.panelCaza.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaza)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbdni;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.Label lbApellidos;
        private System.Windows.Forms.Label lbReservas;
        private System.Windows.Forms.TextBox tbNif;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.TextBox tbApellidos;
        private System.Windows.Forms.Label lbFIngreso;
        private System.Windows.Forms.Label lFSalida;
        private System.Windows.Forms.MonthCalendar ElegirMes;
        private System.Windows.Forms.MaskedTextBox tbFechaIng;
        private System.Windows.Forms.MaskedTextBox tbFechaSal;
        private System.Windows.Forms.Button btFechaIng;
        private System.Windows.Forms.Button btFechaSal;
        private System.Windows.Forms.MaskedTextBox tbFechaRes;
        private System.Windows.Forms.Button btElegirRes;
        private System.Windows.Forms.Button btNueva;
        private System.Windows.Forms.Button btBuscar;
        private System.Windows.Forms.Button btElimiar;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button BtBuscarDNI;
        private System.Windows.Forms.DataGridView dataGridReservas;
        private System.Windows.Forms.ComboBox cbHabitacion;
        private System.Windows.Forms.ComboBox cbPension;
        private System.Windows.Forms.Label labhab;
        private System.Windows.Forms.Label labPens;
        private System.Windows.Forms.Button tbResCaza;
        private System.Windows.Forms.Button btActividades;
        private System.Windows.Forms.GroupBox gbElAct;
        private System.Windows.Forms.RadioButton rbParque;
        private System.Windows.Forms.RadioButton rbRapel;
        private System.Windows.Forms.RadioButton rbPirag;
        private System.Windows.Forms.RadioButton rbTiro;
        private System.Windows.Forms.RadioButton rbPasCab;
        private System.Windows.Forms.RadioButton rbBici;
        private System.Windows.Forms.RadioButton rbQuad;
        private System.Windows.Forms.RadioButton rbLocalidad;
        private System.Windows.Forms.TextBox tblibre;
        private System.Windows.Forms.Label lbLibres;
        private System.Windows.Forms.TextBox tbOcupadas;
        private System.Windows.Forms.Label lbOcupadas;
        private System.Windows.Forms.CheckBox cbPlazaParking;
        private System.Windows.Forms.GroupBox gpOpcReserva;
        private System.Windows.Forms.GroupBox gbCaza;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Panel panelCaza;
        private System.Windows.Forms.Button btcerrarCaza;
        private System.Windows.Forms.DataGridView dgvCaza;
        private System.Windows.Forms.Button btVerDetalleCaza;
        private System.Windows.Forms.Label lbCaza;
        private System.Windows.Forms.TextBox tbSubCaza;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reserva;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ingreso;
        private System.Windows.Forms.DataGridViewTextBoxColumn Planta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Habitacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumDias;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioH;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioAct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Parking;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn AReserva;
        private System.Windows.Forms.DataGridViewTextBoxColumn aFecReserva;
        private System.Windows.Forms.DataGridViewTextBoxColumn TArea;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrArea;
        private System.Windows.Forms.DataGridViewTextBoxColumn TCaza;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrTipoCaza;
        private System.Windows.Forms.DataGridViewTextBoxColumn tComida;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrComida;
        private System.Windows.Forms.DataGridViewTextBoxColumn numEsc;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioPerrera;
        private System.Windows.Forms.DataGridViewTextBoxColumn parkingRemolque;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
    }
}