﻿namespace InterfazDeUsuario
{
    partial class FormClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbClientes = new System.Windows.Forms.GroupBox();
            this.btLimpiar = new System.Windows.Forms.Button();
            this.btSalirCli = new System.Windows.Forms.Button();
            this.dataGridClientes = new System.Windows.Forms.DataGridView();
            this.dgDniCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgNombreCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgApe1Cli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgApe2Cli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgDirCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgPaisCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgProvCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgTlfnCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgEmailCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbDatosPersCli = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbProvinciaCliente = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbDniCliente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPaisCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDirCliente = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbApe2Cliente = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbNombreCliente = new System.Windows.Forms.TextBox();
            this.tbApe1Cliente = new System.Windows.Forms.TextBox();
            this.groupOpcionesDeBusquedaCli = new System.Windows.Forms.GroupBox();
            this.cbFiltroCliente = new System.Windows.Forms.ComboBox();
            this.tbBusquedaCliente = new System.Windows.Forms.TextBox();
            this.btBuscarCliente = new System.Windows.Forms.Button();
            this.gbOpcionesCliente = new System.Windows.Forms.GroupBox();
            this.btModificarCliente = new System.Windows.Forms.Button();
            this.btAltaCliente = new System.Windows.Forms.Button();
            this.btBajaCliente = new System.Windows.Forms.Button();
            this.gbDatosContactoCliente = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbEmailCliente = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbTlfnCliente = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.gbClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridClientes)).BeginInit();
            this.gbDatosPersCli.SuspendLayout();
            this.groupOpcionesDeBusquedaCli.SuspendLayout();
            this.gbOpcionesCliente.SuspendLayout();
            this.gbDatosContactoCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gbClientes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(867, 610);
            this.panel1.TabIndex = 6;
            // 
            // gbClientes
            // 
            this.gbClientes.Controls.Add(this.btLimpiar);
            this.gbClientes.Controls.Add(this.btSalirCli);
            this.gbClientes.Controls.Add(this.dataGridClientes);
            this.gbClientes.Controls.Add(this.gbDatosPersCli);
            this.gbClientes.Controls.Add(this.groupOpcionesDeBusquedaCli);
            this.gbClientes.Controls.Add(this.gbOpcionesCliente);
            this.gbClientes.Controls.Add(this.gbDatosContactoCliente);
            this.gbClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbClientes.Location = new System.Drawing.Point(0, 0);
            this.gbClientes.Name = "gbClientes";
            this.gbClientes.Size = new System.Drawing.Size(867, 610);
            this.gbClientes.TabIndex = 21;
            this.gbClientes.TabStop = false;
            // 
            // btLimpiar
            // 
            this.btLimpiar.Location = new System.Drawing.Point(299, 206);
            this.btLimpiar.Name = "btLimpiar";
            this.btLimpiar.Size = new System.Drawing.Size(92, 81);
            this.btLimpiar.TabIndex = 50;
            this.btLimpiar.Text = "LIMPIAR";
            this.btLimpiar.UseVisualStyleBackColor = true;
            this.btLimpiar.Click += new System.EventHandler(this.btLimpiar_Click);
            // 
            // btSalirCli
            // 
            this.btSalirCli.Location = new System.Drawing.Point(467, 560);
            this.btSalirCli.Name = "btSalirCli";
            this.btSalirCli.Size = new System.Drawing.Size(176, 27);
            this.btSalirCli.TabIndex = 49;
            this.btSalirCli.Text = "Salir";
            this.btSalirCli.UseVisualStyleBackColor = true;
            this.btSalirCli.Click += new System.EventHandler(this.btSalirCli_Click);
            // 
            // dataGridClientes
            // 
            this.dataGridClientes.AllowUserToAddRows = false;
            this.dataGridClientes.AllowUserToDeleteRows = false;
            this.dataGridClientes.AllowUserToResizeColumns = false;
            this.dataGridClientes.AllowUserToResizeRows = false;
            this.dataGridClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgDniCli,
            this.dgNombreCli,
            this.dgApe1Cli,
            this.dgApe2Cli,
            this.dgDirCli,
            this.dgPaisCli,
            this.dgProvCli,
            this.dgTlfnCli,
            this.dgEmailCli});
            this.dataGridClientes.Location = new System.Drawing.Point(23, 318);
            this.dataGridClientes.MultiSelect = false;
            this.dataGridClientes.Name = "dataGridClientes";
            this.dataGridClientes.ReadOnly = true;
            this.dataGridClientes.Size = new System.Drawing.Size(644, 225);
            this.dataGridClientes.TabIndex = 16;
            this.dataGridClientes.Visible = false;
            this.dataGridClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridClientes_CellContentClick);
            this.dataGridClientes.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridClientes_RowHeaderMouseClick_1);
            // 
            // dgDniCli
            // 
            this.dgDniCli.HeaderText = "DNI";
            this.dgDniCli.Name = "dgDniCli";
            this.dgDniCli.ReadOnly = true;
            // 
            // dgNombreCli
            // 
            this.dgNombreCli.HeaderText = "Nombre";
            this.dgNombreCli.Name = "dgNombreCli";
            this.dgNombreCli.ReadOnly = true;
            // 
            // dgApe1Cli
            // 
            this.dgApe1Cli.HeaderText = "Primer Apellido";
            this.dgApe1Cli.Name = "dgApe1Cli";
            this.dgApe1Cli.ReadOnly = true;
            // 
            // dgApe2Cli
            // 
            this.dgApe2Cli.HeaderText = "Segundo Apellido";
            this.dgApe2Cli.Name = "dgApe2Cli";
            this.dgApe2Cli.ReadOnly = true;
            // 
            // dgDirCli
            // 
            this.dgDirCli.HeaderText = "Dirección";
            this.dgDirCli.Name = "dgDirCli";
            this.dgDirCli.ReadOnly = true;
            // 
            // dgPaisCli
            // 
            this.dgPaisCli.HeaderText = "País";
            this.dgPaisCli.Name = "dgPaisCli";
            this.dgPaisCli.ReadOnly = true;
            // 
            // dgProvCli
            // 
            this.dgProvCli.HeaderText = "Provicia";
            this.dgProvCli.Name = "dgProvCli";
            this.dgProvCli.ReadOnly = true;
            // 
            // dgTlfnCli
            // 
            this.dgTlfnCli.HeaderText = "Teléfono";
            this.dgTlfnCli.Name = "dgTlfnCli";
            this.dgTlfnCli.ReadOnly = true;
            // 
            // dgEmailCli
            // 
            this.dgEmailCli.HeaderText = "Correo Electrónico";
            this.dgEmailCli.Name = "dgEmailCli";
            this.dgEmailCli.ReadOnly = true;
            // 
            // gbDatosPersCli
            // 
            this.gbDatosPersCli.Controls.Add(this.label5);
            this.gbDatosPersCli.Controls.Add(this.tbProvinciaCliente);
            this.gbDatosPersCli.Controls.Add(this.label14);
            this.gbDatosPersCli.Controls.Add(this.tbDniCliente);
            this.gbDatosPersCli.Controls.Add(this.label6);
            this.gbDatosPersCli.Controls.Add(this.tbPaisCliente);
            this.gbDatosPersCli.Controls.Add(this.label1);
            this.gbDatosPersCli.Controls.Add(this.tbDirCliente);
            this.gbDatosPersCli.Controls.Add(this.label15);
            this.gbDatosPersCli.Controls.Add(this.label16);
            this.gbDatosPersCli.Controls.Add(this.tbApe2Cliente);
            this.gbDatosPersCli.Controls.Add(this.label17);
            this.gbDatosPersCli.Controls.Add(this.tbNombreCliente);
            this.gbDatosPersCli.Controls.Add(this.tbApe1Cliente);
            this.gbDatosPersCli.Location = new System.Drawing.Point(23, 70);
            this.gbDatosPersCli.Name = "gbDatosPersCli";
            this.gbDatosPersCli.Size = new System.Drawing.Size(648, 107);
            this.gbDatosPersCli.TabIndex = 48;
            this.gbDatosPersCli.TabStop = false;
            this.gbDatosPersCli.Text = "Datos Personales";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(473, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Provincia";
            // 
            // tbProvinciaCliente
            // 
            this.tbProvinciaCliente.Location = new System.Drawing.Point(475, 73);
            this.tbProvinciaCliente.Name = "tbProvinciaCliente";
            this.tbProvinciaCliente.Size = new System.Drawing.Size(169, 20);
            this.tbProvinciaCliente.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "DNI*";
            // 
            // tbDniCliente
            // 
            this.tbDniCliente.Location = new System.Drawing.Point(6, 32);
            this.tbDniCliente.MaxLength = 9;
            this.tbDniCliente.Name = "tbDniCliente";
            this.tbDniCliente.Size = new System.Drawing.Size(142, 20);
            this.tbDniCliente.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(308, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "País";
            // 
            // tbPaisCliente
            // 
            this.tbPaisCliente.Location = new System.Drawing.Point(311, 73);
            this.tbPaisCliente.Name = "tbPaisCliente";
            this.tbPaisCliente.Size = new System.Drawing.Size(159, 20);
            this.tbPaisCliente.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Dirección";
            // 
            // tbDirCliente
            // 
            this.tbDirCliente.Location = new System.Drawing.Point(6, 73);
            this.tbDirCliente.Name = "tbDirCliente";
            this.tbDirCliente.Size = new System.Drawing.Size(294, 20);
            this.tbDirCliente.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(511, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Segundo Apellido";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(375, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Primer Apellido*";
            // 
            // tbApe2Cliente
            // 
            this.tbApe2Cliente.Location = new System.Drawing.Point(514, 31);
            this.tbApe2Cliente.Name = "tbApe2Cliente";
            this.tbApe2Cliente.Size = new System.Drawing.Size(130, 20);
            this.tbApe2Cliente.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(162, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Nombre*";
            // 
            // tbNombreCliente
            // 
            this.tbNombreCliente.Location = new System.Drawing.Point(162, 32);
            this.tbNombreCliente.Name = "tbNombreCliente";
            this.tbNombreCliente.Size = new System.Drawing.Size(207, 20);
            this.tbNombreCliente.TabIndex = 4;
            // 
            // tbApe1Cliente
            // 
            this.tbApe1Cliente.Location = new System.Drawing.Point(378, 31);
            this.tbApe1Cliente.Name = "tbApe1Cliente";
            this.tbApe1Cliente.Size = new System.Drawing.Size(130, 20);
            this.tbApe1Cliente.TabIndex = 5;
            // 
            // groupOpcionesDeBusquedaCli
            // 
            this.groupOpcionesDeBusquedaCli.Controls.Add(this.cbFiltroCliente);
            this.groupOpcionesDeBusquedaCli.Controls.Add(this.tbBusquedaCliente);
            this.groupOpcionesDeBusquedaCli.Controls.Add(this.btBuscarCliente);
            this.groupOpcionesDeBusquedaCli.Location = new System.Drawing.Point(23, 10);
            this.groupOpcionesDeBusquedaCli.Name = "groupOpcionesDeBusquedaCli";
            this.groupOpcionesDeBusquedaCli.Size = new System.Drawing.Size(648, 54);
            this.groupOpcionesDeBusquedaCli.TabIndex = 47;
            this.groupOpcionesDeBusquedaCli.TabStop = false;
            this.groupOpcionesDeBusquedaCli.Text = "Opciones de Búsqueda";
            // 
            // cbFiltroCliente
            // 
            this.cbFiltroCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroCliente.FormattingEnabled = true;
            this.cbFiltroCliente.Items.AddRange(new object[] {
            "Dni",
            "Nombre",
            "Primer Apellido",
            "Segundo Apellido",
            "Dirección",
            "País",
            "Provincia",
            "Teléfono",
            "Correo Electrónico"});
            this.cbFiltroCliente.Location = new System.Drawing.Point(252, 17);
            this.cbFiltroCliente.Name = "cbFiltroCliente";
            this.cbFiltroCliente.Size = new System.Drawing.Size(143, 21);
            this.cbFiltroCliente.TabIndex = 1;
            // 
            // tbBusquedaCliente
            // 
            this.tbBusquedaCliente.Location = new System.Drawing.Point(7, 18);
            this.tbBusquedaCliente.Name = "tbBusquedaCliente";
            this.tbBusquedaCliente.Size = new System.Drawing.Size(234, 20);
            this.tbBusquedaCliente.TabIndex = 0;
            // 
            // btBuscarCliente
            // 
            this.btBuscarCliente.Location = new System.Drawing.Point(455, 18);
            this.btBuscarCliente.Name = "btBuscarCliente";
            this.btBuscarCliente.Size = new System.Drawing.Size(187, 23);
            this.btBuscarCliente.TabIndex = 2;
            this.btBuscarCliente.Text = "Buscar";
            this.btBuscarCliente.UseVisualStyleBackColor = true;
            this.btBuscarCliente.Click += new System.EventHandler(this.btBuscarCliente_Click);
            // 
            // gbOpcionesCliente
            // 
            this.gbOpcionesCliente.Controls.Add(this.btModificarCliente);
            this.gbOpcionesCliente.Controls.Add(this.btAltaCliente);
            this.gbOpcionesCliente.Controls.Add(this.btBajaCliente);
            this.gbOpcionesCliente.Location = new System.Drawing.Point(410, 193);
            this.gbOpcionesCliente.Name = "gbOpcionesCliente";
            this.gbOpcionesCliente.Size = new System.Drawing.Size(261, 109);
            this.gbOpcionesCliente.TabIndex = 29;
            this.gbOpcionesCliente.TabStop = false;
            // 
            // btModificarCliente
            // 
            this.btModificarCliente.Location = new System.Drawing.Point(36, 72);
            this.btModificarCliente.Name = "btModificarCliente";
            this.btModificarCliente.Size = new System.Drawing.Size(188, 23);
            this.btModificarCliente.TabIndex = 15;
            this.btModificarCliente.Text = "Modificar";
            this.btModificarCliente.UseVisualStyleBackColor = true;
            this.btModificarCliente.Click += new System.EventHandler(this.btModificarCliente_Click);
            // 
            // btAltaCliente
            // 
            this.btAltaCliente.Location = new System.Drawing.Point(36, 14);
            this.btAltaCliente.Name = "btAltaCliente";
            this.btAltaCliente.Size = new System.Drawing.Size(188, 23);
            this.btAltaCliente.TabIndex = 13;
            this.btAltaCliente.Text = "Alta";
            this.btAltaCliente.UseVisualStyleBackColor = true;
            this.btAltaCliente.Click += new System.EventHandler(this.btAltaCliente_Click);
            // 
            // btBajaCliente
            // 
            this.btBajaCliente.Location = new System.Drawing.Point(36, 43);
            this.btBajaCliente.Name = "btBajaCliente";
            this.btBajaCliente.Size = new System.Drawing.Size(188, 23);
            this.btBajaCliente.TabIndex = 14;
            this.btBajaCliente.Text = "Baja";
            this.btBajaCliente.UseVisualStyleBackColor = true;
            this.btBajaCliente.Click += new System.EventHandler(this.btBajaCliente_Click);
            // 
            // gbDatosContactoCliente
            // 
            this.gbDatosContactoCliente.Controls.Add(this.label11);
            this.gbDatosContactoCliente.Controls.Add(this.tbEmailCliente);
            this.gbDatosContactoCliente.Controls.Add(this.label12);
            this.gbDatosContactoCliente.Controls.Add(this.tbTlfnCliente);
            this.gbDatosContactoCliente.Location = new System.Drawing.Point(23, 193);
            this.gbDatosContactoCliente.Name = "gbDatosContactoCliente";
            this.gbDatosContactoCliente.Size = new System.Drawing.Size(248, 109);
            this.gbDatosContactoCliente.TabIndex = 28;
            this.gbDatosContactoCliente.TabStop = false;
            this.gbDatosContactoCliente.Text = "Datos de Contacto";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Correo Electrónico";
            // 
            // tbEmailCliente
            // 
            this.tbEmailCliente.Location = new System.Drawing.Point(7, 77);
            this.tbEmailCliente.Name = "tbEmailCliente";
            this.tbEmailCliente.Size = new System.Drawing.Size(220, 20);
            this.tbEmailCliente.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Teléfono";
            // 
            // tbTlfnCliente
            // 
            this.tbTlfnCliente.Location = new System.Drawing.Point(9, 33);
            this.tbTlfnCliente.Name = "tbTlfnCliente";
            this.tbTlfnCliente.Size = new System.Drawing.Size(218, 20);
            this.tbTlfnCliente.TabIndex = 10;
            // 
            // FormClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 610);
            this.Controls.Add(this.panel1);
            this.Name = "FormClientes";
            this.Text = "FormClientes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormClientes_FormClosed);
            this.Load += new System.EventHandler(this.FormClientes_Load);
            this.panel1.ResumeLayout(false);
            this.gbClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridClientes)).EndInit();
            this.gbDatosPersCli.ResumeLayout(false);
            this.gbDatosPersCli.PerformLayout();
            this.groupOpcionesDeBusquedaCli.ResumeLayout(false);
            this.groupOpcionesDeBusquedaCli.PerformLayout();
            this.gbOpcionesCliente.ResumeLayout(false);
            this.gbDatosContactoCliente.ResumeLayout(false);
            this.gbDatosContactoCliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gbClientes;
        private System.Windows.Forms.GroupBox gbOpcionesCliente;
        private System.Windows.Forms.GroupBox gbDatosContactoCliente;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbEmailCliente;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbTlfnCliente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbProvinciaCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPaisCliente;
        private System.Windows.Forms.GroupBox groupOpcionesDeBusquedaCli;
        private System.Windows.Forms.ComboBox cbFiltroCliente;
        private System.Windows.Forms.TextBox tbBusquedaCliente;
        private System.Windows.Forms.Button btBuscarCliente;
        private System.Windows.Forms.GroupBox gbDatosPersCli;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbDniCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDirCliente;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbApe2Cliente;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbNombreCliente;
        private System.Windows.Forms.TextBox tbApe1Cliente;
        private System.Windows.Forms.Button btModificarCliente;
        private System.Windows.Forms.Button btAltaCliente;
        private System.Windows.Forms.Button btBajaCliente;
        private System.Windows.Forms.DataGridView dataGridClientes;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgDniCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgNombreCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgApe1Cli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgApe2Cli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgDirCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPaisCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgProvCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgTlfnCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgEmailCli;
        private System.Windows.Forms.Button btSalirCli;
        private System.Windows.Forms.Button btLimpiar;
    }
}