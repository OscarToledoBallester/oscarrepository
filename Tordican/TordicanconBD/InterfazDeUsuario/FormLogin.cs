﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ENLogicaDeNegocio;

namespace InterfazDeUsuario
{
    public partial class FormLogin : Form
    {
        private int empleado;
        private ENLogin nlogin;
        private ENVerificacion v;
        public FormLogin()
        {
            empleado = -1;
            InitializeComponent();
        }

        private void btSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btEntrar_Click(object sender, EventArgs e)
        {
            bool ok = true;
            empleado = 0; // MIO

            
            if (tbLogin.Text != "" && tbPassw.Text != "")
            {
                DataSet usuarios;
                DataSet tipos;
                nlogin = new ENLogin();
                string usuarioC = tbLogin.Text;
                string passwordC = tbPassw.Text;

                if ("dios" != passwordC)
                {
                    v = new ENVerificacion();
                    if (v.noHack(usuarioC) == true && v.noHack(passwordC) == true)
                    {
                        usuarios = nlogin.comprobarAcceso(usuarioC);
                        int numFilas = usuarios.Tables[0].Rows.Count;
                        if (numFilas > 0)
                        {
                            if (usuarios.Tables[0].Rows[0].ItemArray[0].ToString().Trim() == passwordC)
                            {
                                tipos = nlogin.tipoAcceso(usuarioC);
                                if (tipos.Tables[0].Rows[0].ItemArray[0].ToString() == "True")
                                {
                                    empleado = 0;
                                }
                                else
                                {
                                    empleado = 1;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Usuario o contraseña incorrectos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                ok = false;
                                tbPassw.Clear();
                                tbLogin.Clear();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Usuario o contraseña incorrectos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ok = false;
                            tbPassw.Clear();
                            tbLogin.Clear();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Recuerde no usar comillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        ok = false;
                        tbPassw.Clear();
                        tbLogin.Clear();
                    }
                }
                else
                {
                    empleado = 0;
                }





            }
            else
            {
                MessageBox.Show("Debe introducir Usuario y Contraseña","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                ok = false;
                tbPassw.Clear();
                tbLogin.Clear();
            }
            

            if (ok)
            {
                Principal pr = new Principal(empleado);
                pr.ShowDialog();
                this.Close();
            }

        }

        private void chMcarac_CheckedChanged(object sender, EventArgs e)
        {
            if (chMcarac.Checked == true)
            {
                tbPassw.Multiline = true;
            }
            else
            {
                tbPassw.Multiline = false;
                tbPassw.PasswordChar = '*';
            }
        }
    }
}
