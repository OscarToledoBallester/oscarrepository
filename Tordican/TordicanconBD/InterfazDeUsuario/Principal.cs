﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InterfazDeUsuario
{
    public partial class Principal : Form
    {
        public static int tipoEmp = -1;//Segun el tipo de empleado --> 0- Poder total; 1- Poder Administrativo 2- El lameculos recepcionista.
        private static Button btClientes, btActividades, btPersonal, btReservas, btCoto, btInformes;
        public static FormPersonal formPersonal;
        public static FormClientes formClientes;
        public static FormActividades formActividades;
        public static FormReservas formReservas;
 
        public static FormInformes formInformes;


        public Principal(int tipo)
        {
            formPersonal = null;
            formClientes = null;
            formActividades = null;
            formReservas = null;
         
            formInformes = null;
            
            InitializeComponent();
            tipoEmp = tipo;
            InicializarBotones();
        }

        private void InicializarBotones()
        {
            btClientes = new Button();
            btClientes.Name = "btClientes";
            btClientes.Text = "CLIENTES";
            btClientes.Click += new EventHandler(btClientes_Click);

            btActividades = new Button();
            btActividades.Name = "btActividades";
            btActividades.Text = "ACTIVIDADES";
            btActividades.Click += new EventHandler(btActividades_Click);

            btPersonal = new Button();
            btPersonal.Name = "btPersonal";
            btPersonal.Text = "PERSONAL";
            btPersonal.Click += new EventHandler(btPersonal_Click);

            btReservas = new Button();
            btReservas.Name = "btReservas";
            btReservas.Text = "RESERVAS";
            btReservas.Click += new EventHandler(btReservas_Click);

   

            btInformes = new Button();
            btInformes.Name = "btInformes";
            btInformes.Text = "INFORMES";
            btInformes.Click += new EventHandler(btInformes_Click);

            if (tipoEmp == 1)//Recepcionista
            {
                btPersonal.Enabled = false;

                this.Text = "Tordican S.L.- Recepcion";
            }
            else
            {
                this.Text = "Tordican S.L- Administrador";
            }

            CambioDerecha();
        }


        private void FormTordicanSL_MdiChildActivate(object sender, EventArgs e)
        {
            CambioIzquierda();
        }


 

        private void btInformes_Click(object sender, EventArgs e)
        {
            if (formInformes == null)
            {
                formInformes = new FormInformes();
                formInformes.MdiParent = this;
                this.tbTitulo.Text = "Informes";
                formInformes.Show();
            }
            else
            {
                formInformes.Activate();
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("¿Desea salir de la aplicación?", "Salir", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (res == DialogResult.OK)
                Application.Exit();
        }

        private void CambioIzquierda()//Al panel de la izquierda
        {
            int j = 90;
            int iniY = 0;

            btClientes.Location = new Point(35, iniY + j);
            btClientes.Size = new Size(120, 70);
            panelOpciones.Controls.Add(btClientes);

            btActividades.Location = new Point(35, iniY + 2 * j);
            btActividades.Size = new Size(120, 70);
            panelOpciones.Controls.Add(btActividades);

            btPersonal.Location = new Point(35, iniY + 3 * j);
            btPersonal.Size = new Size(120, 70);
            panelOpciones.Controls.Add(btPersonal);

            btReservas.Location = new Point(35, iniY + 4 * j);
            btReservas.Size = new Size(120, 70);
            panelOpciones.Controls.Add(btReservas);



            btInformes.Location = new Point(35, iniY + 6 * j);
            btInformes.Size = new Size(120, 70);
            panelOpciones.Controls.Add(btInformes);
        }

        public void CambioDerecha()//Otra vez dentro del form
        {
            int i = 150;
            int j = 150;

            btClientes.Location = new Point(i + j, 500);
            btClientes.Size = new Size(100, 100);
            this.Controls.Add(btClientes);

            btActividades.Location = new Point(i + 2 * j, 500);
            btActividades.Size = new Size(100, 100);
            this.Controls.Add(btActividades);

            btPersonal.Location = new Point(i + 3 * j, 500);
            btPersonal.Size = new Size(100, 100);
            this.Controls.Add(btPersonal);

            btReservas.Location = new Point(i + 4 * j, 500);
            btReservas.Size = new Size(100, 100);
            this.Controls.Add(btReservas);


            btInformes.Location = new Point(i + 5 * j, 500);
            btInformes.Size = new Size(100, 100);
            this.Controls.Add(btInformes);

            btInformes.Location = new Point(i + 6 * j, 500);
            btInformes.Size = new Size(100, 100);
            this.Controls.Add(btInformes);
        }


        private void btClientes_Click(object sender, EventArgs e)
        {
            if (formClientes == null)
            {
                formClientes = new FormClientes();
                formClientes.MdiParent = this;
                this.tbTitulo.Text = "Clientes";
                formClientes.Show();

            }
            else
            {
                formClientes.Activate();


            }

            
        }

        private void btReservas_Click(object sender, EventArgs e)
        {
            if (formReservas == null)
            {
                formReservas = new FormReservas();
                formReservas.MdiParent = this;
                this.tbTitulo.Text = "Reservas";
                formReservas.Show();

            }
            else
            {
                formReservas.Activate();


            }
        }

        private void btActividades_Click(object sender, EventArgs e)
        {
            if (formActividades == null)
            {
                formActividades = new FormActividades();
                formActividades.MdiParent = this;
                this.tbTitulo.Text = "Actividades";
                formActividades.Show();

            }
            else
            {
                formActividades.Activate();


            }
        }

        private void btPersonal_Click(object sender, EventArgs e)
        {
            if (formPersonal == null)
            {
                formPersonal = new FormPersonal();
                formPersonal.MdiParent = this;
                this.tbTitulo.Text = "Personal";
                formPersonal.Show();

            }
            else
            {
                formPersonal.Activate();


            }
        }

        private void btHotel_Click(object sender, EventArgs e)
        {
            FormHotel nuevo = new FormHotel();
            nuevo.MdiParent = this;
            this.tbTitulo.Text = "Hotel";
            nuevo.Show();
        }
        
        public static void ActivarDesactivarBotones(bool activado)
        {
            if(activado)
            {

                btClientes.Enabled = false;
                btActividades.Enabled = false; 
                btPersonal.Enabled = false;
                btReservas.Enabled = false;
                btCoto.Enabled = false;
            }
            else
            {

                btClientes.Enabled = true;
                btActividades.Enabled = true;
                btPersonal.Enabled = true;
                btReservas.Enabled = true;
                btCoto.Enabled = true;
            }
        }

        private void Principal_Load(object sender, EventArgs e)
        {
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void acercadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAcercaDe n = new FormAcercaDe();
            n.ShowDialog();
        }
    }
}
