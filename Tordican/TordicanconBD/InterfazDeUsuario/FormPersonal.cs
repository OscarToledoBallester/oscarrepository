﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ENLogicaDeNegocio;


namespace InterfazDeUsuario
{
    public partial class FormPersonal : Form
    {
        ENVerificacion v;
        ENPersonal personal;
        public FormPersonal()
        {
            InitializeComponent();
            cbFiltroPersonal.SelectedIndex = 0;
            cbTipoPersonal.SelectedIndex = 0;
            InicializaTabla();
        }

       

       
        
        private void InicializaTabla()
        {
            

            
            int filas = dataGridPersonal.Rows.Count;
            if (dataGridPersonal.RowCount > 0)
                for (int i = 0; i < filas; i++)
                {
                    dataGridPersonal.Rows.Clear();
                }
            personal = new ENPersonal();
            DataSet todos;
            int numFilas;
            string tipoE = "Empleado";
            todos = personal.mostrarTodos();
            numFilas = todos.Tables[0].Rows.Count;
            if (todos != null && numFilas > 0)
            {
                for (int i = 0; i < numFilas; i++)
                {
                    if(todos.Tables[0].Rows[i].ItemArray[11].ToString() == "True")
                    {
                        tipoE = "Administración";
                    }else
                    {
                        tipoE = "Empleado";
                    }
                    dataGridPersonal.Rows.Add(todos.Tables[0].Rows[i].ItemArray[0].ToString(), todos.Tables[0].Rows[i].ItemArray[1].ToString(), todos.Tables[0].Rows[i].ItemArray[2].ToString(), todos.Tables[0].Rows[i].ItemArray[3].ToString(), todos.Tables[0].Rows[i].ItemArray[4].ToString(), todos.Tables[0].Rows[i].ItemArray[5].ToString(), todos.Tables[0].Rows[i].ItemArray[6].ToString(), todos.Tables[0].Rows[i].ItemArray[7].ToString(), todos.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE , todos.Tables[0].Rows[i].ItemArray[12].ToString());
                   
                }

            }
        }
        


        private void btPersonalBuscar_Click_1(object sender, EventArgs e)
        {
            //Buscamos según los dos filtros
            string buscar = tbBusquedaPersonal.Text.Trim();
            string campo = (string)cbFiltroPersonal.SelectedItem;
            string tipo = (string)cbTipoPersonal.SelectedItem;
            bool bien = true;
            int filas = dataGridPersonal.Rows.Count;
            if (dataGridPersonal.RowCount > 0)
                for (int i = 0; i < filas; i++)
                {
                    dataGridPersonal.Rows.Clear();
                }

            DataSet mostrar;
            int numFilas = 0;
            string tipoE = "";
            v = new ENVerificacion();
            personal = new ENPersonal();

            if (tbBusquedaPersonal.Visible == true && tbBusquedaPersonal.Text == "")
            {
                MessageBox.Show("Inserte patrón para la busqueda", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (tipo != "Todos" && tipo != "Empleado" && tipo != "Administración")
                {
                    MessageBox.Show("Seleccione tipo para busqueda", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if(v.noHack(buscar) == true)
                    {
                        switch (campo)
                        {
                            case "DNI":

                                mostrar = personal.busquedaPersonalDni(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.dni(buscar) == true)
                                {

                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.dni(buscar) == false)
                                    {
                                        MessageBox.Show("Dni incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "Nombre":
                                mostrar = personal.busquedaPersonalNom(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.cadena(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.cadena(buscar) == false)
                                    {
                                        MessageBox.Show("Nombre incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "Primer Apellido":
                                mostrar = personal.busquedaPersonalAp1(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.cadena(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.cadena(buscar) == false)
                                    {
                                        MessageBox.Show("Apellido incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "Segundo Apellido":
                                mostrar = personal.busquedaPersonalAp2(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.cadena(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.cadena(buscar) == false)
                                    {
                                        MessageBox.Show("Apellido incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "Teléfono":
                                mostrar = personal.busquedaPersonalTlf(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.telefono(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.telefono(buscar) == false)
                                    {
                                        MessageBox.Show("Teléfono incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "Correo Electrónico":
                                mostrar = personal.busquedaPersonalEMail(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.email(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.email(buscar) == false)
                                    {
                                        MessageBox.Show("Correo Electrónico incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "País":
                                mostrar = personal.busquedaPersonalPais(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.cadena(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.cadena(buscar) == false)
                                    {
                                        bien = false;
                                        MessageBox.Show("País incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                break;
                            case "Provincia":
                                mostrar = personal.busquedaPersonalProv(tipo, buscar);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0 && v.cadena(buscar) == true)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                else
                                {
                                    if (v.cadena(buscar) == false)
                                    {
                                        MessageBox.Show("Provincia incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        bien = false;
                                    }
                                }
                                break;
                            case "Tipo":
                                mostrar = personal.busquedaPersonalTipo(tipo);
                                numFilas = mostrar.Tables[0].Rows.Count;
                                if (mostrar != null && numFilas > 0)
                                {
                                    for (int i = 0; i < numFilas; i++)
                                    {
                                        if (mostrar.Tables[0].Rows[i].ItemArray[9].ToString() == "True")
                                        {
                                            tipoE = "Administración";
                                        }
                                        else
                                        {
                                            tipoE = "Empleado";
                                        }
                                        dataGridPersonal.Rows.Add(mostrar.Tables[0].Rows[i].ItemArray[0].ToString(), mostrar.Tables[0].Rows[i].ItemArray[1].ToString(), mostrar.Tables[0].Rows[i].ItemArray[2].ToString(), mostrar.Tables[0].Rows[i].ItemArray[3].ToString(), mostrar.Tables[0].Rows[i].ItemArray[4].ToString(), mostrar.Tables[0].Rows[i].ItemArray[5].ToString(), mostrar.Tables[0].Rows[i].ItemArray[6].ToString(), mostrar.Tables[0].Rows[i].ItemArray[7].ToString(), mostrar.Tables[0].Rows[i].ItemArray[8].ToString(), tipoE, mostrar.Tables[0].Rows[i].ItemArray[10].ToString());
                                    }

                                }
                                break;
                            default:
                                MessageBox.Show("Seleccione campo para busqueda", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                        }
                        tbDniPersonal.Text = null;
                        tbNombrePersonal.Text = null;
                        tbPrimerAPersonal.Text = null;
                        tbSegundoAPersonal.Text = null;
                        tbNumSSPersonal.Text = null;
                        tbPaisPersonal.Text = null;
                        tbProvinciaPersonal.Text = null;
                        tbTelefonoPersonal.Text = null;
                        tbCorreoEPersonal.Text = null;
                        tbDireccionPersonal.Text = null;
                        rbAdministracionPersonal.Checked = false;
                        rbEmpleadoPersonal.Checked = false;
                        if (numFilas == 0)
                        {
                            if(bien == true)
                                MessageBox.Show("Ninguna coincidencia", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Cargamos datos...", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }else
                    {
                        MessageBox.Show("Recuerde no utilizar comillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                }
            }
        }

        private void btAltaPersonal_Click(object sender, EventArgs e)
        {
            string DniPersonal = tbDniPersonal.Text.Trim();
            string NombrePersonal = tbNombrePersonal.Text.Trim();
            string PrimerAPersonal = tbPrimerAPersonal.Text.Trim();
            string SegundoAPersonal = tbSegundoAPersonal.Text.Trim();
            string NumSSPersonal = tbNumSSPersonal.Text.Trim();
            string PaisPersonal = tbPaisPersonal.Text.Trim();
            string ProvinciaPersonal = tbProvinciaPersonal.Text.Trim();
            string TelefonoPersonal = tbTelefonoPersonal.Text.Trim();
            string CorreoEPersonal = tbCorreoEPersonal.Text.Trim();
            string DireccionPersonal = tbDireccionPersonal.Text.Trim();
            bool AdministracionPersonal = rbAdministracionPersonal.Checked;
            v = new ENVerificacion();
            //Hacemos una busqueda del cliente por dni y si no está hacemos un alta

            if (DniPersonal == null || NombrePersonal == null || PrimerAPersonal == null || SegundoAPersonal == null || NumSSPersonal == null  || TelefonoPersonal == null ||  (rbAdministracionPersonal.Checked == false && rbEmpleadoPersonal.Checked == false))
            {
                //faltan campos
                MessageBox.Show("Inserte todos los campos obligatorios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //Verificamos campos
                v = new ENVerificacion();
                if (v.dni(DniPersonal) == true)
                {
                    if (v.noHack(NombrePersonal) == true && v.noHack(PrimerAPersonal) && v.noHack(SegundoAPersonal) && v.noHack(DireccionPersonal) && v.noHack(PaisPersonal) && v.noHack(ProvinciaPersonal) && v.noHack(NumSSPersonal) && v.noHack(TelefonoPersonal) && v.noHack(CorreoEPersonal))
                    {
                        if (v.cadena(NombrePersonal) == true && v.cadena(PrimerAPersonal) == true && v.cadena(SegundoAPersonal) == true && (v.cadena(PaisPersonal) == true || PaisPersonal == "") && (v.cadena(ProvinciaPersonal) == true || ProvinciaPersonal == ""))
                        {
                            if (v.email(CorreoEPersonal) == true || CorreoEPersonal == "")
                            {
                                if (v.telefono(TelefonoPersonal) == true)
                                {
                                    if (v.cadenaNum(NumSSPersonal) == true && NumSSPersonal.Length == 12) // v.nssocial(NumSSPersonal) == true)
                                    {
                                        personal = new ENPersonal();
                                        DataSet esta = personal.busquedaPersonalDni("Todos", DniPersonal);
                                        int filas = esta.Tables[0].Rows.Count;
                                        if (filas > 0)
                                        {
                                            MessageBox.Show("Empleado ya insertado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            bool insertado = personal.altaPersonal(DniPersonal, NombrePersonal, PrimerAPersonal, SegundoAPersonal, NumSSPersonal, DireccionPersonal, PaisPersonal, ProvinciaPersonal, TelefonoPersonal, CorreoEPersonal, AdministracionPersonal);
                                            if (insertado == true)
                                            {
                                                MessageBox.Show("Alta Completada", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                tbDniPersonal.Text = null;
                                                tbNombrePersonal.Text = null;
                                                tbPrimerAPersonal.Text = null;
                                                tbSegundoAPersonal.Text = null;
                                                tbNumSSPersonal.Text = null;
                                                tbPaisPersonal.Text = null;
                                                tbProvinciaPersonal.Text = null;
                                                tbTelefonoPersonal.Text = null;
                                                tbCorreoEPersonal.Text = null;
                                                tbDireccionPersonal.Text = null;
                                                rbAdministracionPersonal.Checked = false;
                                                rbEmpleadoPersonal.Checked = false;
                                                int filas2 = dataGridPersonal.Rows.Count;
                                                if (dataGridPersonal.RowCount > 0)
                                                    for (int i = 0; i < filas2; i++)
                                                    {
                                                        dataGridPersonal.Rows.Clear();
                                                    }
                                                InicializaTabla();
                                            }
                                            else
                                            {
                                                MessageBox.Show("No se ha podido completar el alta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Número Seguridad Social erroneo", "Error", MessageBoxButtons.OK);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Teléfono incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Correo Electrónico erroneo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            //Nombre , PrimerA o SegundoA erroneos
                            MessageBox.Show("Nombre, Primer Apellido o Segundo Apellido erroneos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Recuerde no usar comillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    //DNI incorrecto
                    MessageBox.Show("Dni incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btBajaPersonal_Click_1(object sender, EventArgs e)
        {
            //Damos de baja al empleado
            string DniPersonal = tbDniPersonal.Text.Trim();
            v = new ENVerificacion();
            if (DniPersonal != "")
            {
                if (v.noHack(DniPersonal))
                {
                    DataSet mostrar;
                    int numFilas = 0;
                    mostrar = personal.busquedaPersonalDni("Todos", DniPersonal);
                    numFilas = mostrar.Tables[0].Rows.Count;
                    if (numFilas != 0 && v.dni(DniPersonal) == true)
                    {

                        if (MessageBox.Show("Esta seguro que desea dar de baja al empleado con dni " + DniPersonal, "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                        {
                            personal = new ENPersonal();
                            bool baja = personal.bajaPersonal(DniPersonal);
                            tbDniPersonal.Text = null;
                            tbNombrePersonal.Text = null;
                            tbPrimerAPersonal.Text = null;
                            tbSegundoAPersonal.Text = null;
                            tbNumSSPersonal.Text = null;
                            tbPaisPersonal.Text = null;
                            tbProvinciaPersonal.Text = null;
                            tbTelefonoPersonal.Text = null;
                            tbCorreoEPersonal.Text = null;
                            tbDireccionPersonal.Text = null;
                            rbAdministracionPersonal.Checked = false;
                            rbEmpleadoPersonal.Checked = false;
                            int filas = dataGridPersonal.Rows.Count;
                            if (dataGridPersonal.RowCount > 0)
                                for (int i = 0; i < filas; i++)
                                {
                                    dataGridPersonal.Rows.Clear();
                                }
                            InicializaTabla();
                        }
                        else
                        {
                            MessageBox.Show("No se realizó la baja", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No existe el empleado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Recuerde no usar comillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Ningun empleado seleccionado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btModificarPersonal_Click_1(object sender, EventArgs e)
        {
            string DniPersonal = tbDniPersonal.Text.Trim();
            string NombrePersonal = tbNombrePersonal.Text.Trim();
            string PrimerAPersonal = tbPrimerAPersonal.Text.Trim();
            string SegundoAPersonal = tbSegundoAPersonal.Text.Trim();
            string NumSSPersonal = tbNumSSPersonal.Text.Trim();
            string PaisPersonal = tbPaisPersonal.Text.Trim();
            string ProvinciaPersonal = tbProvinciaPersonal.Text.Trim();
            string TelefonoPersonal = tbTelefonoPersonal.Text.Trim();
            string CorreoEPersonal = tbCorreoEPersonal.Text.Trim();
            string DireccionPersonal = tbDireccionPersonal.Text.Trim();
            bool AdministracionPersonal = rbAdministracionPersonal.Checked;
            if (DniPersonal == null)
            {
                //faltan campos
                MessageBox.Show("Ningun empleado seleccionado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (DniPersonal != "")
                {
                    //Modificamos el empleado segun los cambios de los campos
                    v = new ENVerificacion();
                    if (v.dni(DniPersonal) == true && v.noHack(DniPersonal) == true)
                    {
                        personal = new ENPersonal();
                        DataSet esta = personal.busquedaPersonalDni("Todos", DniPersonal);
                        int filas = esta.Tables[0].Rows.Count;
                        if (filas > 0)
                        {
                            if (v.noHack(NombrePersonal) == true && v.noHack(PrimerAPersonal) && v.noHack(SegundoAPersonal) && v.noHack(DireccionPersonal) && v.noHack(PaisPersonal) && v.noHack(ProvinciaPersonal) && v.noHack(NumSSPersonal) && v.noHack(TelefonoPersonal) && v.noHack(CorreoEPersonal))
                            {
                                if (v.cadena(NombrePersonal) == true && NombrePersonal != "" && v.cadena(PrimerAPersonal) == true && PrimerAPersonal != "" && v.cadena(SegundoAPersonal) == true && SegundoAPersonal != "" && v.cadena(PaisPersonal) == true && v.cadena(ProvinciaPersonal) == true)
                                {
                                    if (v.email(CorreoEPersonal) == true || CorreoEPersonal == "")
                                    {
                                        if (v.telefono(TelefonoPersonal) == true && TelefonoPersonal != "")
                                        {
                                            if (v.cadenaNum(NumSSPersonal) == true && NumSSPersonal != "" && NumSSPersonal.Length == 12) //  v.nssocial(NumSSPersonal) == true)
                                            {
                                                bool insertado = personal.modificarPersonal(DniPersonal, NombrePersonal, PrimerAPersonal, SegundoAPersonal, NumSSPersonal, DireccionPersonal, PaisPersonal, ProvinciaPersonal, TelefonoPersonal, CorreoEPersonal, AdministracionPersonal);
                                                if (insertado == true)
                                                {
                                                    MessageBox.Show("Empleado modificado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                    tbDniPersonal.Text = null;
                                                    tbNombrePersonal.Text = null;
                                                    tbPrimerAPersonal.Text = null;
                                                    tbSegundoAPersonal.Text = null;
                                                    tbNumSSPersonal.Text = null;
                                                    tbPaisPersonal.Text = null;
                                                    tbProvinciaPersonal.Text = null;
                                                    tbTelefonoPersonal.Text = null;
                                                    tbCorreoEPersonal.Text = null;
                                                    tbDireccionPersonal.Text = null;
                                                    rbAdministracionPersonal.Checked = false;
                                                    rbEmpleadoPersonal.Checked = false;
                                                    int filas2 = dataGridPersonal.Rows.Count;
                                                    if (dataGridPersonal.RowCount > 0)
                                                        for (int i = 0; i < filas2; i++)
                                                        {
                                                            dataGridPersonal.Rows.Clear();
                                                        }
                                                    InicializaTabla();
                                                }
                                                else
                                                {
                                                    MessageBox.Show("No se ha podido completar el alta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                }

                                            }
                                            else
                                            {
                                                MessageBox.Show("Número Seguridad Social erroneo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Teléfono incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Correo Electrónico erroneo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    //Nombre , PrimerA o SegundoA erroneos
                                    MessageBox.Show("Nombre, Primer Apellido o Segundo Apellido erroneos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Recuerde no usar comillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No existe el empleado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        //DNI incorrecto
                        MessageBox.Show("Dni incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Ningún empleado seleccionado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               
            }
        }

        private void cbFiltroPersonal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)cbFiltroPersonal.SelectedItem == "Tipo")
            {
                tbBusquedaPersonal.Visible = false;
            }
            else
            {
                tbBusquedaPersonal.Visible = true;
            }
        }

        private void dataGridPersonal_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow fila in dataGridPersonal.Rows)
            {
                if (fila.Selected == true)
                {
                    if (fila.Cells[0].Value == null)
                    {
                        tbDniPersonal.Text = "";
                    }
                    else
                    {
                        tbDniPersonal.Text = fila.Cells[0].Value.ToString();
                    }
                    if (fila.Cells[1].Value == null)
                    {
                        tbNombrePersonal.Text = "";
                    }
                    else
                    {
                        tbNombrePersonal.Text = fila.Cells[1].Value.ToString(); 
                    }

                    if (fila.Cells[2].Value == null)
                    {
                        tbPrimerAPersonal.Text = "";
                    }
                    else
                    {
                        tbPrimerAPersonal.Text = fila.Cells[2].Value.ToString();
                    }
                    
                    if (fila.Cells[3].Value == null)
                    {
                        tbSegundoAPersonal.Text = "";
                    }
                    else
                    {
                        tbSegundoAPersonal.Text = fila.Cells[3].Value.ToString();
                    }
                    
                    if (fila.Cells[4].Value == null)
                    {
                        tbDireccionPersonal.Text = "";
                    }
                    else
                    {
                        tbDireccionPersonal.Text = fila.Cells[4].Value.ToString();
                    }
                    
                    if (fila.Cells[5].Value == null)
                    {
                        tbPaisPersonal.Text = "";
                    }
                    else
                    {
                        tbPaisPersonal.Text = fila.Cells[5].Value.ToString();
                    }
                    
                    if (fila.Cells[6].Value == null)
                    {
                        tbProvinciaPersonal.Text = "";
                    }
                    else
                    {
                        tbProvinciaPersonal.Text = fila.Cells[6].Value.ToString();
                    }
                    
                    if (fila.Cells[7].Value == null)
                    {
                        tbTelefonoPersonal.Text = "";
                    }
                    else
                    {
                        tbTelefonoPersonal.Text = fila.Cells[7].Value.ToString();
                    }
                    
                    if (fila.Cells[8].Value == null)
                    {
                        tbCorreoEPersonal.Text = "";
                    }
                    else
                    {
                        tbCorreoEPersonal.Text = fila.Cells[8].Value.ToString();
                    }
                    if (fila.Cells[9].Value == null)
                    {
                        rbAdministracionPersonal.Checked = false;
                        rbEmpleadoPersonal.Checked = false;
                    }
                    else
                    {
                        if (fila.Cells[9].Value.ToString() == "Empleado" || fila.Cells[9].Value.ToString() == "False")
                        {
                            rbEmpleadoPersonal.Checked = true;
                            rbAdministracionPersonal.Checked = false;
                            
                        }
                        else
                        {
                            rbEmpleadoPersonal.Checked = false;
                            rbAdministracionPersonal.Checked = true;
                        }
                    }
                    if (fila.Cells[10].Value == null)
                    {
                        tbNumSSPersonal.Text = "";
                    }
                    else
                    {
                        tbNumSSPersonal.Text = fila.Cells[10].Value.ToString();
                    }
                    
                }
            }
        }

        private void dataGridPersonal_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btContraseña_Click_1(object sender, EventArgs e)
        {
            string DniPersonal = tbDniPersonal.Text.Trim();
            v = new ENVerificacion();
            if (tbDniPersonal.Text == "")
            {
                MessageBox.Show("Ningún empleado seleccionado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if(v.noHack(DniPersonal))
                {
                    if (v.dni(DniPersonal))
                    {
                        personal = new ENPersonal();
                        DataSet esta = personal.busquedaPersonalDni("Todos", DniPersonal);
                        int filas = esta.Tables[0].Rows.Count;
                        if (filas > 0)
                        {
                            FormPersonalLogin consulta = new FormPersonalLogin(DniPersonal);
                            consulta.Show();
                        }
                        else
                        {
                            MessageBox.Show("No existe el empleado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Dni incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Recuerde no usar comillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }                
            }
        }

        private void btLimpiar_Click_1(object sender, EventArgs e)
        {
            if (tbBusquedaPersonal.Visible == true)
            {
                tbBusquedaPersonal.Text = null;
            }
            tbDniPersonal.Text = null;
            tbNombrePersonal.Text = null;
            tbPrimerAPersonal.Text = null;
            tbSegundoAPersonal.Text = null;
            tbNumSSPersonal.Text = null;
            tbPaisPersonal.Text = null;
            tbProvinciaPersonal.Text = null;
            tbTelefonoPersonal.Text = null;
            tbCorreoEPersonal.Text = null;
            tbDireccionPersonal.Text = null;
            rbAdministracionPersonal.Checked = false;
            rbEmpleadoPersonal.Checked = false;
            int filas = dataGridPersonal.Rows.Count;
            if (dataGridPersonal.RowCount > 0)
                for (int i = 0; i < filas; i++)
                {
                    dataGridPersonal.Rows.Clear();
                }
            InicializaTabla();
            cbFiltroPersonal.SelectedIndex = 0;
            cbTipoPersonal.SelectedIndex = 0;
        }

        private void FormPersonal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Principal.ActivarDesactivarBotones(false);
            Principal.formPersonal = null;
        }        
  
      
    }
}
