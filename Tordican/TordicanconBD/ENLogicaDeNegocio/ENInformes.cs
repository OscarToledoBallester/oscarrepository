﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENInformes
    {
        private CADInformes cad;

        public ENInformes()
        {
            cad = new CADInformes();
        }

        ////////////////////////////////////////////
        ///////////// INFORMES DE COTO /////////////
        ////////////////////////////////////////////
        // Piezas cazadas
        public DataSet ObtenerPiezasCazadas()
        {
            return cad.ObtenerPiezasCazadas();
        }

        public DataSet ObtenerPiezasPorTipo(int tipo)
        {
            // Tipo 0 hace referencia a todos
            if (tipo == 0)
                return cad.ObtenerPiezasCazadas();
            else
                return cad.ObtenerPiezasPorTipo(tipo);
        }

        public DataSet ObtenerPiezasPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerPiezasPorFechas(desde, hasta);
        }

        public DataSet ObtenerPiezasPorTipoYFechas(int tipo, DateTime desde, DateTime hasta)
        {
            if (tipo == 0) return cad.ObtenerPiezasPorFechas(desde, hasta);
            else return cad.ObtenerPiezasPorTipoYFechas(tipo, desde, hasta);
        }

        // Reservas del Coto
        public DataSet ObtenerReservasCoto()
        {
            return cad.ObtenerReservasCoto();
        }

        public DataSet ObtenerReservasCotoPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerReservasCotoPorFechas(desde, hasta);
        }

        /////////////////////////////////////////////
        ////////// INFORMES DE ACTIVIDADES //////////
        /////////////////////////////////////////////
        // Incidencias
        public DataSet ObtenerIncidencias(int revisadas)
        {
            return cad.ObtenerIncidencias(revisadas);
        }

        public DataSet ObtenerIncidenciasPorFechas(int revisadas, DateTime desde, DateTime hasta)
        {
            return cad.ObtenerIncidenciasPorFechas(revisadas, desde, hasta);
        }

        public DataSet ObtenerIncidenciasPorTipo(int revisadas, int tipo)
        {
            return cad.ObtenerIncidenciasPorTipo(revisadas, tipo);
        }

        public DataSet ObtenerIncidenciasporTipoyFechas(int revisadas, int tipo, DateTime desde, DateTime hasta)
        {
            return cad.ObtenerIncidenciasPorTipoyFechas(revisadas, tipo, desde, hasta);
        }

        // Actividades Reservadas
        public DataSet ObtenerActividades()
        {
            return cad.ObtenerActividades();
        }

        public DataSet ObtenerActividadesPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesPorFechas(desde, hasta);
        }

       // Tipos de Actividades
        public DataSet ObtenerActividadesQuad()
        {
            return cad.ObtenerActividadesQuad();
        }

        public DataSet ObtenerActividadesQuadPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesQuadPorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesBici()
        {
            return cad.ObtenerActividadesBici();
        }

        public DataSet ObtenerActividadesBiciPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesBiciPorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesPaseo()
        {
            return cad.ObtenerActividadesPaseo();
        }

        public DataSet ObtenerActividadesPaseoPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesPaseoPorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesTiro()
        {
            return cad.ObtenerActividadesTiro();
        }

        public DataSet ObtenerActividadesTiroPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesTiroPorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesPiragua()
        {
            return cad.ObtenerActividadesPiragua();
        }

        public DataSet ObtenerActividadesPiraguaPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesPorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesRapel()
        {
            return cad.ObtenerActividadesRapel();
        }

        public DataSet ObtenerActividadesRapelPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesRapelPorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesParque()
        {
            return cad.ObtenerActividadesParque();
        }

        public DataSet ObtenerActividadesParquePorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesParquePorFechas(desde, hasta);
        }

        public DataSet ObtenerActividadesLocalidad()
        {
            return cad.ObtenerActividadesLocalidad();
        }

        public DataSet ObtenerActividadesLocalidadPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerActividadesLocalidadPorFechas(desde, hasta);
        }

        // Clientes
        public DataSet ObtenerClientes()
        {
            return cad.ObtenerClientes();
        }

        public DataSet ObtenerClientesPorFechas(DateTime desde, DateTime hasta)
        {
            return cad.ObtenerClientesPorFechas(desde, hasta);
        }
    }
}
