﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENParking
    {
        public const int max_vehiculos = 20;
        public const int max_remolques = 10;
        private int numPlaza;
        //Tipo plaza --> {0 = vehiculo, 1 = remolque}
        private int tipo;
        private float precioVehiculo;
        private float precioRemolque;
        //Disponibilidad --> {0 = libre, 1 = ocupada}
        private int disponibildad;
        
        
        public ENParking()
        {
            numPlaza = -1;
            tipo = -1;
            precioRemolque = precioVehiculo = 0;
            disponibildad = 0;
        }

        public int NumPlaza
        {
            get { return numPlaza; }
            set { numPlaza = value; }
        }

        public int Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public float PrecioRemolque
        {
            get { return precioRemolque; }
            set { precioRemolque = value; }
        }

        public float PrecioVehiculo
        {
            get { return precioVehiculo; }
            set { precioVehiculo = value; }
        }

        public int Disponibilidad
        {
            get { return disponibildad; }
            set { disponibildad = value; }
        }


        public bool nuevaPlaza(int numPlaza, int tipo)
        {
            bool reservada = false;

            CADParking park = new CADParking();
            CADReserva res = new CADReserva();
            int i;
            i = res.UltimaReserva();
            reservada = park.nuevaPlazaCAD(numPlaza, tipo, i);
            return reservada;      
        }
    }
}
