﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENRapel
    {
        public ENRapel()
        {
            IdRapel = 0;
            Lugar = "";
            NumEquipos = 0;
            NumCuerdas = 0;
            Precio = 0;
        }
        public bool insertar(string lugar, int numCuerdas, int numEquipos, float precio)
        {
            bool insertado = false;
            CADRapel rapelCAD = new CADRapel();
            
            if (rapelCAD != null)
            {
                Lugar = lugar;
                NumCuerdas = numCuerdas;
                NumEquipos = numEquipos;
                Precio = precio;
            }

            try
            {
                insertado = rapelCAD.insertarCAD(Lugar, NumCuerdas, NumEquipos, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }
        public bool borrar(int idRapel)
        {
            bool borrado = false;
            CADRapel rapelCAD = new CADRapel();

            try
            {
                borrado = rapelCAD.borrarCAD(idRapel);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }
        public bool modificar(int idRapel, string lugar, int numCuerdas, int numEquipos, float precio)
        {
            bool modificado = false;
            CADRapel rapelCAD = new CADRapel();

            if (rapelCAD != null)
            {
                IdRapel = idRapel;
                Lugar = lugar;
                NumCuerdas = numCuerdas;
                NumEquipos = numEquipos;
                Precio = precio;
            }

            try
            {
                modificado = rapelCAD.modificarCAD(IdRapel, Lugar, NumCuerdas, NumEquipos, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }
        public DataSet getTodos()
        {
            CADRapel rapelCAD = new CADRapel();
            DataSet resultado = null;

            try
            {
                resultado = rapelCAD.getTodosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }
        public int getNumTotalLugares()
        {
            int numLugares = 0;
            CADRapel rapelCAD = new CADRapel();

            try
            {
                numLugares = rapelCAD.getNumTotalLugaresCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numLugares;
        }
        public int getNumTotalCuerdas()
        {
            int numCuerdas = 0;
            CADRapel rapelCAD = new CADRapel();

            try
            {
                numCuerdas = rapelCAD.getNumTotalCuerdasCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numCuerdas;
        }
        public int getNumTotalEquipos()
        {
            int numEquipos = 0;
            CADRapel rapelCAD = new CADRapel();

            try
            {
                numEquipos = rapelCAD.getNumTotalEquiposCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numEquipos;
        }

        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdRapel
        {
            get { return idRapel; }
            set { idRapel = value; }
        }
        public string Lugar
        {
            get { return lugar; }
            set { lugar = value; }
        }
        public int NumCuerdas
        {
            get { return numCuerdas; }
            set { numCuerdas = value; }
        }
        public int NumEquipos
        {
            get { return numEquipos; }
            set { numEquipos = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idRapel;
        private string lugar;
        private int numCuerdas;
        private int numEquipos;
        private float precio;
    }
}