﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENBicicleta
    {
        public ENBicicleta()
        {
            IdBici = 0;
            ATipo = 0;
            Tipo = null;
            Precio = 0;
            FechaAlta = DateTime.Today;
            EnUso = false;
        }

        public bool insertar(int tipo, DateTime fechaAlta, bool enUso)
        {
            bool insertado = false;
            CADBicicleta biciCAD = new CADBicicleta();

            if (biciCAD != null)
            {
                ATipo = tipo;
                FechaAlta = fechaAlta;
                EnUso = enUso;
            }

            try
            {
                insertado = biciCAD.insertarCAD(ATipo, FechaAlta, EnUso);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }

        public bool modificar(int idBici, int tipo, DateTime fechaAlta, bool enUso)
        {
            bool modificada = false;
            CADBicicleta biciCAD = new CADBicicleta();

            if (biciCAD != null)
            {
                IdBici = idBici;
                ATipo = tipo;
                FechaAlta = fechaAlta;
                EnUso = enUso;
            }

            try
            {
                modificada = biciCAD.modificarCAD(idBici, tipo, fechaAlta, enUso);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificada;

        }

        public bool borrar(int idBici)
        {
            bool borrado = false;
            CADBicicleta biciCAD = new CADBicicleta();

            try
            {
                borrado = biciCAD.borrarCAD(idBici);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }

        public void getPrecios(ref float precioNino, ref float precioAdulto, ref float precioTandem)
        {
            CADBicicleta biciCAD = new CADBicicleta();
            biciCAD.getPreciosCAD(ref precioNino, ref precioAdulto, ref precioTandem);
        }

        public bool setPrecios(float precioNino, float precioAdulto, float precioTandem)
        {
            bool modificados = false;
            CADBicicleta biciCAD = new CADBicicleta();

            try
            {
                modificados = biciCAD.setPreciosCAD(precioNino, precioAdulto, precioTandem);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificados;
        }


        public DataSet getTodas(int filtro, bool disponibles)
        {
            CADBicicleta biciCAD = new CADBicicleta();
            DataSet resultado = null;

            try
            {
                resultado = biciCAD.getTodasCAD(filtro, disponibles);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        public int getNumTotal(bool estanEnUso)
        {
            int numBicis = 0;
            CADBicicleta biciCAD = new CADBicicleta();

            try
            {
                numBicis = biciCAD.getNumTotalCAD(estanEnUso);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numBicis;
        }






        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdBici
        {
            get { return idBici; }
            set { idBici = value; }
        }
        public int ATipo
        {
            get { return aTipo; }
            set { aTipo = value; }
        }
        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }
        public bool EnUso
        {
            get { return enUso; }
            set { enUso = value; }
        }

        // Atributos privados de la Entidad.
        private int idBici;
        private int aTipo;
        private string tipo;
        private float precio;
        private DateTime fechaAlta;
        private bool enUso;
    }
}
