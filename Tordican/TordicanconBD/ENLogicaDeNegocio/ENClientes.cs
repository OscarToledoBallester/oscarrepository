﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENClientes
    {
        private string dni, nombre, ape1, ape2, dir, pais, prov, telf, email, login, password;

        public ENClientes()
        {
            dni = nombre = ape1 = ape2 = dir = pais = prov = telf = email = login = password = null;
        }

        public string Dni
        {
            get { return dni; }
            set { dni = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Ape1
        {
            get { return ape1; }
            set { ape1 = value; }
        }

        public string Ape2
        {
            get { return ape2; }
            set { ape2 = value; }
        }

        public string Dir
        {
            get { return dir; }
            set { dir = value; }
        }

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        public string Prov
        {
            get { return prov; }
            set { prov = value; }
        }

        public string Telf
        {
            get { return telf; }
            set { telf = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public bool nuevoCliente(string ndni, string nnom, string nape1, string nape2, string ndir, string npais, string nprov, string ntelf, string nemail, string nlogin, string npassword)
        {
            bool insertado = false;
            CADCliente clCAD = new CADCliente();
          
            if (clCAD != null)
            { 
                Dni = ndni;
                Nombre = nnom;
                Ape1 = nape1;
                Ape2 = nape2;
                Dir = ndir;
                Pais = npais;
                Prov = nprov;
                Telf = ntelf;
                Email = nemail;
                Login = nlogin;
                Password = npassword;
            
            }
            
            try
            {
                nlogin = "NULL";
                insertado = clCAD.nuevoClienteCAD(ndni, nnom, nape1, nape2, ndir, npais, nprov, ntelf, nemail, "");
                
            }
            catch(ModelException)
            {
                //throw exc;
            }
            return insertado;
        }
        
        public DataSet obtenerClientePorDni(string dni)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorDniCAD(dni);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        
        public DataSet obtenerClientePorNombre(string nom)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorNombreCAD(nom);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        
        public DataSet obtenerClientePorApe1(string ape1)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorApe1CAD(ape1);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        
        public DataSet obtenerClientePorApe2(string ape2)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorApe2CAD(ape2);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        
        public DataSet obtenerClientePorDireccion(string dir)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorDireccionCAD(dir);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        

        public DataSet obtenerClientePorPais(string pais)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorPaisCAD(pais);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet obtenerClientePorProvincia(string prov)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorProvinciaCAD(prov);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet obtenerClientePorTelefono(string telf)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorTelefonoCAD(telf);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet obtenerClientePorEmail(string email)
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;

            try
            {
                resultado = clienteCAD.obtenerClientePorEmailCAD(email);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        
        public bool actualizarCliente(string dni, string nom, string ape1, string ape2, string dir, string pais, string prov, string telf, string email)
        {
            bool resultado = false;
            CADCliente clienteCAD = new CADCliente();

            try
            {
                resultado = clienteCAD.actualizarClienteCAD(dni, nom, ape1, ape2, dir, pais, prov, telf, email,"");
                if (resultado == true)
                {
                    Dni = dni;
                    Nombre = nom;
                    Ape1 = ape1;
                    Ape2 = ape2;
                    Dir = dir;
                    Pais = pais;
                    Prov = prov;
                    Telf = telf;
                    Email = email;
                }
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;
        }
        
        public bool eliminarCliente(string dni)
        {
            bool resultado = false;
            CADCliente clienteCAD = new CADCliente();

            try
            {
                resultado = clienteCAD.eliminarClienteCAD(dni);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet mostrarTodos()
        {
            CADCliente clienteCAD = new CADCliente();
            DataSet resultado;
            try
            {
                resultado = clienteCAD.mostrarTodosCAD();
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;

        }
    }
}
