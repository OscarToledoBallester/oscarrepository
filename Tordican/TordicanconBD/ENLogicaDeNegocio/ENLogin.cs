﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENLogin
    {
        private string usuario;
        private string password;
        

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
    

        public DataSet comprobarAcceso(string iUsuario)
        {
            CADLogin loginCAD = new CADLogin();
            DataSet encontrados;
            try
            {
                encontrados = loginCAD.comprobarAcceso(iUsuario);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet tipoAcceso(string iUsuario)
        {
            CADLogin loginCAD = new CADLogin();
            DataSet encontrados;
            try
            {
                encontrados = loginCAD.tipoAcceso(iUsuario);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
    }
}
