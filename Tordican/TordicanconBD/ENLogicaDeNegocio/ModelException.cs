﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENLogicaDeNegocio
{
    public class ModelException:System.Exception
    {
        public ModelException(string mensajeModelo)
            : base("Tordican S.L.- ERROR: " + mensajeModelo)
        {
        }
    }
}
