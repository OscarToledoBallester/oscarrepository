﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;

namespace ENLogicaDeNegocio
{
    //enum tipo = {empleado, administracion};
    public class ENPersonal
    {
        private string dni;
        private string nombre;
        private string apellido1;
        private string apellido2;
        private string nssocial;
        private string direccion;
        private string pais;
        private string provincia;
        private string telefono;
        private string email;
        private bool tipoAdministracion;
        private string usuario;
        private string contrasena;

        public ENPersonal()
        {
            dni = nombre = apellido1 = apellido2 = nssocial = direccion = pais = provincia = null;
            tipoAdministracion = false;
        }
        public string Dni
        {
            get{return dni;}
            set{dni = value;}
        }
        public string Nombre
        {
            get{return nombre;}
            set{nombre = value;}
        }
        public string Apellido1
        {
            get{return apellido1;}
            set{apellido1 = value;}
        }
        public string Apellido2
        {
            get{return apellido2;}
            set{apellido2 = value;}
        }
        public string Nssocial
        {
            get { return nssocial; }
            set { nssocial = value; }
        }
        public string Direccion
        {
            get{return direccion;}
            set{direccion = value;}
        }
        public string Pais
        {
            get{return pais;}
            set{pais = value;}
        }
        public string Provincia
        {
            get{return provincia;}
            set{provincia = value;}
        }
        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public bool TipoAdministracion
        {
            get{return tipoAdministracion;}
            set{tipoAdministracion = value;}
        }
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        public string Contrasena
        {
            get { return contrasena; }
            set { contrasena = value; }
        }
        public bool altaPersonal(string idni, string nom, string ap1, string ap2, string nss, string direc,string ipais, string prov, string tlf,string em, bool tipoAdmin)
        {
            
            bool alta = true;
            
            CADPersonal personalCad = new CADPersonal();
            if (personalCad != null)
            {
                Dni = idni;
                Nombre = nom;
                Apellido1 = ap1;
                Apellido2 = ap2;
                Nssocial = nss;
                Direccion = direc;
                Provincia = prov;
                Pais = ipais;
                Telefono = tlf;
                Email = em;
                TipoAdministracion = tipoAdmin;
                Usuario = idni;
                Contrasena = ap2; 

                //Contrasena = "contraseña";
                string log = Usuario;
                string pass = Contrasena;
                
                try
                {
                   alta = personalCad.nuevoPersonal(idni, nom, ap1, ap2, nss, direc, ipais, prov,tlf,em, tipoAdmin, log, pass);
                }
                catch(ModelException exc)
                {
                    throw exc;
                }
            }
            else
            {
                    alta = false;
            }
            return alta;
        }
        public DataSet busquedaPersonalDni(string opTipo,string idni)
        {
            CADPersonal personalCAD = new CADPersonal();
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalDni(opTipo,idni);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalNom(string opTipo, string iNom)
        {
            CADPersonal personalCAD = new CADPersonal();
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalNom(opTipo,iNom);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalAp1(string opTipo, string iAp1)
        {
            CADPersonal personalCAD = new CADPersonal();
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalAp1(opTipo,iAp1);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalAp2(string opTipo, string iAp2)
        {
            CADPersonal personalCAD = new CADPersonal();
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalAp2(opTipo,iAp2);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalTlf(string opTipo, string iTlf)
        {
            CADPersonal personalCAD = new CADPersonal();
            
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalTlf(opTipo,iTlf);
            }
            catch(ModelException exc)
            {
                throw exc;
            }

            return encontrados;
        }
        public DataSet busquedaPersonalEMail(string opTipo, string iEMail)
        {
            CADPersonal personalCAD = new CADPersonal();
            
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalEMail(opTipo,iEMail);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalPais(string opTipo, string iPais)
        {
            CADPersonal personalCAD = new CADPersonal();
            
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalPais(opTipo,iPais);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalProv(string opTipo, string iProv)
        {
            CADPersonal personalCAD = new CADPersonal();
            
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalProv(opTipo,iProv);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalUsuario(string iUsuario)
        {
            CADPersonal personalCAD = new CADPersonal();

            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalUsuario(iUsuario);
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public DataSet busquedaPersonalTipo(string opTipo)
        {
            CADPersonal personalCAD = new CADPersonal();
            
            DataSet encontrados;
            try
            {
                encontrados = personalCAD.busquedaPersonalTipo(opTipo);
            }
            catch(ModelException exc)
            {
                throw exc;
            }
            return encontrados;
        }
        public bool modificarPersonal(string idni, string nom, string ap1, string ap2, string nss, string direc, string ipais, string prov, string tlf, string em, bool tipoAdmin)
        {

            bool modificacion = false;
            
            CADPersonal personalCad = new CADPersonal();

            try
            {

                modificacion = personalCad.modificarPersonal(idni, nom, ap1, ap2, nss, direc, ipais, prov, tlf , em, tipoAdmin);
                if (modificacion == true)
                {
                    Dni = idni;
                    Nombre = nom;
                    Apellido1 = ap1;
                    Apellido2 = ap2;
                    Nssocial = nss;
                    Direccion = direc;
                    Provincia = prov;
                    Pais = ipais;
                    Telefono = tlf;
                    Email = em;
                    TipoAdministracion = tipoAdmin;

                }
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            
            return modificacion;
        }
        public bool bajaPersonal(string idni)
        {
            bool eliminado = false;
            CADPersonal personalCAD = new CADPersonal();
            try
            {
                eliminado = personalCAD.bajaPersonal(idni);
            }
            catch(ModelException exc)
            {
                throw exc;
            }

            return eliminado;
        }
        public DataSet mostrarTodos()
        {
            CADPersonal personalCAD = new CADPersonal();
            DataSet resultado;
            try
            {
                resultado = personalCAD.mostrarPersonal();
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return resultado;

        }
       public bool modificarUC(string idni, string iusuario, string ipass)
        {

            bool modificacion = false;
            
            CADPersonal personalCad = new CADPersonal();

            try
            {

                modificacion = personalCad.modificarUC(idni, iusuario, ipass);
                if (modificacion == true)
                {
                    Usuario = iusuario;
                    Contrasena = ipass;

                }
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            
            return modificacion;
        }
       public DataSet busquedaUC(string idni)
       {
           CADPersonal personalCAD = new CADPersonal();

           DataSet encontrados;
           try
           {
               encontrados = personalCAD.busquedaUC(idni);
           }
           catch (ModelException exc)
           {
               throw exc;
           }
           return encontrados;
       }
    }
}

