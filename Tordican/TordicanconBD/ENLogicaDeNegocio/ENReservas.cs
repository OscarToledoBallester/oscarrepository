﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;
using System.Data;

namespace ENLogicaDeNegocio
{
    
    public class ENReservas
    {
        //Atributos privados para la reservas
        private string dniCli;
        private DateTime fechaRes;
        private DateTime fechaIng;
        private DateTime fechaSal;
        private int numH;
        private int numP;
        private int diasO;
        public int numReservas = 0;

        //Actividades y Caza
        private int idAct;
        private string tipoAct;



        public ENReservas()
        {
            dniCli = tipoAct = null;
            numH = numP = diasO = idAct = - 1;

        }

        public string Dni
        {
            get { return dniCli; }
            set { dniCli = value; }
        }

        public int NumHabitacion
        {
            get { return numH; }
            set { numH = value; }
        }

        public int NumPlanta
        {
            get { return numP; }
            set { numP = value; }
        }

        public int DiasEnHotel
        {
            get { return diasO; }
            set { diasO = value; }
        }

        public DateTime FechaReserva
        {
            get { return fechaRes; }
            set { fechaRes = value; }
        }

        public DateTime FechaIng
        {
            get { return fechaIng; }
            set { fechaIng = value; }
        }

        public DateTime FechaSal
        {
            get { return fechaSal; }
            set { fechaSal = value; }
        }

        public int IdActividad
        {
            get { return idAct; }
            set { idAct = value; }
        }

        public string TipoActividad
        {
            get { return tipoAct; }
            set { tipoAct = value; }
        }

        public bool NuevaReserva(DateTime freg,DateTime fing,DateTime fsal,string dni,string tipoA,int idA,int numHab,int numPla,bool park)
        {
            CADReserva nr = new CADReserva();
            CADHabitaciones cad = new CADHabitaciones();
            CADParking par = new CADParking();
            int plazaparking = 0;
            plazaparking = numP + numH;
            
            
            bool ok = false;
            if (nr != null)
            {
                dniCli = dni.Trim();
                fechaRes = freg;
                fechaIng = fing;
                fechaSal = fsal;
                numH = numHab;
                numP = numPla;
                diasO = fsal.Day - fing.Day;
                numReservas = 0;
                
                try
                {
                    plazaparking = SumarPyH(numP, numH);
                    if (ComprobarFechas(freg,fing,fsal) && ComprobarHabsPlanta(numHab,numPla))
                    {
                        numReservas = nr.UltimaReserva();
                        ok = nr.NuevaReserva(fing, dni, tipoA, idA, numP, fing, diasO,numReservas);
                        if (ok)
                        {
                            int d = fing.Day;
                            int m = fing.Month;
                            int a = fing.Year;
                            nr.MirarDiaAlojamiento(d, m, a);

                            ok = cad.reservarHabitacionCAD(numH, numP);
                            if (ok) 
                            {
                                ok = nr.EnContrataHab(numH, numP, d, m, a, numReservas, diasO);
                                if (park)
                                {
                                    
                                    par.nuevaPlazaCAD(plazaparking,0,numReservas);
                                }
                            }
                        }
                    }
                }
                catch (ModelException exc)
                {
                    throw exc;
                }
            }
            return ok;
        }

        public int SumarPyH(int p, int h)
        {
            int pp = 0;
            string pl = "";

            if (p > 0 && h < 10)
            {
                pl = p.ToString() + "0" + h.ToString();
                pp = int.Parse(pl.ToString());
            }
            else
            {
                pl = p.ToString() + h.ToString();
                pp = int.Parse(pl.ToString());
            }

            return pp;
        }

        public bool ModifcarReserva()
        {
            return true;
        }

        public bool ComprobarHabsPlanta(int nh,int np)
        {
            bool ok = false;
            if (np == 0)
            {
                ok = false;
                throw new ModelException("Debe elegir un tipo de habitación");
            }
            else if (nh == 0)
            {
                ok = false;
                throw new ModelException("Eliga una habitación");
            }
            else ok = true;

            return ok;
        }

        public bool ComprobarFechas(DateTime freg,DateTime fing,DateTime fsal)
        {
            bool ok;
            if (freg > fing)
            {
                ok = false;
                throw new ModelException("La Fecha de Reserva no puede ser mayor que la fecha de ingreso");
            }
            else if(fing > fsal)
            {
                ok = false;
                throw new ModelException("La Fecha de Ingreso no puede ser mayor que la fecha de Salida");
            }
            else
                ok = true;

            return ok;
        }

        public bool EliminarReserva(string nif,int p,int h,int r,DateTime fE,int dE)
        {
            CADReserva nre = new CADReserva();
            CADParking pa = new CADParking();
            int res = -1;
            int aTipo = -1;
            int aHab = -1;
            int parking = 0;

            bool ok = false;
            try
            {
                if (TieneCaza(nif, fE, dE))
                {
                    parking = SumarPyH(p, h);
                    pa.cancelarPlazaCAD(parking, 0, r);
                    if (p == -1)
                    {
                        res = nre.UnaReserva(nif);
                        aTipo = nre.UnaTipoPlanta(res);
                        aHab = nre.UnaHabitacion(res);
                    }
                    else
                    {
                        aHab = h;
                        aTipo = p;
                        res = r;
                    }
                    if (EliminarTodo(aHab, aTipo, res, nif))
                    {
                        ok = nre.EliminarReserva(nif, res);
                    }
                }
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return ok;
        }

        public bool EliminarTodo(int aHab,int aTipo,int res,string nif)
        {
            bool ok = false;
            CADReserva nre = new CADReserva();
            CADHabitaciones ha = new CADHabitaciones();

            if (!nre.EliminarEnContrataHabitacion(res))
            {
                throw new ModelException("No se Puede Borrar la Habitacion");
            }
            else if (!ha.cancelarReservaHabitacionCAD(aHab, aTipo))
            {
                throw new ModelException("No se Puede Borrar el contrato de la habitacion");
            }
            else ok = true;

            return ok;
        }

        public bool TieneCaza(string nif,DateTime f1,int dias)
        {
            bool ok = false;
            CADReserva nre = new CADReserva();
            DataSet tiene = new DataSet();
            DateTime f2 = new DateTime();
            f2 = f1.AddDays(dias);
            tiene = nre.TieneCaza(nif,f1,f2);
            if (tiene.Tables[0].Rows.Count > 0)
            {
                throw new ModelException("No se puede borrar la reserva,tiene reservas de caza pendientes");
            }
            else ok = true;

            return ok;
        }

        public bool EliminarReservaCaza(int nr)
        {
            bool ok = false;
            CADParking par = new CADParking();
            try
            {
                if (par.cancelaParkin(nr))
                {
                    if (EliminaTodoLoDeCaza(nr))
                    {
                        ok = true;
                    }

                }
            }
            catch (ModelException exc)
            {
                throw exc;
            }
            return ok;
        }

        private bool EliminaTodoLoDeCaza(int r)
        {
            bool ok = false;
            CADCoto coto = new CADCoto();
            CADPerrera per = new CADPerrera();
            CADReserva res = new CADReserva();
            if (!coto.CADEliminarContrataCoto(r))
                throw new ModelException("No se ha podido eliminar esta reserva,CADContrataCoto");
            else if (!per.CADEliminaPerrera(r))
                throw new ModelException("No se ha podido eliminar la reserva,CADPerrera");
            else if (!res.EliminaReservaCaza(r))
                throw new ModelException("No se ha podido eliminar la reserva,CADReserva");
            else ok = true;
            return ok;
        }
    }
}
