﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ENLogicaDeNegocio
{
    public class ENCoto
    {
        private string nombre;

        public string Nombre {
            get { return nombre; }
            set { nombre = value; }
        }
        
        public bool ENActualizarPiezas(int tipo, int cazadas, int dia, int mes, int anyo) {
            bool hecho = false;
            try
            {
                Persistencia.CADCoto n = new Persistencia.CADCoto();
                hecho = n.CADActualizarPiezas(tipo, cazadas, dia, mes, anyo);
            }
            catch (ModelException exc) {
                throw exc;
            }
            return hecho;
        }

        public bool ENRepoblar(int tipo, int piezas) {
            bool hecho = false;
            try
            {
                Persistencia.CADCoto n = new Persistencia.CADCoto();
                n.CADRepoblar(tipo, piezas);
                hecho = true;

            }
            catch (ModelException exc) {
                throw exc;
            }
            return hecho;
        }
        
        public int ENObtenerTotales(int tipo) { 
            Persistencia.CADCoto n = new Persistencia.CADCoto();
            int valor = n.CADObtenerTotales(tipo);
            return valor;
        }

    }
}
