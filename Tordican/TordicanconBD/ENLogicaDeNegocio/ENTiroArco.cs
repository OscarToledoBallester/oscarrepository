﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENTiroArco
    {
        public ENTiroArco()
        {
            IdTiro = 0;
            NumFlechas = 0;
            NumDianas = 0;
            Precio = 0;
        }

        public bool insertar(int numFlechas, int numDianas, float precio)
        {
            bool insertado = false;
            CADTiroArco tiroCAD = new CADTiroArco();

            if (tiroCAD != null)
            {
                NumFlechas = numFlechas;
                NumDianas = numDianas;
                Precio = precio;
            }

            try
            {
                insertado = tiroCAD.insertarCAD(NumFlechas, NumDianas, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }
        public bool borrar(int idTiro)
        {
            bool borrado = false;
            CADTiroArco tiroCAD = new CADTiroArco();

            try
            {
                borrado = tiroCAD.borrarCAD(idTiro);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }
        public bool modificar(int idTiro, int numFlechas, int numDianas, float precio)
        {
            bool modificado = false;
            CADTiroArco tiroCAD = new CADTiroArco();

            if (tiroCAD != null)
            {
                IdTiro = idTiro;
                NumFlechas = numFlechas;
                NumDianas = numDianas;
                Precio = precio;
            }

            try
            {
                modificado = tiroCAD.modificarCAD(IdTiro, NumFlechas, NumDianas, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }
        public DataSet getTodos()
        {
            CADTiroArco tiroCAD = new CADTiroArco();
            DataSet resultado = null;

            try
            {
                resultado = tiroCAD.getTodosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdTiro
        {
            get { return idTiro; }
            set { idTiro = value; }
        }
        public int NumFlechas
        {
            get { return numFlechas; }
            set { numFlechas = value; }
        }
        public int NumDianas
        {
            get { return numDianas; }
            set { numDianas = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idTiro;
        private int numFlechas;
        private int numDianas;
        private float precio;
    }
}