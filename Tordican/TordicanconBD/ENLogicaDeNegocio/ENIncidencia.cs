﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENIncidencia
    {
        public ENIncidencia()
        {
            IdIncidencia = 0;
            Descripcion = "";
            Fecha = DateTime.Today;
            revisada = false;
            Actividad = 0; // Asegurar que no sea 0 antes de insertar, porque es FK y depende.
        }
        public bool insertar(string descripcion, DateTime fecha, bool revisada, int actividad)
        {
            bool insertado = false;
            CADIncidencia inciCAD = new CADIncidencia();

            if (inciCAD != null)
            {
                Descripcion = descripcion;
                Fecha = fecha;
                Revisada = revisada;
                Actividad = actividad;
            }

            try
            {
                insertado = inciCAD.insertarCAD(Descripcion, Fecha, Revisada, Actividad);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }
        public bool modificar(int idIncidencia, string descripcion, DateTime fecha, bool revisada, int actividad)
        {
            bool modificada = false;
            CADIncidencia inciCAD = new CADIncidencia();

            if (inciCAD != null)
            {
                IdIncidencia = idIncidencia;
                Descripcion = descripcion;
                Fecha = fecha;
                Revisada = revisada;
                Actividad = actividad;
            }

            try
            {
                modificada = inciCAD.modificarCAD(IdIncidencia, Descripcion, Fecha, Revisada, Actividad);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificada;

        }
        public bool borrar(int idIncidencia)
        {
            bool borrado = false;
            CADIncidencia inciCAD = new CADIncidencia();

            try
            {
                borrado = inciCAD.borrarCAD(idIncidencia);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }
        public DataSet getTodas(bool pendientesRevisar)
        {
            CADIncidencia inciCAD = new CADIncidencia();
            DataSet resultado = null;

            try
            {
                resultado = inciCAD.getTodasCAD(pendientesRevisar);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }
        public int getNumTotal(bool pendientesRevisar)
        {
            int numIncidencias = 0;
            CADIncidencia inciCAD = new CADIncidencia();

            try
            {
                numIncidencias = inciCAD.getNumTotalCAD(pendientesRevisar);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numIncidencias;
        }

        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdIncidencia
        {
            get { return idIncidencia; }
            set { idIncidencia = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        public bool Revisada
        {
            get { return revisada; }
            set { revisada = value; }
        }
        public int Actividad
        {
            get { return actividad; }
            set { actividad = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idIncidencia;
        private string descripcion;
        private DateTime fecha;
        private bool revisada;
        private int actividad;
    }
}