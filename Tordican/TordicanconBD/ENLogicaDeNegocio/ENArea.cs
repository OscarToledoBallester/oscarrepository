﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENLogicaDeNegocio
{
    public class ENArea
    {
        // Has de extension
        private int extension;
        float precio;

        public int Extension {
            get { return extension; }
            set { extension = value; }
        }

        public float Precio {
            get { return precio; }
            set { precio = value; }
        }
        public int ENGetNumEscopetas(int area, int dia, int mes, int anyo)
        {
            Persistencia.CADArea nuevo = new Persistencia.CADArea();
            int num = nuevo.CADGetNumEscopetas(area, dia, mes, anyo);            
            return num;
            
        }

  
        public bool ENGetEstado(int area)
        {
            Persistencia.CADArea nuevo = new Persistencia.CADArea();
            bool num = nuevo.CADGetEstado(area);
            return num;
        }
        public bool ENSetEstado(int area, bool es)
        {
            Persistencia.CADArea nuevo = new Persistencia.CADArea();
            bool hecho = false;
            if (nuevo.CADGetEstado(area) != es) {
                
                //He de buscar cual esta repoblando y modificarlo            
                hecho = nuevo.CADSetEstado(area, es); 
            }
            //En caso contrario devuelvo false, controlamos en interfaz;


            return hecho;
        }
        public float ENGetPrecio(int area)
        {
            float p = 0;
            return p;
        }
        public bool ENSetPrecio(int area)
        {
            bool hecho = false;
            return hecho;
        }
        public int ENGetTipoComida(int area)
        {
            int n = 0;
            return n;
        }
        public int ENGetTipoCaza(int area, int dia, int mes, int anyo) 
        {
            Persistencia.CADArea nuevo = new Persistencia.CADArea();
            int num = nuevo.CADGetTipoCazaMenor(area,dia,mes,anyo);
            return num;
        }

    }
}
