﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENPiraguismo
    {

        public ENPiraguismo()
        {
            IdPiraguismo = 0;
            Pantano = "";
            NumChalecos = 0;
            Precio = 0;
        }


        // Llamada al metodo insertar del CAD.
        public bool insertar(string pantano, int numChalecos, float precio)
        {
            // No necesitamos pasar el parametro "idQuad" en la insercion porque es un atributo AUTO_INCREMENT.
            bool insertado = false;
            CADPiraguismo piraCAD = new CADPiraguismo();

            if (piraCAD != null)
            {
                Pantano = pantano;
                NumChalecos = numChalecos;
                Precio = precio;
            }

            try
            {
                insertado = piraCAD.insertarCAD(Pantano, NumChalecos, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }

        public bool borrar(int idPiraguismo)
        {
            bool borrado = false;
            CADPiraguismo piraCAD = new CADPiraguismo();

            try
            {
                borrado = piraCAD.borrarCAD(idPiraguismo);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }

        // Llamada al metodo modificar del CAD.
        public bool modificar(int idPiraguimo, string pantano, int numChalecos, float precio)
        {
            bool modificado = false;
            CADPiraguismo piraCAD = new CADPiraguismo();

            if (piraCAD != null)
            {
                IdPiraguismo = idPiraguimo;
                Pantano = pantano;
                NumChalecos = numChalecos;
                Precio = precio;
            }

            try
            {
                modificado = piraCAD.modificarCAD(IdPiraguismo, Pantano, NumChalecos, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }

        // Llamada al metodo get del CAD para mostrarlos todos.
        public DataSet getTodos()
        {
            CADPiraguismo piraCAD = new CADPiraguismo();
            DataSet resultado = null;

            try
            {
                resultado = piraCAD.getTodosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        public int getNumTotalPantanos()
        {
            int numPantanos = 0;
            CADPiraguismo piraCAD = new CADPiraguismo();

            try
            {
                numPantanos = piraCAD.getNumTotalPantanosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numPantanos;
        }


        public int getNumTotalChalecos()
        {
            int numChalecos = 0;
            CADPiraguismo piraCAD = new CADPiraguismo();

            try
            {
                numChalecos = piraCAD.getNumTotalChalecosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numChalecos;
        }



        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdPiraguismo
        {
            get { return idPiraguismo; }
            set { idPiraguismo = value; }
        }
        public string Pantano
        {
            get { return pantano; }
            set { pantano = value; }
        }
        public int NumChalecos
        {
            get { return numChalecos; }
            set { numChalecos = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idPiraguismo;
        private string pantano;
        private int numChalecos;
        private float precio;
    }
}
