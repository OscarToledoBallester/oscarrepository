﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENLogicaDeNegocio
{
    public class ENComidaCoto
    {
        private int tipo;
        private string descripcion;
        private float precio;

        public int Tipo {
            get { return tipo; }
            set { tipo = value; }
        }
        public string Descripcion {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public float Precio {
            get { return precio; }
            set { precio = value; }
        }

        //Falta Asignarles un precio a cada comida
        public ENComidaCoto(int num) {
            switch (num) { 
                case 0:
                    tipo = num;
                    descripcion = "";
                    break;
                case 1:
                    tipo = num;
                    descripcion = "Barbacoa";
                    break;
                case 2:
                    tipo = num;
                    descripcion = "Migas";
                    break;
                case 3:
                    tipo = num;
                    descripcion = "Gachas";
                    break;
                case 4:
                    tipo = num;
                    descripcion = "Gazpacho Manchego";
                    break;
                default:
                    //Excepcion
                    break;
            }
        }
    }
}
