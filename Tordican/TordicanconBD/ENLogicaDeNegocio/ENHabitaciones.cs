﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENHabitaciones
    {
        private int numero;//Numero de habitacion
        private int planta;
        private int tipo;// {0 = Individual,1 = Doble,2 = Suite }; 
        //PrecioBase de la habitacion Individual
        private float pb_individual;
        //PrecioBase de la habitacion Doble
        private float pb_doble;
        //PrecioBase de la habitacion Suite
        private float pb_suite;
        private bool cuna;
        private bool cama_supletoria;
        //int diponibilidad--> {0 = libre, 1 = ocupada, 2 = porRevisar}
        private int disponibilidad;

        public ENHabitaciones()
        {
            numero = -1;
            planta = -1;
            tipo = -1;
            pb_individual = 0;
            pb_doble = 0;
            pb_suite = 0;
            cuna = false;
            cama_supletoria = false;
            disponibilidad = 0;
        }

        public int Numero
        {
            get { return numero; }
            set { numero = value;  }
        }

        public int Planta
        {
            get { return planta;}
            set { planta = value;}

        }

        public int Tipo
        {
            get { return tipo; }
            set { tipo = value; }

        }

        public float Pb_individual
        {
            get { return pb_individual; }
            set { pb_individual = value; }
        }

        public float Pb_doble
        {
            get { return pb_doble; }
            set { pb_doble = value; }
        }

        public float Pb_suite
        {
            get { return pb_suite; }
            set { pb_suite = value; }
        }

        public bool Cuna
        {
            get { return cuna; }
            set { cuna = value; }
        }

        public bool Cama_supletoria
        {
            get { return cama_supletoria; }
            set { cama_supletoria = value; }
        }

        public int Disponibilidad
        {
            get { return disponibilidad; }
            set { disponibilidad = value; }
        }


    }
}
