﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENVehiculoQuad
    {
        // Constructor por defecto.
        public ENVehiculoQuad()
        {
            IdQuad = 0;
            Matricula = null;
            Marca = null;
            Modelo = null;
            Potencia = 0;
            Peso = 0;
            Precio = 0;
            FechaAlta = DateTime.Today; // Por defecto la actual
            EnUso = false;
        }

        // Llamada al metodo insertar del CAD.
        public bool insertar(string matricula, string marca, string modelo, float potencia, float peso, float precio, DateTime fechaAlta, bool enUso)
        {
            // No necesitamos pasar el parametro "idQuad" en la insercion porque es un atributo AUTO_INCREMENT.
            bool insertado = false;
            CADVehiculoQuad quadCAD = new CADVehiculoQuad();

            if (quadCAD != null)
            {
                Matricula = matricula;
                Marca = marca;
                Modelo = modelo;
                Potencia = potencia;
                Peso = peso;
                Precio = precio;
                FechaAlta = fechaAlta;
                EnUso = enUso;
            }

            try
            {
                insertado = quadCAD.insertarCAD(Matricula, Marca, Modelo, Potencia, Peso, Precio, FechaAlta, EnUso);
            }
            catch(ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }

        public bool borrar(int idQuad)
        {
            bool borrado = false;
            CADVehiculoQuad quadCAD = new CADVehiculoQuad();

            try
            {
                borrado = quadCAD.borrarCAD(idQuad);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }

        // Llamada al metodo modificar del CAD.
        public bool modificar(int idQuad, string matricula, string marca, string modelo, float potencia, float peso, float precio, DateTime fechaAlta, bool enUso)
        {
            bool modificado = false;
            CADVehiculoQuad quadCAD = new CADVehiculoQuad();

            if (quadCAD != null)
            {
                IdQuad = idQuad; // Identificador que usaremos para seleccionar quad a modificar (condicion WHERE)
                Matricula = matricula;
                Marca = marca;
                Modelo = modelo;
                Potencia = potencia;
                Peso = peso;
                Precio = precio;
                FechaAlta = fechaAlta;
                EnUso = enUso;
            }

            try
            {
                modificado = quadCAD.modificarCAD(IdQuad, Matricula, Marca, Modelo, Potencia, Peso, Precio, FechaAlta, EnUso);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }

        // Llamada al metodo get del CAD para mostrarlos todos.
        public DataSet getTodos(bool disponibles, string filtro, string cadenaBuscar)
        {
            CADVehiculoQuad quadCAD = new CADVehiculoQuad();
            DataSet resultado = null;

            try
            {
                resultado = quadCAD.getTodosCAD(disponibles, filtro, cadenaBuscar);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        // Llamada al metodo get del CAD para mostrar el numero de quads total y en uso.
        public int getNumTotal(bool estanEnUso)
        {
            int numQuads = 0;
            CADVehiculoQuad quadCAD = new CADVehiculoQuad();

            try
            {
                numQuads = quadCAD.getNumTotalCAD(estanEnUso);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numQuads;
        }


        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdQuad
        {
            get { return idQuad; }
            set { idQuad = value; }
        }
        public string Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }
        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }
        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }
        public float Potencia
        {
            get { return potencia; }
            set { potencia = value; }
        }
        public float Peso
        {
            get { return peso; }
            set { peso = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }
        public bool EnUso
        {
            get { return enUso; }
            set { enUso = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idQuad;
        private string matricula;
        private string marca;
        private string modelo;
        private float potencia;
        private float peso;
        private float precio;
        private DateTime fechaAlta;
        private bool enUso;
    }
}