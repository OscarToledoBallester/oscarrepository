﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENParqueTematico
    {
        public ENParqueTematico()
        {
            IdParque = 0;
            Nombre = "";
            Localidad = "";
            Descripcion = "";
            Precio = 0;
        }
        public bool insertar(string nombre, string localidad, string descripcion, float precio)
        {
            bool insertado = false;
            CADParqueTematico parqueCAD = new CADParqueTematico();

            if (parqueCAD != null)
            {
                Nombre = nombre;
                Localidad = localidad;
                Descripcion = descripcion;
                Precio = precio;
            }

            try
            {
                insertado = parqueCAD.insertarCAD(Nombre, Localidad, Descripcion, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }
        public bool borrar(int idParque)
        {
            bool borrado = false;
            CADParqueTematico parqueCAD = new CADParqueTematico();

            try
            {
                borrado = parqueCAD.borrarCAD(idParque);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }
        public bool modificar(int idParque, string nombre, string localidad, string descripcion, float precio)
        {
            bool modificado = false;
            CADParqueTematico parqueCAD = new CADParqueTematico();

            if (parqueCAD != null)
            {
                IdParque = idParque;
                Nombre = nombre;
                Localidad = localidad;
                Descripcion = descripcion;
                Precio = precio;
            }

            try
            {
                modificado = parqueCAD.modificarCAD(IdParque, Nombre, Localidad, Descripcion, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }
        public DataSet getTodos()
        {
            CADParqueTematico parqueCAD = new CADParqueTematico();
            DataSet resultado = null;

            try
            {
                resultado = parqueCAD.getTodosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdParque
        {
            get { return idParque; }
            set { idParque = value; }
        }
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Localidad
        {
            get { return localidad; }
            set { localidad = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idParque;
        private string nombre;
        private string localidad;
        private string descripcion;
        private float precio;
    }
}