﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ENLogicaDeNegocio
{
    public class ENVerificacion
    {
        public string Correspondencia;
        public string Correspondencia1;
        public ENVerificacion()
        {
            Correspondencia = "TRWAGMYFPDXBNJZSQVHLCKE";
            Correspondencia1 = "trwagmyfpdxbnjzsqvhlcke";
        }
        public string correspondencia
        {
            get { return Correspondencia; }
            set { Correspondencia = value; }
        }
        public bool dni(string nif)
        {
            bool correcto = false;
            int n = -1;

            //if ((dni == null) || (dni.Length != 8) || (!int.TryParse(dni, out n)))
            if ((nif == null) || (nif.Length != 9) || (!int.TryParse(nif.Substring(0, 8), out n)))
            {
                correcto = false;
            }
            else
            {
                if (Correspondencia[n % 23] == nif[nif.Length - 1])
                {
                    correcto = true;
                }
                else
                {
                    if (Correspondencia1[n % 23] == nif[nif.Length - 1])
                    {
                        correcto = true;
                    }
                }
            }

            return correcto;
        }
        public bool cadena(string c)
        {
            bool correcto = true;
            
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] > '0' && c[i] < '9')
                {
                    correcto = false;
                }
            }
            return correcto;
        }
        public bool noHack(string c)
        {
            bool correcto = true;

            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == '"' || c[i] == '\'' || c[i] == '´' || c[i] == '`')
                {
                    correcto = false;
                }
            }
            return correcto;
        }
        public bool nssocial(string nss)
        {//aa/bbbbbbbbb/cc
            bool correcto = true;
            int a, b, d, c, c2;
            int cont = 0;
            string baux, baux2, aaux, caux;
            bool ceros = true;
            
            if (nss.Length != 12)
            {
                correcto = false;
            }
            else
            {
                aaux = nss.Substring(0, 1);
                a = int.Parse(aaux);
                baux = nss.Substring(2, 9);
                b = int.Parse(baux);
                caux = nss.Substring(10, 11);
                c2 = int.Parse(caux);
                if (b < 10000000)
                {
                    d = b + a * 10000000;
                }
                else
                {
                    for (int i = baux.Length-1; i > 0 && ceros == true; i--)
                    {
                        if (baux[i] == '0')
                        {
                            cont++;
                        }
                        else
                        {
                            ceros = false;
                        }
                    }
                    baux2 = baux.Substring(0, baux.Length-1-cont);
                    d = int.Parse(aaux+baux2);

                }

                c = d % 97;

                if (c == c2)
                {
                    correcto = true;
                }
            }

            return correcto;
        }
        public bool email(string mail)
        {
            bool correcto = false;
            string patron1 = @"^\S+@\w+\.*\w*\.com$";
            string patron2 = @"^\S+@\w+\.*\w*\.es$";

            if(Regex.Match(mail, patron1).Success)
            {
                correcto = true;
            }
            if(Regex.Match(mail, patron2).Success)
            {
                correcto = true;
            }
            
            return correcto;
        }
        public bool telefono(string tlf)
        {
            bool correcto = false;
            string patron1 = @"^\d{9}$";
            if (Regex.Match(tlf, patron1).Success)
            {
                correcto = true;
            }
                
            return correcto;
        }
        public bool matricula(string m)
        {
            bool correcto = false;
            string patron = @"^\d{4}\D{3}$";
             
            if(Regex.Match(m, patron).Success)
            {
                correcto = true;
            }

            return correcto;
        }
        public bool cadenaNum(string m)
        {
            bool correcto = false;
            string patron = @"^\d*$";

            if (Regex.Match(m, patron).Success)
            {
                correcto = true;
            }

            return correcto;
        }
    }
}
