﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENPaseoCaballo
    {
        // Constructor.
        public ENPaseoCaballo()
        {
            IdPaseo = 0;
            NombreRuta = "";
            Descripcion = "";
            Duracion = 0;
            Precio = 0;
            ConPony = false;
        }


        // Llamada al metodo insertar del CAD.
        public bool insertar(string nombreRuta, string descripcion, int duracion, float precio, bool conPony)
        {
            // No necesitamos pasar el parametro "idQuad" en la insercion porque es un atributo AUTO_INCREMENT.
            bool insertado = false;
            CADPaseoCaballo paseoCAD = new CADPaseoCaballo();

            if (paseoCAD != null)
            {
                NombreRuta = nombreRuta;
                Descripcion = descripcion;
                Duracion = duracion;
                Precio = precio;
                ConPony = conPony;
            }

            try
            {
                insertado = paseoCAD.insertarCAD(NombreRuta, Descripcion, Duracion, Precio, ConPony);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }

        public bool borrar(int idPaseo)
        {
            bool borrado = false;
            CADPaseoCaballo paseoCAD = new CADPaseoCaballo();

            try
            {
                borrado = paseoCAD.borrarCAD(idPaseo);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }

        // Llamada al metodo modificar del CAD.
        public bool modificar(int idPaseo, string nombreRuta, string descripcion, int duracion, float precio, bool conPony)
        {
            bool modificado = false;
            CADPaseoCaballo paseoCAD = new CADPaseoCaballo();

            if (paseoCAD != null)
            {
                IdPaseo = idPaseo;
                NombreRuta = nombreRuta;
                Descripcion = descripcion;
                Duracion = duracion;
                Precio = precio;
                ConPony = conPony;
            }

            try
            {
                modificado = paseoCAD.modificarCAD(IdPaseo, NombreRuta, Descripcion, Duracion, Precio, ConPony);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }

        // Llamada al metodo get del CAD para mostrarlos todos.
        public DataSet getTodos()
        {
            CADPaseoCaballo paseoCAD = new CADPaseoCaballo();
            DataSet resultado = null;

            try
            {
                resultado = paseoCAD.getTodosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        // Llamada al metodo get del CAD para mostrar el numero de quads total y en uso.
        public int getNumTotal(bool conPony)
        {
            int numPaseos = 0;
            CADPaseoCaballo paseoCAD = new CADPaseoCaballo();

            try
            {
                numPaseos = paseoCAD.getNumTotalCAD(conPony);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return numPaseos;
        }


        // ===================================================================================================================


        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdPaseo
        {
            get { return idPaseo; }
            set { idPaseo = value; }
        }
        public string NombreRuta
        {
            get { return nombreRuta; }
            set { nombreRuta = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public int Duracion
        {
            get { return duracion; }
            set { duracion = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        public bool ConPony
        {
            get { return conPony; }
            set { conPony = value; }
        }


        // ===================================================================================================================


        // Atributos privados de la Entidad.
        private int idPaseo;
        private string nombreRuta;
        private string descripcion;
        private int duracion; // Tiempo en minutos.
        private float precio;
        private bool conPony;
    }
}
