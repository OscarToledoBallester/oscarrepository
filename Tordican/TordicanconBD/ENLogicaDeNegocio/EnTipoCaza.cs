﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENLogicaDeNegocio
{
    public class EnTipoCaza
    {
        ///1-En Mano, 2-Ojeo, 3-Al Asalto, 4-Al Reclamo, 5-Al Pase
        private int tipo;
        ///Descripción
        private string desc;
        ///Precio
        private float precio;

        public int Tipo {
            get { return tipo; }
            set { tipo = value; }
        }
        public string Desc {
            get { return desc; }
            set { desc = value; }
        
        }
        public float Precio {
            get { return precio; }
            set { precio = value; }
        }
        public EnTipoCaza(int opc) {
            switch (opc) {
                case 0:
                    tipo = opc;
                    desc = "";
                    break;
                case 1:
                    tipo = opc;
                    desc = "En Mano";
                    break;
                case 2:
                    tipo = opc;
                    desc = "Ojeo";
                    break;
                case 3:
                    tipo = opc;
                    desc = "Al Asalto";
                    break;
                case 4:
                    tipo = opc;
                    desc = "Al Reclamo";
                    break;
                case 5:
                    tipo = opc;
                    desc = "Al Pase";
                    break;
                default:
                    
                    break;
            
            
            }
        
        }
    }
}
