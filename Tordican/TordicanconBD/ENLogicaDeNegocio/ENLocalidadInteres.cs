﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Persistencia;

namespace ENLogicaDeNegocio
{
    public class ENLocalidadInteres
    {
        public ENLocalidadInteres()
        {
            IdLocalidad = 0;
            Nombre = "";
            Descripcion = "";
            Distancia = 0;
            Precio = 0;
        }
        public bool insertar(string nombre, string descripcion, float distancia, float precio)
        {
            bool insertado = false;
            CADLocalidadInteres localiCAD = new CADLocalidadInteres();

            if (localiCAD != null)
            {
                Nombre = nombre;
                Descripcion = descripcion;
                Distancia = distancia;
                Precio = precio;
            }

            try
            {
                insertado = localiCAD.insertarCAD(Nombre, Descripcion, Distancia, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return insertado;
        }
        public bool borrar(int idLocalidad)
        {
            bool borrado = false;
            CADLocalidadInteres localiCAD = new CADLocalidadInteres();

            try
            {
                borrado = localiCAD.borrarCAD(idLocalidad);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return borrado;
        }
        public bool modificar(int idLocalidad, string nombre, string descripcion, float distancia, float precio)
        {
            bool modificado = false;
            CADLocalidadInteres localiCAD = new CADLocalidadInteres();

            if (localiCAD != null)
            {
                IdLocalidad = idLocalidad;
                Nombre = nombre;
                Descripcion = descripcion;
                Distancia = distancia;
                Precio = precio;
            }

            try
            {
                modificado = localiCAD.modificarCAD(IdLocalidad, Nombre, Descripcion, Distancia, Precio);
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return modificado;
        }
        public DataSet getTodos()
        {
            CADLocalidadInteres localiCAD = new CADLocalidadInteres();
            DataSet resultado = null;

            try
            {
                resultado = localiCAD.getTodosCAD();
            }
            catch (ModelException ex)
            {
                throw ex;
            }
            return resultado;
        }

        // ===================================================================================================================

        // Metodos publicos get/set para el acceso a las propiedades.
        public int IdLocalidad
        {
            get { return idLocalidad; }
            set { idLocalidad = value; }
        }
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public float Distancia
        {
            get { return distancia; }
            set { distancia = value; }
        }
        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        // ===================================================================================================================

        // Atributos privados de la Entidad.
        private int idLocalidad;
        private string nombre;
        private string descripcion;
        private float distancia;
        private float precio;
    }
}