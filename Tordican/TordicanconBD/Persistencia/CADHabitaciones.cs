﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADHabitaciones
    {
        private string conBD;

        public CADHabitaciones()
        {
            //Poner la bd en C
            conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";
        }

        public bool reservarHabitacionCAD(int numHab,int numP)
        {
            bool reservada = false;

            //string comando = "UPDATE Habitacion SET disponibilidad = 0 WHERE numero = ('"+numHab+"')";
            string comando = "Insert into Habitacion (numero,numPlanta,aTipo,disponibilidad) Values ('" + numHab + "', '" + numP + "', '" + numP + "' , '" + false + "')";
        
            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                reservada = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return reservada;
        }
        public bool cambiarDisopnibilidadAOcupada(int numHab, int numP)
        {
            bool cambiada = false;

            string comando = "UPDATE Habitacion SET disponibilidad = 1 WHERE numero = ('" + numHab + "') AND numPlanta = ('"+ numP+"')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                cambiada = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return cambiada;
        }
        public bool cambiarDisopnibilidadADisponible(int numHab, int numP)
        {
            bool cambiada = false;

            string comando = "UPDATE Habitacion SET disponibilidad = 0 WHERE numero = ('" + numHab + "') AND planta = ('" + numP + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                cambiada = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return cambiada;
        }

        public bool cancelarReservaHabitacionCAD(int numHab,int nump)
        {
            bool reservada = false;

            string comando = "Delete From Habitacion WHERE numero = ('" + numHab + "') AND numPlanta = '" + nump + "'";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                reservada = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return reservada;
        }

        public DataSet DevolverTodasHabitacionesCAD()
        {
            string comando = "SELECT * FROM Habitacion ";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Habitacion");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet DevolverLibresIndividualCAD()
        {
            string comando = "SELECT * FROM Habitacion WHERE disponibilidad = 0 AND aTipo = 1";

            DataSet resultado = new DataSet();
           

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Habitacion");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet DevolverLibresDoblesCAD()
        {
            string comando = "SELECT * FROM Habitacion WHERE disponibilidad = 0 AND aTipo = 2";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Habitacion");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet DevolverLibresSuiteCAD()
        {
            string comando = "SELECT * FROM Habitacion WHERE disponibilidad = 0 AND aTipo = 3";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Habitacion");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }

        //El DataSet solo contendra una variable que será la disponibilidad de la Habitación
        public DataSet consultarDisponibilidadHabCAD(int numHab,int numplan)
        {
            string comando = "SELECT disponibilidad FROM Habitacion WHERE numero = ('" + numHab + "') AND numPlanta = ('" + numplan + "')";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Habitacion");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
    }
}
