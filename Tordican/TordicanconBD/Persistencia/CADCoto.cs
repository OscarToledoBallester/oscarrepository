﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;




namespace Persistencia
{
    public class CADCoto
    {
        private const string conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";
        public const string nombreCoto = "Tordican";
        public CADCoto() {
        }

        public bool RellenarCoto() {
            bool hecho = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO Coto values ('" + nombreCoto + "',5000,3000,3000,2000,2000,500,500,200)";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
                hecho = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return hecho;
        }
        //1-piezasTordo
        //2-piezasPalomaTorcaz
        //3-piezasPalomaBravia
        //4-piezasPerdiz
        //5-piezasConeLie
        //6-piezasFaisan
        //7-piezasCodorniz
        //8-piezasBecada
        //Acceso desconectado
        public bool CADActualizarPiezas(int tipoPieza, int cazadas, int dia, int mes, int anyo) {
            bool hecho = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from DiaCaza where dia="+dia+" and mes="+mes+" and año="+anyo+"", conBD);
                da.Fill(bdvirtual, "DiaCaza");
                
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["DiaCaza"];
                if (t.Rows.Count != 0) { 
                    //hago la insercion y hecho = true
                    //Obtengo la fila, en este caso la única que tengo
                    DataRow fila = t.Rows[0];
                    //modifico el valor de la columna seleccionada
                    t.Rows[0][tipoPieza + 2] = cazadas;
                    //Construyo la sentencia
                    SqlCommandBuilder cbuilder = new SqlCommandBuilder(da);

                    //Actualizo la base de datos real
                    da.Update(bdvirtual, "DiaCaza");
                    hecho = true;
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return hecho;
                   
        }

        public bool CADRepoblar(int tipoPieza, int piezas)
        {
            bool hecho = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from Coto", conBD);
                da.Fill(bdvirtual, "coto");

                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["coto"];

                //Obtengo la fila, en este caso la única que tengo
                DataRow fila = t.Rows[0];

                //modifico el valor de la columna seleccionada
                t.Rows[0][tipoPieza] = (int)t.Rows[0][tipoPieza] + piezas;

                //Construyo la sentencia
                SqlCommandBuilder cbuilder = new SqlCommandBuilder(da);

                //Actualizo la base de datos real
                da.Update(bdvirtual, "coto");



            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return hecho;

        }

        public int CADObtenerTotales(int tipo) {
            int valor = 0;
            SqlConnection conexionBD = null;
            try
            {
                
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from Coto", conBD);
                da.Fill(bdvirtual, "coto");

                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["coto"];

                //modifico el valor de la columna seleccionada
                valor = (int)t.Rows[0][tipo];

            }
            catch (SqlException exc){
                throw exc;
            }
            finally{
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return valor;
                        
        }

        public DataSet CADObtenerContrataCoto(int dia, int mes, int anyo) {
            DataSet resultado = new DataSet();
            string comando = "SELECT numArea, dia, mes, año,tipoCaza, numEscopetas FROM ContrataCoto  WHERE dia = ('" + dia + "') and mes = ('" + mes + "') and año = ('" + anyo + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "ContrataCoto");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        
        
        }

        public string CADObtenerDescripcionTipoCaza(int tipo) {
            string desc = "";
            SqlConnection conexionBD = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT descripcion from TipoCaza where numTipo = "+tipo+"", conBD);
                da.Fill(bdvirtual, "tipo");

                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["tipo"];

                //modifico el valor de la columna seleccionada
                desc = t.Rows[0][0].ToString();

            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return desc;
        
        
        }


        public float CADObtenerPrecioReservaCaza(int area, int tipoCaza, int tipoComida, int perrera) {
            Persistencia.CADPerrera per = new CADPerrera();
            Persistencia.CADArea ar = new CADArea();
            Persistencia.CADTipoCaza caz = new CADTipoCaza();
            float valor = per.CADObtenerPrecioPerreras(perrera) + ar.CADGetPrecio(area) + ar.CADGetPrecioTipoComida(tipoComida) +(per.CADObtenerPrecioPerreras(perrera));
            return valor;
        }

        //Acceso desconectado porque solo insertaré cuando no exista
        public bool CADInsertarDiaCoto(int dia, int mes, int anyo) {
            bool hecho = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from DiaCaza where dia="+dia+" and mes="+mes+" and año="+anyo+"", conBD);
                da.Fill(bdvirtual, "dia");

                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["dia"];
                if (t.Rows.Count == 0) {
                    DataRow fila = t.NewRow();
                    fila["dia"] = dia;
                    fila["mes"] = mes;
                    fila["año"] = anyo;

                    t.Rows.Add(fila);
                    //Construyo la sentencia
                    SqlCommandBuilder cbuilder = new SqlCommandBuilder(da);

                    //Actualizo la base de datos real
                    da.Update(bdvirtual, "dia");
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return hecho;
                   

         
        }

        public bool CADComprobarContrataCoto(int area, int dia, int mes, int anyo) {
            bool hecho = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from ContrataCoto where numArea ="+area+" and dia=" + dia + " and mes=" + mes + " and año=" + anyo + "", conBD);
                da.Fill(bdvirtual, "ContrataCoto");

                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["ContrataCoto"];
                if (t.Rows.Count == 1)
                {
                    hecho = true;
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return hecho;
        
        
        }

        //Acceso desconectado porque solo modificare datos cuando exista el dia en DiaCaza
        public bool CADActualizarPiezasToday(int dia, int mes, int anyo) {
            bool hecho = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from DiaCaza where dia=" + dia + " and mes=" + mes + " and año=" + anyo + "", conBD);
                da.Fill(bdvirtual, "dia");
                //Obtengo la tabla DiaCaza
                DataTable t = new DataTable();
                t = bdvirtual.Tables["dia"];
                
                //Obtengo la tabla Coto
                DataSet bdvirtualCoto = new DataSet();
                SqlDataAdapter db = new SqlDataAdapter("select * from Coto",conBD);
                db.Fill(bdvirtualCoto, "coto");
                DataTable tb = new DataTable();
                tb = bdvirtualCoto.Tables["coto"];
                if (t.Rows.Count != 0)
                {
                    DataRow filaCoto = tb.Rows[0];
                    DataRow filaDia = t.Rows[0];
                    if (filaDia[3] != DBNull.Value)
                        filaCoto[1] = (int)filaCoto[1] - (int)filaDia[3];
                    if (filaDia[4] != DBNull.Value)
                        filaCoto[2] = (int)filaCoto[2] - (int)filaDia[4];
                    if (filaDia[5] != DBNull.Value)
                        filaCoto[3] = (int)filaCoto[3] - (int)filaDia[5];
                    if (filaDia[6] != DBNull.Value)
                        filaCoto[4] = (int)filaCoto[4] - (int)filaDia[6];
                    if (filaDia[7] != DBNull.Value)
                        filaCoto[5] = (int)filaCoto[5] - (int)filaDia[7];
                    if (filaDia[8] != DBNull.Value)
                        filaCoto[6] = (int)filaCoto[6] - (int)filaDia[8];
                    if (filaDia[9] != DBNull.Value)
                        filaCoto[7] = (int)filaCoto[7] - (int)filaDia[9];
                    if (filaDia[10] != DBNull.Value)
                        filaCoto[8] = (int)filaCoto[8] - (int)filaDia[10];

                    //Construyo la sentencia
                    SqlCommandBuilder cbuilder = new SqlCommandBuilder(db);

                    //Actualizo la base de datos real
                    db.Update(bdvirtualCoto, "coto");
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return hecho;

        }
       
        public bool CADInsertarContrataCoto(int numArea, int dia, int mes, int anyo, int aReserva, int tipoCaza, int tipoComida, int numEscopetas) {
            bool hecho = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO ContrataCoto values (" + numArea + "," + dia + "," + mes + "," + anyo + "," + aReserva + "," + tipoCaza + "," + tipoComida + "," + numEscopetas + ")";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
                hecho = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return hecho;
        
        }

        public bool CADEliminarContrataCoto(int res)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "Delete From ContrataCoto where aReserva = ('" + res + "')";

            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

    }
}
