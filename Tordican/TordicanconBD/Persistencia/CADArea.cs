﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;


namespace Persistencia
{
    public class CADArea
    {
        private const string conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";


        public bool CADCargaArea() {
            bool insert = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO Area values(1,750,1,150)";
            string comando2 = "INSERT INTO Area values(2,750,1,200)";
            string comando3 = "INSERT INTO Area values(3,750,1,300)";
            string comando4 = "INSERT INTO Area values(4,750,0,350)";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando2, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando3, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando4, conexionBD);
                cmd.ExecuteNonQuery();
                insert = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return insert;
        }

        public int CADGetTipoCazaMenor(int area, int dia, int mes, int anyo) { 
            SqlConnection conexionBD = null;
            int valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from ContrataCoto WHERE numArea='" + area + "' and dia='" + dia + "' and mes='" + mes + "' and año='" + anyo + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["area"];
                if (t.Rows.Count != 0) {
                    //Obtengo la fila, en este caso la única que tengo
                    DataRow fila = t.Rows[0];
                    //recupero el valor
                    valor = (int)t.Rows[0][5]; 
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }
        
        
        
        public int CADGetNumEscopetas(int area, int dia, int mes, int anyo)
        {
            SqlConnection conexionBD = null;
            int valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from ContrataCoto WHERE numArea='" + area + "' and dia='" + dia + "' and mes='" + mes + "' and año='" + anyo + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["area"];
                if (t.Rows.Count != 0)
                {
                    //Obtengo la fila, en este caso la única que tengo
                    DataRow fila = t.Rows[0];
                    //recupero el valor
                    valor = (int)t.Rows[0][7];
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }

        public bool CADGetEstado(int area)
        {
            SqlConnection conexionBD = null;
            bool valor;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from Area WHERE numero='" + area + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["area"];
                //Obtengo la fila, en este caso la única que tengo
                DataRow fila = t.Rows[0];
                //recupero el valor
                valor = (bool)t.Rows[0][2];


            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }
        public bool CADSetEstado(int area, bool es)
        {

            SqlConnection conexionBD = null;
            bool hecho = false;
            bool valor = false;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from Area where numero='" + area + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor
                
                valor = (bool)bdvirtual.Tables[0].Rows[0]["estado"];

                if (valor != es)
                {

                    da = new SqlDataAdapter("update Area set estado='" + es + "' where numero='" + area + "'", conBD);
                    da.Fill(bdvirtual, "datos");
                    hecho = true;
                }

            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return hecho;
             
        }
        public float CADGetPrecio(int area)
        {
            SqlConnection conexionBD = null;
            float valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from Area where numero='" + area + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor

                valor = float.Parse(bdvirtual.Tables[0].Rows[0]["precio"].ToString());

               }

            
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }
        public float CADGetPrecioTipoComida(int tipo)
        {
            SqlConnection conexionBD = null;
            float valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from Comida where numTipo='" + tipo + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor

                valor = float.Parse(bdvirtual.Tables[0].Rows[0]["precio"].ToString());

            }


            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }

        public string CADGetDescripcionComida(int tipo)
        {
            SqlConnection conexionBD = null;
            string valor = "";
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from Comida where numTipo='" + tipo + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor

                valor = bdvirtual.Tables[0].Rows[0]["descripcion"].ToString();

            }


            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }
    }
}
