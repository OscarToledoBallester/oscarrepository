﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;


namespace Persistencia
{
    public class CADVehiculoQuad
    {
        private string conBD; // Cadena de conexion (ver constructor de clase).

        // Constructor por defecto.
        public CADVehiculoQuad()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }

        // Inserta un quad en la BD.
        public bool insertarCAD(string matricula, string marca, string modelo, float potencia, float peso, float precio, DateTime fechaAlta, bool enUso)
        {
            bool insertado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string potencia_R = potencia.ToString().Replace(",", ".");
            string peso_R = peso.ToString().Replace(",", ".");
            string precio_R = precio.ToString().Replace(",", ".");
            // Reemplazo en tipos "bool" con lo siguiente: "true"-->"1" / "false"-->"0" 
            string enUso_R = (enUso) ? "1" : "0";
            // Reemplazo en tipos "DateTime"
            string fechaAlta_R = fechaAlta.Date.ToString();

            string comando = "INSERT INTO VehiculoQuad (matricula, marca, modelo, potencia, peso, precio, fechaAlta, enUso) " +
                             "VALUES ('" + matricula + "', '" + marca + "', '" + modelo + "', " + potencia_R + ", " + peso_R + ", " + precio_R + ", '" + fechaAlta_R + "', " + enUso_R + ");";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException)
            {
                //throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }

        // Modifica los atributos de un quad seleccionado mediante su atributo "idQuad" (clave primaria).
        public bool modificarCAD(int idQuad, string matricula, string marca, string modelo, float potencia, float peso, float precio, DateTime fechaAlta, bool enUso)
        {
            bool modificado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string potencia_R = potencia.ToString().Replace(",", ".");
            string peso_R = peso.ToString().Replace(",", ".");
            string precio_R = precio.ToString().Replace(",", ".");
            // Reemplazo en tipos "bool" con lo siguiente: "true"-->"1" / "false"-->"0" 
            string enUso_R = (enUso) ? "1" : "0";
            // Reemplazo en tipos "DateTime"
            string fechaAlta_R = fechaAlta.Date.ToString();

            string comando = "UPDATE VehiculoQuad " +
                             "SET matricula = '" + matricula + "', " +
                                 "marca = '" + marca + "', " +
                                 "modelo = '" + modelo + "', " +
                                 "potencia = " + potencia_R + ", " +
                                 "peso = " + peso_R + ", " +
                                 "precio = " + precio_R + ", " +
                                 "fechaAlta = '" + fechaAlta_R + "', " +
                                 "enUso = " + enUso_R + " " +
                             "WHERE idQuad = " + idQuad + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificado = true;
            }
            catch (SqlException)
            {
                //throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificado;
        }

        // Borra un quad seleccionado mediante su ID.
        public bool borrarCAD(int idQuad)
        {
            bool borrado = false;
            string comando = "DELETE FROM VehiculoQuad " +
                             "WHERE idQuad = " + idQuad + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }
        
        // Devuelve un DataSet con los quads de la BD. ("disponibles = true" -> Devuelve SOLO los disponibles).
        public DataSet getTodosCAD(bool disponibles, string filtro, string cadenaBuscar)
        {
            // Muestra todos los quads, tanto disponibles como no disponibles.
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            // Seleccion del comando adecuado: Solo disponibles / Todos.
            switch (filtro)
            {
                case "marca": // Quads filtrados por marca.
                    if (disponibles == true)
                    {
                        comando = "SELECT * " +
                                  "FROM VehiculoQuad " +
                                  "WHERE marca LIKE '%" + cadenaBuscar + "%' " +
                                  "AND enUso = 0;";
                    }
                    else
                    {
                        comando = "SELECT * " +
                                  "FROM VehiculoQuad " +
                                  "WHERE marca LIKE '%" + cadenaBuscar + "%';";
                    }
                    break;

                case "matricula": // Quads filtrados por matricula.
                    if (disponibles == true)
                    {
                        comando = "SELECT * " +
                                  "FROM VehiculoQuad " +
                                  "WHERE matricula LIKE '%" + cadenaBuscar + "%' " +
                                  "AND enUso = 0;";
                    }
                    else
                    {
                        comando = "SELECT * " +
                                  "FROM VehiculoQuad " +
                                  "WHERE matricula LIKE '%" + cadenaBuscar + "%';";
                    }
                    break;

                case "todos": // SIN FILTROS, muestra todos los quads.
                    if (disponibles == true)
                    {
                        comando = "SELECT * " +
                                  "FROM VehiculoQuad " +
                                  "WHERE enUso = 0;";
                    }
                    else
                    {
                        comando = "SELECT * " +
                                  "FROM VehiculoQuad;";
                    }
                    break;
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "VehiculoQuad");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }

        // Obtiene el numero total de quads disponibles.
        public int getNumTotalCAD(bool estanEnUso)
        {
            int numQuads = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            // Seleccion del comando adecuado
            if (estanEnUso == true) // Muestra SOLO los que estan en uso.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM VehiculoQuad " +
                          "WHERE enUso = 1;";
            }
            else // Muestra todos los existentes en la BD.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM VehiculoQuad;";
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "VehiculoQuad");

                // Cogemos del DataSet el valor del COUNT(*)
                numQuads = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return numQuads;
        }

        // Obtiene el precio de un idQuad seleccionado.
        public float getPrecio(int idQuad)
        {
            // Si devuelve -1 es que no existe precio para el idQuad seleccionado.
            // Es decir, ese idQuad no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT precio " +
                             "FROM VehiculoQuad " +
                             "WHERE idQuad = " + idQuad + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "VehiculoQuad");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }


        public bool existeMatricula(string matricula)
        {
            bool existe = false;
            SqlConnection conn = new SqlConnection(conBD);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT matricula " +
                              "FROM VehiculoQuad " +
                              "WHERE matricula = '" +  matricula + "';";
            try
            {
                //conn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                da.Fill(ds, "quad");

                // Comprobamos si existe la matricula. Si existe, devuelve una fila.
                if (ds.Tables[0].Rows.Count > 0)
                    existe = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
             //   if (conn != null)
                    
            }
            return existe;
        }
    }
}
