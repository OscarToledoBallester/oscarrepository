﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADReserva
    {
        private string conBD;

        public CADReserva()
        {
            conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";
        }

        public bool NuevaReserva(DateTime fechaReg,string dni,string tipoAct,int idAct,int np,DateTime fi,int nDias,int res)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "";

            if(idAct != 0)
                comando = "Insert into Reserva (numReserva,fechaReserva,aCliente,aAlojamiento," + tipoAct + ") VALUES ('" + res + "', '" + fechaReg + "', '" + dni + "', '" + np + "', '" + idAct + "')";
            else
                comando = "Insert into Reserva (numReserva,fechaReserva,aCliente,aAlojamiento) VALUES ('" +res + "', '" + fechaReg + "', '" + dni + "', '" + np + "')";
           
            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

        public bool NuevaReservaCaza(DateTime fechaReg, string dni,int res)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "";
            int i = 4;
            comando = "Insert into Reserva (numReserva,fechaReserva,aCliente,aAlojamiento) VALUES ('" + res + "', '" + fechaReg + "', '" + dni + "', '" + i + "')";

            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

        public void MirarDiaAlojamiento(int d, int m, int a)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT * FROM DiaAlojamiento WHERE dia = ('" + d + "') AND mes= ('" + m + "') AND año= ('" + a + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "DiaAlojamiento");
                //conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado.Tables[0].Rows.Count == 0)
            {
                comando = "Insert into DiaAlojamiento Values ('" + d + "', '" + m + "', '" + a + "')";
                try
                {
                    conexionBD = new SqlConnection(conBD);
                    adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                    adaptadorSQL.Fill(resultado, "DiaAlojamiento");
                    conexionBD.Close();
                }
                catch (SqlException exc)
                {
                    throw exc;
                }
            }
            else
                conexionBD.Close();


        }

        public bool EnContrataHab(int nh, int np, int dia, int mes, int an, int aReserva,int dOc)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "Insert into ContrataHabitacion Values('" + nh + "', '" + np + "', '" + dia + "', '" + mes +"', '" + an + "','" + aReserva + "', '" + dOc +"')";
           
            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

        public int UltimaReserva()
        {
            int ult = 0;
            string comando = "Select MAX(numReserva) from Reserva";
            SqlConnection c = new SqlConnection(conBD);
            DataSet resultado = new DataSet();
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "Reserva");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado != null && resultado.Tables[0].Rows.Count > 0)
            {
                if (resultado.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                    ult = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());  
            }
            return ult + 1;
        }

        public DataSet ConsultarReservas(string dni, string fec)
        {
            DataSet reservas = new DataSet();
            string comando;
            if (fec == "")
            {
                comando = "SELECT res.numReserva, res.fechaReserva, hb.numPlanta, hb.numHabitacion, hb.diasOcupacion, alo.precio, res.aQuad, res.aBicicleta, res.aPaseo, res.aTiro, " + 
                           "res.aPiraguismo, res.aRapel, res.aParque, res.aLocalidad " +
                           "FROM Reserva AS res INNER JOIN ContrataHabitacion AS hb ON res.numReserva = hb.aReserva " +
                           "INNER JOIN Alojamiento AS alo ON res.aAlojamiento = alo.numTipo WHERE (res.aCliente = '" + dni + "')";
            }
            else
            {
                comando = "SELECT res.numReserva, res.fechaReserva, hb.numPlanta, hb.numHabitacion, hb.diasOcupacion, alo.precio, res.aQuad, res.aBicicleta, res.aPaseo, res.aTiro, " +
                           "res.aPiraguismo, res.aRapel, res.aParque, res.aLocalidad " +
                           "FROM Reserva AS res INNER JOIN ContrataHabitacion AS hb ON res.numReserva = hb.aReserva " +
                           "INNER JOIN Alojamiento AS alo ON res.aAlojamiento = alo.numTipo WHERE (res.aCliente = '" + dni + "') AND (res.FechaReserva = '" + fec +"')";

            }
            try
            {
                SqlConnection con = new SqlConnection(conBD);
                SqlDataAdapter adap = new SqlDataAdapter(comando, con);
                adap.Fill(reservas, "Reserva");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return reservas;
        }

        public DataSet TieneCaza(string nif,DateTime f1,DateTime f2)
        {
            string comando = "";
            DataSet caza = new DataSet();
            comando = "SELECT  ContrataCoto.aReserva, Reserva.fechaReserva, ContrataCoto.numArea, ContrataCoto.tipoCaza, ContrataCoto.tipoComida, ContrataCoto.numEscopetas " +
                "FROM Reserva INNER JOIN ContrataCoto ON Reserva.numReserva = ContrataCoto.aReserva " +
                "WHERE (Reserva.aCliente = '"+ nif +"') AND (Reserva.fechaReserva >= '"+ f1 +"') AND (Reserva.fechaReserva <= '" + f2 +"') ORDER BY Reserva.fechaReserva";

            try
            {
                SqlConnection con = new SqlConnection(conBD);
                SqlDataAdapter adap = new SqlDataAdapter(comando, con);
                adap.Fill(caza, "Reserva");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return caza;
        }

        public DataSet SoloTieneCaza(string nif)
        {
            DataSet reservas = new DataSet();
            string comando = "SELECT res.numReserva, res.fechaReserva, alo.descripcion, alo.precio " +
                             "FROM Reserva AS res INNER JOIN Alojamiento AS alo ON res.aAlojamiento = alo.numTipo WHERE (res.aCliente = '" + nif + "')";
            try
            {
                SqlConnection con = new SqlConnection(conBD);
                SqlDataAdapter adap = new SqlDataAdapter(comando, con);
                adap.Fill(reservas, "Reserva");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return reservas;
        }

        public int Perrera(int res)
        {
            int ult = 0;
            string comando = "Select numero From Perrera Where aReserva = '" + res + "'";
            SqlConnection c = new SqlConnection(conBD);
            DataSet resultado = new DataSet();
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "Perrera");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado != null && resultado.Tables[0].Rows.Count > 0)
            {
                if (resultado.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                    ult = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());  
            }
            return ult;
        }

        public int UnaReserva(string dni)
        {
            string comando = "Select numReserva From Reserva where aCliente = '" + dni + "'";
            int ult = 0;

            SqlConnection c = new SqlConnection(conBD);
            DataSet resultado = new DataSet();
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "Reserva");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado != null && resultado.Tables["Reserva"].Rows.Count > 0)
                ult = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            return ult;
        }

        public int UnaTipoPlanta(int res)
        {
            string comando = "Select aAlojamiento From Reserva where numReserva = '" + res + "'";
            int ult = 0;

            SqlConnection c = new SqlConnection(conBD);
            DataSet resultado = new DataSet();
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "Reserva");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado != null && resultado.Tables["Reserva"].Rows.Count > 0)
                ult = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            return ult;
        }

        public int UnaHabitacion(int res)
        {
            string comando = "Select numHabitacion From ContrataHabitacion where aReserva = '" + res + "'";
            int ult = 0;

            SqlConnection c = new SqlConnection(conBD);
            DataSet resultado = new DataSet();
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "Reserva");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado != null && resultado.Tables["Reserva"].Rows.Count > 0)
                ult = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            return ult;
        }

        public bool EliminarReserva(string nif,int res)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "Delete From Reserva where aCliente = ('" + nif + "') AND numReserva = ('"+ res +"')";

            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

        public bool EliminarEnContrataHabitacion(int res)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "DELETE FROM ContrataHabitacion where aReserva ='" + res +"'";

            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

        public DataSet CazaDNI(string nif, string fecha)
        {
            string comando = "";
            DataSet caza = new DataSet();
            

            if (fecha == "")
            {
                comando = "SELECT  ContrataCoto.aReserva, Reserva.fechaReserva, ContrataCoto.numArea, ContrataCoto.tipoCaza, ContrataCoto.tipoComida, ContrataCoto.numEscopetas " +
                    "FROM Reserva INNER JOIN ContrataCoto ON Reserva.numReserva = ContrataCoto.aReserva " +
                    "WHERE (Reserva.aCliente = '" + nif + "')ORDER BY Reserva.fechaReserva ASC";
            }
            else
            {
                DateTime f1 = DateTime.Parse(fecha);
                comando = "SELECT  ContrataCoto.aReserva, Reserva.fechaReserva, ContrataCoto.numArea, ContrataCoto.tipoCaza, ContrataCoto.tipoComida, ContrataCoto.numEscopetas " +
                    "FROM Reserva INNER JOIN ContrataCoto ON Reserva.numReserva = ContrataCoto.aReserva " +
                    "WHERE (Reserva.aCliente = '" + nif + "') AND (Reserva.fechaReserva >= '" + f1 + "')ORDER BY Reserva.fechaReserva";
            }

            try
            {
                SqlConnection con = new SqlConnection(conBD);
                SqlDataAdapter adap = new SqlDataAdapter(comando, con);
                adap.Fill(caza, "Reserva");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return caza;
        }

        /*public bool ALgo()
        {
            bool si = false;

            string comando = "Select * from Alojamiento al, Reserva re " +
                              "WHERE re.aCliente = '" + nif + "' AND re.aAlojamiento = al.numTipo";
            SqlConnection c = new SqlConnection(conBD);
            DataSet resultado = new DataSet();
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "Reserva");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if (resultado.Tables[0].Rows.Count > 0)
                si = true;

            return si;
        }*/

        public DataSet TodoCaza(int res,int d,int m,int a)
        {
            string comando = "Select * From ContrataCoto where aReserva = '" + res + "' AND dia = '"+ d +"' AND mes = '" + m +"' AND año = '" + a + "'";
            DataSet resultado = new DataSet();
            SqlConnection c = new SqlConnection(conBD);
            c.Open();
            SqlCommand cmd = new SqlCommand(comando, c);
            SqlDataAdapter adaptadorSQL;

            try
            {
                adaptadorSQL = new SqlDataAdapter(comando, c);
                adaptadorSQL.Fill(resultado, "ContrataCoto");
                c.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public bool EliminaReservaCaza(int res)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "Delete From Reserva where numReserva = ('" + res + "')";

            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }
    }
}
