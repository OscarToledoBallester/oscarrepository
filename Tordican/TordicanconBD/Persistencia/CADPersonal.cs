﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;


namespace Persistencia
{
    public class CADPersonal
    {
        private string conBD;

        public CADPersonal()
        {
            conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";
        }
        public bool nuevoPersonal(string idni, string nom, string ap1, string ap2, string nss, string direc, string ipais, string prov, string tlf, string correoe, bool tipoAdmin, string log, string pass)
        {
            bool insertado = false;
            int ad;
            if (tipoAdmin == true)
            {
                ad = 1;
            }
            else
            {
                ad = 0;
            }
            string comando = "INSERT INTO Personal values('" + idni + "','" + nom + "','" + ap1 + "','" + ap2 + "','" + direc + "','" + ipais + "','" + prov + "','" + tlf + "','" + correoe + "','" + log + "','" + pass + "','" + ad + "','" + nss + "')";


            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return insertado;
        }
        
        public DataSet busquedaPersonalDni(string opTipo, string idni)
        {
            string comando;
            int nopTipo;
            
            DataSet resultado = new DataSet();
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE dni = ('" + idni + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE dni = ('" + idni + "') and esAdmin = ('" + nopTipo + "')";
            }

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalUsuario(string usuario)
        {
            string comando;

            DataSet resultado = new DataSet();
            comando = "SELECT dni FROM Personal WHERE usuario = ('" + usuario + "')";
            

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalNom(string opTipo, string iNom)
        {
            DataSet resultado = new DataSet();
            int nopTipo;
            string comando;
                       
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE nombre = ('" + iNom + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE nombre = ('" + iNom + "') and esAdmin = ('" + nopTipo + "')";
            }
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalAp1(string opTipo, string iAp1)
        {
            DataSet resultado = new DataSet();
            string comando;
            int nopTipo;
            
            
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE apellido1 = ('" + iAp1 + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE apellido1 = ('" + iAp1 + "') and esAdmin = ('" + nopTipo + "')";
            }
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalAp2(string opTipo, string iAp2)
        {
            DataSet resultado = new DataSet();
            string comando;
            int nopTipo;
            
            
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE apellido2 = ('" + iAp2 + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE apellido2 = ('" + iAp2 + "') and esAdmin = ('" + nopTipo + "')";
            }

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalTlf(string opTipo, string iTlf)
        {
            DataSet resultado = new DataSet();
            string comando;
            int nopTipo;
            
            
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE telefono = ('" + iTlf + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE telefono = ('" + iTlf + "') and esAdmin = ('" + nopTipo + "')";
            }
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalEMail(string opTipo, string iEMail)
        {
            DataSet resultado = new DataSet();
            string comando;
            int nopTipo;
            
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE email = ('" + iEMail + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE email = ('" + iEMail + "') and esAdmin = ('" + nopTipo + "')";
            }          

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalPais(string opTipo, string iPais)
        {
            DataSet resultado = new DataSet();
            string comando;
            int nopTipo;
            
            
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE pais = ('" + iPais + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE pais = ('" + iPais + "') and esAdmin = ('" + nopTipo + "')";
            } 

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalProv(string opTipo, string iProv)
        {
            DataSet resultado = new DataSet();
            string comando;
            int nopTipo;
            
           
            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE provincia = ('" + iProv + "')";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;
                    
                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE provincia = ('" + iProv + "') and esAdmin = ('" + nopTipo + "')";
            }           

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaPersonalTipo(string opTipo)
        {
            DataSet resultado = new DataSet();

            int nopTipo;
            string comando;

            if (opTipo == "Todos")
            {
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal";
            }
            else
            {
                if (opTipo == "Empleado")
                {
                    nopTipo = 0;
                }
                else
                {
                    nopTipo = 1;

                }
                comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email, esAdmin, nss FROM Personal WHERE esAdmin = ('" + nopTipo + "')";
            } 

            

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public bool modificarPersonal(string ndni, string nnom, string nape1, string nape2, string nss, string ndir, string npais, string nprov, string ntelf, string nemail, bool tipoAdmin)
        {
            bool insertado = false;
            string comando = "UPDATE Personal SET nombre = '" + nnom + "', apellido1 = '" + nape1 + "', apellido2 = '" + nape2 + "', direccion = '" + ndir + "', pais = '" + npais + "', provincia = '" + nprov + "', telefono = '" + ntelf + "', email = '" + nemail + "', esAdmin = '" + tipoAdmin  + "', nss = '" + nss  + "' WHERE dni = ('" + ndni + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return insertado;
        }
        public bool bajaPersonal(string idni)
        {
            bool insertado = false;
            string comando = "DELETE FROM Personal WHERE Personal.dni =('" + idni + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return insertado;
        }
        public DataSet mostrarPersonal()
        {
            DataSet resultado = new DataSet();

            string comando = "SELECT * FROM Personal";
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet busquedaUC(string idni)
        {
            string comando;
            

            DataSet resultado = new DataSet();
            comando = "SELECT usuario, contraseña FROM Personal WHERE dni = ('" + idni + "')";
            
          
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Personal");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public bool modificarUC(string ndni, string iusuario, string ipass)
        {
            bool insertado = false;
            string comando = "UPDATE Personal SET usuario = '" + iusuario + "', contraseña = '" + ipass + "' WHERE dni = ('" + ndni + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return insertado;
        }
    }
}
