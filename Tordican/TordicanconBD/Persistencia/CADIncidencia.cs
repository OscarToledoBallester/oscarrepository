﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADIncidencia
    {

        private string conBD; // Cadena de conexion (ver constructor de clase).

        // Constructor por defecto.
        public CADIncidencia()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }
        public bool insertarCAD(string descripcion, DateTime fecha, bool revisada, int actividad)
        {
            bool insertado = false;

            // Reemplazos.
            int revisada_R = (revisada) ? 1 : 0; // "true"-->"1" / "false"-->"0"
            string fecha_R = fecha.Date.ToString();

            string comando = "INSERT INTO Incidencia (descripcion, fecha, revisada, aActividad) " +
                             "VALUES ('" + descripcion + "', '" + fecha_R + "', " + revisada_R + ", " + actividad + ");";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
 
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }
        public bool modificarCAD(int idIncidencia, string descripcion, DateTime fecha, bool revisada, int actividad)
        {
            bool modificada = false;

            // Reemplazos.
            int revisada_R = (revisada) ? 1 : 0; // "true"-->"1" / "false"-->"0"
            string fecha_R = fecha.Date.ToString();
            string comando = "UPDATE Incidencia " +
                             "SET descripcion = '" + descripcion + "', " +
                                 "fecha = '" + fecha_R + "', " +
                                 "revisada = " + revisada_R + ", " +
                                 "aActividad = " + actividad + " " +
                             "WHERE idIncidencia = " + idIncidencia + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificada = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificada;
        }
        public bool borrarCAD(int idIncidencia)
        {
            bool borrado = false;
            string comando = "DELETE FROM Incidencia " +
                             "WHERE idIncidencia = " + idIncidencia + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }
        public DataSet getTodasCAD(bool pendientesRevisar)
        {
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            if (pendientesRevisar == true) // Mostramos las pendientes de revisar (No revisadas).
            {
                comando = "SELECT * " + 
                          "FROM Incidencia " +
                          "WHERE revisada = 0 " +
                          "ORDER BY fecha ASC;";
            }
            else // Mostramos todas.
            {
                comando = "SELECT * " +
                          "FROM Incidencia " +
                          "ORDER BY fecha ASC;";
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Incidencia");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }
        public int getNumTotalCAD(bool pendientesRevisar)
        {
            int numIncidencias = 0;
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            if (pendientesRevisar == true) // SOLO las no revisadas (Pendientes).
            {
                comando = "SELECT COUNT(*) " +
                          "FROM Incidencia " +
                          "WHERE revisada = 0;";
            }
            else // Todas las de la tabla.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM Incidencia;";
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Incidencia");

                
                // Cogemos del DataSet el valor del COUNT(*)
                numIncidencias = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numIncidencias;
        }
    }
}