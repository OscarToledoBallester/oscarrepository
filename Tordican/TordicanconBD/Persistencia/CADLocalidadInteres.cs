﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADLocalidadInteres
    {
        private string conBD; // Cadena de conexion.
        public CADLocalidadInteres()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }
        public bool insertarCAD(string nombre, string descripcion, float distancia, float precio)
        {
            bool insertado = false;
            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string distancia_R = distancia.ToString().Replace(",", ".");
            string precio_R = precio.ToString().Replace(",", ".");

            string comando = "INSERT INTO LocalidadInteres (nombre, descripcion, distancia, precio) " +
                             "VALUES ('" + nombre + "', '" + descripcion + "', " + distancia_R + ", " + precio_R + ");";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException)
            {
                //throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }
        public bool modificarCAD(int idLocalidad, string nombre, string descripcion, float distancia, float precio)
        {
            bool modificado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string distancia_R = distancia.ToString().Replace(",", ".");
            string precio_R = precio.ToString().Replace(",", ".");

            string comando = "UPDATE LocalidadInteres " +
                             "SET nombre = '" + nombre + "', " +
                                 "descripcion = '" + descripcion + "', " +
                                 "distancia = " + distancia_R + ", " +
                                 "precio = " + precio_R + " " +
                             "WHERE idLocalidad = " + idLocalidad + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificado = true;
            }
            catch (SqlException)
            {
                //throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificado;
        }
        public bool borrarCAD(int idLocalidad)
        {
            bool borrado = false;
            string comando = "DELETE FROM LocalidadInteres " +
                             "WHERE idLocalidad = " + idLocalidad + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }    
        public DataSet getTodosCAD()
        {
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT * " +
                             "FROM LocalidadInteres;";

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "LocalidadInteres");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }

        public float getPrecio(int idLocalidad)
        {
            // Si devuelve -1 es que no existe precio para el idLocalidad seleccionado.
            // Es decir, ese idLocalidad no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT precio " +
                             "FROM LocalidadInteres " +
                             "WHERE idLocalidad = " + idLocalidad + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "LocalidadInteres");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }
    }
}