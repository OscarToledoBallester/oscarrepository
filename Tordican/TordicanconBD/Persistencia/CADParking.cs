﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADParking
    {
        private int numPlazasRemolques = 20;

        public CADParking()
        {
            conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";

        }

        public bool CADCargarTipoPlazaParking()
        {
            
            bool insert = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO TipoPlazaParking values(0,'Plaza para cualquier tipo de vehiculo',30)";
            string comando2 = "INSERT INTO TipoPlazaParking values(1,'Plaza para remolques ideal para los clientes amantes de la caza',30)";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando2, conexionBD);
                cmd.ExecuteNonQuery();
                
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return insert;
        
        
        }

        private string conBD;

        public bool nuevaPlazaRemolqueCAD(int numP, int tipo, int res)
        {
            bool insertado = false;
            int i;
            DataSet resultado = new DataSet();
            for (i = 1; i <= numPlazasRemolques && !insertado; i++)
            {
                resultado = comprobarOcupaciónPlazaRem(i);
                if (resultado.Tables[0].Rows.Count == 0)
                {
                    string comando = "Insert into Parking (numero,aTipo,aReserva) Values ('" + i + "', '" + tipo + "', '" + (res - 1) + "')";

                    SqlConnection conexionBD;
                    SqlCommand comandoBD;
                    try
                    {
                        conexionBD = new SqlConnection(conBD);
                        conexionBD.Open();
                        comandoBD = new SqlCommand(comando, conexionBD);
                        comandoBD.CommandType = CommandType.Text;
                        comandoBD.ExecuteNonQuery();
                        insertado = true;
                    }
                    catch (SqlException exc)
                    {
                        throw exc;
                    }

                }
            }


            return insertado;
        }


        public bool nuevaPlazaCAD(int numP, int tipo,int res)
        {
            bool insertado = false;

            string comando = "Insert into Parking (numero,aTipo,aReserva) Values ('" + numP + "', '" + tipo + "', '" + (res - 1) + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return insertado;        
        }

        public int numPlazasRemolquesCAD()
        {
            int num;

            string comando = "SELECT * FROM Parking WHERE aTipo = 1";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Parking");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            num = resultado.Tables[0].Rows.Count;

            return num;
        
        }

        public int numPlazasVehiculosCAD()
        {
            int num;

            string comando = "SELECT * FROM Parking WHERE aTipo = 0";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Parking");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            num = resultado.Tables[0].Rows.Count;

            return num;

        }

        public bool cancelarPlazaCAD(int numP, int tipo, int res)
        {
            bool insertado = false;

            string comando = "Delete From Parking WHERE numero = ('" + numP + "') AND aTipo = ('" + tipo + "') AND  aReserva = ('" + res + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return insertado;
        }

        public bool cancelaParkin(int res)
        {
            bool eliminado = false;

            string comando = "Delete From Parking WHERE aReserva = ('" + res + "')";

            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                eliminado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return eliminado;
        }

        public DataSet devuelvePlazasTurismosCAD()
        {
            string comando = "SELECT numero FROM Parking WHERE aTipo = 0";

            DataSet resultado = new DataSet();


            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Parking");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }

        public DataSet comprobarOcupaciónPlazaTur(int numP)
        {
            string comando = "SELECT * FROM Parking WHERE aTipo = 0 AND numero = ('" + numP + "')";

            DataSet resultado = new DataSet();

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Parking");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return resultado;
        }

        public float PrecioPlaza(int res,int tipo)//Añadido tipo para normal o remolque
        {
            /*string comando = "SELECT p.numero,  p.aReserva, pa.tipo, pa.descripcion, pa.precio " +
                             "FROM Parking AS p INNER JOIN " +
                             "TipoPlazaParking AS pa ON p.aTipo = pa.tipo WHERE (p.aReserva = '" + res + "')";*/

            string comando= "SELECT TipoPlazaParking.precio FROM TipoPlazaParking INNER JOIN Parking ON TipoPlazaParking.tipo=Parking.aTipo " +
                "WHERE (Parking.aTipo='" + tipo + "') AND (Parking.aReserva='"+ res +"')";

            DataSet resultado = new DataSet();
            float pr = 0;

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Parking");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            if(resultado != null && resultado.Tables[0].Rows.Count > 0)
                pr = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            
            return pr;
        }

        public DataSet comprobarOcupaciónPlazaRem(int numP)
        {
            string comando = "SELECT * FROM Parking WHERE aTipo = 1 AND numero = ('" + numP + "')";

            DataSet resultado = new DataSet();

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Parking");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return resultado;
        }


    }
}
