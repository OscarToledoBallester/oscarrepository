﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADPaseoCaballo
    {
        private string conBD; // Cadena de conexion (ver constructor de clase).

        // Constructor por defecto.
        public CADPaseoCaballo()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }

        // Inserta un quad en la BD.
        public bool insertarCAD(string nombreRuta, string descripcion, int duracion, float precio, bool conPony)
        {
            bool insertado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");
            // Reemplazo en tipos "bool" con lo siguiente: "true"-->"1" / "false"-->"0" 
            string conPony_R = (conPony) ? "1" : "0";

            string comando = "INSERT INTO PaseoCaballo (nombreRuta, descripcion, duracion, precio, conPony) " +
                             "VALUES ('" + nombreRuta + "', '" + descripcion + "', " + duracion + ", " + precio_R + ", " + conPony_R + ");";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }

        // Modifica los atributos de un quad seleccionado mediante su atributo "idQuad" (clave primaria).
        public bool modificarCAD(int idPaseo, string nombreRuta, string descripcion, int duracion, float precio, bool conPony)
        {
            bool modificado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");
            // Reemplazo en tipos "bool" con lo siguiente: "true"-->"1" / "false"-->"0" 
            string conPony_R = (conPony) ? "1" : "0";

            string comando = "UPDATE PaseoCaballo " +
                             "SET nombreRuta = '" + nombreRuta + "', " +
                                 "descripcion = '" + descripcion + "', " +
                                 "duracion = " + duracion + ", " +
                                 "precio = " + precio_R + ", " +
                                 "conPony = " + conPony_R + " " +
                             "WHERE idPaseo = " + idPaseo + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificado;
        }

        // Borra un quad seleccionado mediante su ID.
        public bool borrarCAD(int idPaseo)
        {
            bool borrado = false;
            string comando = "DELETE FROM PaseoCaballo " +
                             "WHERE idPaseo = " + idPaseo + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }
        
        // Devuelve un DataSet con los quads de la BD. ("disponibles = true" -> Devuelve SOLO los disponibles).
        public DataSet getTodosCAD()
        {
            // Muestra todos los quads, tanto disponibles como no disponibles.
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT * " +
                             "FROM PaseoCaballo;";

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "PaseoCaballo");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }

        // Obtiene el numero total de quads disponibles.
        public int getNumTotalCAD(bool conPony)
        {
            int numPaseos = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            // Seleccion del comando adecuado
            if (conPony == true) // Muestra SOLO los que estan en uso.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM PaseoCaballo " +
                          "WHERE conPony = 1;";
            }
            else // Muestra todos los existentes en la BD.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM PaseoCaballo;";
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "PaseoCaballo");

                // Cogemos del DataSet el valor del COUNT(*)
                numPaseos = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numPaseos;
        }

        public float getPrecio(int idPaseo)
        {
            // Si devuelve -1 es que no existe precio para el idPaseo seleccionado.
            // Es decir, ese idPaseo no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT precio " +
                             "FROM PaseoCaballo " +
                             "WHERE idPaseo = " + idPaseo + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "PaseoCaballo");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }
    }
}
