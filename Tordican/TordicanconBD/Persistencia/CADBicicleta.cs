﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADBicicleta
    {
        private string conBD; // Cadena de conexion (ver constructor de clase).

        // Constructor por defecto.
        public CADBicicleta()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }

        public bool insertarCAD(int aTipo, DateTime fechaAlta, bool enUso)
        {
            bool insertado = false;

            // Reemplazos.
            int enUso_R = (enUso) ? 1 : 0; // "true"-->"1" / "false"-->"0"
            string fechaAlta_R = fechaAlta.Date.ToString();

            string comando = "INSERT INTO Bicicleta (tipo, fechaAlta, enUso) " +
                             "VALUES (" + aTipo + ", '" + fechaAlta_R + "', " + enUso_R + ");";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
 
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }

        public bool modificarCAD(int idBici, int tipo, DateTime fechaAlta, bool enUso)
        {
            bool modificada = false;

            // Reemplazos.
            int enUso_R = (enUso) ? 1 : 0; // "true"-->"1" / "false"-->"0"
            string fechaAlta_R = fechaAlta.Date.ToString();
            string comando = "UPDATE Bicicleta " +
                             "SET tipo = " + tipo + ", " +
                                 "fechaAlta = '" + fechaAlta_R + "', " +
                                 "enUso = " + enUso_R + " " +
                             "WHERE idBici = " + idBici + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificada = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificada;
        }

        public bool borrarCAD(int idBici)
        {
            bool borrado = false;
            string comando = "DELETE FROM Bicicleta " +
                             "WHERE idBici = " + idBici + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }

        public void getPreciosCAD(ref float precioNino, ref float precioAdulto, ref float precioTandem)
        {
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            comando = "SELECT precio " +
                      "FROM TipoBicicleta " +
                      "ORDER BY idTipo;";

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "TipoBicicleta");

                precioNino = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
                precioAdulto = float.Parse(resultado.Tables[0].Rows[1].ItemArray[0].ToString());
                precioTandem = float.Parse(resultado.Tables[0].Rows[2].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
        }


        public bool setPreciosCAD(float precioNino, float precioAdulto, float precioTandem)
        {
            bool modificados = false;
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precioNino_R = precioNino.ToString().Replace(",", ".");
            string precioAdulto_R = precioAdulto.ToString().Replace(",", ".");
            string precioTandem_R = precioTandem.ToString().Replace(",", ".");

            string comandoNino = "UPDATE TipoBicicleta " + 
                                 "SET precio = " + precioNino_R + " " +
                                 "WHERE idTipo = 1;";

            string comandoAdulto = "UPDATE TipoBicicleta " +
                                   "SET precio = " + precioAdulto_R + " " +
                                   "WHERE idTipo = 2;";

            string comandoTandem = "UPDATE TipoBicicleta " +
                                   "SET precio = " + precioTandem_R + " " +
                                   "WHERE idTipo = 3;";

            try
            {
                conexionBD = new SqlConnection(conBD);

                // Actualiza precio Nino.
                comandoBD = new SqlCommand(comandoNino, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                conexionBD.Close();
                
                // Actualiza precio Adulto.
                comandoBD = new SqlCommand(comandoAdulto, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                conexionBD.Close();

                // Actualiza precio Tandem.
                comandoBD = new SqlCommand(comandoTandem, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                conexionBD.Close();
                
                modificados = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return modificados;
        }

        public DataSet getTodasCAD(int filtro, bool disponibles)
        {
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            switch (filtro)
            {
                case 0: // (Ver todas)
                    if (disponibles == true)
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.enUso = 0;";
                    }
                    else
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo;";
                    }
                    break;

                case 1: // Niño
                    if (disponibles == true)
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.enUso = 0 " +
                                  "AND Bicicleta.tipo = 1;";
                    }
                    else
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.tipo = 1;";
                    }
                    break;

                case 2: // Adulto.
                    if (disponibles == true)
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.enUso = 0 " +
                                  "AND Bicicleta.tipo = 2;";
                    }
                    else
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.tipo = 2;";
                    }
                    break;

                case 3: // Tandem.
                    if (disponibles == true)
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.enUso = 0 " +
                                  "AND Bicicleta.tipo = 3;";
                    }
                    else
                    {
                        comando = "SELECT Bicicleta.idBici, TipoBicicleta.tipo, Bicicleta.fechaAlta, TipoBicicleta.precio, Bicicleta.enUso " +
                                  "FROM Bicicleta INNER JOIN TipoBicicleta " +
                                       "ON Bicicleta.tipo = TipoBicicleta.idTipo " +
                                  "WHERE Bicicleta.tipo = 3;";
                    }
                    break;
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Bicicleta");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }

        public int getNumTotalCAD(bool estanEnUso)
        {
            int numBicis = 0;
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "";

            if (estanEnUso == true) // SOLO las que estan en uso.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM Bicicleta " +
                          "WHERE enUso = 1;";
            }
            else // Todas las de la tabla.
            {
                comando = "SELECT COUNT(*) " +
                          "FROM Bicicleta;";
            }

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Bicicleta");

                // Cogemos del DataSet el valor del COUNT(*)
                numBicis = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numBicis;
        }


        public float getPrecio(int idBici)
        {
            // Si devuelve -1 es que no existe precio para el idLocalidad seleccionado.
            // Es decir, ese idLocalidad no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT tipo.precio " +
                             "FROM TipoBicicleta AS tipo " +
                             "INNER JOIN Bicicleta AS bici " +
                             "ON tipo.idTipo = bici.tipo " +
                             "AND bici.idBici = " + idBici + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "LocalidadInteres");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }
    }
}
