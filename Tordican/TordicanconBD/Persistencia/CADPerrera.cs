﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Data;


namespace Persistencia
{
    public class CADPerrera
    {
        public const int numeroPerreras = 20;
        private const string conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";


        public bool CADCargaPerrera()
        {
            bool insert = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                string comando;
                for (int i = 1; i <= numeroPerreras; i++) {
                    comando = @"INSERT INTO Perrera values(  "+i+" ,100,null,0)";
                    SqlCommand cmd = new SqlCommand(comando, conexionBD);
                    cmd.ExecuteNonQuery();
                }
                insert = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return insert;
        }

        public  int CADObtenerPerrerasDisponibles(){
            SqlConnection conexionBD = null;
            int valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from Perrera" , conBD);
                da.Fill(bdvirtual, "perrera");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["perrera"];
                

                for (int i = 0; i < t.Rows.Count; i++){
                    DataRow fila = t.Rows[i];

                    bool n = (bool)fila["ocupada"];
                    if (n)
                        valor++;
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numeroPerreras-valor;
        }

        public float CADObtenerPrecioPerreras(int numero) {
            SqlConnection conexionBD = null;
            float valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from Perrera where numero='" + numero + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor

                valor = float.Parse(bdvirtual.Tables[0].Rows[0]["precio"].ToString());

            }


            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;

        
        }

        public bool CADEliminaPerrera(int reserva)
        {
            bool ok = false;
            SqlConnection c = new SqlConnection(conBD);
            string comando = "update Perrera set aReserva=NULL, ocupada=0 where aReserva ='" + reserva + "'";

            try
            {
                c.Open();
                SqlCommand com = new SqlCommand(comando, c);
                com.ExecuteNonQuery();
                ok = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally { c.Close(); }
            return ok;
        }

        public bool CADInsertaPerrera(int reserva)
        {
            bool hecho = false;
            SqlConnection conexionBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select *  from Perrera", conBD);
                da.Fill(bdvirtual, "Perrera");

                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables["Perrera"];

                if (t.Rows.Count > 0){
                    for (int i = 0; i < t.Rows.Count && !hecho; i++){
                        object dis = t.Rows[i]["ocupada"];
                        if (dis.Equals(false))
                        {
                            t.Rows[i]["ocupada"] = true;
                            t.Rows[i]["aReserva"] = reserva;
                            hecho = true;
                        }
                    
                }
                    //Construyo la sentencia
                    SqlCommandBuilder cbuilder = new SqlCommandBuilder(da);
                    //Actualizo la base de datos real
                    da.Update(bdvirtual, "Perrera");
                    hecho = true;
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return hecho;

        }
    }
}
