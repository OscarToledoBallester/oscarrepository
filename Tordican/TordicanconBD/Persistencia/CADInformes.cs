﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADInformes
    {

        private string conBD; // Cadena de conexion (ver constructor de clase).

        // Constructor por defecto.
        public CADInformes()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }

        ////////////////////////////////////////////
        ///////////// INFORMES DE COTO /////////////
        ////////////////////////////////////////////
        // Piezas cazadas
        public DataSet ObtenerPiezasCazadas()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * from DiaCaza", conBD);
                da.Fill(bdvirtual, "DiaCaza");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerPiezasPorTipo(int tipo)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;

            // Construimos el comando sql
            String comando = "SELECT * FROM DiaCaza WHERE ";
            // Dependiendo del valor del tipo
            switch (tipo)
            {
                // Tordos
                case 1:
                    comando = comando + "piezasTordo IS NOT NULL";
                    break;
                // Palomas torcaces
                case 2:
                    comando = comando + "piezasPalomaTorcaz IS NOT NULL";
                    break;
                // Palomas bravias
                case 3:
                    comando = comando + "piezasPalomaBravia IS NOT NULL";
                    break;
                // Perdiz roja
                case 4:
                    comando = comando + "piezasPerdizRoja IS NOT NULL";
                    break;
                // Conejos o liebres
                case 5:
                    comando = comando + "piezasConeLie IS NOT NULL";
                    break;
                // Faisanes
                case 6:
                    comando = comando + "piezasFaisan IS NOT NULL";
                    break;
                //Codornices
                case 7:
                    comando = comando + "piezasCodorniz IS NOT NULL";
                    break;
                // Becadas
                case 8:
                    comando = comando + "piezasBecada IS NOT NULL";
                    break;

            }

            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "DiaCaza");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerPiezasPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            // Fechas con años que estén estrictamente en medio
            String comando = "SELECT * FROM DiaCaza WHERE ";
            comando += "año > " + desde.Year + " AND año < " + hasta.Year;
            // Fechas con meses estrictamente en medio dentro del mimo año
            comando += " UNION ";
            comando += "(";
            comando += "(";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "mes > " + desde.Month;
            comando += " AND ";
            comando += "año = " + desde.Year;
            comando += " INTERSECT ";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "mes < " + hasta.Month;
            comando += " AND ";
            comando += "año = " + hasta.Year;
            comando += ")";
            // Fechas con días estrictamente en medio dentro del mismo mes y año
            comando += " UNION ";
            comando += "(";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "dia > " + (desde.Day - 1);
            comando += " AND ";
            comando += "mes = " + desde.Month;
            comando += " AND ";
            comando += "año = " + desde.Year;
            comando += " INTERSECT ";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "dia < " + (hasta.Day + 1);
            comando += " AND ";
            comando += "mes = " + hasta.Month;
            comando += " AND ";
            comando += "año = " + hasta.Year;
            comando += ")";
            comando += ")";
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "DiaCaza");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerPiezasPorTipoYFechas(int tipo, DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;

            // Construimos el comando sql
            String comando = "SELECT * FROM DiaCaza WHERE ";
            // Dependiendo del valor del tipo
            switch (tipo)
            {
                // Tordos
                case 1:
                    comando = comando + "piezasTordo IS NOT NULL";
                    break;
                // Palomas torcaces
                case 2:
                    comando = comando + "piezasPalomaTorcaz IS NOT NULL";
                    break;
                // Palomas bravias
                case 3:
                    comando = comando + "piezasPalomaBravia IS NOT NULL";
                    break;
                // Perdiz roja
                case 4:
                    comando = comando + "piezasPerdizRoja IS NOT NULL";
                    break;
                // Conejos o liebres
                case 5:
                    comando = comando + "piezasConeLie IS NOT NULL";
                    break;
                // Faisanes
                case 6:
                    comando = comando + "piezasFaisan IS NOT NULL";
                    break;
                //Codornices
                case 7:
                    comando = comando + "piezasCodorniz IS NOT NULL";
                    break;
                // Becadas
                case 8:
                    comando = comando + "piezasBecada IS NOT NULL";
                    break;

            }
            comando += " INTERSECT ";
            comando += "(";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "año > " + desde.Year + " AND año < " + hasta.Year;
            // Fechas con meses estrictamente en medio dentro del mimo año
            comando += " UNION ALL (";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "mes >= " + desde.Month;
            comando += " AND ";
            comando += "año >= " + desde.Year;
            comando += " INTERSECT ";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "mes <= " + hasta.Month;
            comando += " AND ";
            comando += "año <= " + desde.Year;
            // Fechas con días estrictamente en medio dentro del mismo mes y año
            comando += ") UNION ALL (";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "dia >= " + desde.Day;
            comando += " AND ";
            comando += "mes >= " + desde.Month;
            comando += " AND ";
            comando += "año >= " + desde.Year;
            comando += " INTERSECT ";
            comando += "SELECT * FROM DiaCaza WHERE ";
            comando += "dia <= " + hasta.Day;
            comando += " AND ";
            comando += "mes <= " + hasta.Month;
            comando += " AND ";
            comando += "año <= " + hasta.Year;
            comando += "))";

            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "DiaCaza");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        // Reservas del coto
        public DataSet ObtenerReservasCoto()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();

                String comando = "SELECT *";
                comando += " FROM ContrataCoto";
                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "ContrataCoto");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerReservasCotoPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();

                String comando = "SELECT * FROM ContrataCoto WHERE ";
                comando += "año > " + desde.Year + " AND año < " + hasta.Year;
                // Fechas con meses estrictamente en medio dentro del mimo año
                comando += " UNION ALL (";
                comando += "SELECT * FROM ContrataCoto WHERE ";
                comando += "mes > " + desde.Month;
                comando += " AND ";
                comando += "año = " + desde.Year;
                comando += " INTERSECT ";
                comando += "SELECT * FROM ContrataCoto WHERE ";
                comando += "mes < " + hasta.Month;
                comando += " AND ";
                comando += "año >= " + desde.Year;
                // Fechas con días estrictamente en medio dentro del mismo mes y año
                comando += ") UNION ALL (";
                comando += "SELECT * FROM ContrataCoto WHERE ";
                comando += "dia > " + (desde.Day - 1);
                comando += " AND ";
                comando += "mes >= " + desde.Month;
                comando += " AND ";
                comando += "año = " + desde.Year;
                comando += " INTERSECT ";
                comando += "SELECT * FROM ContrataCoto WHERE ";
                comando += "dia < " + (hasta.Day + 1);
                comando += " AND ";
                comando += "mes <= " + hasta.Month;
                comando += " AND ";
                comando += "año >= " + hasta.Year;
                comando += ")";

                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "ContrataCoto");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }


        /////////////////////////////////////////////
        ////////// INFORMES DE ACTIVIDADES //////////
        /////////////////////////////////////////////
        // Incidencias
        public DataSet ObtenerIncidencias(int revisadas)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                String comando = "SELECT * FROM Incidencia";
                // Según el valor de revisadas construye una opción más o no del comando
                // 0 - Todas, revisadas o no
                switch (revisadas)
                {
                    // Revisadas
                    case 1:
                        comando += " WHERE revisada = 1";
                        break;
                    // No revisadas
                    case 2:
                        comando += " WHERE revisada = 0";
                        break;
                }
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Incidencia");
                comando = "SELECT * FROM Actividad";
                da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "Actividad");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerIncidenciasPorFechas(int revisadas, DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                String comando = "SELECT * FROM Incidencia";
                comando += " WHERE fecha >= '" + desde + "'";
                comando += " AND fecha <= '" + hasta + "'";
                // Según el valor de revisadas construye una opción más o no del comando
                // 0 - Todas, revisadas o no
                switch (revisadas)
                {
                    // Revisadas
                    case 1:
                        comando += " AND revisada = 1";
                        break;
                    // No revisadas
                    case 2:
                        comando += " AND revisada = 0";
                        break;
                }
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Incidencia");
                comando = "SELECT * FROM Actividad";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Actividad");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerIncidenciasPorTipo(int revisadas, int tipo)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                String comando = "SELECT * FROM Incidencia WHERE aActividad = " + tipo;
                // Según el valor de revisadas construye una opción más o no del comando
                // 0 - Todas, revisadas o no
                switch (revisadas)
                {
                    // Revisadas
                    case 1:
                        comando += " AND revisada = 1";
                        break;
                    // No revisadas
                    case 2:
                        comando += " AND revisada = 0";
                        break;
                }
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Incidencia");
                comando = "SELECT * FROM Actividad";
                da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "Actividad");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerIncidenciasPorTipoyFechas(int revisadas, int tipo, DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                String comando = "SELECT * FROM Incidencia WHERE aActividad = " + tipo;
                comando += " AND fecha >= '" + desde.Date.ToShortDateString() + "'";
                comando += " AND fecha <= '" + hasta.Date.ToShortDateString() + "'";
                // Según el valor de revisadas construye una opción más o no del comando
                // 0 - Todas, revisadas o no
                switch (revisadas)
                {
                    // Revisadas
                    case 1:
                        comando += " AND revisada = 1";
                        break;
                    // No revisadas
                    case 2:
                        comando += " AND revisada = 0";
                        break;
                }
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Incidencia");
                comando = "SELECT * FROM Actividad";
                da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "Actividad");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        // Actividades
        public DataSet ObtenerActividades()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aQuad IS NOT NULL";
                comando += " OR aBicicleta IS NOT NULL";
                comando += " OR aPaseo IS NOT NULL";
                comando += " OR aTiro IS NOT NULL";
                comando += " OR aPiraguismo IS NOT NULL";
                comando += " OR aRapel IS NOT NULL";
                comando += " OR aParque IS NOT NULL";
                comando += " OR aLocalidad IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                comando += " AND (";
                comando += " aQuad IS NOT NULL";
                comando += " OR aBicicleta IS NOT NULL";
                comando += " OR aPaseo IS NOT NULL";
                comando += " OR aTiro IS NOT NULL";
                comando += " OR aPiraguismo IS NOT NULL";
                comando += " OR aRapel IS NOT NULL";
                comando += " OR aParque IS NOT NULL";
                comando += " OR aLocalidad IS NOT NULL";
                comando += ")";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        // Tipos de actividades
        public DataSet ObtenerActividadesQuad()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aQuad IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM VehiculoQuad";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "VehiculoQuad");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesQuadPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aQuad IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM VehiculoQuad";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "VehiculoQuad");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesBici()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aBicicleta IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM TipoBicicleta";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "TipoBicicleta");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesBiciPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aBicicleta IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM TipoBicicleta";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "TipoBicicleta");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesPaseo()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aPaseo IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM PaseoCaballo";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "PaseoCaballo");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesPaseoPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aPaseo IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM PaseoCaballo";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "PaseoCaballo");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesTiro()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aTiro IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM TiroArco";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "TiroArco");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesTiroPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aTiro IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM TiroArco";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "TiroArco");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesPiragua()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aPiraguismo IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM Piraguismo";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Piraguismo");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesPiragua(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aPiraguismo IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM Piraguismo";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Piraguismo");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesRapel()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aRapel IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM Rapel";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Rapel");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesRapelPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aRapel IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM Rapel";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Rapel");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesParque()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aParque IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM ParqueTematico";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "ParqueTematico");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesParquePorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aParque IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM ParqueTematico";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "ParqueTematico");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesLocalidad()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aLocalidad IS NOT NULL";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM LocalidadInteres";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "LocalidadInteres");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        public DataSet ObtenerActividadesLocalidadPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = new DataSet();

            try
            {
                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                // Carga de tabla reservas
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE aLocalidad IS NOT NULL";
                comando += " AND fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "Reserva");
                comando = "SELECT * FROM LocalidadInteres";
                da = new SqlDataAdapter(comando, conexionBD);
                da.Fill(bdvirtual, "LocalidadInteres");
            }
            catch (SqlException exc)
            {
                throw exc;
            }

            return bdvirtual;
        }

        ////////////////////////////////////////////////
        ///////////// INFORMES DE CLIENTES /////////////
        ////////////////////////////////////////////////
        // Piezas cazadas
        public DataSet ObtenerClientes()
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                String comando = "SELECT * FROM Reserva";
                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "Reserva");
                da = new SqlDataAdapter("SELECT * from Cliente", conBD);
                da.Fill(bdvirtual, "Cliente");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }

        public DataSet ObtenerClientesPorFechas(DateTime desde, DateTime hasta)
        {
            SqlConnection conexionBD = null;
            DataSet bdvirtual = null;
            try
            {

                conexionBD = new SqlConnection(conBD);
                bdvirtual = new DataSet();
                String comando = "SELECT * FROM Reserva";
                comando += " WHERE fechaReserva >= '" + desde.ToShortDateString() + "'";
                comando += " AND fechaReserva <= '" + hasta.ToShortDateString() + "'";
                SqlDataAdapter da = new SqlDataAdapter(comando, conBD);
                da.Fill(bdvirtual, "Reserva");
                da = new SqlDataAdapter("SELECT * from Cliente", conBD);
                da.Fill(bdvirtual, "Cliente");
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }

            return bdvirtual;
        }
    }
}