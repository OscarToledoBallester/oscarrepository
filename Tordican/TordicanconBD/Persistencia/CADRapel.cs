﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADRapel
    {
        private string conBD; // Cadena de conexion.

        public CADRapel()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }
        public bool insertarCAD(string lugar, int numCuerdas, int numEquipos, float precio)
        {
            bool insertado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");
            string comando = "INSERT INTO Rapel (lugar, numCuerdas, numEquipos, precio) " +
                             "VALUES ('" + lugar + "', " + numCuerdas + ", " + numEquipos + ", " + precio_R + ");";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }
        public bool modificarCAD(int idRapel, string lugar, int numCuerdas, int numEquipos, float precio)
        {
            bool modificado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");

            string comando = "UPDATE Rapel " +
                             "SET lugar = '" + lugar + "', " +
                                 "numCuerdas = " + numCuerdas + ", " +
                                 "numEquipos = " + numEquipos + ", " +
                                 "precio = " + precio_R + " " +
                             "WHERE idRapel = " + idRapel + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificado;
        }
        public bool borrarCAD(int idRapel)
        {
            bool borrado = false;
            string comando = "DELETE FROM Rapel " +
                             "WHERE idRapel = " + idRapel + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }
        public DataSet getTodosCAD()
        {
            // Muestra todos los quads, tanto disponibles como no disponibles.
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT * " +
                             "FROM Rapel;";

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Rapel");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }
        public int getNumTotalLugaresCAD()
        {
            int numLugares = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT COUNT(*) " +
                             "FROM Rapel;";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Rapel");

                // Cogemos del DataSet el valor del COUNT(*)
                numLugares = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numLugares;
        }
        public int getNumTotalCuerdasCAD()
        {
            int numCuerdas = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT SUM(numCuerdas) " +
                             "FROM Rapel;";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Rapel");

                // Cogemos del DataSet el valor del SUM(numChalecos)
                numCuerdas = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numCuerdas;
        }
        public int getNumTotalEquiposCAD()
        {
            int numEquipos = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT SUM(numEquipos) " +
                             "FROM Rapel;";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Rapel");

                // Cogemos del DataSet el valor del SUM(numChalecos)
                numEquipos = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numEquipos;
        }
        public float getPrecio(int idRapel)
        {
            // Si devuelve -1 es que no existe precio para el idRapel seleccionado.
            // Es decir, ese idRapel no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT precio " +
                             "FROM Rapel " +
                             "WHERE idRapel = " + idRapel + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Rapel");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }
    }
}