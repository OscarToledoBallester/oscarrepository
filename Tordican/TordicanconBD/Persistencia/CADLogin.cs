﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADLogin
    {
        private string conBD;

        public CADLogin()
        {
            conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";
        }
        public DataSet comprobarAcceso(string iUsuario)
        {
            
            DataSet resultado = new DataSet();
            string comando = "SELECT contraseña FROM Personal WHERE usuario = ('" + iUsuario + "')";
                           
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Login");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public DataSet tipoAcceso(string iUsuario)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT esAdmin FROM Personal WHERE usuario = ('" + iUsuario + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Login");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
    }
}
