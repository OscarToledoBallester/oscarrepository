﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADTiroArco
    {
        private string conBD; // Cadena de conexion.
        public CADTiroArco()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }

        public bool insertarCAD(int numDianas, int numFlechas, float precio)
        {
            bool insertado = false;
            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");

            string comando = "INSERT INTO TiroArco (numDianas, numFlechas, precio) " +
                             "VALUES (" + numDianas + ", " + numFlechas + ", " + precio_R + ");";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }
        public bool modificarCAD(int idTiro, int numDianas, int numFlechas, float precio)
        {
            bool modificado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");

            string comando = "UPDATE TiroArco " +
                             "SET numDianas = " + numDianas + ", " +
                                 "numFlechas = " + numFlechas + ", " +
                                 "precio = " + precio_R + " " +
                             "WHERE idTiro = " + idTiro + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificado;
        }
        public bool borrarCAD(int idTiro)
        {
            bool borrado = false;
            string comando = "DELETE FROM TiroArco " +
                             "WHERE idTiro = " + idTiro + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }    
        public DataSet getTodosCAD()
        {
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT * " +
                             "FROM TiroArco;";

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "TiroArco");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }
        public float getPrecio(int idTiro)
        {
            // Si devuelve -1 es que no existe precio para el idTiro seleccionado.
            // Es decir, ese idTiro no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT precio " +
                             "FROM TiroArco " +
                             "WHERE idTiro = " + idTiro + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "TiroArco");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }

    }
}