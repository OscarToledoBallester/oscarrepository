﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;



namespace Persistencia
{
    public class CADTipoCaza
    {
        private const string conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";


        public bool CADCargarTipoCaza() {
            bool hecho = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO TipoCaza values (1,'En Mano',1800)";
            string comando2 = "INSERT INTO TipoCaza values (2,'Ojeo',2000)";
            string comando3 = "INSERT INTO TipoCaza values (3,'Al Asalto',3100)";
            string comando4 = "INSERT INTO TipoCaza values (4,'Al Reclamo',2500)";
            string comando5 = "INSERT INTO TipoCaza values (5,'Al Pase',3300)";
            string comando6 = "INSERT INTO TipoCaza values (0,'',0)";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando2, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando3, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando4, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando5, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando6, conexionBD);
                cmd.ExecuteNonQuery();
                hecho = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return hecho;
        
        }


        public float CADObtenerPrecio(int tipo) {
            SqlConnection conexionBD = null;
            float valor = 0;
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from TipoCaza where numTipo='" + tipo + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor

                valor = float.Parse(bdvirtual.Tables[0].Rows[0]["precio"].ToString());

            }


            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }

        public string CADObtenerDescripcion(int tipo)
        {
            SqlConnection conexionBD = null;
            string valor = "";
            try
            {
                conexionBD = new SqlConnection(conBD);
                DataSet bdvirtual = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("select * from TipoCaza where numTipo='" + tipo + "'", conBD);
                da.Fill(bdvirtual, "area");
                //Obtengo la tabla
                DataTable t = new DataTable();
                t = bdvirtual.Tables[0];
                //recupero el valor

                valor = bdvirtual.Tables[0].Rows[0]["descripcion"].ToString();

            }


            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return valor;
        }


        public bool AsignarTipoCaza(int opc) {
            bool insert = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO TipoCaza values ('Tordican',0,5000,3000,3000,2000,2000,500,500,200)";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insert;
        }
    }
}
