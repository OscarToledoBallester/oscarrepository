﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;



namespace Persistencia
{
    public class CADCliente
    {
        private string conBD;

        public CADCliente()
        {
            //Poner la bd en C
            conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";
        }

        public bool nuevoClienteCAD(string ndni, string nnom, string nape1, string nape2, string ndir, string npais, string nprov, string ntelf, string nemail, string ncontra)
        {
            bool insertado = false;
            string comando = "INSERT INTO Cliente values('" + ndni + "','" + nnom + "','" + nape1 + "','" + nape2 + "','" + ndir + "','" + npais + "','" + nprov + "','" + ntelf + "','" + nemail + "','" + ncontra + "')";
            
            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException)
            {
                //throw exc;
            }
            return insertado;
        }
        public DataSet obtenerClientePorDniCAD(string dni)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE dni = ('" + dni + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
            
        }
        public DataSet obtenerClientePorNombreCAD(string nom)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE nombre = ('" + nom + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorApe1CAD(string ape1)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE apellido1 = ('" + ape1 + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorApe2CAD(string ape2)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE apellido2 = ('" + ape2 + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorDireccionCAD(string dir)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE direccion = ('" + dir + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorPaisCAD(string pais)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE pais = ('" + pais + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorProvinciaCAD(string prov)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE provincia = ('" + prov + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorTelefonoCAD(string telf)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email FROM Cliente WHERE telefono = ('" + telf + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public DataSet obtenerClientePorEmailCAD(string email)
        {
            DataSet resultado = new DataSet();
            string comando = "SELECT dni,nombre,apellido1,apellido2,direccion,pais,provincia,telefono,email,contraseña FROM Cliente WHERE email = '" + email + "'";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;

        }
        public bool mirarReservasDelCliente(string dni)
        {
            bool sepuede;
            DataSet resultado = new DataSet();

            string comando = "SELECT aCliente FROM Reserva WHERE aCliente = ('" + dni + "')";

            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Reserva");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            //Si devuelve alguna fila es que el cliente tiene alguna reserva i no se puuede eliminar
            if (resultado.Tables[0].Rows.Count > 0)
                sepuede = false;
            else
                sepuede = true;

            return sepuede;
        }
        public bool eliminarClienteCAD(string ndni)
        {
            bool eliminado = false;

            //Miraremos si el cliente tiene alguna reserva, si la tiene no lo podemos eliminar
            if (mirarReservasDelCliente(ndni))
            {

                string comando = "DELETE FROM Cliente WHERE Cliente.dni =('" + ndni + "')";

                SqlConnection conexionBD;
                SqlCommand comandoBD;
                try
                {
                    conexionBD = new SqlConnection(conBD);
                    conexionBD.Open();
                    comandoBD = new SqlCommand(comando, conexionBD);
                    comandoBD.CommandType = CommandType.Text;
                    comandoBD.ExecuteNonQuery();
                    eliminado = true;
                }
                catch (SqlException exc)
                {
                    throw exc;
                }

            }
            else
            {
                
                eliminado = false;
            }

            return eliminado;
        }
        /*public bool actualizarClienteCAD(string ndni)
        {
            bool modificado = false;
            string comando = 
        }*/
        public DataSet mostrarTodosCAD()
        {
            DataSet resultado = new DataSet();

            string comando = "SELECT * FROM Cliente";
            SqlConnection conexionBD;
            SqlDataAdapter adaptadorSQL;

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Cliente");
                conexionBD.Close();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return resultado;
        }
        public bool actualizarClienteCAD(string ndni, string nnom, string nape1, string nape2, string ndir, string npais, string nprov, string ntelf, string nemail, string ncontra)
        {
            bool insertado = false;
            string comando = "UPDATE Cliente SET nombre = '" + nnom + "', apellido1 = '" + nape1 + "', apellido2 = '" + nape2 + "', direccion = '" + ndir + "', pais = '" + npais + "', provincia = '" + nprov + "', telefono = '" + ntelf + "', email = '" + nemail + "', contraseña = '" + ncontra + "' WHERE dni = ('" + ndni + "')";
            
            SqlConnection conexionBD;
            SqlCommand comandoBD;
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                comandoBD = new SqlCommand(comando, conexionBD);
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return insertado;
        }

        public bool comprobarLoginWeb(string emailP, string passwordP)
        {
            // Devuelve "true" si el login es correcto / "false" en caso contrario.
            bool loginOK = false;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT * " +
                             "FROM Cliente " +
                             "WHERE email = '" + emailP + "' " +
                             "AND contraseña = '" + passwordP + "';";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "LoginCliente");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    loginOK = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            
            return loginOK;
        }
    }
}
