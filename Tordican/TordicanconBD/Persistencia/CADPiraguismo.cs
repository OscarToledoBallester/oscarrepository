﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADPiraguismo
    {
        private string conBD; // Cadena de conexion.


        public CADPiraguismo()
        {
            conBD = @"Data Source = .\SQLEXPRESS;
                      AttachDbFilename = C:\TordicanconBD\Persistencia\TordicanBD.mdf;
                      Integrated Security = True;
                      User Instance = True;";
        }

        // Inserta un quad en la BD.
        public bool insertarCAD(string pantano, int numChalecos, float precio)
        {
            bool insertado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");
            string comando = "INSERT INTO Piraguismo (pantano, numChalecos, precio) " +
                             "VALUES ('" + pantano + "', " + numChalecos + ", " + precio_R + ");";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                insertado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return insertado;
        }

        // Modifica los atributos de un quad seleccionado mediante su atributo "idQuad" (clave primaria).
        public bool modificarCAD(int idPiraguismo, string pantano, int numChalecos, float precio)
        {
            bool modificado = false;

            // Reemplazo (R) en tipos "float" de la coma decimal (,) por el punto (.)
            string precio_R = precio.ToString().Replace(",", ".");

            string comando = "UPDATE Piraguismo " +
                             "SET pantano = '" + pantano + "', " +
                                 "numChalecos = " + numChalecos + ", " +
                                 "precio = " + precio_R + " " +
                             "WHERE idPiraguismo = " + idPiraguismo + ";";

            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                modificado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return modificado;
        }

        public bool borrarCAD(int idPiraguismo)
        {
            bool borrado = false;
            string comando = "DELETE FROM Piraguismo " +
                             "WHERE idPiraguismo = " + idPiraguismo + ";";
            SqlConnection conexionBD = null;
            SqlCommand comandoBD = null;
            try
            {
                conexionBD = new SqlConnection(conBD);
                comandoBD = new SqlCommand(comando, conexionBD);
                conexionBD.Open();
                comandoBD.CommandType = CommandType.Text;
                comandoBD.ExecuteNonQuery();
                borrado = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return borrado;
        }
        
        // Devuelve un DataSet con los quads de la BD. ("disponibles = true" -> Devuelve SOLO los disponibles).
        public DataSet getTodosCAD()
        {
            // Muestra todos los quads, tanto disponibles como no disponibles.
            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT * " +
                             "FROM Piraguismo;";

            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Piraguismo");
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return resultado;
        }

        public int getNumTotalPantanosCAD()
        {
            int numPantanos = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT COUNT(*) " +
                             "FROM Piraguismo;";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Piraguismo");

                // Cogemos del DataSet el valor del COUNT(*)
                numPantanos = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numPantanos;
        }

        public int getNumTotalChalecosCAD()
        {
            int numChalecos = 0;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT SUM(numChalecos) " +
                             "FROM Piraguismo;";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Piraguismo");

                // Cogemos del DataSet el valor del SUM(numChalecos)
                numChalecos = int.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return numChalecos;
        }

        public float getPrecio(int idPiraguismo)
        {
            // Si devuelve -1 es que no existe precio para el idPiraguismo seleccionado.
            // Es decir, ese idPiraguismo no existe.
            float precio = -1;

            DataSet resultado = new DataSet();
            SqlConnection conexionBD = null;
            SqlDataAdapter adaptadorSQL = null;
            string comando = "SELECT precio " +
                             "FROM Piraguismo " +
                             "WHERE idPiraguismo = " + idPiraguismo + ";";
            try
            {
                conexionBD = new SqlConnection(conBD);
                adaptadorSQL = new SqlDataAdapter(comando, conexionBD);
                adaptadorSQL.Fill(resultado, "Piraguismo");

                if (resultado.Tables[0].Rows.Count != 0) // Hay lineas (una).
                    precio = float.Parse(resultado.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return precio;
        }
    }
}
