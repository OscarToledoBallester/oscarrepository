﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Persistencia
{
    public class CADComidaCoto
    {
        private const string conBD = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\TordicanconBD\Persistencia\TordicanBD.mdf;Integrated Security=True;User Instance=True;";

        public bool CADCargarComidaCoto() {
            bool hecho = false;
            SqlConnection conexionBD = null;
            string comando1 = "INSERT INTO Comida values (1,'Barbacoa',80)";
            string comando2 = "INSERT INTO Comida values (2,'Migas',100)";
            string comando3 = "INSERT INTO Comida values (3,'Gachas',100)";
            string comando4 = "INSERT INTO Comida values (4,'Gazpacho Manchego',120)";
            string comando5 = "INSERT INTO Comida values (0,'',0)";
            try
            {
                conexionBD = new SqlConnection(conBD);
                conexionBD.Open();
                SqlCommand cmd = new SqlCommand(comando1, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando2, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando3, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando4, conexionBD);
                cmd.ExecuteNonQuery();
                cmd = new SqlCommand(comando5, conexionBD);
                cmd.ExecuteNonQuery();
                hecho = true;
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            finally
            {
                if (conexionBD != null)
                    conexionBD.Close();
            }
            return hecho;
        }
    }
}
