﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class variables_de_session : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        labelDNI.Text = (Session["dni"] != null) ? Session["dni"].ToString() : "";
        labelNombre.Text = (Session["dni"] != null) ? Session["nombre"].ToString() : "";
        labelApellido1.Text = (Session["dni"] != null) ? Session["apellido1"].ToString() : "";
        labelApellido2.Text = (Session["dni"] != null) ? Session["apellido2"].ToString() : "";
        labelDireccion.Text = (Session["dni"] != null) ? Session["direccion"].ToString() : "";
        labelPais.Text = (Session["dni"] != null) ? Session["pais"].ToString() : "";
        labelProvincia.Text = (Session["dni"] != null) ? Session["provincia"].ToString() : "";
        labelTelefono.Text = (Session["dni"] != null) ? Session["telefono"].ToString() : "";
        labelEmail.Text = (Session["dni"] != null) ? Session["email"].ToString() : "";
        labelIdActividad.Text = (Session["idActividad"] != null) ? Session["idActividad"].ToString() : "";
    }
}