﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Subgurim.Controles;

public partial class localizacion2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MostrarValenciaStreet();
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuLocalizacion');</script>");
    }
    public void MostrarValenciaStreet()
    {
        GMapStreet.Height = 600;
        GMapStreet.Width = 600;
        GMapStreet.StreetviewPanoramaOptions = new GStreetviewPanoramaOptions(new GLatLng(39.467643526668795, -0.37715211510658264));

    }
    
}