﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-bicicleta.aspx.cs" Inherits="quads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" 
        ImageUrl="~/images/activ-back-bicicleta.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/actividades.aspx"
            CssClass="seccionAnterior">Actividades &gt;</asp:HyperLink> Bicicletas</h1>
    <table style="width: 100%;">
        <tr>
            <td style="width: 359px">
                <asp:Image ID="Image2" runat="server" 
                    ImageUrl="~/images/activ-bicicleta-1.jpg" />
                <br />
                <br />
                <asp:Image ID="Image3" runat="server" 
                    ImageUrl="~/images/activ-bicicleta-2.jpg" />
                &nbsp;<br />
                <br />
                <asp:Image ID="Image4" runat="server" 
                    ImageUrl="~/images/activ-bicicleta-3.jpg" />
                <br />
&nbsp;&nbsp;
            </td>
            <td valign="top">
                <asp:Label ID="Label1" runat="server" Text="- Rodeados de naturaleza, resulta prácticamente imposible resistirse a la tentación de salir a descubrir nuevos parajes a lomos de una bicicleta. Conscientes de ello, en Tordican ponemos a su disposición un servicio de alquiler de bicicletas para que no tenga que venir con el coche cargado. Tanto si desea dar un paseo familiar con los más pequeños, perderse para encontrar esa vista perdida o hacer fluir la adrenalina viviendo aventuras más extremas con sus amigos, tenemos a su disposición la bicicleta que más se adapte a sus necesidades" Height="210px" Width="550px"></asp:Label><br />
                <asp:Label ID="Label2" runat="server" 
                    Text="- Si nunca ha visto cómo acaba el día sobre su bici, si nunca has disfrutado del paisaje mientra pedalea tranquilamente sin preocuparse de nada, no sabe lo que se pierdes. Llegar a un cruce y decidir sobre la marcha por donde tirar o decidir quedarse en un pueblo o ciudad para conocerlo mejor le dará una sensación de libertad inigualable. Sólo tienes que planteárselo y lanzarse a la aventura. Vivir esta aventura, hacer frente a los pequeños problemas que se pueden presentar, conocer nuevos lugares, marcarse un nuevo reto y lograr superarlo le animará a continuar adelante." 
                    Height="190px" Width="550px"></asp:Label>
                    <br />
                <asp:Label ID="Label3" runat="server" 
                    Text="- Salga a la aventura y no se preocupe por las herramientas, el botiquín, los parches, etc. Despreocúpese ya que su diversión es nuestra principal tarea. En Tordican nos hacemos cargo de cualquier imprevisto que pueda surgirle durante su travesía. Acudiremos a  arreglarle un pinchazo, traerlo de vuelta o, incluso, dejarle un nuevo vehículo para que prosiga con su camino. Salga a la aventura y nosotros nos encargarémos del resto." 
                    Width="550px"></asp:Label>
                <br />
                <br />
                <br />
                <br />
                <div align="center">
                    <asp:ImageButton ID="imgBtReserveAhora" runat="server" 
                        ImageUrl="~/images/reserve-ahora.png" onclick="imgBtReserveAhora_Click" />
                </div>
                </td>
        </tr>
        </table>
</asp:Content>

