﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="localizacion2.aspx.cs" Inherits="localizacion2" %>

<%@ Register assembly="GMaps" namespace="Subgurim.Controles" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" 
        ImageUrl="~/images/imalocalizacionRedimensionada.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
        <h1>Localización<br />
        </h1>
    <hr />
        <p style="font-size: 16px; color: #749F01">
            Vista con StreetView</p>
        <p>Desde esta sección podrá observar el edificio principal, ya localizado en la 
            vista a pie de calle, y tendrá acceso a comprobar los alrededores del mismo. Así 
            estará en posición de mirar desde su casa los servicios que rodean nuestro 
            enclave y el ambiente de este.</p>
        <cc1:GStreetviewPanorama ID="GMapStreet" 
        runat="server" Height="500px" Width="980px" />
</asp:Content>

