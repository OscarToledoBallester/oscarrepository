﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="actividades.aspx.cs" Inherits="public_actividades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" 
        ImageUrl="~/images/activ-back-main.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Actividades</h1>
    <asp:Label ID="lbActivInfoFotos" runat="server" 
        Text="Haga clic sobre las fotos para encontrar más información de las actividades que podrá reservar desde ya mismo online. ¡Regístrese como cliente! ¡Tan sólo le pedimos 2 minutos de su tiempo!" 
        Height="60px"></asp:Label>
    <table cellspacing="0" style="width:100%;" border="0" cellpadding="0">
        <tr>
            <td align="center" 
                
                
                style="padding: 3px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; font-weight: bold; background-color: #C0C0C0; width: 25%; color: #000000;">QUADS</td>
            <td align="center" 
                
                
                style="padding: 3px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; font-weight: bold; background-color: #C0C0C0; width: 25%; color: #000000;">BICICLETAS</td>
            <td align="center" 
                
                
                style="padding: 3px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; font-weight: bold; background-color: #C0C0C0; width: 25%; color: #000000;">PARQUES TEMÁTICOS</td>
            <td align="center" 
                
                
                style="padding: 3px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; font-weight: bold; background-color: #C0C0C0; width: 25%; color: #000000;">LOCALIDADES DE INTERÉS</td>
        </tr>
        <tr>
            <td align="center" 
                style="border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; background-color: #C0C0C0">
                <asp:ImageButton ID="imgBtQuad" runat="server" 
                    ImageUrl="~/images/activ-mini-quad.jpg" onclick="imgBtQuad_Click" />                
            </td>
            <td align="center" 
                style="border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; background-color: #C0C0C0">
                <asp:ImageButton ID="imgBtBicicleta" runat="server" 
                    ImageUrl="~/images/activ-mini-bicicleta.jpg" onclick="imgBtBicicleta_Click" 
                     />
            </td>
            <td align="center" 
                style="border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; background-color: #C0C0C0">
                <asp:ImageButton ID="imgBtParqueTematico" runat="server" 
                    ImageUrl="~/images/activ-mini-parque-tematico.jpg" 
                    onclick="imgBtParqueTematico_Click" />
            </td>
            <td align="center" 
                style="border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #C0C0C0 #FFFFFF; background-color: #C0C0C0">
                <asp:ImageButton ID="imgBtLocalidad" runat="server" 
                    ImageUrl="~/images/activ-mini-localidad-interes.jpg" 
                    onclick="imgBtLocalidad_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" 
                
                style="padding: 5px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #FFFFFF #FFFFFF; background-color: #E0F8B6;">
                ¡Libera toda tu adrenalina! Súbete con tus amigos y demuestra quién es el 
                mejor.</td>
            <td align="center" 
                
                style="padding: 5px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #FFFFFF #FFFFFF; background-color: #E0F8B6;">
                Nunca un paseo en bici fue tan agradable. ¡Disfrute del paisaje y siéntase 
                libre!</td>
            <td align="center" 
                
                style="padding: 5px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #FFFFFF #FFFFFF; background-color: #E0F8B6;">
                ¿Te gusta la diversión?<br />
                Actividades para todos<br />
                ¡Esto es para ti!</td>
            <td align="center" 
                
                style="padding: 5px; border-style: solid; border-width: 0px 3px 0px 3px; border-color: #C0C0C0 #FFFFFF #FFFFFF #FFFFFF; background-color: #E0F8B6;">
                Visita lugares turísticos y culturales en localidades de nuestro alrededor.</td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <h1>También disponibles en el Complejo:</h1> 
        <asp:Label ID="lbActivOffline" runat="server" 
        
        Text="Disponemos también de las siguientes actividades, pero éstas deberán ser contratadas directamente en nuestra recepción. ¡Anímese con la aventura! ¡Estaremos encantados de atenderle!" 
        Height="70px"></asp:Label>
    <table cellspacing="7" style="width:100%;">
        <tr>
            <td align="center" style="font-weight: bold; color: #000000;">PASEOS A CABALLO</td>
            <td align="center" style="font-weight: bold; color: #000000;">TIRO CON ARCO</td>
            <td align="center" style="font-weight: bold; color: #000000;">PIRAGÜISMO</td>
            <td align="center" style="font-weight: bold; color: #000000;">RÁPEL</td>
        </tr>
        <tr>
            <td>
                <asp:Image ID="imgPaseoCaballo" runat="server" 
                    ImageUrl="~/images/activ-mini-paseo-caballo.jpg" BorderColor="Black" 
                    BorderStyle="Solid" BorderWidth="1px" />
            </td>
            <td>
                <asp:Image ID="imgTiroArco" runat="server" ImageUrl="~/images/activ-mini-tiro-arco.jpg" 
                    BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            </td>
            <td>
                <asp:Image ID="imgPiraguismo" runat="server" 
                    ImageUrl="~/images/activ-mini-piraguismo.jpg" BorderColor="Black" 
                    BorderStyle="Solid" BorderWidth="1px" />
            </td>
            <td>
                <asp:Image ID="imgRapel" runat="server" 
                    ImageUrl="~/images/activ-mini-rapel.jpg" BorderColor="Black" BorderStyle="Solid" 
                    BorderWidth="1px" />
            </td>
        </tr>
        </table> 
</asp:Content>

