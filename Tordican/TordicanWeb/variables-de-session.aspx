﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="variables-de-session.aspx.cs" Inherits="variables_de_session" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <asp:Table ID="tablaVariablesSession" runat="server">
        <asp:TableRow>
            <asp:TableCell>Session["dni"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelDNI" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["nombre"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelNombre" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["apellido1"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelApellido1" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["apellido2"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelApellido2" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["direccion"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelDireccion" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["pais"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelPais" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["provincia"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelProvincia" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["telefono"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelTelefono" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["email"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelEmail" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Session["idActividad"] = </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="labelIdActividad" runat="server" Text=""></asp:Label>
             </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>