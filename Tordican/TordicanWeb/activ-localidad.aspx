﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-localidad.aspx.cs" Inherits="localidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" 
        ImageUrl="~/images/activ-back-localidad.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/actividades.aspx"
            CssClass="seccionAnterior">Actividades &gt;</asp:HyperLink> Localidades de Interés</h1>
     <table style="width: 100%;">
        <tr>
            <td style="width: 359px">
                <asp:Image ID="imgLocalidad1" runat="server" 
                    ImageUrl="~/images/activ-localidad-1.jpg" />
                <br />
                <br />
                <asp:Image ID="imgLocalidad2" runat="server" 
                    ImageUrl="~/images/activ-localidad-2.jpg" />
                &nbsp;<br />
                <br />
                <asp:Image ID="imgLocalidad3" runat="server" 
                    ImageUrl="~/images/activ-localidad-3.jpg" />
                <br />
&nbsp;&nbsp;
            </td>
            <td valign="top">
                <asp:Label ID="lbLocal1" runat="server" 
                    Text="- Un museo (del latín musēum y éste a su vez del griego Μουσείον) es una institución pública o privada, permanente, con o sin fines de lucro, al servicio de la sociedad y su desarrollo, y abierta al público, que adquiere, conserva, investiga, comunica y expone o exhibe, con propósitos de estudio, educación y deleite colecciones de arte, científicas, etc., siempre con un valor cultural, según el Consejo Internacional de Museos (ICOM).2 La ciencia que los estudia se denomina museología y la técnica de su gestión museografía." 
                    Height="175px" Width="500px"></asp:Label><br />
                <asp:Label ID="lbLocal2" runat="server" 
                    Text="- El Castillo de la Atalaya o de Villena se levanta sobre una estribación del monte de San Cristóbal o de la Villa, en la localidad de Villena, al noroeste de la provincia de Alicante (España), próximo a la línea de separación con la provincia de Albacete y domina la antigua línea fronteriza en Castilla y Aragón. Fue construido por los árabes en fecha desconocida, aunque no después del siglo XII, ya que las fuentes árabes ya lo mencionan en el año 1172. Se ha especulado mucho sobre la posibilidad de que la fortaleza se asiente sobre una fortificación o villa romana anterior, aunque la arqueología no ha arrojado luz a este respecto." 
                    Height="210px" Width="500px"></asp:Label>
                    <br />
                <asp:Label ID="lbLocal3" runat="server" 
                    Text="- La Ciudad de las Artes y las Ciencias de Valencia es un conjunto único dedicado a la divulgación científica y cultural, que está integrado por seis grandes elementos: el Hemisfèric (cine IMAX y proyecciones digitales), el Umbracle (mirador ajardinado y aparcamiento), el Museo de las Ciencias Príncipe Felipe (innovador centro de ciencia interactiva), el Oceanográfico (el mayor acuario de Europa con más de 500 especies marinas) y el Palau de les Arts Reina Sofía (dedicado la programación operística). Y el Ágora, que dota al complejo de un espacio multifuncional." 
                    Width="500px"></asp:Label>
                <br />
                <br />
                <div align="center">
                    <asp:ImageButton ID="imgBtReserveAhora" runat="server" 
                        ImageUrl="~/images/reserve-ahora.png" onclick="imgBtReserveAhora_Click" />
                </div>
                </td>
        </tr>
        </table>
</asp:Content>

