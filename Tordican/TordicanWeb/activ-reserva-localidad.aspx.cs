﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class activ_reserva_localidad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuReservas');</script>");

        if (Session["email"] == null) // Usuario no identificado.
            Response.Redirect("~/error.aspx");
    }

    protected void asignarActividad_Click(object sender, ImageClickEventArgs e)
    {
        Session["idActividad"] = Convert.ToString(gvLocalidad.SelectedRow.Cells[1].Text);
        Response.Redirect("~/reservas2.aspx");
    }
}