﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-reserva-parque.aspx.cs" Inherits="activ_reserva_localidad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/back-inside.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Reserva de Parques Temáticos</h1>
    <div align="center">
    
    
        <asp:SqlDataSource ID="SqlDSParque" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TordicanBDConnectionString %>" 
            SelectCommand="SELECT * FROM [ParqueTematico]" 
            ProviderName="<%$ ConnectionStrings:TordicanBDConnectionString.ProviderName %>"></asp:SqlDataSource>
        <asp:GridView ID="gvParque" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataKeyNames="idParque" DataSourceID="SqlDSParque" 
            ForeColor="#333333" GridLines="None" Width="800px" 
            style="text-align:center;">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                <asp:BoundField DataField="idParque" HeaderText="ID" InsertVisible="False" 
                    ReadOnly="True" SortExpression="idParque" />
                <asp:BoundField DataField="nombre" HeaderText="Nombre" 
                    SortExpression="nombre" />
                <asp:BoundField DataField="localidad" HeaderText="Localidad" 
                    SortExpression="localidad" />
                <asp:BoundField DataField="descripcion" HeaderText="Descripcion" 
                    SortExpression="descripcion" />
                <asp:BoundField DataField="precio" HeaderText="Precio" 
                    SortExpression="precio" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <br />
                <asp:ImageButton ID="asignarActividad" runat="server" 
                    ImageUrl="~/images/índiceimagencontinuar.jpg" 
                    onclick="asignarActividad_Click" />    
    </div>
</asp:Content>

