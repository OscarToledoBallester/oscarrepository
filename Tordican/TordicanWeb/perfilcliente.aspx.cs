﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ENLogicaDeNegocio;
using Persistencia;

public partial class PerfilCliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["email"] == null)
            Response.Redirect("~/error.aspx");
        else
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuRegistro');</script>");
            //cargar desde las variables de session en los textbox
            if (!Page.IsPostBack)
            {
                TBDni.Text = Session["dni"].ToString();
                TBNombre.Text = Session["nombre"].ToString();
                TBPrimerApellido.Text = Session["apellido1"].ToString();
                TBSegundoApellido.Text = Session["apellido2"].ToString();
                TBPais.Text = Session["pais"].ToString();
                TBProvincia.Text = Session["provincia"].ToString();
                TBDireccion.Text = Session["direccion"].ToString();
                TBTelefono.Text = Session["telefono"].ToString();
                TBEmail.Text = Session["email"].ToString();
            }
        }
         
    }
    
    protected void CustomValidatorDni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (ver.dni(args.Value))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
    protected void CustomValidatorCadena_ServerValidate(object source, ServerValidateEventArgs args)
    {


    }
    protected void CustomValidatorEmail_ServerValidate(object source, ServerValidateEventArgs args)
    {


    }
    protected void CustomValidatorTelefono_ServerValidate(object source, ServerValidateEventArgs args)
    {


    }
    protected void btModificar_Click1(object sender, EventArgs e)
    {

        if (!comprobarComillas(TBDni.Text.Trim(), TBNombre.Text.Trim(), TBPrimerApellido.Text.Trim(), TBSegundoApellido.Text.Trim(), TBDireccion.Text.Trim(), TBPais.Text.Trim(), TBProvincia.Text.Trim(), TBTelefono.Text.Trim(), TBEmail.Text.Trim()))
        {
            
            Persistencia.CADCliente cad = new Persistencia.CADCliente();
            DataSet d;
            d = cad.obtenerClientePorEmailCAD(TBEmail.Text.Trim());
            if (d.Tables[0].Rows.Count > 0)
            {
                if (TBEmail.Text.Trim() != Session["email"].ToString())
                {
                    TBEmail.Text = Session["email"].ToString();
                    Label3.Text = "* E-mail ya registrado ";
                    Label3.Visible = true;
                }
                else
                {
                    //no hemos modificado el mail
                    Session["dni"] = TBDni.Text.Trim();
                    Session["nombre"] = TBNombre.Text.Trim();
                    Session["apellido1"] = TBPrimerApellido.Text.Trim();
                    Session["apellido2"] = TBSegundoApellido.Text.Trim();
                    Session["direccion"] = TBDireccion.Text.Trim();
                    Session["pais"] = TBPais.Text.Trim();
                    Session["provincia"] = TBProvincia.Text.Trim();
                    Session["telefono"] = TBTelefono.Text.Trim();
                    Session["email"] = TBEmail.Text.Trim();
                    

                    cad.actualizarClienteCAD(Session["dni"].ToString(), Session["nombre"].ToString(), Session["apellido1"].ToString(), Session["apellido2"].ToString(), Session["direccion"].ToString(), Session["pais"].ToString(), Session["provincia"].ToString(), Session["telefono"].ToString(), Session["email"].ToString(), Session["contraseña"].ToString());

                    Label3.Text = "* Perfil actualizado";
                    Label3.Visible = true;
                }

            }
            else
            {
                //el email no esta repetido
                Session["dni"] = TBDni.Text.Trim();
                Session["nombre"] = TBNombre.Text.Trim();
                Session["apellido1"] = TBPrimerApellido.Text.Trim();
                Session["apellido2"] = TBSegundoApellido.Text.Trim();
                Session["direccion"] = TBDireccion.Text.Trim();
                Session["pais"] = TBPais.Text.Trim();
                Session["provincia"] = TBProvincia.Text.Trim();
                Session["telefono"] = TBTelefono.Text.Trim();
                Session["email"] = TBEmail.Text.Trim();
                

                cad.actualizarClienteCAD(Session["dni"].ToString(), Session["nombre"].ToString(), Session["apellido1"].ToString(), Session["apellido2"].ToString(), Session["direccion"].ToString(), Session["pais"].ToString(), Session["provincia"].ToString(), Session["telefono"].ToString(), Session["email"].ToString(), Session["contraseña"].ToString());
                
                
                Label3.Text = "* Perfil actualizado";
                Label3.Visible = true;

            }
        }
        else
        {
            Label3.Text = "* Recuerde no usar comillas";
            Label3.Visible = true;
        }
    }
    protected bool comprobarComillas(string dni, string nom, string ape1, string ape2, string dir, string pais, string prov, string tel, string email)
    {
        bool error = false;
        ENVerificacion verifico = new ENVerificacion();

        if (!verifico.noHack(dni) || !verifico.noHack(nom) || !verifico.noHack(ape1) || !verifico.noHack(ape2) || !verifico.noHack(dir) || !verifico.noHack(pais) || !verifico.noHack(prov) || !verifico.noHack(tel) || !verifico.noHack(email))
            error = true;

        return error;
    }
}