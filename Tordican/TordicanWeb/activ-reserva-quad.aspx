﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-reserva-quad.aspx.cs" Inherits="activ_reserva_quad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/back-inside.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Reserva de Quads</h1>
            <div align="center">
    <asp:SqlDataSource ID="SqlDSVehiculoQuad" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TordicanBDConnectionString %>" 
        SelectCommand="SELECT * FROM [VehiculoQuad]"></asp:SqlDataSource>
<asp:GridView ID="gvQuads" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="idQuad" 
        DataSourceID="SqlDSVehiculoQuad" ForeColor="#333333" GridLines="None" Width="800px">
    <AlternatingRowStyle BackColor="White" />
    <Columns>
        <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
        <asp:BoundField DataField="idQuad" HeaderText="ID" InsertVisible="False" 
            ReadOnly="True" SortExpression="idQuad" />
        <asp:BoundField DataField="matricula" HeaderText="Matrícula" 
            SortExpression="matricula" />
        <asp:BoundField DataField="marca" HeaderText="Marca" SortExpression="marca" />
        <asp:BoundField DataField="modelo" HeaderText="Modelo" 
            SortExpression="modelo" />
        <asp:BoundField DataField="potencia" HeaderText="Potencia" 
            SortExpression="potencia" />
        <asp:BoundField DataField="peso" HeaderText="Peso" SortExpression="peso" />
        <asp:BoundField DataField="precio" HeaderText="Precio" 
            SortExpression="precio" />
        <asp:BoundField DataField="fechaAlta" HeaderText="Fecha Alta" 
            SortExpression="fechaAlta" Visible="False" />
        <asp:CheckBoxField DataField="enUso" HeaderText="En Uso" 
            SortExpression="enUso" Visible="False" />
    </Columns>
    <EditRowStyle BackColor="#7C6F57" />
    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#E3EAEB" />
    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#F8FAFA" />
    <SortedAscendingHeaderStyle BackColor="#246B61" />
    <SortedDescendingCellStyle BackColor="#D4DFE1" />
    <SortedDescendingHeaderStyle BackColor="#15524A" />
</asp:GridView><br />
                <asp:ImageButton ID="asignarActividad" runat="server" 
                    ImageUrl="~/images/índiceimagencontinuar.jpg" 
                    onclick="asignarActividad_Click" />
</div>
</asp:Content>
