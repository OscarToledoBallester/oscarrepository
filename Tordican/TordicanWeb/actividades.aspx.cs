﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class public_actividades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuActividades');</script>");
    }
    protected void btEnviar_Click(object sender, EventArgs e)
    {
        
    }
    protected void imgBtQuad_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/activ-quad.aspx");
    }
    protected void imgBtLocalidad_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/activ-localidad.aspx");
    }
    protected void imgBtBicicleta_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/activ-bicicleta.aspx");
    }
    protected void imgBtParqueTematico_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/activ-parque.aspx");
    }
}