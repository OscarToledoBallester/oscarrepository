﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/error-acceso.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Error de Acceso</h1>
    <asp:Label ID="Label1" runat="server" 
        Text="- Necesita estar identificado para visualizar el contenido de la página anterior." 
        Height="20px" Font-Bold="True" ForeColor="Red"></asp:Label><br />
    <asp:Label ID="Label2" runat="server" 
        Text="- Acceda mediante email y contraseña, si dispone de ellos, o regístrese a continuación." 
        Height="50px"></asp:Label><br />
    <div style="text-align: center;">
    <asp:ImageButton ID="imgBtRegistro" runat="server" 
        ImageUrl="~/images/registrese-ahora.png" onclick="imgBtRegistro_Click" />
    </div>
</asp:Content>