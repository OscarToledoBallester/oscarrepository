﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="Registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/imagenAltaRedi.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Registro<br />
    </h1>
    <hr />
    <p style="font-size: 16px; color: #749F01">
        Datos Personales</p>
    <asp:Panel ID="PanelDatosPersonales" runat="server">
        <table style="width:100%;">
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelDni" runat="server" CssClass="textos" Text="Dni*:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBDni" runat="server" Width="93%" TabIndex="2"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelNombre" runat="server" CssClass="textos" Text="Nombre*:"></asp:Label>
                </td>
                <td style="width: 39%">
                    <asp:TextBox ID="TBNombre" runat="server" Width="100%" TabIndex="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 20px">
                </td>
                <td style="height: 20px">
                    <asp:Label ID="lbdniincorrect" runat="server" ForeColor="Red" 
                        Text="* Dni incorrecto" Visible="False"></asp:Label>
                    <asp:CustomValidator ID="CustomValidatorDni0" runat="server" 
                        ControlToValidate="TBDni" ErrorMessage="* Dni incorrecto" 
                        onservervalidate="CustomValidatorDni_ServerValidate"
                        SetFocusOnError="True" ValidationGroup="User" Display="Dynamic"></asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="RFVDni" runat="server" 
                        ControlToValidate="TBDni" Display="Dynamic" ErrorMessage="* Dni obligatorio" 
                        SetFocusOnError="True" ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td style="height: 20px">
                </td>
                <td style="width: 39%; height: 20px;">
                    <asp:RegularExpressionValidator ID="REVNombre" runat="server" 
                        ControlToValidate="TBNombre" Display="Dynamic" 
                        ErrorMessage="* Nombre incorrecto: caracteres no numéricos" SetFocusOnError="True" 
                        ValidationExpression="\D+" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNombre" runat="server" 
                        ControlToValidate="TBNombre" Display="Dynamic" 
                        ErrorMessage=" * Nombre obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="15%" style="height: 23px">
                    <asp:Label ID="LabelPrimerApellido" runat="server" CssClass="textos" 
                        Text="1º Apellido*:"></asp:Label>
                </td>
                <td width="35%" style="height: 23px">
                    <asp:TextBox ID="TBPrimerApellido" runat="server" Width="94%" TabIndex="4"></asp:TextBox>
                </td>
                <td width="15%" style="height: 23px">
                    <asp:Label ID="LabelSegundoApellido" runat="server" CssClass="textos" 
                        Text="2º Apellido*:"></asp:Label>
                </td>
                <td style="width: 39%; height: 23px;">
                    <asp:TextBox ID="TBSegundoApellido" runat="server" Width="100%" TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 22px">
                </td>
                <td style="height: 22px">
                    <asp:RegularExpressionValidator ID="REVPrimerApellido" runat="server" 
                        ControlToValidate="TBPrimerApellido" Display="Dynamic" 
                        ErrorMessage="* Primer apellido incorrecto: caracteres no numéricos" SetFocusOnError="True" 
                        ValidationExpression="\D+" ValidationGroup="User" Visible="False"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RFVPrimerApellido" runat="server" 
                        ControlToValidate="TBPrimerApellido" Display="Dynamic" 
                        ErrorMessage="* Primer apellido obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User" Visible="False"></asp:RequiredFieldValidator>
                </td>
                <td style="height: 22px">
                </td>
                <td style="width: 39%; height: 22px;">
                    <asp:RegularExpressionValidator ID="REVSegundoApellido" runat="server" 
                        ControlToValidate="TBSegundoApellido" Display="Dynamic" 
                        ErrorMessage="* Segundo apellido incorrecto: caracteres no numéricos" ValidationExpression="\D+" 
                        ValidationGroup="User"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RFVSegundoApellido" runat="server" 
                        ControlToValidate="TBSegundoApellido" Display="Dynamic" 
                        ErrorMessage="* Segundo apellido obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelDireccion" runat="server" CssClass="textos" 
                        Text="Dirección:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBDireccion" runat="server" Width="93%" TabIndex="6"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelPais" runat="server" CssClass="textos" Text="Pais:"></asp:Label>
                </td>
                <td style="width: 39%">
                    <asp:TextBox ID="TBPais" runat="server" Width="100%" TabIndex="7"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 22px">
                </td>
                <td style="height: 22px">
                </td>
                <td style="height: 22px">
                </td>
                <td style="width: 39%; height: 22px;">
                </td>
            </tr>
            <tr>
                <td width="15%" style="height: 22px">
                    <asp:Label ID="LabelProvincia" runat="server" CssClass="textos" 
                        Text="Provincia:"></asp:Label>
                </td>
                <td width="35%" style="height: 22px">
                    <asp:TextBox ID="TBProvincia" runat="server" Width="93%" TabIndex="8"></asp:TextBox>
                </td>
                <td width="15%" style="height: 22px">
                    <asp:Label ID="LabelEmail" runat="server" CssClass="textos" Text="E-mail*:"></asp:Label>
                </td>
                <td style="width: 39%; height: 22px;">
                    <asp:TextBox ID="TBEmail" runat="server" Width="100%" TabIndex="9"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 22px">
                </td>
                <td style="height: 22px">
                </td>
                <td style="height: 22px">
                </td>
                <td style="width: 39%; height: 22px;">
                    <asp:RegularExpressionValidator ID="REVEmail" runat="server" 
                        ControlToValidate="TBEmail" Display="Dynamic" 
                        ErrorMessage="* E-mail incorrecto" SetFocusOnError="True" 
                        ValidationExpression="\S+@\S+\.\S+" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RFVSegundoApellido0" runat="server" 
                        ControlToValidate="TBEmail" Display="Dynamic" 
                        ErrorMessage="* E-mail obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelTelefono" runat="server" CssClass="textos" 
                        Text="Teléfono*:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBTelefono" runat="server" Width="91%" TabIndex="10"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelEmail0" runat="server" CssClass="textos" 
                        Text="Repetir E-mail*:"></asp:Label>
                </td>
                <td style="width: 39%">
                    <asp:TextBox ID="TBEmailConfir" runat="server" TabIndex="9" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 22px">
                </td>
                <td style="height: 22px">
                    <asp:RegularExpressionValidator ID="REVTelefono" runat="server" 
                        ControlToValidate="TBTelefono" Display="Dynamic" 
                        ErrorMessage="* Telefono incorrecto: número de 9 dígitos" SetFocusOnError="True" 
                        ValidationExpression="\d{9}" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RFVTelefono" runat="server" 
                        ControlToValidate="TBTelefono" Display="Dynamic" 
                        ErrorMessage="* Telefono obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td style="height: 22px">
                </td>
                <td style="width: 39%; height: 22px;">
                    <asp:CompareValidator ID="CompareValidator2" runat="server" 
                        ControlToCompare="TBEmail" ControlToValidate="TBEmailConfir" 
                        ErrorMessage="* Los E-mail no coinciden" ValidationGroup="User" 
                        Display="Dynamic"></asp:CompareValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RFVSegundoApellido1" runat="server" 
                        ControlToValidate="TBEmailConfir" Display="Dynamic" 
                        ErrorMessage="* Repetir e-mail obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td style="width: 39%">
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    
    </p>
    <hr />
    <p style="font-size: 16px">
        Datos de Registro</p>
    <table style="width:100%;">
        <tr>
            <td style="text-align: right; width: 161px">
                <asp:Label ID="Label1" runat="server" Text="Contraseña*"></asp:Label>
            </td>
            <td style="width: 223px">
                <asp:TextBox ID="tbContrasena" runat="server" TextMode="Password" 
                    ValidationGroup="user"></asp:TextBox>
            </td>
            <td>
                    &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right; width: 161px">
                &nbsp;</td>
            <td style="width: 223px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorcontrasena" runat="server" 
                        ControlToValidate="tbContrasena" ErrorMessage="* Contraseña obligatoria" 
                    ValidationGroup="User" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                    &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 161px; text-align: right">
                <asp:Label ID="Label2" runat="server" Text="Confirma Contraseña*"></asp:Label>
            </td>
            <td style="width: 223px">
                <asp:TextBox ID="tbContrasenaConfir" runat="server" TextMode="Password"></asp:TextBox>
            </td>
            <td>
                    <br />
            </td>
        </tr>
        <tr>
            <td style="width: 161px; text-align: right">
                &nbsp;</td>
            <td style="width: 223px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorcontrasena0" runat="server" 
                        ControlToValidate="tbContrasenaConfir" 
                    ErrorMessage="* Contraseña obligatoria" ValidationGroup="User" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <br />
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="tbContrasena" ControlToValidate="tbContrasenaConfir" 
                    ErrorMessage="* Las contraseñas no coinciden" ValidationGroup="User" 
                        Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                    &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 161px; text-align: right">
    <asp:Label ID="lbObligatorio0" runat="server" Text="Campos Obligatorios*" 
            style="font-size: x-small; text-align: right;"></asp:Label>
                </td>
            <td style="width: 223px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 161px">
                &nbsp;</td>
            <td style="width: 223px">
    &nbsp;<asp:Button ID="BtCompletar" runat="server" onclick="BtCompletar_Click" 
        Text="Completar Registro" ValidationGroup="User" />
                </td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Mensaje enviado" Visible="False" 
                    ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 161px">
                &nbsp;</td>
            <td style="width: 223px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ShowMessageBox="True" ValidationGroup="User" />
    <br />
&nbsp;
</asp:Content>

