﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cazaDisponibilidad : System.Web.UI.Page
{
    public enum tipos { Ninguno = 0, Mano = 1, Ojeo = 2, Asalto = 3, Reclamo = 4, Pase = 5 };

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["email"] == null)
            Response.Redirect("~/error.aspx");
        
        string auxF = "";
        Application.Lock();
        auxF = Application["FechaCaza"].ToString();
        Application.UnLock();

        if (auxF != "")
        {
            LabelFecha2.Text = auxF;
            DateTime fecha = Convert.ToDateTime(auxF);

            int dia = fecha.Day;
            int mes = fecha.Month;
            int anyo = fecha.Year;

            if (!IsPostBack)
            {
                Response.Cookies.Add(new HttpCookie("dia", dia.ToString()));
                Response.Cookies.Add(new HttpCookie("mes", mes.ToString()));
                Response.Cookies.Add(new HttpCookie("año", anyo.ToString()));
            }
            ENLogicaDeNegocio.ENArea n = new ENLogicaDeNegocio.ENArea();
            int area = 1;
            bool estado = n.ENGetEstado(area);
            //Area 1
            if (estado)
            {
                RBLArea1.SelectedIndex = 0;
                TBNumEscopetasArea1.Text = n.ENGetNumEscopetas(area, dia, mes, anyo).ToString();
                TBTipoCazaArea1.Text = Enum.GetName(typeof(tipos), n.ENGetTipoCaza(area, dia, mes, anyo));
            }
            else
            {
                RBLArea1.SelectedIndex = 1;
            }
            area++;
            //Area 2
            estado = n.ENGetEstado(area);
            if (estado)
            {
                RBLArea2.SelectedIndex = 0;
                TBNumEscopetasArea2.Text = n.ENGetNumEscopetas(area, dia, mes, anyo).ToString();
                TBTipoCazaArea2.Text = Enum.GetName(typeof(tipos), n.ENGetTipoCaza(area, dia, mes, anyo));

            }
            else
            {
                RBLArea2.SelectedIndex = 1;
            }
            area++;
            //Area 3
            estado = n.ENGetEstado(area);
            if (estado)
            {
                RBLArea3.SelectedIndex = 0;
                TBNumEscopetasArea3.Text = n.ENGetNumEscopetas(area, dia, mes, anyo).ToString();
                TBTipoCazaArea3.Text = Enum.GetName(typeof(tipos), n.ENGetTipoCaza(area, dia, mes, anyo));

            }
            else
            {
                RBLArea3.SelectedIndex = 1;
            }
            area++;
            //Area 4
            estado = n.ENGetEstado(area);
            if (estado)
            {
                RBLArea4.SelectedIndex = 0;
                TBNumEscopetasArea4.Text = n.ENGetNumEscopetas(area, dia, mes, anyo).ToString();
                TBTipoCazaArea4.Text = Enum.GetName(typeof(tipos), n.ENGetTipoCaza(area, dia, mes, anyo));

            }
            else
            {
                RBLArea4.SelectedIndex = 1;
            }
        }
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuCaza');</script>");

    }
    protected void ImageButtonVolver_Click(object sender, ImageClickEventArgs e)
    {

        Response.Redirect("~/cazacontratacion.aspx");


    }
}