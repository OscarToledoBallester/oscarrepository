﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="hotel.aspx.cs" Inherits="hotel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="ImageCabeceraHotel" runat="server" 
        ImageUrl="~/images/ponoigretocada2.jpg" Height="228px" Width="979px" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>El Hotel</h1>
    <table style="width:100%;">
        <tr>
            <td style="width: 379px">
                <asp:Image ID="ImageBody1" runat="server" ImageUrl="~/images/hotel2retocado2.jpg" Height="195px" Width="465" />
           
            </td>
            <td valign="middle" style="width: 298px">
                <p class="textosinicio">
                    En las cercanías del &quot Valle de Guadalest &quot rodeado de una naturaleza inmejorable. 
                    Un lugar acogedor, sencillo, rústico y con solera, para personas amantes de la caza, del 
                    buen gusto, el confort, el lujo, la intimidad y la privacidad donde podremos disfrutar de la 
                    mejor de las compañías. 
                </p>
                
                
            </td>
        </tr>
        <tr>
            <td style="width: 379px; vertical-align:top; vertical-align:middle;">
                <p class="textosinicio">
                    Nuestro restaurante, abierto para todos nuestros clientes y preparado para la 
                    celebración de eventos. Ha sido pensado y decorado para que te sientas en un 
                    lugar sumamente grato, acogedor y cálido con un toque de romanticismo y glamour, 
                    donde degustar nuestra gastronomía acompañada de una variada bodega
                </p>
            </td>
            <td style="width: 298px">
                <asp:Image ID="ImageBody2" runat="server" ImageUrl="~/images/restaurante2retocado2.jpg" Height="195px" Width="465" />
                
            </td>
        </tr>
        <tr>
            <td style="width: 379px">
                <asp:Image ID="ImageBody3" runat="server" ImageUrl="~/images/actividadesretocado.jpg" Height="195px" Width="465" />
            </td>
            <td style="width: 298px; vertical-align:top;">

                <p class="textosinicio">
                    En un entorno maravilloso de importante zona paisajística, cinegética de fauna y 
                    flora donde podrá disfrutar de muchas actividades al aire libre: 
                <br />  
                    - Circuitos para bicicleta y paseos a caballo, rapel,<br /> &nbsp piragüismo, tiro con arco, quads.<br />
                    - Visitas turísticas y culturales a pueblos llenos  <br />&nbsp de encanto y de historia.<br />
                    - Al final de una jornada de caza podrá reunirse  <br />&nbsp con sus amigos y disfrutar de un 
                    buen gazpacho.<br />
                </p>    
            </td>

        </tr>
    </table>
</asp:Content>