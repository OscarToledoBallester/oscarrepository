﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ENLogicaDeNegocio;

public partial class reservas2 : System.Web.UI.Page
{
    private DateTime fentrada = DateTime.Today;
    private DateTime fsalida = DateTime.Today;
    private string descHab = "";
    private float precioHab ;
    private int numNoches = 0;
    private string extraPension = "Sólo Alojamiento";
    private string extraParking = "Sin Parking";
    private string extraActividad = "";
    

    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuReservas');</script>");

        //Aqui comprobamos que la pagina venga o no de alguna actividad si viene mostraremos el paso3
        if (Session["idActividad"] != null)
        {
            CambiarAPaso3DesdeActividades();
        }

        if (!Page.IsPostBack)
        {
            if (Session["email"] == null)
                Response.Redirect("~/error.aspx");

            //CambiarAPaso1();//Esta funcion se encargara de hacer visible el Panel 1 y remarcar el label
            /*LBFechaIncorrecta.Text = "";
            descHab = "";
            precioHab = 0;
            numNoches = 0;
            extraPension = "Sólo Alojamiento";
            extraParking = "Sin Parking";
            extraActividad = "";*/
        }

        // Estilos de titulo (paso 1, 2, 3)
        LBPaso1.Font.Underline = true;
        LBPaso2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
        LBPaso3.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
    }
    protected void ImageButtonReservaInd_Click(object sender, ImageClickEventArgs e)
    {
        bool fecha_ok = false;
        //Comprobar las fechas primero
        if (TBFechaLlegada.Text == "" || TBFechaSalida.Text == "")
        {
            LBFechaIncorrecta.Text = "*Introduce Fechas de Entrada y Salida";
            fecha_ok = false;
        }
        else
        {

            //Asignamos las fechas introducidas
            fentrada = DateTime.Parse(TBFechaLlegada.Text);
            fsalida = DateTime.Parse(TBFechaSalida.Text);

            if (!ComprobarFechas(fentrada, fsalida))
            {
                LBFechaIncorrecta.Text = "*Las fechas introducidas no son correctas";
            }
            else if (fentrada < DateTime.Today)
            {
                LBFechaIncorrecta.Text = "*La fecha debe ser posterior a la actual";
            }
            else
            {
                fecha_ok = true;
                //Metemos las fechas en una sesion
                Session["fecha_entrada"] = fentrada;
                Session["fecha_salida"] = fsalida;
            }
        }
        if (fecha_ok == true)
        {
            numNoches = ObtenerDias(fentrada, fsalida);
            Session["noches"] = numNoches;


            //Guardamos Datos Habitacion y tmb los guardamos en la session
            descHab = "Individual";
            Session["descripcion"] = descHab;
            precioHab = 50;
            Session["precio"] = numNoches*precioHab;

            //Borramos contenido del Label (la fecha introducida sera correcta)
            LBFechaIncorrecta.Text = "";
            //Mostramos el panel del Paso2
            CambiarAPaso2();

            //LAbel NOCHES
            LBNoches.Text = "Ha seleccionado " + numNoches.ToString() + " Noche/s en una habitación Individual (" + (numNoches * precioHab).ToString() + " €)";
        }
        
    }
    protected void ImageButtonReservaDob_Click(object sender, ImageClickEventArgs e)
    {
        bool fecha_ok = false;
        //Comprobar las fechas primero
        if (TBFechaLlegada.Text == "" || TBFechaSalida.Text == "")
        {
            LBFechaIncorrecta.Text = "*Introduce Fechas de Entrada y Salida";
            fecha_ok = false;
        }
        else
        {

            //Asignamos las fechas introducidas
            fentrada = DateTime.Parse(TBFechaLlegada.Text);
            fsalida = DateTime.Parse(TBFechaSalida.Text);
            if (!ComprobarFechas(fentrada, fsalida))
            {
                LBFechaIncorrecta.Text = "*Las fechas introducidas no son correctas";
            }
            else if (fentrada < DateTime.Today)
            {
                LBFechaIncorrecta.Text = "*La fecha debe ser posterior a la actual";
            }
            else
            {
                fecha_ok = true;
                //Metemos las fechas en una sesion
                Session["fecha_entrada"] = fentrada;
                Session["fecha_salida"] = fsalida;
            }
        }
        if (fecha_ok == true)
        {
            
            numNoches = ObtenerDias(fentrada, fsalida);
            Session["noches"] = numNoches;

            //Guardamos Datos Habitacion
            descHab = "Doble";
            Session["descripcion"] = descHab;
            precioHab = 80;
            Session["precio"] = precioHab*numNoches;

            //Borramos contenido del Label (la fecha introducida sera correcta)
            LBFechaIncorrecta.Text = "";
            //Mostramos el panel del Paso2
            CambiarAPaso2();

            //LAbel NOCHES
            LBNoches.Text = "Ha seleccionado " + numNoches.ToString() + " Noche/s en una habitación Doble (" + (numNoches * precioHab).ToString() + " €)";
        }
    }
    protected void ImageButtonReservaSuite_Click(object sender, ImageClickEventArgs e)
    {
        bool fecha_ok = false;
        //Comprobar las fechas primero
        if (TBFechaLlegada.Text == "" || TBFechaSalida.Text == "")
        {
            LBFechaIncorrecta.Text = "*Introduce Fechas de Entrada y Salida";
            fecha_ok = false;
        }
        else
        {

            //Asignamos las fechas introducidas
            fentrada = DateTime.Parse(TBFechaLlegada.Text);
            fsalida = DateTime.Parse(TBFechaSalida.Text);
            if (!ComprobarFechas(fentrada, fsalida))
            {
                LBFechaIncorrecta.Text = "*Las fechas introducidas no son correctas";
            }
            else if (fentrada < DateTime.Today)
            {
                LBFechaIncorrecta.Text = "*La fecha debe ser posterior a la actual";
            }
            else
            {
                fecha_ok = true;
                //Metemos las fechas en una sesion
                Session["fecha_entrada"] = fentrada;
                Session["fecha_salida"] = fsalida;
            }
        }
        if (fecha_ok == true)
        {
            numNoches = ObtenerDias(fentrada, fsalida);
            Session["noches"] = numNoches;

            //Borramos contenido del Label (la fecha introducida sera correcta)
            LBFechaIncorrecta.Text = "";
            //Guardamos Datos Habitacion
            descHab = "Suite";
            Session["descripcion"] = descHab;
            precioHab = 200;
            Session["precio"] = precioHab*numNoches;

            //Borramos contenido del Label (la fecha introducida sera correcta) y canviamos al Paso 2
            LBFechaIncorrecta.Text = "";
            //Mostramos el panel del Paso2
            CambiarAPaso2();

            //LAbel NOCHES
            LBNoches.Text = "Ha seleccionado " + numNoches.ToString() + " Noche/s en una habitación Suite (" + (numNoches * precioHab).ToString() + " €)";
        }
    }
    protected void ImageButtonPasarPaso3_Click(object sender, ImageClickEventArgs e)
    {
        float preciofin = float.Parse(Session["precio"].ToString());
        //Guardaremos los datos de Extras seleccionados
        extraPension = RBLPension.SelectedValue;
        Session["pension"] = extraPension;
        extraParking = RBLParking.SelectedValue;
        Session["parking"] = extraParking;
        if (extraParking == "Parking Turismo")
            preciofin += 30;
        else if (extraParking == "Parking Remolque")
            preciofin += 50;

        extraActividad = RBLActividades.SelectedValue;
        Session["actividad"] = extraActividad;
        Session["precio"] = preciofin;

        if (extraActividad == "Quads") {
            Response.Redirect("~/activ-reserva-quad.aspx");
        }
        else if (extraActividad == "Localidades de Interés") {
            Response.Redirect("~/activ-reserva-localidad.aspx");
        }
        else if (extraActividad == "Bicicleta") {
            Response.Redirect("~/activ-reserva-bicicleta.aspx");
        }
        else if (extraActividad == "Parques Temáticos") {
            Response.Redirect("~/activ-reserva-parque.aspx");
        }
        else {
            Session["idActividad"] = "";
        }

        //Mostramos el Paso 3
        CambiarAPaso3();

        //Sacamos las fechas
        
        DateTime muestrofentrada = Convert.ToDateTime(Session["fecha_entrada"].ToString());
        DateTime muestrofsalida = Convert.ToDateTime(Session["fecha_salida"].ToString());

        //Ponemos los datos seleccionados
        LBTipoHabitacion.Text = "Resumen de reserva habitación " + Session["descripcion"];
        LBNumNoches.Text = Session["noches"] + " Noches";
        LBFechas.Text = "Desde: " + muestrofentrada.ToString("d") + " - " + muestrofsalida.ToString("d");
        LBPrecioTotal.Text = preciofin + " €";
        LBTipoRegimen.Text = extraPension;
        LBTipoParking.Text = extraParking;
        LBActividad.Text = extraActividad;
    }
    private int ObtenerDias(DateTime fe, DateTime fs)
    {
        TimeSpan span = fs - fe;
        int iDays = span.Days;
        return iDays;
    }
    public bool ComprobarFechas(DateTime fing, DateTime fsal)
    {
        bool ok;
        if (fing >= fsal)
        {
            ok = false;
        }
        else
            ok = true;


        return ok;
    }
    public void CambiarAPaso1()
    {
        PanelPaso1.Visible = true;
        if (PanelPaso1.Visible == true)
        {
            LBPaso1.Font.Underline = true;
            LBPaso1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#749F01");

            PanelPaso2.Visible = false;
            LBPaso2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
            LBPaso2.Font.Underline = false;

            PanelPaso3.Visible = false;
            LBPaso3.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
            LBPaso3.Font.Underline = false;
        }
    }
    public void CambiarAPaso2()
    {
        PanelPaso2.Visible = true;
        if (PanelPaso2.Visible == true)
        {
            LBPaso2.Font.Underline = true;
            LBPaso2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#749F01");

            PanelPaso1.Visible = false;
            LBPaso1.Font.Underline = false;
            LBPaso1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");

            PanelPaso3.Visible = false;
            LBPaso3.Font.Underline = false;
            LBPaso3.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
        }

    }
    public void CambiarAPaso3()
    {
        PanelPaso3.Visible = true;
        if (PanelPaso3.Visible == true)
        {
            LBPaso3.Font.Underline = true;
            LBPaso3.ForeColor = System.Drawing.ColorTranslator.FromHtml("#749F01");

            PanelPaso1.Visible = false;
            LBPaso1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
            LBPaso1.Font.Underline = false;

            PanelPaso2.Visible = false;
            LBPaso2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
            LBPaso2.Font.Underline = false;
        }
    }
    public void CambiarAPaso3DesdeActividades()
    {
        // Colores de opciones 1, 2, 3
        LBPaso3.Font.Underline = true;
        LBPaso3.ForeColor = System.Drawing.ColorTranslator.FromHtml("#749F01");

        PanelPaso1.Visible = false;
        LBPaso1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
        LBPaso1.Font.Underline = false;

        PanelPaso2.Visible = false;
        LBPaso2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#bbb");
        LBPaso2.Font.Underline = false;


        float preciofin = float.Parse(Session["precio"].ToString());
        //Guardaremos los datos de Extras seleccionados
        extraPension = RBLPension.SelectedValue;
        Session["pension"] = extraPension;
        extraParking = RBLParking.SelectedValue;
      //  Session["parking"] = extraParking;
      //  if (extraParking == "Parking Turismo")
        /*
        if (Session["parking"].ToString() == "Parking Turismo")
            preciofin += 30;
        else if (Session["parking"].ToString() == "Parking Remolque")
            preciofin += 50;
        */
        //Mostramos el Paso 3
        CambiarAPaso3();

        //Sacamos las fechas
        DateTime muestrofentrada = Convert.ToDateTime(Session["fecha_entrada"].ToString());
        DateTime muestrofsalida = Convert.ToDateTime(Session["fecha_salida"].ToString());

        //Ponemos los datos seleccionados
        LBTipoHabitacion.Text = "Resumen de reserva habitación " + Session["descripcion"];
        LBNumNoches.Text = Session["noches"] + " Noche/s";
        LBFechas.Text = "Desde: " + muestrofentrada.ToString("d") + " - " + muestrofsalida.ToString("d");
        LBTipoRegimen.Text = extraPension;
        LBTipoParking.Text = Session["parking"].ToString();
        LBActividad.Text = Session["actividad"].ToString();

        // SUMA DEL PRECIO DE LA ACTIVIDAD AL PRECIO FINAL => (precioTotal = precioFin + precioActividad)
        float precioTotal = preciofin; // Incrementamos al ya calculado.
        string actividad = Session["actividad"].ToString();

        int id_Actividad = Convert.ToInt32(Session["idActividad"].ToString());
        if (actividad == "Quads")
        {
            Persistencia.CADVehiculoQuad quad = new Persistencia.CADVehiculoQuad();
            precioTotal += quad.getPrecio(id_Actividad);
        }
        else if (actividad == "Bicicleta")
        {
            Persistencia.CADBicicleta bici = new Persistencia.CADBicicleta();
            precioTotal += bici.getPrecio(id_Actividad);
        }
        else if (actividad == "Localidades de Interés")
        {
            Persistencia.CADLocalidadInteres localidad = new Persistencia.CADLocalidadInteres();
            precioTotal += localidad.getPrecio(id_Actividad);
        }
        else
        {
            Persistencia.CADParqueTematico parque = new Persistencia.CADParqueTematico();
            precioTotal += parque.getPrecio(id_Actividad);
        }
        LBPrecioTotal.Text = precioTotal + " €";
    }
    protected void ImageButtonVolverPaso1_Click(object sender, ImageClickEventArgs e)
    {
        CambiarAPaso1();
    }
    protected void ImageButtonVolverPaso2_Click(object sender, ImageClickEventArgs e)
    {
        CambiarAPaso2();
    }
    protected void ImageButtonConfirmarReserva_Click(object sender, ImageClickEventArgs e)
    {
        bool quedan_libres = true;
        //Aquí llamaremos al EN de Reservas para realizar la reserva en la BD pero antes......

        //Necesito 
        
        //DateTime fecha_registro(freg)--> Session["fecha_entrada"]
        //DateTime fecha_ingreso(fing)--> Session["fecha_entrada"] es lo mismo que fecha_registro
        //DateTime fecha_salida(fsal) --> Session["fecha_salida"]
        //string dni(dni) --> Session["dni"]
        //string tipo Aactividad(tipoA) --> aQuad,aBicicleta,aLocalidad,aParque
        //int id Actividad(idA) --> Session["idActividad"] pablo me lo pasa desde su pagina
        //int numero Habitacion(numHab) --> Llamar a: ComprobarLibresTipoHab ---> Coger la primera libre del DataSet
        //int numero Planta(numPla) --> 1(Individual), 2(Doble), 3(Suite) lo tengo en Session["descripcion"]
        //bool parking (park) --> si escoje algun parking se le pasara un 0

        ENReservas reservaWeb = new ENReservas();

        //fecha_registro
        DateTime fecha_registro = Convert.ToDateTime(Session["fecha_entrada"].ToString());
        DateTime fecha_ingreso = Convert.ToDateTime(Session["fecha_entrada"].ToString());
        DateTime fecha_salida = Convert.ToDateTime(Session["fecha_salida"].ToString());
        string dni = Session["dni"].ToString();
        string tipo_Actividad = Session["actividad"].ToString();

        //ACTIVIDADES
        string tipo_Actividad_paraBD = "";
        int id_Actividad = 0;
        if (tipo_Actividad != "Ninguna")
        {
            if (Session["idActividad"] != null)
                id_Actividad = int.Parse(Session["idActividad"].ToString());
            else
                id_Actividad = 0;
        }
        else
        {
            id_Actividad = 0;
            tipo_Actividad_paraBD = "";
        }
       
        //aqui le cambiare el nombre a la actividad para introducir en la bd
        if (tipo_Actividad == "Quads")
        {
            tipo_Actividad_paraBD = "aQuad";
        }
        else
        {
            if (tipo_Actividad == "Bicicleta")
            {
                tipo_Actividad_paraBD = "aBicicleta";
            }
            else
            {
                if (tipo_Actividad == "Localidades de Interés")
                {
                    tipo_Actividad_paraBD = "aLocalidad";
                }
                else
                {
                    if (tipo_Actividad == "Parques Temáticos")
                    {
                        tipo_Actividad_paraBD = "aParque";
                    }
                }
            }
        }
        
        //Sacamos el numero de planta
        int numero_planta;
        string descripcion_habitacion = Session["descripcion"].ToString();
        if(descripcion_habitacion == "Individual")
            numero_planta = 1;
        else if(descripcion_habitacion == "Doble")
            numero_planta = 2;
        else
            numero_planta = 3;
        
        //Sacamos la primera habitación libre para insertar en ella
        int numero_habitacion = 0;
        Persistencia.CADHabitaciones habitaciones = new Persistencia.CADHabitaciones();
        DataSet ocupadas;

        if (numero_planta == 1)
        {
            ocupadas = habitaciones.DevolverLibresIndividualCAD();//Devuelve las ocupadas
            if (ocupadas.Tables[0].Rows.Count == 20)
                quedan_libres = false;
        }
        else if (numero_planta == 2)
        {
            ocupadas = habitaciones.DevolverLibresDoblesCAD();//Devuelve las ocupadas
            if (ocupadas.Tables[0].Rows.Count == 20)
                quedan_libres = false;
        }
        else
        {
            ocupadas = habitaciones.DevolverLibresSuiteCAD();//Devuelve las ocupadas
            if (ocupadas.Tables[0].Rows.Count == 2)
                quedan_libres = false;
        }
        
        //Esta funcion esta implementada mas abajo y lo que hace es comprobar recorriendo los numeros cual no esta entre las ocupadas
        numero_habitacion = DevuelvePrimeraLibre(ocupadas);

        //Comprobamos si ha elegido parking
        string parking = Session["parking"].ToString();
        bool parking_elegido = true;
        if (parking == "Sin Parking")
            parking_elegido = false;

        if (quedan_libres == true)
        {
            reservaWeb.NuevaReserva(fecha_registro, fecha_ingreso, fecha_salida, dni, tipo_Actividad_paraBD, id_Actividad, numero_habitacion, numero_planta, parking_elegido);

            // Borramos todas las variables de Session utilizadas en "RESERVAS" al reservar con exito.
            Session.Remove("idActividad");
            Session.Remove("actividad");
            Session.Remove("precio");
            Session.Remove("fecha_entrada");
            Session.Remove("fecha_salida");
            Session.Remove("noches");
            Session.Remove("descripcion");
            Session.Remove("pension");
            Session.Remove("parking");
            
            // Finalmente redirigimos.
            Response.Redirect("~/hotel.aspx");
        }
        else
            LBNoHayHabitacion.Text = "No quedan " + descripcion_habitacion + " libres";
    }
    public int DevuelvePrimeraLibre(DataSet ocupadas)
    {
        int libre = 1;
        bool encontrada = false;
       
        while (libre < 20 && !encontrada)//Busca la primera que no este ocupada
        {
            bool paro = false;
            for (int i = 0; i < ocupadas.Tables[0].Rows.Count && !paro; i++)
            { 
                if(libre == int.Parse(ocupadas.Tables[0].Rows[i].ItemArray[0].ToString()))
                    paro = true;
            }
            if(paro)
                libre++;
            else
                encontrada = true;
        }
        return libre;
    }
}