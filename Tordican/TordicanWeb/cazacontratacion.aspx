﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cazacontratacion.aspx.cs" Inherits="cazaContratacion" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register assembly="System.Web.Ajax" namespace="System.Web.UI" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="ImagePrincipal" runat="server" ImageUrl="~/images/cazaOK.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Contratación del Coto</h1>
    <h1>
         <asp:Label ID="LabelFechaContra" runat="server" CssClass="h1" Text="Fecha Actual: "></asp:Label>
         <asp:TextBox ID="TBFechaActual" runat="server" CssClass="tbfechacontratacion" 
             Enabled="False" ReadOnly="True" BackColor="White" BorderStyle="None" 
             Font-Names="Georgia" Font-Size="26px" ForeColor="#749F01"></asp:TextBox>
     </h1>


    <asp:Panel ID="PanelInicial" runat="server">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                
                    <asp:AjaxScriptManager ID="AjaxScriptManager1" runat="server">
                    </asp:AjaxScriptManager>
                
                </td>
                <td width="50%">
                
                    <br />
                </td>
            </tr>
            <tr>
                <td width="50%">
                    <asp:Label ID="LabelFechaContratacion" runat="server" CssClass="textos" 
                        Text="Fecha Contratación: "></asp:Label>
                    <asp:TextBox ID="TBFecLlegada" runat="server" CssClass="tbfechacontratacion"></asp:TextBox>
                    <asp:CalendarExtender ID="TBFecLlegada_CalendarExtender" runat="server" 
                        Enabled="True" Format="dd/MM/yyyy" TargetControlID="TBFecLlegada" 
                        TodaysDateFormat="d MMMM, yyyy" >
                    </asp:CalendarExtender>
                </td>
                <td width="50%" align="justify">
                    <asp:Button ID="ButtonDisponibilidad0" runat="server" 
                        CssClass="botondisponibilidad" ForeColor="#516E01" height="32px" 
                        onclick="ButtonDisponibilidad_Click" Text="Ver Disponibilidad" 
                        ValidationGroup="Fecha" width="167px" />
                </td>
            </tr>
            <tr>
                <td width="50%" align="center">
                    <asp:RequiredFieldValidator ID="RFVFecha" runat="server" 
                        ControlToValidate="TBFecLlegada" 
                        ErrorMessage="* Debes seleccionar una fecha" 
                        SetFocusOnError="True" ValidationGroup="Fecha" Display="Dynamic"></asp:RequiredFieldValidator>
                    
                    <asp:Label ID="LabelErrorFecha" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td width="50%" align="center">
                    
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelDatosPersonales" runat="server">
        <table style="width:100%;">
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelDni" runat="server" CssClass="textos" Text="Dni:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBDni" runat="server" Width="50%" TabIndex="2" Enabled="False"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelNombre" runat="server" CssClass="textos" Text="Nombre:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBNombre" runat="server" Width="100%" TabIndex="3" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:CustomValidator ID="CustomValidatorDni" runat="server" 
                        ControlToValidate="TBDni" Display="Dynamic" ErrorMessage="* Dni incorrecto" 
                        onservervalidate="CustomValidatorDni_ServerValidate" 
                        SetFocusOnError="True" ValidationGroup="User"></asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="RFVDni" runat="server" 
                        ControlToValidate="TBDni" Display="Dynamic" ErrorMessage="* Dni obligatorio" 
                        SetFocusOnError="True" ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="REVNombre" runat="server" 
                        ControlToValidate="TBNombre" Display="Dynamic" 
                        ErrorMessage="* Nombre incorrecto: caracteres no numéricos" SetFocusOnError="True" 
                        ValidationExpression="\D+" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNombre" runat="server" 
                        ControlToValidate="TBNombre" Display="Dynamic" 
                        ErrorMessage=" * Nombre obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelPrimerApellido" runat="server" CssClass="textos" 
                        Text="1º Apellido:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBPrimerApellido" runat="server" Width="100%" TabIndex="4" 
                        Enabled="False"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelSegundoApellido" runat="server" CssClass="textos" 
                        Text="2º Apellido:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBSegundoApellido" runat="server" Width="100%" TabIndex="5" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="REVPrimerApellido" runat="server" 
                        ControlToValidate="TBPrimerApellido" Display="Dynamic" 
                        ErrorMessage="* Primer apellido incorrecto: caracteres no numéricos" SetFocusOnError="True" 
                        ValidationExpression="\D+" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RFVPrimerApellido" runat="server" 
                        ControlToValidate="TBPrimerApellido" Display="Dynamic" 
                        ErrorMessage="* Primer apellido obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="REVSegundoApellido" runat="server" 
                        ControlToValidate="TBSegundoApellido" Display="Dynamic" 
                        ErrorMessage="* Segundo apellido incorrecto: caracteres no numéricos" ValidationExpression="\D+" 
                        ValidationGroup="User"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RFVSegundoApellido" runat="server" 
                        ControlToValidate="TBSegundoApellido" Display="Dynamic" 
                        ErrorMessage="* Segundo apellido obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelDireccion" runat="server" CssClass="textos" 
                        Text="Dirección:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBDireccion" runat="server" Width="100%" TabIndex="6" 
                        Enabled="False"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelPais" runat="server" CssClass="textos" Text="Pais:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBPais" runat="server" Width="100%" TabIndex="7" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelProvincia" runat="server" CssClass="textos" 
                        Text="Provincia:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBProvincia" runat="server" Width="100%" TabIndex="8" 
                        Enabled="False"></asp:TextBox>
                </td>
                <td width="15%">
                    <asp:Label ID="LabelEmail" runat="server" CssClass="textos" Text="E-mail:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBEmail" runat="server" Width="100%" TabIndex="9" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="REVEmail" runat="server" 
                        ControlToValidate="TBEmail" Display="Dynamic" 
                        ErrorMessage="* E-mail incorrecto" SetFocusOnError="True" 
                        ValidationExpression="\S+@\S+\.\S+" ValidationGroup="User"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td width="15%">
                    <asp:Label ID="LabelTelefono" runat="server" CssClass="textos" 
                        Text="Teléfono:"></asp:Label>
                </td>
                <td width="35%">
                    <asp:TextBox ID="TBTelefono" runat="server" Width="50%" TabIndex="10" 
                        Enabled="False"></asp:TextBox>
                </td>
                <td width="15%">
                    &nbsp;</td>
                <td width="35%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="REVTelefono" runat="server" 
                        ControlToValidate="TBTelefono" Display="Dynamic" 
                        ErrorMessage="* Telefono incorrecto: número de 9 dígitos" SetFocusOnError="True" 
                        ValidationExpression="\d{9}" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RFVTelefono" runat="server" 
                        ControlToValidate="TBTelefono" Display="Dynamic" 
                        ErrorMessage="* Telefono obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <br />

    <asp:Panel ID="PanelDatosCoto" runat="server">
        <table style="width:100%;">
            <tr>
                <td width="20%">
                    <asp:Label ID="LabelArea" runat="server" CssClass="textos" 
                        Text="Selecciona Área:"></asp:Label>
                </td>
                <td align="center" width="25%">
                    <asp:DropDownList ID="DDLArea" runat="server" Width="200px" TabIndex="11" 
                        DataSourceID="EnlaceArea" DataTextField="numero" DataValueField="numero">
                        <asp:ListItem Value="0">(Selecciona)</asp:ListItem>
                        <asp:ListItem Value="1">Área 1</asp:ListItem>
                        <asp:ListItem Value="2">Área 2</asp:ListItem>
                        <asp:ListItem Value="3">Área 3</asp:ListItem>
                        <asp:ListItem Value="4">Área 4</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="EnlaceArea" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:TordicanBDConnectionString %>" 
                        
                        SelectCommand="SELECT numero FROM Area WHERE (estado = 'true') EXCEPT SELECT numArea FROM ContrataCoto WHERE (dia = @dia) AND (mes = @mes) AND (año = @año) AND (ContrataCoto.mes = @mes) AND (ContrataCoto.año = @año)" 
                        onselecting="EnlaceArea_Selecting">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="true" Name="estado" Type="Boolean" />

                            <asp:CookieParameter CookieName="dia" DefaultValue="1" Name="dia" />
                            <asp:CookieParameter CookieName="mes" DefaultValue="1" Name="mes" />
                            <asp:CookieParameter CookieName="año" DefaultValue="2000" Name="año" />

                        </SelectParameters>
                    </asp:SqlDataSource>
                    <br />
                </td>
                <td width="25%">
                    <asp:Label ID="LabelTipoCaza" runat="server" CssClass="textos" 
                        Text="Selecciona Tipo de Caza:"></asp:Label>
                </td>
                <td align="justify" width="25%">
                    <asp:DropDownList ID="DDLTipoCaza" runat="server" Width="200px" TabIndex="12" DataTextField="descripcion" 
                        DataValueField="descripcion">
                        <asp:ListItem Value="0">Mano</asp:ListItem>
                        <asp:ListItem Value="1">Ojeo</asp:ListItem>
                        <asp:ListItem Value="2">Asalto</asp:ListItem>
                        <asp:ListItem Value="3">Reclamo</asp:ListItem>
                        <asp:ListItem Value="4">Pase</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td width="20%">
                    <asp:Label ID="LabelNumEscopetas" runat="server" CssClass="textos" 
                        Text="Número de Escopetas:"></asp:Label>
                </td>
                <td width="30%">
                    <asp:RadioButtonList ID="RBLNumEscopetas" runat="server" 
                        RepeatDirection="Horizontal" Width="100%" TabIndex="13">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td align="center" width="25%">
                    <asp:CheckBox ID="CBPerrera" runat="server" CssClass="textos" Text="Perrera" 
                        TabIndex="14" />
                </td>
                <td align="center" width="25%">
                    <asp:CheckBox ID="CBParking" runat="server" CssClass="textos" 
                        Text="Parking Remolques" TabIndex="15" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>

        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="PanelComida" runat="server">
        <table style="width:100%;">
            <tr>
                <td align="center" width="50%">
                    <asp:CheckBox ID="CBComidaCoto" runat="server" CssClass="textos" 
                        Text="Contrata Comida" AutoPostBack="True" 
                        oncheckedchanged="CBComidaCoto_CheckedChanged" TabIndex="16" />
                </td>
                <td align="center" width="50%">
                    <asp:DropDownList ID="DDLTiposComida" runat="server" Width="75%" TabIndex="17" 
                        DataTextField="descripcion" DataValueField="descripcion" Enabled="False">
                        <asp:ListItem Value="0">Barbacoa</asp:ListItem>
                        <asp:ListItem Value="1">Migas</asp:ListItem>
                        <asp:ListItem Value="2">Gachas</asp:ListItem>
                        <asp:ListItem Value="3">Gazpacho</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:ValidationSummary ID="VSErrores" runat="server" BorderStyle="Double" 
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="User" />
                </td>
                <td align="center">
                    <asp:ImageButton ID="ImageButtonReserva" runat="server" 
                        ImageUrl="~/images/reserve-ahora.png" onclick="ImageButtonReserva_Click" 
                        TabIndex="18" ValidationGroup="User" />
                </td>
            
            </tr>
        </table>
    </asp:Panel>
    <br />
    <br />
    <br />
</asp:Content>

