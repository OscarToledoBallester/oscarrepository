﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-quad.aspx.cs" Inherits="quads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/activ-back-quad.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/actividades.aspx"
            CssClass="seccionAnterior">Actividades &gt;</asp:HyperLink> Quads</h1>
    <table style="width: 100%;">
        <tr>
            <td style="width: 359px">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/activ-quad-1.jpg" />
                <br />
                <br />
                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/activ-quad-2.jpg" />
                &nbsp;<br />
                <br />
                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/activ-quad-3.jpg" />
                <br />
&nbsp;&nbsp;
            </td>
            <td valign="top">
                <asp:Label ID="Label1" runat="server" Text="- Los ATV/Quads, son vehículos muy especiales por su rendimiento, fiabilidad y 
                respuesta a distintas exigencias de diferentes tipos de usuarios. Potencia, 
                agilidad y resistencia son algunas de las principales cualidades de estas 
                máquinas. Los ATV/Quads son multifuncionales, en cuanto a sus prestaciones y 
                pueden vérseles trepando angostos, circulando sobre superficies arenosas, 
                cruzando arroyos o atravesando frondosos bosques. La ligereza y fiabilidad de 
                estos vehículos los hacen capaces de superar terrenos donde otros vehículos no 
                deben adentrarse o no pueden hacerlo. Un potente motor y una flexible 
                combinación de la caja de cambios automática son características deseables para 
                este tipo de vehículos." Height="210px" Width="550px"></asp:Label><br />
                <asp:Label ID="Label2" runat="server" 
                    Text="- Comparten características con las motos como su ligereza, agilidad, posición de conducción, y sin embargo su estilo de conducción es más parecido al de un coche. Por muy difícil que parezca el obstáculo, para el ATV/Quad probablemente no suponga ningún problema. El manejo es sencillo salvo para los usuarios de motocicletas. Esta particularidad viene dada ya que el conductor de moto suele estar acostumbrado a mover su cuerpo para tomar la curva, mientras que para pilotar el ATV/Quad conviene hacer movimientos del manillar, ya que sólo con la inclinación, este vehículo no entrará en la curva de la forma que queremos." 
                    Height="190px" Width="550px"></asp:Label>
                    <br />
                <asp:Label ID="Label3" runat="server" 
                    Text="- La posición que hay que mantener es similar a la de la moto. Suele ser común que en lugar de un puño como acelerador se utilice un gatillo situado en el manillar. Conviene señalar que mientras el ATV/Quad está circulando nunca hay que sacar los pies e intentarlos poner en el suelo ya que la velocidad y las ruedas del eje trasero podrían jugarnos una mala pasada." 
                    Width="550px"></asp:Label>
                <br />
                <br />
                <br />
                <br />
                <div align="center">
                    <asp:ImageButton ID="imgBtReserveAhora" runat="server" 
                        ImageUrl="~/images/reserve-ahora.png" onclick="imgBtReserveAhora_Click" />
                </div>
                </td>
        </tr>
        </table>
</asp:Content>

