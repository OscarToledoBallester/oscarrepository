﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Subgurim.Controles;

public partial class Localizacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuLocalizacion');</script>");
        GMapPrueba.addControl(new GControl(GControl.preBuilt.LargeMapControl));   
        GMapPrueba.addControl(new GControl(GControl.preBuilt.MapTypeControl));
        MostrarValencia();
        NuevoMarker();
        GMapPrueba.enableHookMouseWheelToZoom = true;
        GMapPrueba.mapType = GMapType.GTypes.Normal;
        //GMapPrueba.addControl(new GControl(GControl.extraBuilt.TextualCoordinatesControl));
        if (!Page.IsPostBack)
        {
            GDirection gdir = new GDirection();
            gdir.autoGenerate = false;
            gdir.buttonElementId = "btRuta";
            gdir.fromElementId = tbProvincia.ClientID;
            gdir.toElementId = tbDestino.ClientID;
            gdir.divElementId = "divDirections";
            gdir.clearMap = true;
            gdir.errorMessage = "No se encuentra la ciudad";
            gdir.locale = "es";
            GMapPrueba.Add(gdir);
        }
    }
    
    public void MostrarValencia()
	{
	    GMapPrueba.Height = 600;
	    GMapPrueba.Width = 600;
        GMapPrueba.setCenter(new GLatLng(39.467643526668795, -0.37715211510658264), 12);
	    GMapPrueba.enableHookMouseWheelToZoom = true;
	    GMapPrueba.enableGKeyboardHandler = true;
	}
    public void NuevoMarker()
    {
        GLatLng lating = new GLatLng(39.467643526668795, -0.37715211510658264);
        GMarker marker = new GMarker(lating);
        GInfoWindowOptions IWoptions = new GInfoWindowOptions(12, GMapType.GTypes.Normal);
        GShowMapBlowUp mbUp = new GShowMapBlowUp(marker, false, IWoptions);
        GMapPrueba.addShowMapBlowUp(mbUp);

    }

}