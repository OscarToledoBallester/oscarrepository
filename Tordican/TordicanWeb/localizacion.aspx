﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="localizacion.aspx.cs" Inherits="Localizacion" %>

<%@ Register assembly="GMaps" namespace="Subgurim.Controles" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" 
        DescriptionUrl="~/images/imalocalizacionRedimensionada.jpg" Height="200px" 
        ImageUrl="~/images/imalocalizacionRedimensionada.jpg" Width="978px" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
        <h1>Localización<br />
        </h1>
        <hr />
        <p style="font-size: 16px; color: #749F01">
            Ruta</p>
        <p style="font-size: 14px; color: #749F01">
            Introduzca la ciudad de origen para poder generar la ruta. También se generarán 
            una serie de indicaciones sobre las carreteras,autovias o autopistas de la ruta. 
            Tambien es posible ver nuestro complejo desde Google Street View
            <asp:HyperLink ID="HyperLink7" runat="server" 
                NavigateUrl="~/localizacion2.aspx">aqui.</asp:HyperLink>
            <br />
        <asp:Label ID="Label2" runat="server" Text="Tordicán " style="text-align: right"></asp:Label>
                se encuentra en
        <asp:TextBox ID="tbDestino" runat="server" Enabled="False" BackColor="White" 
                BorderStyle="None" ForeColor="#516E01" Width="59px">Valencia</asp:TextBox>
                en la c/Libertad nº19. </p>
        <table style="width:100%;">
            <tr>
                <td style="width: 157px; text-align: right;">
        <asp:Label ID="Label1" runat="server" Text="Ciudad de Origen:" style="text-align: right"></asp:Label>
                </td>
                <td style="width: 137px">
        <asp:TextBox ID="tbProvincia" runat="server" ValidationGroup="localizacion"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 157px; height: 26px; text-align: right;">
                    &nbsp;</td>
                <td style="width: 137px; height: 26px;">
                    &nbsp;</td>
                <td style="height: 26px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 157px; text-align: right;">
        <input type ="button" id = "btRuta" value="Generar Ruta" 
                        style="width: 124px; margin-left: 0px" />
                </td>
                <td style="width: 137px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
&nbsp;<br />
        <table 
            style="width:100%;">
            <tr>
                <td>
<cc1:GMap ID="GMapPrueba" runat="server" Height="500px" Width="980px" style="text-align: center"/>
                </td>
            </tr>
            <tr>
                <td>
        <div id = "divDirections"></div>
                </td>
            </tr>
            </table>
        <br />
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
</asp:Content>

