﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-parque.aspx.cs" Inherits="localidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" 
        ImageUrl="~/images/activ-back-parque.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/actividades.aspx"
            CssClass="seccionAnterior">Actividades &gt;</asp:HyperLink> Parques 
        Temáticos</h1>
     <table style="width: 100%;">
        <tr>
            <td style="width: 359px">
                <asp:Image ID="imgLocalidad1" runat="server" 
                    ImageUrl="~/images/activ-parque-1.jpg" />
                <br />
                <br />
                <asp:Image ID="imgLocalidad2" runat="server" 
                    ImageUrl="~/images/activ-parque-2.jpg" />
                &nbsp;<br />
                <br />
                <asp:Image ID="imgLocalidad3" runat="server" 
                    ImageUrl="~/images/activ-parque-3.jpg" />
                <br />
&nbsp;&nbsp;
            </td>
            <td valign="top">
                <asp:Label ID="lbLocal1" runat="server" 
                    Text="- Debido a su emplazamiento privilegiado, Tordican ofrece visitas organizadas a diversos parques temáticos situados a pocos kilómetros. Los parques temáticos son un recinto con un conjunto de atracciones, espacios para el ocio, entretenimiento, educación y cultura, normalmente organizadas en torno a una línea argumental que les sirve de inspiración. Precisamente por esto un parque temático es algo mucho más complejo que un parque de atracciones o una feria. Esto también implica que vaya ligado a un proyecto empresarial más sólido y con importantes inversiones económicas." 
                    Height="175px" Width="500px"></asp:Label><br />
                <asp:Label ID="lbLocal2" runat="server" 
                    Text="- Terra Mítica es un parque temático situado en Benidorm (Alicante, España) y basado en las antiguas civilizaciones del Mediterráneo, por lo que se distribuye en cinco zonas temáticas: Egipto, Grecia, Roma, Iberia y Las Islas. Sus principales atracciones son Magnus Colossus, una montaña rusa de madera; Titánide, una montaña rusa invertida con cinco inversiones; El Vuelo del Fénix, una torre de caída libre de 54 m; Synkope, un péndulo gigante; La Furia de Tritón, un splash con dos caídas de 10 y 17 metros de altura; e Inferno, una montaña rusa de cuarta generación en la que los carros giran verticalmente a su libre albedrío a lo largo de todo su recorrido." 
                    Height="210px" Width="500px"></asp:Label>
                    <br />
                <asp:Label ID="lbLocal3" runat="server" 
                    Text="- PortAventura consta de seis áreas temáticas: Mediterrània, Polynesia, China, México, Far West y SésamoAventura (inaugurada en 2011). El parque posee una ambientación y espectáculos repartidos por las distintas áreas. También existe ambientación concreta en épocas determinadas como Halloween o Navidad, extensible al resto del resort como Hoteles y demás. Las animaciones son un punto fuerte del parque, ya que consiguen arrancar una sonrisa a los clientes en los momentos menos esperados. El parque se divide en seis áreas temáticas basadas en distintas civilizaciones históricas." 
                    Width="500px"></asp:Label>
                <br />
                <br />
                <div align="center">
                    <asp:ImageButton ID="imgBtReserveAhora" runat="server" 
                        ImageUrl="~/images/reserve-ahora.png" onclick="imgBtReserveAhora_Click" />
                </div>
                </td>
        </tr>
        </table>
</asp:Content>

