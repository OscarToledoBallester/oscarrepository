﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Persistencia; // Exclusivamente para LOGIN! Los demas pasan por las EN.
using ENLogicaDeNegocio;
using System.Data; // Creacion de DataSet.

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Comprobacion de la SESSION y carga condicionada de un panel u otro.
        if (Session["email"] != null)
        {
            panelLogin.Visible = false;
            panelCerrar.Visible = true;
            string nombreUsuario = Session["nombre"].ToString();
            nombreUsuario += " " + Session["apellido1"].ToString();
            nombreUsuario += " " + Session["apellido2"].ToString();
            labelEmailSession.Text = nombreUsuario; // Casting.
            
            // Cambio de "REGISTRO" a "PERFIL"
            hyperRegistro.Text = "PERFIL";
            hyperRegistro.NavigateUrl = "perfilcliente.aspx";
        }
        else
        {
            panelCerrar.Visible = false;
            panelLogin.Visible = true;
        }
    }
    protected void flagCastellano_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("index.aspx#castellano");
    }
    protected void flagValenciano_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("index.aspx#valenciano");
    }
    protected void flagIngles_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("index.aspx#ingles");
    }
    protected void btLogin_Click(object sender, EventArgs e)
    {
        // Comprobamos que los campos "usuario" y "password" no esten vacios.
        string email = tbEmail.Text.Trim();
        string password = tbPassword.Text.Trim();

        if (email != "" && password != "")
        {
            CADCliente cli = new CADCliente();
            if (cli.comprobarLoginWeb(email, password)) // INICIO DE SESION.
            {
                // ¡¡Identificacion correcta!!
                // Cargamos en la Session todas los datos del usuario (Facilitar acceso a campos).
                ENClientes cliEN = new ENClientes();
                DataSet cliDS;

                cliDS = cliEN.obtenerClientePorEmail(email);
                if (cliDS != null)
                {
                    Session["dni"] = cliDS.Tables[0].Rows[0].ItemArray[0].ToString();
                    Session["nombre"] = cliDS.Tables[0].Rows[0].ItemArray[1].ToString();
                    Session["apellido1"] = cliDS.Tables[0].Rows[0].ItemArray[2].ToString();
                    Session["apellido2"] = cliDS.Tables[0].Rows[0].ItemArray[3].ToString();
                    Session["direccion"] = cliDS.Tables[0].Rows[0].ItemArray[4].ToString();
                    Session["pais"] = cliDS.Tables[0].Rows[0].ItemArray[5].ToString();
                    Session["provincia"] = cliDS.Tables[0].Rows[0].ItemArray[6].ToString();
                    Session["telefono"] = cliDS.Tables[0].Rows[0].ItemArray[7].ToString();
                    Session["email"] = cliDS.Tables[0].Rows[0].ItemArray[8].ToString();
                    Session["contraseña"] = cliDS.Tables[0].Rows[0].ItemArray[9].ToString();
                }
                Response.Redirect("~/hotel.aspx"); // Recarga la pagina con la Session creada.
            }
            else
            {
                Response.Redirect("~/hotel.aspx");
            }
        }
        else
        {
            return;
        }
    }
    protected void btCerrarSession_Click(object sender, EventArgs e)
    {
        // Borra variables de session, la cierra y redirige.
        Session.Clear();
        Session.Abandon();
        Response.Redirect("~/hotel.aspx");
    }
}
