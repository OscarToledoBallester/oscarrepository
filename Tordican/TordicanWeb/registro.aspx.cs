﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Data;
using ENLogicaDeNegocio;


public partial class Registro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuRegistro');</script>");
    }
    protected void BtCompletar_Click(object sender, EventArgs e)
    {
        if (!comprobarComillas(TBDni.Text.Trim(), TBNombre.Text.Trim(), TBPrimerApellido.Text.Trim(), TBSegundoApellido.Text.Trim(), TBDireccion.Text.Trim(), TBPais.Text.Trim(), TBProvincia.Text.Trim(), TBTelefono.Text.Trim(), TBEmail.Text.Trim(), tbContrasena.Text.Trim()))
        {
            ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
            bool enviar = false;
            if (ver.dni(TBDni.Text) == true)
            {
                //guardar en las variables
                Persistencia.CADCliente cad = new Persistencia.CADCliente();
                DataSet d;
                d = cad.obtenerClientePorDniCAD(TBDni.Text.Trim());
                if (d.Tables[0].Rows.Count > 0)//dni ya registrado
                {
                    d = cad.obtenerClientePorEmailCAD(TBEmail.Text.Trim());
                    if (d.Tables[0].Rows.Count > 0)//email tb registrado ->usuario ya registrado)
                    {
                        //error no se puede registrar
                        Label3.Text = "* Usuario ya registrado";
                        Label3.Visible = true;
                    }
                    else
                    {
                        //solo actualizamos los datos
                        cad.actualizarClienteCAD(TBDni.Text.Trim(), TBNombre.Text.Trim(), TBPrimerApellido.Text.Trim(), TBSegundoApellido.Text.Trim(), TBDireccion.Text.Trim(), TBPais.Text.Trim(), TBProvincia.Text.Trim(), TBTelefono.Text.Trim(), TBEmail.Text.Trim(), tbContrasena.Text);//Falta meter la contraseña
                        enviar = true;

                    }
                }
                else
                {
                    d = cad.obtenerClientePorEmailCAD(TBEmail.Text.Trim());
                    if (d.Tables[0].Rows.Count > 0)//email ya registrado
                    {
                        //error no se puede registrar
                        Label3.Text = "* E-mail ya registrado";
                        Label3.Visible = true;
                    }
                    else
                    {
                        //cliente nuevo completamente                   
                        cad.nuevoClienteCAD(TBDni.Text.Trim(), TBNombre.Text.Trim(), TBPrimerApellido.Text.Trim(), TBSegundoApellido.Text.Trim(), TBDireccion.Text.Trim(), TBPais.Text.Trim(), TBProvincia.Text.Trim(), TBTelefono.Text.Trim(), TBEmail.Text.Trim(), tbContrasena.Text);//Falta meter la contraseña
                        enviar = true;
                    }
                }


            }
            else
            {
                //  lbdniincorrect.Visible = true;

            }
            if (enviar == true)
            {
                try
                {
                    SmtpClient smtpClient = new SmtpClient();
                    MailMessage message = new MailMessage();
                    MailAddress fromAddress = new MailAddress("tordican@gmail.com", "Dpto de Clientes de Tordicán");
                    MailAddress toAddress = new MailAddress(TBEmail.Text.Trim(), TBNombre.Text.Trim() + " " + TBPrimerApellido.Text.Trim());
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = "Confirmación de Registro";
                    string presentacion = "Estimado cliente estamos en disposición de confirmar que su registro a sido completado con éxito.\n";
                    string usuariocontra = "Estos son los datos con los que podrá acceder a su cuenta:\n\n" + "Usuario: " + TBEmail.Text + "\n" + "Contraseña: " + tbContrasena.Text + "\n\n";
                    string despedida = "Gracias por confiar en Tordican para su tiempo de ocio. \nDpto de Clientes de Tordican.\n";
                    message.Body = presentacion + usuariocontra + despedida;
                    smtpClient.Host = "smtp.gmail.com";
                    smtpClient.Credentials = new System.Net.NetworkCredential("tordican@gmail.com", "aprobar2011");
                    smtpClient.EnableSsl = true;
                    smtpClient.Port = 587;
                    smtpClient.Send(message);
                    Label3.Visible = true;
                    Response.Redirect("registrocompletado.aspx");
                }
                catch (ModelException ex)
                {
                    Label3.Text = ex.Message + "No se pudo enviar el mensaje";
                    Label3.Visible = true;
                }
            }
        }
        else
        {
            Label3.Text = "* Recuerde no usar comillas";
            Label3.Visible = true;
        }
    }
    protected void tbProvincia0_TextChanged(object sender, EventArgs e)
    {

    }
    protected void CustomValidatorDni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (ver.dni(args.Value))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
    protected void CustomValidatorCadena_ServerValidate(object source, ServerValidateEventArgs args)
    {
        

    }
    protected void CustomValidatorEmail_ServerValidate(object source, ServerValidateEventArgs args)
    {
        

    }
    protected void CustomValidatorTelefono_ServerValidate(object source, ServerValidateEventArgs args)
    {
        

    }
    protected bool comprobarComillas(string dni, string nom, string ape1, string ape2, string dir, string pais, string prov, string tel, string email, string contra)
    {
        bool error = false;
        ENVerificacion verifico = new ENVerificacion();

        if (!verifico.noHack(dni) || !verifico.noHack(nom) || !verifico.noHack(ape1) || !verifico.noHack(ape2) || !verifico.noHack(dir) || !verifico.noHack(pais) || !verifico.noHack(prov) || !verifico.noHack(tel) || !verifico.noHack(email) || !verifico.noHack(contra))
            error = true;

        return error;
    }
}