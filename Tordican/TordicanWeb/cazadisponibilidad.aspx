﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cazadisponibilidad.aspx.cs" Inherits="cazaDisponibilidad" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register assembly="System.Web.Ajax" namespace="System.Web.UI" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="ImagePrincipal" runat="server" ImageUrl="~/images/cazaOK.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
     <h1>Situación del Coto</h1>
     <h1><asp:Label ID="LabelFecha" runat="server" CssClass="h1" Text="Fecha: "></asp:Label>
         <asp:Label ID="LabelFecha2" runat="server" CssClass="h1" Text="Label"></asp:Label>
     </h1>

    <table style="width: 100%;">
        <tr>
            <td align="justify" style="width: 25%">
                <asp:Label ID="Label2" runat="server" CssClass="textos"  Font-Bold="True" 
                    style=" margin-left:10px; margin-right:10px; width:75%; "  
                    Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" 
                    Font-Underline="False" Text="1ª Área" BorderStyle="Solid" Width="100px" 
                    Height="100%"></asp:Label>
            </td>
            <td style="width: 25%">
                &nbsp;</td>
            <td align="center" width="50%" style="width: 25%">
                <asp:Label ID="Label1" runat="server" CssClass="textos"  Font-Bold="True"
                    style=" margin-left:10px; margin-right:10px; width:75%; "  
                    Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" 
                    Font-Underline="False" Text="2ª Área" BorderStyle="Solid" Width="100px" 
                    Height="100%"></asp:Label>
            </td>
            <td style="width: 25%">
            
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 25%">
                <asp:RadioButtonList ID="RBLArea1" runat="server" BorderStyle="None" 
                    CssClass="textos" 
                    style=" margin-left:10px; margin-right:10px; width:100%; " 
                    RepeatDirection="Horizontal" CellPadding="1" CellSpacing="3" Enabled="False" 
                    AutoPostBack="False" Width="150px">
                    <asp:ListItem>Hábil</asp:ListItem>
                    <asp:ListItem>Repoblando</asp:ListItem>
                </asp:RadioButtonList>            </td>
            <td>
            
            </td>
            <td align="center">
                <asp:RadioButtonList ID="RBLArea2" runat="server" BorderStyle="None" 
                    CssClass="textos" 
                    style=" margin-left:10px; margin-right:10px; width:100%; " 
                    RepeatDirection="Horizontal" CellPadding="1" CellSpacing="3" Enabled="False" 
                    AutoPostBack="False" Width="75%">
                    <asp:ListItem>Hábil</asp:ListItem>
                    <asp:ListItem>Repoblando</asp:ListItem>
                </asp:RadioButtonList>            </td>
            <td>
            
            </td>
        </tr>
        <tr>
            <td align="justify" style="width: 25%">
                <asp:Label ID="Label3" runat="server" CssClass="textos" style="margin-left:10px"  Text="Tipo de caza:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBTipoCazaArea1" runat="server" height="20px" 
                    style="margin-right: 10px" width="100px" Enabled="False" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
            <td align="justify">
                <asp:Label ID="Label5" runat="server" CssClass="textos" style="margin-left:10px"  Text="Tipo de caza:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBTipoCazaArea2" runat="server" height="20px" 
                    style="margin-right: 10px" width="100px" Enabled="False" ReadOnly="True" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="justify">
                <asp:Label ID="Label4" runat="server" CssClass="textos" style="margin-left:10px" Text="Número Escopetas:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBNumEscopetasArea1" runat="server" height="20px" 
                    ReadOnly="True" style="margin-right: 10px" width="100px" Enabled="False" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
            <td align="justify">
                <asp:Label ID="Label6" runat="server" CssClass="textos" style="margin-left:10px" Text="Número Escopetas:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBNumEscopetasArea2" runat="server" height="20px" 
                    ReadOnly="True" style="margin-right: 10px" width="100px" Enabled="False" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
        </tr>

    </table>
    <table style="width: 100%;">
        <tr>
            <td align="center" style="width: 25%">
                <asp:Label ID="Label7" runat="server" CssClass="textos"  Font-Bold="True" 
                    style=" margin-left:10px; margin-right:10px; width:75%; "  
                    Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" 
                    Font-Underline="False" Text="3ª Área" BorderStyle="Solid" Width="100px" 
                    Height="100%"></asp:Label>
            </td>
            <td style="width: 25%">
            </td>
            <td align="justify" width="50%" style="width: 25%">
                <asp:Label ID="Label8" runat="server" CssClass="textos"  Font-Bold="True" 
                    style=" margin-left:10px; margin-right:10px; width:75%; "  
                    Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" 
                    Font-Underline="False" Text="4ª Área" BorderStyle="Solid" Width="100px" 
                    Height="100%"></asp:Label>
            </td>
            <td style="width: 25%">
            
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 25%">
                <asp:RadioButtonList ID="RBLArea3" runat="server" BorderStyle="None" 
                    CssClass="textos" 
                    style=" margin-left:10px; margin-right:10px; width:100%; " 
                    RepeatDirection="Horizontal" CellPadding="1" CellSpacing="3" Enabled="False" 
                    AutoPostBack="False" Width="150px">
                    <asp:ListItem>Hábil</asp:ListItem>
                    <asp:ListItem>Repoblando</asp:ListItem>
                </asp:RadioButtonList>            </td>
            <td>
            
            </td>
            <td align="center">
                <asp:RadioButtonList ID="RBLArea4" runat="server" BorderStyle="None" 
                    CssClass="textos" 
                    style=" margin-left:10px; margin-right:10px; width:100%; " 
                    RepeatDirection="Horizontal" CellPadding="1" CellSpacing="3" Enabled="False" 
                    AutoPostBack="False" Width="75%">
                    <asp:ListItem>Hábil</asp:ListItem>
                    <asp:ListItem>Repoblando</asp:ListItem>
                </asp:RadioButtonList>            </td>
            <td>
            
            </td>
        </tr>
        <tr>
            <td align="justify" style="width: 25%">
                <asp:Label ID="Label9" runat="server" CssClass="textos" style="margin-left:10px"  Text="Tipo de caza:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBTipoCazaArea3" runat="server" height="20px" 
                    style="margin-right: 10px" width="100px" Enabled="False" ReadOnly="True" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
            <td align="justify" style="25%">
                <asp:Label ID="Label10" runat="server" CssClass="textos" style="margin-left:10px"  Text="Tipo de caza:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBTipoCazaArea4" runat="server" height="20px" 
                    style="margin-right: 10px" width="100px" Enabled="False" ReadOnly="True" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="justify" style="25%">
                <asp:Label ID="Label11" runat="server" CssClass="textos" style="margin-left:10px" Text="Número Escopetas:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBNumEscopetasArea3" runat="server" height="20px" 
                    ReadOnly="True" style="margin-right: 10px" width="100px" Enabled="False" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
            <td align="justify">
                <asp:Label ID="Label12" runat="server" CssClass="textos" style="margin-left:10px" Text="Número Escopetas:"></asp:Label>
            </td>
            <td align="center">
                <asp:TextBox ID="TBNumEscopetasArea4" runat="server" height="20px" 
                    ReadOnly="True" style="margin-right: 10px" width="100px" Enabled="False" 
                    CssClass="tbfechacontratacion"></asp:TextBox>
            </td>
        </tr>

    </table>
     <asp:Panel ID="Panel2" runat="server">
         <table style="width:100%;">
             <tr>
                 <td align="center" width="25%">
                     <asp:Label ID="Label13" runat="server" CssClass="textos" Font-Underline="False" 
                         Text="Piezas Disponibles" BorderStyle="Solid" Font-Bold="True" 
                         Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Height="100%" 
                         style=" margin-left:10px; margin-right:10px; width:75%; " Width="100px"></asp:Label>
                 </td>
                 <td align="center" width="25%">
                     &nbsp;</td>
                 <td align="center" width="25%">
                     <asp:AjaxScriptManager ID="AjaxScriptManager1" runat="server">
                     </asp:AjaxScriptManager>
                 </td>
                 <td align="center" width="25%">
                     &nbsp;</td>

             </tr>
         </table>
     </asp:Panel>
     <asp:Panel ID="Panel3" runat="server">
         <table style="width:100%;">
             <tr>
                 <td align="center">
                     <asp:GridView ID="GridViewPiezasDisponibles" runat="server" AllowPaging="True" 
                         AllowSorting="True" AutoGenerateColumns="False" 
                         CellPadding="4" DataSourceID="EnlacePiezasDisponibles" 
                         ForeColor="#333333" GridLines="None" Width="100%" HorizontalAlign="Center">
                         <AlternatingRowStyle BackColor="White" />
                         <Columns>
                             <asp:BoundField DataField="piezasTotalesTordo" HeaderText="Tordo" 
                                 SortExpression="piezasTotalesTordo"  />
                             <asp:BoundField DataField="piezasTotalesPalomaTorcaz" HeaderText="Paloma Torcaz" 
                                 SortExpression="piezasTotalesPalomaTorcaz"  />
                             <asp:BoundField DataField="piezasTotalesPalomaBravia" HeaderText="Paloma Bravía" 
                                 SortExpression="piezasTotalesPalomaBravia" />
                             <asp:BoundField DataField="piezasTotalesPerdiz" HeaderText="Perdiz Roja" 
                                 SortExpression="piezasTotalesPerdiz" />
                             <asp:BoundField DataField="piezasTotalesConeLie" HeaderText="Conejo y Liebre" 
                                 SortExpression="piezasTotalesConeLie" />
                             <asp:BoundField DataField="piezasTotalesFaisan" HeaderText="Faisán" 
                                 SortExpression="piezasTotalesFaisan" />
                             <asp:BoundField DataField="piezasTotalesCodorniz" HeaderText="Codorniz" 
                                 SortExpression="piezasTotalesCodorniz" />
                             <asp:BoundField DataField="piezasTotalesBecada" HeaderText="Becada" 
                                 SortExpression="piezasTotalesBecada" />
                         </Columns>
                         <EditRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                         <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                         <HeaderStyle BackColor="#516E01" Font-Bold="True" ForeColor="White" 
                             CssClass="textos" HorizontalAlign="Center" VerticalAlign="Middle" />
                         <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                         <RowStyle BackColor="#FFFBD6" ForeColor="#333333" HorizontalAlign="Center" 
                             VerticalAlign="Middle" />
                         <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" 
                             HorizontalAlign="Center" VerticalAlign="Middle" />
                         <SortedAscendingCellStyle BackColor="#FDF5AC" />
                         <SortedAscendingHeaderStyle BackColor="#4D0000" />
                         <SortedDescendingCellStyle BackColor="#FCF6C0" />
                         <SortedDescendingHeaderStyle BackColor="#820000" />
                     </asp:GridView>
                     <asp:SqlDataSource ID="EnlacePiezasDisponibles" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:TordicanBDConnectionString %>" 
                         SelectCommand="SELECT [piezasTotalesTordo], [piezasTotalesPalomaTorcaz], [piezasTotalesPalomaBravia], [piezasTotalesPerdiz], [piezasTotalesConeLie], [piezasTotalesFaisan], [piezasTotalesCodorniz], [piezasTotalesBecada] FROM [Coto]">
                     </asp:SqlDataSource>
                 </td>
             </tr>
             <tr>
                <td align="right">
                
                    <asp:ImageButton ID="ImageButtonVolver" runat="server" 
                        ImageUrl="~/images/índiceimagencontinuar.jpg" 
                        onclick="ImageButtonVolver_Click" />
                
                </td>
             </tr>
         </table>
     </asp:Panel>

    



















</asp:Content>

