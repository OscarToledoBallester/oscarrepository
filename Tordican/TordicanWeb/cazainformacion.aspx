﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cazainformacion.aspx.cs" Inherits="caza" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="ImagePrincipal" runat="server" ImageUrl="~/images/cazaOK.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td>
                <h1>Información Caza</h1>
            </td>
            <td>
                <h1 align="right">Características Tordican S.L.        
                    <asp:HyperLink ID="HyperLink1" NavigateUrl="cazainformacion.aspx?archivo=CaracteristicasTordican.pdf" runat="server">Descargar PDF</asp:HyperLink>                
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/PDF.png" />
                </h1>

            </td>
        </tr>
    </table>
    
    <div align="center" class="textos">
     Nuestro coto, ubicado en un lugar privilegiado para el desarrollo de este deporte, cuenta con una extensión total de 1000 hectáreas.
     Dividido en cuatro áreas, cada una de 250 hectáreas, y a excepción de una, que periódicamente ira cambiando, destinada a la repoblación, 
     podrá disfrutar de los cinco tipos de caza diferentes que se pueden realizar: en mano, ojeo, al asalto, al reclamo y al pase.
 
        <br />
 
    </div>
    <div align="center">
        <asp:Image ID="ImageCazaMano" runat="server" 
            ImageUrl="~/images/cazaenmanoOK.jpg" />
        <asp:Image ID="ImageCazaOjeo" runat="server" 
            ImageUrl="~/images/cazaojeoOK.jpg" />
        <asp:Image ID="ImageCazaAsalto" runat="server" 
            ImageUrl="~/images/cazaalsaltoOK.jpg" />
    </div>
    <div align="center">
        <asp:Image ID="ImageCazaReclamo" runat="server" 
            ImageUrl="~/images/cazaalreclamoOK.jpg" />
        <asp:Image ID="ImageCazaPase" runat="server" ImageAlign="NotSet" ImageUrl="~/images/cazaalpaseOK.jpg" />
    
    </div>
    <div align="center" class="textos">

        Disfrutamos de una gran parque de variedades disponibles para el desarrollo de la caza, siendo repobladas periódicamente.
        Dentro de éstas disponemos de: tordo, paloma torcaz, paloma bravía, perdiz roja, conejo, liebre, faisán, codorniz común y becada.
        Tambien ponemos a su disponibilidad la posibilidad en la contratación de plazas de parking destinadas a remolques como de perreras para sus canes.

        <br />

    </div>
    <div align="center">
        <asp:Image ID="ImageTordo" runat="server" ImageUrl="~/images/tordoOK.jpg" />
        <asp:Image ID="ImagePalomaTorcaz" runat="server" ImageUrl="~/images/Paloma_TorcazOK.jpg" />
        <asp:Image ID="ImagePalomaBravia" runat="server" ImageUrl="~/images/paloma_braviaOK.jpg" />
        <asp:Image ID="ImagePerdiz" runat="server" ImageUrl="~/images/perdiz_rojaOK.jpg" />
        <asp:Image ID="ImageConeLie" runat="server" ImageUrl="~/images/conejo_liebreOK.jpg" />
        <asp:Image ID="ImageFaisan" runat="server" ImageUrl="~/images/FaisanOK.jpg" />
        <asp:Image ID="ImageCodorniz" runat="server" ImageUrl="~/images/codornizOK.jpg" />
        <asp:Image ID="ImageBecada" runat="server" ImageUrl="~/images/becadaOK.jpg" />
    </div>
    <div>
        
        <table style="width:100%;">
            <tr>
                <td width="100%" align="right">
                    <asp:ImageButton ID="ImageButtonReserva1" runat="server" 
                        ImageUrl="~/images/reserve-ahora.png" onclick="ImageButtonReserva_Click" 
                        TabIndex="18" />
                    
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>

