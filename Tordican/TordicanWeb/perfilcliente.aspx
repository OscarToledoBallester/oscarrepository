﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="perfilcliente.aspx.cs" Inherits="PerfilCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/imagenperfil.png" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>
        Perfil Usuario</h1>
    <hr />
    <p style="font-size: 16px; color: #749F01">
        Datos Personales</p>
    <p>
    <br />
        <table style="width:100%;" __designer:mapid="de">
            <tr __designer:mapid="df">
                <td width="15%" __designer:mapid="e0">
                    <asp:Label ID="LabelDni" runat="server" CssClass="textos" Text="Dni*:"></asp:Label>
                </td>
                <td width="35%" __designer:mapid="e2">
                    <asp:TextBox ID="TBDni" runat="server" Width="50%" TabIndex="2" Enabled="False"></asp:TextBox>
                </td>
                <td width="15%" __designer:mapid="e4">
                    <asp:Label ID="LabelNombre" runat="server" CssClass="textos" Text="Nombre*:"></asp:Label>
                </td>
                <td style="width: 39%" __designer:mapid="e6">
                    <asp:TextBox ID="TBNombre" runat="server" Width="100%" TabIndex="3"></asp:TextBox>
                </td>
            </tr>
            <tr __designer:mapid="e8">
                <td style="height: 20px" __designer:mapid="e9">
                </td>
                <td style="height: 20px" __designer:mapid="ea">
                    <asp:RequiredFieldValidator ID="RFVDni" runat="server" 
                        ControlToValidate="TBDni" Display="Dynamic" ErrorMessage="* Dni obligatorio" 
                        SetFocusOnError="True" ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td style="height: 20px" __designer:mapid="ed">
                </td>
                <td style="width: 39%; height: 20px;" __designer:mapid="ee">
                    <asp:RegularExpressionValidator ID="REVNombre" runat="server" 
                        ControlToValidate="TBNombre" Display="Dynamic" 
                        ErrorMessage="* Nombre incorrecto: caracteres no numéricos" SetFocusOnError="True" 
                        ValidationExpression="\D+" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <br __designer:mapid="f0" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNombre" runat="server" 
                        ControlToValidate="TBNombre" Display="Dynamic" 
                        ErrorMessage=" * Nombre obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr __designer:mapid="f2">
                <td width="15%" style="height: 23px" __designer:mapid="f3">
                    <asp:Label ID="LabelPrimerApellido" runat="server" CssClass="textos" 
                        Text="1º Apellido*:"></asp:Label>
                </td>
                <td width="35%" style="height: 23px" __designer:mapid="f5">
                    <asp:TextBox ID="TBPrimerApellido" runat="server" Width="94%" TabIndex="4"></asp:TextBox>
                </td>
                <td width="15%" style="height: 23px" __designer:mapid="f7">
                    <asp:Label ID="LabelSegundoApellido" runat="server" CssClass="textos" 
                        Text="2º Apellido*:"></asp:Label>
                </td>
                <td style="width: 39%; height: 23px;" __designer:mapid="f9">
                    <asp:TextBox ID="TBSegundoApellido" runat="server" Width="100%" TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr __designer:mapid="fb">
                <td style="height: 22px" __designer:mapid="fc">
                </td>
                <td style="height: 22px" __designer:mapid="fd">
                    <asp:RegularExpressionValidator ID="REVPrimerApellido" runat="server" 
                        ControlToValidate="TBPrimerApellido" Display="Dynamic" 
                        ErrorMessage="* Primer apellido incorrecto: caracteres no numéricos" SetFocusOnError="True" 
                        ValidationExpression="\D+" ValidationGroup="User" Visible="False"></asp:RegularExpressionValidator>
                    <br __designer:mapid="ff" />
                    <asp:RequiredFieldValidator ID="RFVPrimerApellido" runat="server" 
                        ControlToValidate="TBPrimerApellido" Display="Dynamic" 
                        ErrorMessage="* Primer apellido obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User" Visible="False"></asp:RequiredFieldValidator>
                </td>
                <td style="height: 22px" __designer:mapid="101">
                </td>
                <td style="width: 39%; height: 22px;" __designer:mapid="102">
                    <asp:RegularExpressionValidator ID="REVSegundoApellido" runat="server" 
                        ControlToValidate="TBSegundoApellido" Display="Dynamic" 
                        ErrorMessage="* Segundo apellido incorrecto: caracteres no numéricos" ValidationExpression="\D+" 
                        ValidationGroup="User"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RFVSegundoApellido" runat="server" 
                        ControlToValidate="TBSegundoApellido" Display="Dynamic" 
                        ErrorMessage="* Segundo apellido obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr __designer:mapid="105">
                <td width="15%" __designer:mapid="106">
                    <asp:Label ID="LabelDireccion" runat="server" CssClass="textos" 
                        Text="Dirección:"></asp:Label>
                </td>
                <td width="35%" __designer:mapid="108">
                    <asp:TextBox ID="TBDireccion" runat="server" Width="93%" TabIndex="6"></asp:TextBox>
                </td>
                <td width="15%" __designer:mapid="10a">
                    <asp:Label ID="LabelPais" runat="server" CssClass="textos" Text="Pais:"></asp:Label>
                </td>
                <td style="width: 39%" __designer:mapid="10c">
                    <asp:TextBox ID="TBPais" runat="server" Width="100%" TabIndex="7"></asp:TextBox>
                </td>
            </tr>
            <tr __designer:mapid="10e">
                <td style="height: 22px" __designer:mapid="10f">
                </td>
                <td style="height: 22px" __designer:mapid="110">
                </td>
                <td style="height: 22px" __designer:mapid="111">
                </td>
                <td style="width: 39%; height: 22px;" __designer:mapid="112">
                </td>
            </tr>
            <tr __designer:mapid="113">
                <td width="15%" style="height: 22px" __designer:mapid="114">
                    <asp:Label ID="LabelProvincia" runat="server" CssClass="textos" 
                        Text="Provincia:"></asp:Label>
                </td>
                <td width="35%" style="height: 22px" __designer:mapid="116">
                    <asp:TextBox ID="TBProvincia" runat="server" Width="93%" TabIndex="8"></asp:TextBox>
                </td>
                <td width="15%" style="height: 22px" __designer:mapid="118">
                    <asp:Label ID="LabelEmail" runat="server" CssClass="textos" Text="E-mail*:"></asp:Label>
                </td>
                <td style="width: 39%; height: 22px;" __designer:mapid="11a">
                    <asp:TextBox ID="TBEmail" runat="server" Width="100%" TabIndex="9"></asp:TextBox>
                </td>
            </tr>
            <tr __designer:mapid="11c">
                <td style="height: 22px" __designer:mapid="11d">
                </td>
                <td style="height: 22px" __designer:mapid="11e">
                </td>
                <td style="height: 22px" __designer:mapid="11f">
                </td>
                <td style="width: 39%; height: 22px;" __designer:mapid="120">
                    <asp:RegularExpressionValidator ID="REVEmail" runat="server" 
                        ControlToValidate="TBEmail" Display="Dynamic" 
                        ErrorMessage="* E-mail incorrecto" SetFocusOnError="True" 
                        ValidationExpression="\S+@\S+\.\S+" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <br __designer:mapid="122" />
                    <asp:RequiredFieldValidator ID="RFVSegundoApellido0" runat="server" 
                        ControlToValidate="TBEmail" Display="Dynamic" 
                        ErrorMessage="* E-mail obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr __designer:mapid="124">
                <td width="15%" __designer:mapid="125">
                    <asp:Label ID="LabelTelefono" runat="server" CssClass="textos" 
                        Text="Teléfono*:"></asp:Label>
                </td>
                <td width="35%" __designer:mapid="127">
                    <asp:TextBox ID="TBTelefono" runat="server" Width="50%" TabIndex="10"></asp:TextBox>
                </td>
                <td width="15%" __designer:mapid="129">
                    &nbsp;</td>
                <td style="width: 39%" __designer:mapid="12b">
                    &nbsp;</td>
            </tr>
            <tr __designer:mapid="12d">
                <td style="height: 22px" __designer:mapid="12e">
                </td>
                <td style="height: 22px" __designer:mapid="12f">
                    <asp:RegularExpressionValidator ID="REVTelefono" runat="server" 
                        ControlToValidate="TBTelefono" Display="Dynamic" 
                        ErrorMessage="* Telefono incorrecto: número de 9 dígitos" SetFocusOnError="True" 
                        ValidationExpression="\d{9}" ValidationGroup="User"></asp:RegularExpressionValidator>
                    <br __designer:mapid="131" />
                    <asp:RequiredFieldValidator ID="RFVTelefono" runat="server" 
                        ControlToValidate="TBTelefono" Display="Dynamic" 
                        ErrorMessage="* Telefono obligatorio" SetFocusOnError="True" 
                        ValidationGroup="User"></asp:RequiredFieldValidator>
                </td>
                <td style="height: 22px" __designer:mapid="133">
                </td>
                <td style="width: 39%; height: 22px;" __designer:mapid="134">
                    <br __designer:mapid="136" />
                </td>
            </tr>
            <tr __designer:mapid="138">
                <td __designer:mapid="139">
                    &nbsp;</td>
                <td __designer:mapid="13a">
                    &nbsp;</td>
                <td __designer:mapid="13b">
                    &nbsp;</td>
                <td style="width: 39%" __designer:mapid="13c">
                    &nbsp;</td>
            </tr>
            <tr __designer:mapid="138">
                <td __designer:mapid="139">
                    &nbsp;</td>
                <td __designer:mapid="13a">
                    <asp:HyperLink ID="HyperLink8" runat="server" 
                        NavigateUrl="~/cambiocontrasena.aspx">Cambiar contraseña</asp:HyperLink>
                </td>
                <td __designer:mapid="13b">
                    &nbsp;</td>
                <td style="width: 39%" __designer:mapid="13c">
                    <asp:Button ID="btModificar" runat="server" Text="Modificar" 
                        ValidationGroup="User" Width="141px" onclick="btModificar_Click1" />
                    <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="Label" 
                        Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;</p>
    <hr />
    <p style="font-size: 16px; color: #749F01;">
        Reservas</p>
    <p style="font-size: 16px; color: #749F01;">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="numReserva" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="fechaReserva" HeaderText="Fecha Reserva" 
                    SortExpression="fechaReserva" />
                <asp:BoundField DataField="numReserva" HeaderText="Número de reserva" 
                    ReadOnly="True" SortExpression="numReserva" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TordicanBDConnectionString %>" 
            SelectCommand="SELECT [fechaReserva], [numReserva] FROM [Reserva] WHERE ([aCliente] = @aCliente)">
            <SelectParameters>
                <asp:SessionParameter Name="aCliente" SessionField="dni" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
</asp:Content>

