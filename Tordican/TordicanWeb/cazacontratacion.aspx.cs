﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;


public partial class cazaContratacion : System.Web.UI.Page
{
    public enum tipos {Mano = 0, Ojeo = 1, Asalto = 2, Reclamo = 3, Pase = 4 };
    public enum Comida {Barbacoa = 0, Migas = 1, Gachas = 2,Gazpacho = 3};
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["email"] == null)
            Response.Redirect("~/error.aspx");

        /*Si viene logueado introducir los datos en los textbox*/
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuCaza');</script>");
        Application.Lock();
        string aux = "";
        aux = Application["FechaCaza"].ToString();
        Application.UnLock();
        if (aux != "") {
            TBFecLlegada.Text = Application["FechaCaza"].ToString();

        }
        if (Session["email"] != null)
        { // Usuario no identificado.
            TBFechaActual.Text = System.DateTime.Today.ToString("d");
            //Relleno los textbox con los datos del usuario
            TBDni.Text = Session["dni"].ToString();
            TBNombre.Text = Session["nombre"].ToString();
            TBPrimerApellido.Text = Session["apellido1"].ToString();
            TBSegundoApellido.Text = Session["apellido2"].ToString();
            TBDireccion.Text = Session["direccion"].ToString();
            TBPais.Text = Session["pais"].ToString();
            TBProvincia.Text = Session["provincia"].ToString();
            TBEmail.Text = Session["email"].ToString();
            TBTelefono.Text = Session["telefono"].ToString();
        
        } 

        
    }
    protected void ImageButtonReserva_Click(object sender, ImageClickEventArgs e)
    {
        Persistencia.CADCoto c = new Persistencia.CADCoto();
        Persistencia.CADReserva r = new Persistencia.CADReserva();
        Persistencia.CADCliente n = new Persistencia.CADCliente();
        Persistencia.CADParking p = new Persistencia.CADParking();
        Persistencia.CADPerrera per = new Persistencia.CADPerrera();

        //Capturo el area seleccionada
        string area = DDLArea.SelectedValue;
        //Capturo el tipo de caza seleccionada
        string tipoCaza = DDLTipoCaza.SelectedValue;
        //Capturo el tipo de comida seleccionada
        string tipoComida = DDLTiposComida.SelectedValue;
        //Capturo el numero de escopetas
        string numEscopetas = RBLNumEscopetas.SelectedValue;
        //Capturo la fecha seleccionada
        DateTime fecha = Convert.ToDateTime(TBFecLlegada.Text);
        int dia = fecha.Day;
        int mes = fecha.Month;
        int anyo = fecha.Year;

        //Inserto el dia coto si no estuviera en la BD
        c.CADInsertarDiaCoto(dia, mes, anyo);

        //Introduzco en tabla contratacoto
        int numR = r.UltimaReserva();
        r.NuevaReservaCaza(fecha, TBDni.Text, numR);

        //Introduzco en tabla reserva
        //El cliente ya debe estar en la base de datos
        //por lo que no he de insertar el cliente
        //n.nuevoClienteCAD(dni, nombre, tbPrimerApellido.Text, tbSegundoApellido.Text, tbDireccion.Text, tbPais.Text, tbProvincia.Text, telefono, email, "", "");

        //Compruebo si el area ya esta reservada
        
        int numReserva = r.UltimaReserva();
        int comida = 0;
        if (CBComidaCoto.Checked)
            comida = Convert.ToInt32( System.Enum.Parse(typeof(Comida), tipoComida, true));
        int numTipoCaza = Convert.ToInt32(System.Enum.Parse(typeof(tipos), tipoCaza, true));
        c.CADInsertarContrataCoto(Convert.ToInt32(area), fecha.Day, fecha.Month, fecha.Year, numReserva - 1, numTipoCaza, comida, Convert.ToInt32(numEscopetas));
     
        //Hacer la insercion de perrera y parking remolques si estan checked
        if (CBParking.Checked) {
            p.nuevaPlazaRemolqueCAD(0, 1, numReserva - 1);
        }
        if (CBPerrera.Checked) {
            per.CADInsertaPerrera(numReserva - 1);
        }
        Response.Redirect("~/cazainformacion.aspx");

    }
    protected void CustomValidatorDni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (ver.dni(args.Value)){
            args.IsValid = true;            
        }
        else
            args.IsValid = false;
    }
    protected void CustomValidatorCadena_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (ver.cadena(args.Value))
        {
            args.IsValid = true;
        }
        else
            args.IsValid = false;

    }
    protected void CustomValidatorEmail_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (ver.email(args.Value))
        {
            args.IsValid = true;
        }
        else
            args.IsValid = false;

    }
    protected void CustomValidatorTelefono_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (ver.telefono(args.Value))
        {
            args.IsValid = true;
        }
        else
            args.IsValid = false;

    }
    protected void CustomValidatorSelecciona_ServerValidate(object source, ServerValidateEventArgs args)
    {
        ENLogicaDeNegocio.ENVerificacion ver = new ENLogicaDeNegocio.ENVerificacion();
        if (args.Value == "(Selecciona)")
        {
            args.IsValid = false;
        }
        else
            args.IsValid = true;

    }
    protected void CBComidaCoto_CheckedChanged(object sender, EventArgs e)
    {
        DDLTiposComida.Enabled = true;
    }
    protected void ButtonDisponibilidad_Click(object sender, EventArgs e)
    {
        DateTime fecha = Convert.ToDateTime(TBFecLlegada.Text);

        if (fecha >= DateTime.Today)
        {
            if (Session["email"] != null)
            {
                Application.Lock();
                Application["FechaCaza"] = TBFecLlegada.Text;
                Application.UnLock();
            }
            Response.Redirect("~/cazadisponibilidad.aspx");
        }
        else {
            LabelErrorFecha.Text = "* La fecha debe ser superior a la actual";
        
        }



        

    }
    protected void EnlaceArea_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
}