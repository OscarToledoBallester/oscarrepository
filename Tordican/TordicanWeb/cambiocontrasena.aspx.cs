﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ENLogicaDeNegocio;
using System.Net.Mail;

public partial class cambiocontrasena : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["email"] == null)
            Response.Redirect("~/error.aspx");

        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuRegistro');</script>");
    }

    protected void btActualizarContra_Click(object sender, EventArgs e)
    {
        ENVerificacion v = new ENVerificacion();
        if(v.noHack(tbcontraactual.Text) && v.noHack(tbcontraactualconfir.Text) && v.noHack( tbnuevacontra.Text ) && v.noHack( tbnuevacontraconfir.Text))
        {
            if (tbcontraactual.Text == Session["contraseña"].ToString())
            {
                //Base de Datos
                Persistencia.CADCliente cad = new Persistencia.CADCliente();
                cad.actualizarClienteCAD(Session["dni"].ToString(), Session["nombre"].ToString(), Session["apellido1"].ToString(), Session["apellido2"].ToString(), Session["direccion"].ToString(), Session["pais"].ToString(), Session["provincia"].ToString(), Session["telefono"].ToString(), Session["email"].ToString(), tbnuevacontra.Text);
                Session["contraseña"] = tbnuevacontra.Text;
                tbcontraactual.Text = "";
                tbcontraactualconfir.Text = "";
                tbnuevacontra.Text = "";
                tbnuevacontraconfir.Text = "";
                lbModificado.Visible = true;
                try
                {
                    SmtpClient smtpClient = new SmtpClient();
                    MailMessage message = new MailMessage();
                    MailAddress fromAddress = new MailAddress("tordican@gmail.com", "Dpto de Clientes de Tordicán");
                    MailAddress toAddress = new MailAddress(Session["email"].ToString(), Session["nombre"].ToString() + " " + Session["apellido2"].ToString());
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = "Confirmación de Cambio de contraseña";
                    string presentacion = "Su cambio de contraseña se ha realizado con éxito.\n";
                    string usuariocontra = "Estos son los nuevos datos con los que podrá acceder a su cuenta:\n\n" + "Usuario: " + Session["email"].ToString() + "\n" + "Contraseña: " + Session["contraseña"].ToString() + "\n\n";
                    string despedida = "Gracias por confiar en Tordican para su tiempo de ocio. \nDpto de Clientes de Tordican.\n";
                    message.Body = presentacion + usuariocontra + despedida;
                    smtpClient.Host = "smtp.gmail.com";
                    smtpClient.Credentials = new System.Net.NetworkCredential("tordican@gmail.com", "aprobar2011");
                    smtpClient.EnableSsl = true;
                    smtpClient.Port = 587;
                    smtpClient.Send(message);
                }
                catch (ModelException ex)
                {
                
                    lbModificado.Text = ex.Message + "No se pudo enviar el mensaje";
                    lbModificado.Visible = true;
                }
            
            }
            else
            {
                tbcontraactual.Text = "";
                tbcontraactualconfir.Text = "";
                tbnuevacontra.Text = "";
                tbnuevacontraconfir.Text = "";
                lbModificado.Text = "* Introduzca su contraseña actual correctamente";
                lbModificado.Visible = true;
            }
        }else
        {
            lbModificado.Text = "* Recuerde no introducir comillas";
            lbModificado.Visible = true;
        }
    }
}