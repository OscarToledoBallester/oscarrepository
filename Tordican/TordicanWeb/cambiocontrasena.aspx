﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cambiocontrasena.aspx.cs" Inherits="cambiocontrasena" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/imagenperfil.png" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>
        Cambio contraseña</h1>
    <hr />
    <p style="font-size: 16px; color: #749F01">
        Datos Actuales</p>
    <p style="font-size: 16px; color: #749F01">
        <table style="width:100%;">
            <tr>
                <td style="width: 194px">
                    <asp:Label ID="Label1" runat="server" Text="Contraseña Actual:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbcontraactual" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="tbcontraactual" 
                        ErrorMessage="* Contraseña actual obligatoria" ValidationGroup="contra"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    <asp:Label ID="Label2" runat="server" Text="Repita Contraseña Actual:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbcontraactualconfir" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    &nbsp;</td>
                <td>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="tbcontraactual" ControlToValidate="tbcontraactualconfir" 
                        ErrorMessage="* Las contraseñas actuales no coinciden" ValidationGroup="contra"></asp:CompareValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="tbcontraactualconfir" 
                        ErrorMessage="* Repetir contraseña actual obligatorio" ValidationGroup="contra"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </p>
    <hr />
    <p style="font-size: 16px; color: #749F01">
        Nuevos Datos</p>
        <table style="width:100%;">
            <tr>
                <td style="width: 194px">
                    <asp:Label ID="Label3" runat="server" Text="Nueva Contraseña:"></asp:Label>
                </td>
                <td style="width: 335px">
                    <asp:TextBox ID="tbnuevacontra" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    &nbsp;</td>
                <td style="width: 335px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="tbnuevacontra" ErrorMessage="* Nueva contraseña obligatoria" 
                        ValidationGroup="contra"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    <asp:Label ID="Label4" runat="server" Text="Repita Nueva Contraseña:"></asp:Label>
                </td>
                <td style="width: 335px">
                    <asp:TextBox ID="tbnuevacontraconfir" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    &nbsp;</td>
                <td style="width: 335px">
                    <asp:CompareValidator ID="CompareValidator5" runat="server" 
                        ControlToCompare="tbnuevacontra" ControlToValidate="tbnuevacontraconfir" 
                        ErrorMessage="* Las contraseñas nuevas no coinciden" ValidationGroup="contra"></asp:CompareValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="tbnuevacontraconfir" 
                        ErrorMessage="* Repetir nueva contraseña obligatorio" ValidationGroup="contra"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    &nbsp;</td>
                <td style="width: 335px">
                    <asp:Button ID="btActualizarContra" runat="server" 
                        onclick="btActualizarContra_Click" Text="Actualizar contraseña" 
                        ValidationGroup="contra" Width="143px" />
&nbsp;&nbsp;
                    <asp:Label ID="lbModificado" runat="server" Text="Contraseña modificada" 
                        Visible="False"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 194px">
                    &nbsp;</td>
                <td style="width: 335px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    <h1>
    </h1>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ShowMessageBox="True" ValidationGroup="contra" />
</asp:Content>

