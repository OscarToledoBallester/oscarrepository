﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class localidades : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuActividades');</script>");
    }
    protected void imgBtReserveAhora_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/activ-reserva-localidad.aspx");
    }
}