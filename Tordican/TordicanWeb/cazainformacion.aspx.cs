﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class caza : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuCaza');</script>");
        Application.Lock();
        Application["FechaCaza"] = "";
        Application.UnLock();
        if (Session["email"] == null) // Usuario no identificado.
            ImageButtonReserva1.ImageUrl = "~/images/registrese-ahora.png";

        // FORZAMOS LA DESCARGA DEL ARCHIVO (en vez de visualizarlo en el navegador)
        string filename = Request.QueryString.Get("archivo");

        if ( !String.IsNullOrEmpty(filename) )
        { 
            String dlDir = @"resources/";
            String path = Server.MapPath(dlDir + filename);

            System.IO.FileInfo toDownload =
                        new System.IO.FileInfo(path);

            if (toDownload.Exists)
            {
                Response.Clear(); 
                Response.AddHeader("Content-Disposition",
                        "attachment; filename=" + toDownload.Name);
                Response.AddHeader("Content-Length",
                        toDownload.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(dlDir + filename);
                Response.End();
            } 
        } 
    }
    protected void ImageButtonReserva_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["email"] == null) // Usuario no identificado.
            Response.Redirect("~/registro.aspx");
        else
            Response.Redirect("~/cazacontratacion.aspx");

    }
}