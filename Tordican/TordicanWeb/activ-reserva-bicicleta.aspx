﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="activ-reserva-bicicleta.aspx.cs" Inherits="activ_reserva_quad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/back-inside.jpg" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <h1>Reserva de Bicicletas</h1>
            <div align="center">
    <asp:SqlDataSource ID="SqlDSTipoBicicleta" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TordicanBDConnectionString %>" 
        
                    
                    SelectCommand="SELECT Bicicleta.idBici, Bicicleta.tipo, Bicicleta.fechaAlta, Bicicleta.enUso, TipoBicicleta.idTipo, TipoBicicleta.tipo AS TipoBiciTexto, TipoBicicleta.precio FROM Bicicleta INNER JOIN TipoBicicleta ON Bicicleta.tipo = TipoBicicleta.idTipo"></asp:SqlDataSource>
<asp:GridView ID="gvBicis" runat="server" 
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="idBici,idTipo" 
        DataSourceID="SqlDSTipoBicicleta" ForeColor="#333333" GridLines="None" Width="800px" 
                    style="text-align:center;">
    <AlternatingRowStyle BackColor="White" />
    <Columns>
        <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
        <asp:BoundField DataField="idBici" HeaderText="ID" InsertVisible="False" 
            ReadOnly="True" SortExpression="idBici" />
        <asp:BoundField DataField="fechaAlta" DataFormatString="{0:d}" 
            HeaderText="Fecha Alta" SortExpression="fechaAlta" />
        <asp:BoundField DataField="TipoBiciTexto" HeaderText="Tipo Bicicleta" 
            SortExpression="TipoBiciTexto" />
        <asp:BoundField DataField="precio" HeaderText="Precio" 
            SortExpression="precio" />
    </Columns>
    <EditRowStyle BackColor="#7C6F57" />
    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#E3EAEB" />
    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#F8FAFA" />
    <SortedAscendingHeaderStyle BackColor="#246B61" />
    <SortedDescendingCellStyle BackColor="#D4DFE1" />
    <SortedDescendingHeaderStyle BackColor="#15524A" />
</asp:GridView>
        <br />
                <asp:ImageButton ID="asignarActividad" runat="server" 
                    ImageUrl="~/images/índiceimagencontinuar.jpg" 
                    onclick="asignarActividad_Click" />  
</div>
</asp:Content>
