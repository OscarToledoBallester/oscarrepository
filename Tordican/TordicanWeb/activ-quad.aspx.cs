﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class quads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(Page.GetType(), "activarBotonMenu", "<script>activarBotonMenu('btMenuActividades');</script>");

        if (Session["email"] == null) // Usuario no identificado.
            imgBtReserveAhora.ImageUrl = "~/images/registrese-ahora.png";
    }
    protected void imgBtReserveAhora_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["email"] == null) // Usuario no identificado.
            Response.Redirect("~/registro.aspx");
        else
            Response.Redirect("~/reservas2.aspx");
    }
}