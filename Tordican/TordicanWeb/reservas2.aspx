﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="reservas2.aspx.cs" Inherits="reservas2" %>

<%@ Register Assembly="System.Web.Ajax" Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH_Fotografia" Runat="Server">
    <asp:Image ID="ImageCabecera" runat="server" ImageUrl="~/images/back-outside.jpg" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPH_Contenido" Runat="Server">
    <asp:AjaxScriptManager ID="AjaxScriptManager1" runat="server">
    </asp:AjaxScriptManager>
    <table style="width: 100%;">
        <tr>
            <td id="celdaP1" valign="middle" align="center" dir="ltr" 
                style="padding-top: 15px; padding-bottom: 15px; width: 66%; text-align: left;">
                <asp:Label ID="LBINIReserva" runat="server" Text="Reservas" CssClass="pasos"></asp:Label>
                &nbsp;&nbsp;&nbsp;&gt;&gt;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="LBPaso1" runat="server" Text="1) Fechas y Habitación"  
                    CssClass="pasos" ></asp:Label>&nbsp;&nbsp;&nbsp;&gt;&gt;&nbsp;&nbsp;&nbsp;              
                <asp:Label ID="LBPaso2" runat="server" Text="2) Extras"  CssClass="pasos"></asp:Label>
                &nbsp;&nbsp;&nbsp;&gt;&gt;&nbsp;&nbsp;&nbsp;                             
                <asp:Label ID="LBPaso3" runat="server" Text="3) Confirmación"  CssClass="pasos"></asp:Label>                          
            </td>
        </tr>
        
        <tr>
            <td>
            <!-- Panel PASO 1 -->
                <asp:Panel ID="PanelPaso1" runat="server" Width="100%" Visible="True">
                    <table style="width:100%;">
                        <tr id="celdasFecha">
                            <td width="33%" style="padding-top: 40px">
                                <asp:Label ID="Label1" runat="server" Text="Fecha Llegada "></asp:Label>
                                <asp:TextBox ID="TBFechaLlegada" runat="server" TabIndex="1"></asp:TextBox>
                                <asp:CalendarExtender ID="TBFecLlegada_CalendarExtender" runat="server" 
                                Enabled="True" PopupButtonID="IBCalendarLlegada" TargetControlID="TBFechaLlegada"
                                Format="dd/MM/yyyy" TodaysDateFormat="d MMMM, yyyy">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="IBCalendarLlegada" runat="server" 
                                ImageUrl="~/images/Calendar_scheduleHS.png" />

                            </td>
                            <td width="33%" style="padding-top: 40px">
                                <asp:Label ID="Label2" runat="server" Text="Fecha Salida "></asp:Label>
                                
                                <asp:TextBox ID="TBFechaSalida" runat="server" TabIndex="2"></asp:TextBox>

                                <asp:CalendarExtender ID="TBFecSalida_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TodaysDateFormat="d MMMM, yyyy"
                                PopupButtonID="IBCalendarSalida" TargetControlID="TBFechaSalida">
                                </asp:CalendarExtender>
                                
                                <asp:ImageButton ID="IBCalendarSalida" runat="server" 
                                ImageUrl="~/images/Calendar_scheduleHS.png" />
                            </td>
                            <td width="33%" style="padding-top: 40px">
                                <asp:Label ID="LBFechaIncorrecta" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                        
                            <td colspan="3">
                            <br />
                            <!-- Tabla de las habitaciones con la descripcion y el precio !-->
                                <table ID="tabHabitaciones" style="width: 90%;"  align="center" >
                                    <tr valign=middle align=center>
                                        <td width="300px">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text="Descripción" Font-Bold="True"></asp:Label>
                                        </td>
                                        <td width="120px">
                                            <asp:Label ID="Label4" runat="server" Text="Precio" Font-Bold="True"></asp:Label>
                                        </td>
                                        <td>
                                        
                                        </td>
                                    </tr>
                                    <tr valign=middle align=center>
                                        <td width="300px" style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="images/hab1.jpg" />
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Label ID="Label5" runat="server" Text="Individual" Font-Size="18"></asp:Label>
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Label ID="Label8" runat="server" Text="50 €" Font-Size="18"></asp:Label>
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:ImageButton ID="ImageButtonReservaInd" runat="server" 
                                                ImageUrl="~/images/índiceimagencontinuar.jpg" 
                                                onclick="ImageButtonReservaInd_Click" />
                                        </td>
                                    </tr>
                                    <tr valign=middle align=center>
                                        <td width="300" style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="images/hab2.jpg" />
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Label ID="Label6" runat="server" Text="Doble" Font-Size="18"></asp:Label>
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Label ID="Label9" runat="server" Text="80 €" Font-Size="18"></asp:Label>
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:ImageButton ID="ImageButtonReservaDob" runat="server" 
                                                ImageUrl="~/images/índiceimagencontinuar.jpg" 
                                                onclick="ImageButtonReservaDob_Click" />
                                        </td>
                                    </tr>
                                    <tr valign=middle align=center>
                                        <td width="300px" style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000" >
                                            <asp:Image ID="Image3" runat="server" ImageUrl="images/hab3.jpg" />
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Label ID="Label7" runat="server" Text="Suite" Font-Size="18"></asp:Label>
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:Label ID="Label10" runat="server" Text="200 €" Font-Size="18"></asp:Label>
                                        </td>
                                        <td style="border-top-style: dotted; border-top-width: 1px; border-top-color: #008000">
                                            <asp:ImageButton ID="ImageButtonReservaSuite" runat="server" 
                                                ImageUrl="~/images/índiceimagencontinuar.jpg" 
                                                onclick="ImageButtonReservaSuite_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
           
                </asp:Panel>
                <asp:Panel ID="PanelPaso2" runat="server" Width="100%" Visible="False">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="3">
                                <br />
                                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Label ID="LBNoches" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 30px 0px 0px 45px;">
                                <asp:Label ID="Label15" runat="server" Text="Alojamiento" Font-Size="18px" ></asp:Label>
                            </td>
                        </tr>
                        <tr style="border-top-style: dotted">
                            <td width="200px" align="center" style="border-top-style: dotted">
                                <asp:Image ID="Image4" runat="server" ImageUrl="images/buffet.jpg" 
                                    Height="140px" Width="160px" />
                            </td>
                            <td width="300px" style="border-top-style: dotted">
                                <asp:RadioButtonList ID="RBLPension" runat="server" 
                                RepeatDirection="Vertical" Width="100%" CellSpacing="25">
                                    <asp:ListItem Selected="True">Sólo Alojamiento</asp:ListItem>
                                    <asp:ListItem>Media Pensión</asp:ListItem>
                                    <asp:ListItem>Pensión Completa</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td width="200px" align="center" style="border-top-style: dotted;">
                                <asp:Image ID="Image5" runat="server" ImageUrl="images/foto_para_parking.jpg" 
                                    Height="140px" Width="160px" />
                            </td>
                            <td style="border-top-style: dotted" width="100px">
                                <asp:RadioButtonList ID="RBLParking" runat="server" RepeatDirection="Vertical" Width="100%" CellSpacing="25">
                                    <asp:ListItem Selected="True">Sin Parking</asp:ListItem>
                                    <asp:ListItem Value="Parking Turismo">Parking Turismo (+30 €)</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" 
                                style="padding: 30px 0px 0px 45px; border-bottom-style: dotted;">
                                <asp:Label ID="Label16" runat="server" Text="Actividades" Font-Size="18px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" style="width: 30%" width="25%">
                                <br />
                                <br />
                                <asp:Image ID="Image6" runat="server" ImageUrl="images/elige_actividades.jpg" />
                            </td>
                            <td align="center" width="50%" colspan="2">
                                <asp:RadioButtonList ID="RBLActividades" runat="server" 
                                RepeatDirection="Vertical" RepeatColumns="2" Width="100%" CellSpacing="15">
                                    <asp:ListItem>Quads</asp:ListItem>
                                    <asp:ListItem>Bicicleta</asp:ListItem>
                                    <asp:ListItem>Localidades de Interés</asp:ListItem>
                                    <asp:ListItem>Parques Temáticos</asp:ListItem>
                                    <asp:ListItem Selected="True">Ninguna</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" style="width: 30%" width="25%">
                                &nbsp;</td>
                            <td style="width: 17%; padding-left: 50px;" align="center">
                                <asp:ImageButton ID="ImageButtonVolverPaso1" runat="server"
                                ImageUrl="~/images/indiceimagenvolver.jpg" 
                                    onclick="ImageButtonVolverPaso1_Click" />
                                 
                            </td>
                            <td align="center" style="width: 25%" width="50%">
                                <asp:ImageButton ID="ImageButtonPasarPaso3" runat="server" 
                                    ImageUrl="~/images/índiceimagencontinuar.jpg" 
                                    onclick="ImageButtonPasarPaso3_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelPaso3" runat="server" Visible="False">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="3" width="100%">
                                <br />
                                <asp:Label ID="LBTipoHabitacion" runat="server" Text="Resumen de reserva habitación " Font-Bold="true" Font-Size="22px"></asp:Label>
                                <br />
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding-left: 50px" width="25%">
                                
                                <asp:Label ID="LBNumNoches" runat="server" Text="X Noche/s" Font-Size="15pt"></asp:Label>
                                
                                
                            </td>
                            <td align="left" valign="middle">
                                
                                <asp:Label ID="LBFechas" runat="server" Text="Desde: FechaLlegada - FechaSalida" Font-Size="15"></asp:Label>
                                
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">
                                <asp:Label ID="Label12" runat="server" Text="Régimen" style="padding-left: 50px" width="25%" Font-Size="15"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LBTipoRegimen" runat="server" Text="TipoRégimen" Font-Size="15"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="Label11" runat="server" Text="Precio Total" Font-Size="15"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">
                                <asp:Label ID="Label14" runat="server" Text="Parking" style="padding-left: 50px" width="25%" Font-Size="15"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LBTipoParking" runat="server" Text="TipoParking" Font-Size="15"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:Label ID="LBPrecioTotal" runat="server" Text="" Font-Bold="true" Font-Size="28px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle">
                                <asp:Label ID="Label13" runat="server" Text="Actividad" 
                                    style="padding-left: 50px" width="57%" Font-Size="15"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LBActividad" runat="server" Text="TipoActividad" Font-Size="16"></asp:Label>    
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" valign="middle">
                                <asp:Label ID="LBNoHayHabitacion" runat="server" Text="" ForeColor="Red"></asp:Label>    
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle">
                                <asp:ImageButton ID="ImageButtonVolverPaso2" runat="server" 
                                ImageUrl="~/images/indiceimagenvolver.jpg" 
                                    onclick="ImageButtonVolverPaso2_Click" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td align="center">
                                
                                <asp:ImageButton ID="ImageButtonConfirmarReserva" runat="server" 
                                    ImageUrl="~/images/reserve-ahora.png" 
                                    onclick="ImageButtonConfirmarReserva_Click" />
                            </td>
                        </tr>
                    </table>

                </asp:Panel>  
            </td>
        </tr>
        </table>

</asp:Content>

