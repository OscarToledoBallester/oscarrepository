﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SORTaller
{
    public partial class DetallesPedido : Form
    {
        public DetallesPedido(Pedido p)
        {
            InitializeComponent();
            cargarDatos(p);
        }

        private void DetallesPedido_Load(object sender, EventArgs e)
        {

        }
        private void cargarDatos(Pedido p)
        {
            lbRefPedProp.Text = p.getNumRef();
            lbRefPedGes.Text = p.getNumRefGestor();
            lbPreMax.Text = p.getPrecioMax().ToString();
            lbFechFin.Text = p.getFechaFin();
            lbNumPiez.Text = p.getNumPiezas().ToString();
            if (p.getAutomatico() == true)
            {
                lbPedAut.Text = "Si";
                lbEtiqCrit.Visible = true;
                string crite = "";
                switch (p.getCriterio())
                {
                    case 0: crite = "Precio";
                        break;
                    case 1: crite = "Fecha de entrega";
                        break;
                }
                lbCritSel.Text = crite;
            }
            else
            {
                lbPedAut.Text = "No";
                lbEtiqCrit.Visible = false;
                string crite = "";
                lbCritSel.Text = crite;
            }
            string estados = "Cualquiera";
            
            for (int i = 0; i < p.getNumPiezas(); i++)
            {
                               
                switch(p.getPiezaEst(i))
                {
                    case 0:
                        estados = "Cualquiera";
                        break;
                    case 1:
                        estados = "Nuevo";
                        break;
                    case 2:
                        estados = "Segunda Mano";
                        break;
                }
                dgvPiezas.Rows.Add(p.getPiezaRef(i), p.getPiezaDescr(i),estados,p.getPiezaCant(i));
            }
        }
    }
}
