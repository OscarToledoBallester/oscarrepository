﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SORTaller
{
    public class Oferta
    {
        string numRefPedido;
        string numRefOferta;
        string fechaFin;
        string fechaEntrega;
        int ofertaEstado;
        float precioTotal;

        public void setofertaofertaEstado(int i)
        {
            ofertaEstado = i;
        }
        public int getofertaEstado()
        {
            return ofertaEstado;
        }
        public void setprecioTotal(float i)
        {
            precioTotal = i;
        }
        public float getprecioTotal()
        {
            return precioTotal;
        }
        public void setfechaFin(string i)
        {
            fechaFin = i;
        }
        public string getfechaFin()
        {
            return fechaFin;
        }
        public void setfechaEntrega(string i)
        {
            fechaEntrega = i;
        }
        public string getfechaEntrega()
        {
            return fechaEntrega;
        }
        public void setNumRefPedido(string i)
        {
            numRefPedido = i;
        }
        public string getNumRefPedido()
        {
            return numRefPedido;
        }
        public void setNumRefOferta(string i)
        {
            numRefOferta = i;
        }
        public string getNumRefOferta()
        {
            return numRefOferta;
        }
        public Oferta()
        {
            numRefPedido = "";
            fechaFin = "";
            fechaEntrega = "";
            precioTotal = 0;
            ofertaEstado = 0;
        }
        public int SeleccionarOferta()
        {
            //Hacemos la peticion
            return 0;
        }
        public int RechazarOferta()
        {
            //Hacemos la peticion
            return 0;
        }
        public int cargaOferta(string datosEntrada)
        {
            int res = 0;

            if (datosEntrada.Length <= 2)
            {
                res = Convert.ToInt32(datosEntrada);
            }
            else
            {
                //numRefGestor|fechaCaducidad|FechaEntrega|Estado|PrecioTotal
                String[] datos = datosEntrada.Split('|');
                numRefOferta = datos[0];
                fechaFin = datos[1];
                fechaEntrega = datos[2];
                ofertaEstado = Convert.ToInt32(datos[3]);
                precioTotal = float.Parse(datos[4]);
            }

            return res;
        }
    }
}
