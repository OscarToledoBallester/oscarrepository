﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SORTaller
{
    class Peticion
    {
        private string nID;
        private string nClave;
        private WSGespiezas.Service1SoapClient serv = new WSGespiezas.Service1SoapClient();
        public Peticion()
        {
            nID = "";
            nClave = "";
        }
        public Peticion(string id, string clave)
        {
            nID = id;
            nClave = clave;         
        }

        public int Alta(String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion)
        {
            int r = 0;

            //Llamada a web service AltaTaller
            try
            {
                nID = serv.AltaTaller(n_nif, n_nombre, n_email, n_telefono, n_localidad, n_direccion).ToString();
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
                        
            return r;
        }
        public int Baja()
        {
            int r = 0;   
            //String n_id, String n_clave
            //Llamada a web service Baja
            try
            {
                serv.Baja(nID, nClave);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            
            return r;
        }
        public int Modificacion( String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion)
        {
            int r = 0;

            //String n_id, string n_clave
            //Llamada a web service Baja
            try
            {
                serv.Modificacion(nID, n_nif, n_nombre, n_email, n_telefono, n_localidad, n_direccion, nClave);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            return r;
        }
        public string RecogerDatos()
        {
            string r = "";

            //String n_id, string n_clave
            //Llamada a web service Baja
            try
            {
                r = serv.RecogerDatos(nID, nClave);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = "-10";
            }
            return r;
        }
                                
        public int NuevoPedido(string fecha_fin, int criterio, bool automatico, int precio_max, string[] referencias, string[] descripciones, int[] cantidades, float[] precios, int[] estados)
        {
            int r = 0;

            //string idTaller, string claveTaller,
            //Llamada a web service NuevoPedido
            SORTaller.WSGespiezas.ArrayOfString refs = new WSGespiezas.ArrayOfString();
            for (int i = 0; i < referencias.Length; i++)
            {
                refs.Add(referencias[i]);
            }
            SORTaller.WSGespiezas.ArrayOfString descr = new WSGespiezas.ArrayOfString();
            for (int i = 0; i < descripciones.Length; i++)
            {
                descr.Add(descripciones[i]);
            }
            SORTaller.WSGespiezas.ArrayOfInt cants = new WSGespiezas.ArrayOfInt();
            for (int i = 0; i < cantidades.Length; i++)
            {
                cants.Add(cantidades[i]);
            }
            SORTaller.WSGespiezas.ArrayOfFloat precs = new WSGespiezas.ArrayOfFloat();
            for (int i = 0; i < precios.Length; i++)
            {
                precs.Add(precios[i]);
            }
            SORTaller.WSGespiezas.ArrayOfInt ests = new WSGespiezas.ArrayOfInt();
            for (int i = 0; i < estados.Length; i++)
            {
                ests.Add(estados[i]);
            }
            try
            {
                //string[] referencias, string[] descripciones, int[] cantidades, float[] precios, int[] estados
                serv.NuevoPedido(nID, nClave, nID, fecha_fin, criterio, automatico, precio_max, refs, descr, cants, precs, ests);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            return r;
        }
        public string RecogerPedidos()
        {
            string r = "";

            //string idTaller, string claveTaller,
            //Llamada a web service RecogerPedidos
            try
            {
                //r = serv.RecogerPedidos(nID,nClave);
                //idPedidoLocal|idPedido|PrecioMax|Automatico|Criterio|Fecha|RefPieza#Descripcion#Cantidad#Estado@Pieza2?Pedido2
                r = "1|100|200|0|-1|2|12/04/2014|222#Pieza chula#2#1@333#Pieza chuli#2#0?2|105|350|1|2|1|12/04/2014|111#Pieza guay#2#1";
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = "-10";
            }
            return r;
        }
        public int ModificarPedido(string idPedido, string fecha_modificada, bool automatica, int criterio)
        {
            int r = 0;

            //string idTaller, string claveTaller,
            //Llamada a web service ModificarPedido
            try
            {
                serv.ModificarPedido(nID, nClave, idPedido, fecha_modificada, automatica, criterio);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            return r;
        }
        public int CancelarPedido(string idPedido)
        {
            int r = 0;

            //string idTaller, string claveTaller,
            //Llamada a web service CancelarPedido
            try
            {
                serv.CancelarPedido(nID, nClave, idPedido);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            return r;
        }
        public int SeleccionarOferta(string idPedido, string idOferta)
        {
            int r = 0;

            //string idTaller, string claveTaller,
            //Llamada a web service SeleccionarOferta
            try
            {
                //serv.SeleccionarOferta(nID, nClave,idPedido,idOferta);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            return r;
        }
        public int RechazarOferta(string idPedido, string idOferta)
        {
            int r = 0;

            //string idTaller, string claveTaller,
            //Llamada a web service RechazarOferta
            try
            {
                //serv.RechazarOferta(nID, nClave,idPedido,idOferta);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = -10;
            }
            return r;
        }
        public string RecogerOfertas(string RefPedidoG)
        {
            string r = "";

            //string idTaller, string claveTaller,
            //Llamada a web service RecogerOfertas
            try
            {
                //NumeroDeOfertas?numRefGestor|fechaCaducidad|FechaEntrega|Estado|PrecioTotal#...
                r = "2?1|12/04/2014|20/04/2014|1|200#2|16/04/2014|18/04/2014|2|150";
                //serv.RecogerOfertas(nID, nClave, RefPedidoG);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = "-10";
            }
            return r;
        }
        public string RecogerActualizacionOfertas(string RefPedidoG)
        {
            string r = "";

            //string idTaller, string claveTaller,
            //Llamada a web service RecogerActualizacionOfertas
            try
            {
                //NumeroDeOfertas?numRefGestor|fechaCaducidad|FechaEntrega|Estado|PrecioTotal#...
                r = "2?1|12/04/2014|20/04/2014|1|200#2|16/04/2014|18/04/2014|2|150";
                //serv.RecogerActualizacionOfertas(nID, nClave, RefPedidoG);
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                r = "-10";
            }
            return r;
        }
    }
}
