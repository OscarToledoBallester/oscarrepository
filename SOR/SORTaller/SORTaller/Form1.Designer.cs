﻿namespace SORTaller
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbErrorAlta = new System.Windows.Forms.Label();
            this.btModificacion = new System.Windows.Forms.Button();
            this.btBaja = new System.Windows.Forms.Button();
            this.btAlta = new System.Windows.Forms.Button();
            this.tbDireccion = new System.Windows.Forms.TextBox();
            this.tbLocalidad = new System.Windows.Forms.TextBox();
            this.tbTelefono = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.tbNIF = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tpPedidos = new System.Windows.Forms.TabPage();
            this.tcPedidos = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lbErrorEnviarPedido = new System.Windows.Forms.Label();
            this.dgvPiezas = new System.Windows.Forms.DataGridView();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripción = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btCancelar = new System.Windows.Forms.Button();
            this.btEnviarPedido = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.NUDPrecioMaximo = new System.Windows.Forms.NumericUpDown();
            this.lbCriterio = new System.Windows.Forms.Label();
            this.cbCriterio = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chbNegociadoAutomatico = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpFechaFinalizacion = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NUDCantidad = new System.Windows.Forms.NumericUpDown();
            this.lbErrorAnadirPiezas = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tbDescripcion = new System.Windows.Forms.TextBox();
            this.btAnadirPieza = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cbEstado = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbReferencia = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btAceptarOferta = new System.Windows.Forms.Button();
            this.btRechazarOferta = new System.Windows.Forms.Button();
            this.dgvOfertas = new System.Windows.Forms.DataGridView();
            this.RefOfertaGes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaCaducidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEntrega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstadoOferta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btDetalles = new System.Windows.Forms.Button();
            this.btVerOfertas = new System.Windows.Forms.Button();
            this.btRecogerPedidos = new System.Windows.Forms.Button();
            this.dgvPedidosEnCurso = new System.Windows.Forms.DataGridView();
            this.RefProp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RefGes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.auto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.criterio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cbMostrarCaracteres = new System.Windows.Forms.CheckBox();
            this.lbErrorConexion = new System.Windows.Forms.Label();
            this.btConectar = new System.Windows.Forms.Button();
            this.tbClave = new System.Windows.Forms.TextBox();
            this.tbId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tpPedidos.SuspendLayout();
            this.tcPedidos.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiezas)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPrecioMaximo)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidad)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOfertas)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidosEnCurso)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tpPedidos);
            this.tabControl1.Location = new System.Drawing.Point(5, 87);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(685, 411);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lbErrorAlta);
            this.tabPage1.Controls.Add(this.btModificacion);
            this.tabPage1.Controls.Add(this.btBaja);
            this.tabPage1.Controls.Add(this.btAlta);
            this.tabPage1.Controls.Add(this.tbDireccion);
            this.tabPage1.Controls.Add(this.tbLocalidad);
            this.tabPage1.Controls.Add(this.tbTelefono);
            this.tabPage1.Controls.Add(this.tbEmail);
            this.tabPage1.Controls.Add(this.tbNombre);
            this.tabPage1.Controls.Add(this.tbNIF);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(677, 385);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Perfil";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // lbErrorAlta
            // 
            this.lbErrorAlta.AutoSize = true;
            this.lbErrorAlta.ForeColor = System.Drawing.Color.Red;
            this.lbErrorAlta.Location = new System.Drawing.Point(136, 292);
            this.lbErrorAlta.Name = "lbErrorAlta";
            this.lbErrorAlta.Size = new System.Drawing.Size(0, 13);
            this.lbErrorAlta.TabIndex = 15;
            this.lbErrorAlta.Visible = false;
            // 
            // btModificacion
            // 
            this.btModificacion.Location = new System.Drawing.Point(263, 252);
            this.btModificacion.Name = "btModificacion";
            this.btModificacion.Size = new System.Drawing.Size(119, 23);
            this.btModificacion.TabIndex = 14;
            this.btModificacion.Text = "Modificar datos";
            this.btModificacion.UseVisualStyleBackColor = true;
            this.btModificacion.Click += new System.EventHandler(this.btModificacion_Click);
            // 
            // btBaja
            // 
            this.btBaja.Location = new System.Drawing.Point(388, 252);
            this.btBaja.Name = "btBaja";
            this.btBaja.Size = new System.Drawing.Size(119, 23);
            this.btBaja.TabIndex = 13;
            this.btBaja.Text = "Solicitar Baja";
            this.btBaja.UseVisualStyleBackColor = true;
            // 
            // btAlta
            // 
            this.btAlta.Location = new System.Drawing.Point(138, 252);
            this.btAlta.Name = "btAlta";
            this.btAlta.Size = new System.Drawing.Size(119, 23);
            this.btAlta.TabIndex = 12;
            this.btAlta.Text = "Solicitar Alta";
            this.btAlta.UseVisualStyleBackColor = true;
            this.btAlta.Click += new System.EventHandler(this.btAlta_Click);
            // 
            // tbDireccion
            // 
            this.tbDireccion.Location = new System.Drawing.Point(292, 181);
            this.tbDireccion.Name = "tbDireccion";
            this.tbDireccion.Size = new System.Drawing.Size(144, 20);
            this.tbDireccion.TabIndex = 11;
            // 
            // tbLocalidad
            // 
            this.tbLocalidad.Location = new System.Drawing.Point(292, 155);
            this.tbLocalidad.Name = "tbLocalidad";
            this.tbLocalidad.Size = new System.Drawing.Size(144, 20);
            this.tbLocalidad.TabIndex = 10;
            // 
            // tbTelefono
            // 
            this.tbTelefono.Location = new System.Drawing.Point(292, 129);
            this.tbTelefono.Name = "tbTelefono";
            this.tbTelefono.Size = new System.Drawing.Size(144, 20);
            this.tbTelefono.TabIndex = 9;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(292, 103);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(144, 20);
            this.tbEmail.TabIndex = 8;
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(292, 77);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(144, 20);
            this.tbNombre.TabIndex = 7;
            // 
            // tbNIF
            // 
            this.tbNIF.Location = new System.Drawing.Point(292, 51);
            this.tbNIF.Name = "tbNIF";
            this.tbNIF.Size = new System.Drawing.Size(144, 20);
            this.tbNIF.TabIndex = 6;
            this.tbNIF.TextChanged += new System.EventHandler(this.tbNIF_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(178, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Dirección";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(178, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Localidad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(178, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Teléfono";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Email";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(178, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "NIF";
            // 
            // tpPedidos
            // 
            this.tpPedidos.Controls.Add(this.tcPedidos);
            this.tpPedidos.Location = new System.Drawing.Point(4, 22);
            this.tpPedidos.Name = "tpPedidos";
            this.tpPedidos.Padding = new System.Windows.Forms.Padding(3);
            this.tpPedidos.Size = new System.Drawing.Size(677, 385);
            this.tpPedidos.TabIndex = 1;
            this.tpPedidos.Text = "Pedidos";
            this.tpPedidos.UseVisualStyleBackColor = true;
            // 
            // tcPedidos
            // 
            this.tcPedidos.Controls.Add(this.tabPage3);
            this.tcPedidos.Controls.Add(this.tabPage4);
            this.tcPedidos.Controls.Add(this.tabPage5);
            this.tcPedidos.Enabled = false;
            this.tcPedidos.Location = new System.Drawing.Point(7, 7);
            this.tcPedidos.Name = "tcPedidos";
            this.tcPedidos.SelectedIndex = 0;
            this.tcPedidos.Size = new System.Drawing.Size(664, 372);
            this.tcPedidos.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lbErrorEnviarPedido);
            this.tabPage3.Controls.Add(this.dgvPiezas);
            this.tabPage3.Controls.Add(this.btCancelar);
            this.tabPage3.Controls.Add(this.btEnviarPedido);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(656, 346);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Nuevo";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lbErrorEnviarPedido
            // 
            this.lbErrorEnviarPedido.AutoSize = true;
            this.lbErrorEnviarPedido.ForeColor = System.Drawing.Color.Red;
            this.lbErrorEnviarPedido.Location = new System.Drawing.Point(454, 319);
            this.lbErrorEnviarPedido.Name = "lbErrorEnviarPedido";
            this.lbErrorEnviarPedido.Size = new System.Drawing.Size(0, 13);
            this.lbErrorEnviarPedido.TabIndex = 9;
            // 
            // dgvPiezas
            // 
            this.dgvPiezas.AllowUserToAddRows = false;
            this.dgvPiezas.AllowUserToDeleteRows = false;
            this.dgvPiezas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPiezas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia,
            this.Cantidad,
            this.Estado,
            this.Descripción});
            this.dgvPiezas.Location = new System.Drawing.Point(7, 233);
            this.dgvPiezas.Name = "dgvPiezas";
            this.dgvPiezas.ReadOnly = true;
            this.dgvPiezas.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvPiezas.Size = new System.Drawing.Size(441, 107);
            this.dgvPiezas.TabIndex = 8;
            // 
            // Referencia
            // 
            this.Referencia.Frozen = true;
            this.Referencia.HeaderText = "Referencia";
            this.Referencia.Name = "Referencia";
            this.Referencia.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.Frozen = true;
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.Frozen = true;
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            // 
            // Descripción
            // 
            this.Descripción.Frozen = true;
            this.Descripción.HeaderText = "Descripción";
            this.Descripción.Name = "Descripción";
            this.Descripción.ReadOnly = true;
            // 
            // btCancelar
            // 
            this.btCancelar.Location = new System.Drawing.Point(555, 293);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(92, 23);
            this.btCancelar.TabIndex = 3;
            this.btCancelar.Text = "Cancelar ";
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // btEnviarPedido
            // 
            this.btEnviarPedido.Location = new System.Drawing.Point(457, 293);
            this.btEnviarPedido.Name = "btEnviarPedido";
            this.btEnviarPedido.Size = new System.Drawing.Size(92, 23);
            this.btEnviarPedido.TabIndex = 2;
            this.btEnviarPedido.Text = "Enviar pedido";
            this.btEnviarPedido.UseVisualStyleBackColor = true;
            this.btEnviarPedido.Click += new System.EventHandler(this.btEnviarPedido_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.NUDPrecioMaximo);
            this.groupBox2.Controls.Add(this.lbCriterio);
            this.groupBox2.Controls.Add(this.cbCriterio);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.chbNegociadoAutomatico);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.dtpFechaFinalizacion);
            this.groupBox2.Location = new System.Drawing.Point(289, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(361, 205);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos generales del pedido";
            // 
            // NUDPrecioMaximo
            // 
            this.NUDPrecioMaximo.Location = new System.Drawing.Point(139, 69);
            this.NUDPrecioMaximo.Name = "NUDPrecioMaximo";
            this.NUDPrecioMaximo.Size = new System.Drawing.Size(62, 20);
            this.NUDPrecioMaximo.TabIndex = 11;
            this.NUDPrecioMaximo.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // lbCriterio
            // 
            this.lbCriterio.AutoSize = true;
            this.lbCriterio.Location = new System.Drawing.Point(22, 132);
            this.lbCriterio.Name = "lbCriterio";
            this.lbCriterio.Size = new System.Drawing.Size(39, 13);
            this.lbCriterio.TabIndex = 8;
            this.lbCriterio.Text = "Criterio";
            this.lbCriterio.Visible = false;
            // 
            // cbCriterio
            // 
            this.cbCriterio.DisplayMember = "-";
            this.cbCriterio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCriterio.FormattingEnabled = true;
            this.cbCriterio.Items.AddRange(new object[] {
            "Precio",
            "Fecha de entrega"});
            this.cbCriterio.Location = new System.Drawing.Point(139, 129);
            this.cbCriterio.Name = "cbCriterio";
            this.cbCriterio.Size = new System.Drawing.Size(121, 21);
            this.cbCriterio.TabIndex = 7;
            this.cbCriterio.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Precio Máximo (€)\r\n";
            // 
            // chbNegociadoAutomatico
            // 
            this.chbNegociadoAutomatico.AutoSize = true;
            this.chbNegociadoAutomatico.Location = new System.Drawing.Point(25, 100);
            this.chbNegociadoAutomatico.Name = "chbNegociadoAutomatico";
            this.chbNegociadoAutomatico.Size = new System.Drawing.Size(134, 17);
            this.chbNegociadoAutomatico.TabIndex = 8;
            this.chbNegociadoAutomatico.Text = "Negociado Automático";
            this.chbNegociadoAutomatico.UseVisualStyleBackColor = true;
            this.chbNegociadoAutomatico.CheckedChanged += new System.EventHandler(this.chbNegociadoAutomatico_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Fecha finalización";
            // 
            // dtpFechaFinalizacion
            // 
            this.dtpFechaFinalizacion.Location = new System.Drawing.Point(139, 39);
            this.dtpFechaFinalizacion.Name = "dtpFechaFinalizacion";
            this.dtpFechaFinalizacion.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaFinalizacion.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NUDCantidad);
            this.groupBox1.Controls.Add(this.lbErrorAnadirPiezas);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tbDescripcion);
            this.groupBox1.Controls.Add(this.btAnadirPieza);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cbEstado);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbReferencia);
            this.groupBox1.Location = new System.Drawing.Point(7, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 205);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Piezas";
            // 
            // NUDCantidad
            // 
            this.NUDCantidad.Location = new System.Drawing.Point(119, 68);
            this.NUDCantidad.Name = "NUDCantidad";
            this.NUDCantidad.Size = new System.Drawing.Size(120, 20);
            this.NUDCantidad.TabIndex = 4;
            this.NUDCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbErrorAnadirPiezas
            // 
            this.lbErrorAnadirPiezas.AutoSize = true;
            this.lbErrorAnadirPiezas.ForeColor = System.Drawing.Color.Red;
            this.lbErrorAnadirPiezas.Location = new System.Drawing.Point(104, 167);
            this.lbErrorAnadirPiezas.Name = "lbErrorAnadirPiezas";
            this.lbErrorAnadirPiezas.Size = new System.Drawing.Size(0, 13);
            this.lbErrorAnadirPiezas.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Descripcion";
            // 
            // tbDescripcion
            // 
            this.tbDescripcion.Location = new System.Drawing.Point(118, 129);
            this.tbDescripcion.Name = "tbDescripcion";
            this.tbDescripcion.Size = new System.Drawing.Size(121, 20);
            this.tbDescripcion.TabIndex = 8;
            // 
            // btAnadirPieza
            // 
            this.btAnadirPieza.Location = new System.Drawing.Point(23, 162);
            this.btAnadirPieza.Name = "btAnadirPieza";
            this.btAnadirPieza.Size = new System.Drawing.Size(75, 23);
            this.btAnadirPieza.TabIndex = 6;
            this.btAnadirPieza.Text = "Añadir pieza";
            this.btAnadirPieza.UseVisualStyleBackColor = true;
            this.btAnadirPieza.Click += new System.EventHandler(this.btAnadirPieza_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Estado";
            // 
            // cbEstado
            // 
            this.cbEstado.DisplayMember = "Cual";
            this.cbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEstado.FormattingEnabled = true;
            this.cbEstado.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbEstado.Items.AddRange(new object[] {
            "Cualquiera",
            "Nuevo",
            "Segunda Mano"});
            this.cbEstado.Location = new System.Drawing.Point(118, 98);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(121, 21);
            this.cbEstado.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Cantidad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Referencia";
            // 
            // tbReferencia
            // 
            this.tbReferencia.Location = new System.Drawing.Point(118, 39);
            this.tbReferencia.Name = "tbReferencia";
            this.tbReferencia.Size = new System.Drawing.Size(121, 20);
            this.tbReferencia.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(656, 346);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "En curso";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btAceptarOferta);
            this.groupBox4.Controls.Add(this.btRechazarOferta);
            this.groupBox4.Controls.Add(this.dgvOfertas);
            this.groupBox4.Location = new System.Drawing.Point(7, 173);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(643, 147);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ofertas";
            // 
            // btAceptarOferta
            // 
            this.btAceptarOferta.Location = new System.Drawing.Point(407, 115);
            this.btAceptarOferta.Name = "btAceptarOferta";
            this.btAceptarOferta.Size = new System.Drawing.Size(112, 23);
            this.btAceptarOferta.TabIndex = 2;
            this.btAceptarOferta.Text = "Seleccionar Oferta";
            this.btAceptarOferta.UseVisualStyleBackColor = true;
            this.btAceptarOferta.Click += new System.EventHandler(this.btAceptarOferta_Click);
            // 
            // btRechazarOferta
            // 
            this.btRechazarOferta.Location = new System.Drawing.Point(525, 115);
            this.btRechazarOferta.Name = "btRechazarOferta";
            this.btRechazarOferta.Size = new System.Drawing.Size(112, 23);
            this.btRechazarOferta.TabIndex = 1;
            this.btRechazarOferta.Text = "Rechazar Oferta";
            this.btRechazarOferta.UseVisualStyleBackColor = true;
            this.btRechazarOferta.Click += new System.EventHandler(this.btRechazarOferta_Click);
            // 
            // dgvOfertas
            // 
            this.dgvOfertas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOfertas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RefOfertaGes,
            this.fechaCaducidad,
            this.FechaEntrega,
            this.EstadoOferta,
            this.PrecioTotal});
            this.dgvOfertas.Location = new System.Drawing.Point(7, 20);
            this.dgvOfertas.Name = "dgvOfertas";
            this.dgvOfertas.Size = new System.Drawing.Size(630, 89);
            this.dgvOfertas.TabIndex = 0;
            // 
            // RefOfertaGes
            // 
            this.RefOfertaGes.HeaderText = "Referencia Oferta (Gestor)";
            this.RefOfertaGes.Name = "RefOfertaGes";
            // 
            // fechaCaducidad
            // 
            this.fechaCaducidad.HeaderText = "Fecha Caducidad";
            this.fechaCaducidad.Name = "fechaCaducidad";
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.HeaderText = "Fecha Entrega";
            this.FechaEntrega.Name = "FechaEntrega";
            // 
            // EstadoOferta
            // 
            this.EstadoOferta.HeaderText = "Estado de la Oferta";
            this.EstadoOferta.Name = "EstadoOferta";
            // 
            // PrecioTotal
            // 
            this.PrecioTotal.HeaderText = "Precio Total";
            this.PrecioTotal.Name = "PrecioTotal";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btDetalles);
            this.groupBox3.Controls.Add(this.btVerOfertas);
            this.groupBox3.Controls.Add(this.btRecogerPedidos);
            this.groupBox3.Controls.Add(this.dgvPedidosEnCurso);
            this.groupBox3.Location = new System.Drawing.Point(7, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(643, 160);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pedidos en curso";
            // 
            // btDetalles
            // 
            this.btDetalles.Location = new System.Drawing.Point(407, 129);
            this.btDetalles.Name = "btDetalles";
            this.btDetalles.Size = new System.Drawing.Size(112, 23);
            this.btDetalles.TabIndex = 5;
            this.btDetalles.Text = "Detalles";
            this.btDetalles.UseVisualStyleBackColor = true;
            this.btDetalles.Click += new System.EventHandler(this.btDetalles_Click);
            // 
            // btVerOfertas
            // 
            this.btVerOfertas.Location = new System.Drawing.Point(525, 129);
            this.btVerOfertas.Name = "btVerOfertas";
            this.btVerOfertas.Size = new System.Drawing.Size(112, 23);
            this.btVerOfertas.TabIndex = 4;
            this.btVerOfertas.Text = "Ver Ofertas";
            this.btVerOfertas.UseVisualStyleBackColor = true;
            this.btVerOfertas.Click += new System.EventHandler(this.btVerOfertas_Click);
            // 
            // btRecogerPedidos
            // 
            this.btRecogerPedidos.Location = new System.Drawing.Point(7, 131);
            this.btRecogerPedidos.Name = "btRecogerPedidos";
            this.btRecogerPedidos.Size = new System.Drawing.Size(112, 23);
            this.btRecogerPedidos.TabIndex = 3;
            this.btRecogerPedidos.Text = "Recoger Pedidos";
            this.btRecogerPedidos.UseVisualStyleBackColor = true;
            this.btRecogerPedidos.Click += new System.EventHandler(this.btRecogerPedidos_Click);
            // 
            // dgvPedidosEnCurso
            // 
            this.dgvPedidosEnCurso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidosEnCurso.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RefProp,
            this.RefGes,
            this.PrecioMax,
            this.auto,
            this.criterio,
            this.FechaFin});
            this.dgvPedidosEnCurso.Location = new System.Drawing.Point(7, 20);
            this.dgvPedidosEnCurso.Name = "dgvPedidosEnCurso";
            this.dgvPedidosEnCurso.Size = new System.Drawing.Size(630, 103);
            this.dgvPedidosEnCurso.TabIndex = 0;
            // 
            // RefProp
            // 
            this.RefProp.HeaderText = "Referencia (Propia)";
            this.RefProp.Name = "RefProp";
            // 
            // RefGes
            // 
            this.RefGes.HeaderText = "Referencia (Gestor)";
            this.RefGes.Name = "RefGes";
            // 
            // PrecioMax
            // 
            this.PrecioMax.HeaderText = "Precio Máximo";
            this.PrecioMax.Name = "PrecioMax";
            // 
            // auto
            // 
            this.auto.HeaderText = "Automatico";
            this.auto.Name = "auto";
            // 
            // criterio
            // 
            this.criterio.HeaderText = "Criterio";
            this.criterio.Name = "criterio";
            // 
            // FechaFin
            // 
            this.FechaFin.HeaderText = "Fecha Finalización";
            this.FechaFin.Name = "FechaFin";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox6);
            this.tabPage5.Controls.Add(this.groupBox5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(656, 346);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Histórico";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGridView4);
            this.groupBox6.Location = new System.Drawing.Point(6, 131);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(643, 139);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Detalles pedido";
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(8, 20);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(629, 113);
            this.dataGridView4.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridView3);
            this.groupBox5.Location = new System.Drawing.Point(7, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(643, 118);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pedidos Realizados";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(8, 20);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(629, 92);
            this.dataGridView3.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cbMostrarCaracteres);
            this.groupBox7.Controls.Add(this.lbErrorConexion);
            this.groupBox7.Controls.Add(this.btConectar);
            this.groupBox7.Controls.Add(this.tbClave);
            this.groupBox7.Controls.Add(this.tbId);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Location = new System.Drawing.Point(5, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(681, 78);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Datos de conexión";
            // 
            // cbMostrarCaracteres
            // 
            this.cbMostrarCaracteres.AutoSize = true;
            this.cbMostrarCaracteres.Location = new System.Drawing.Point(181, 46);
            this.cbMostrarCaracteres.Name = "cbMostrarCaracteres";
            this.cbMostrarCaracteres.Size = new System.Drawing.Size(114, 17);
            this.cbMostrarCaracteres.TabIndex = 14;
            this.cbMostrarCaracteres.Text = "Mostrar caracteres";
            this.cbMostrarCaracteres.UseVisualStyleBackColor = true;
            this.cbMostrarCaracteres.CheckedChanged += new System.EventHandler(this.cbMostrarCaracteres_CheckedChanged);
            // 
            // lbErrorConexion
            // 
            this.lbErrorConexion.AutoSize = true;
            this.lbErrorConexion.ForeColor = System.Drawing.Color.Red;
            this.lbErrorConexion.Location = new System.Drawing.Point(412, 22);
            this.lbErrorConexion.Name = "lbErrorConexion";
            this.lbErrorConexion.Size = new System.Drawing.Size(0, 13);
            this.lbErrorConexion.TabIndex = 13;
            // 
            // btConectar
            // 
            this.btConectar.Location = new System.Drawing.Point(293, 17);
            this.btConectar.Name = "btConectar";
            this.btConectar.Size = new System.Drawing.Size(113, 23);
            this.btConectar.TabIndex = 12;
            this.btConectar.Text = "Conectar";
            this.btConectar.UseVisualStyleBackColor = true;
            this.btConectar.Click += new System.EventHandler(this.btConectar_Click_1);
            // 
            // tbClave
            // 
            this.tbClave.Location = new System.Drawing.Point(181, 19);
            this.tbClave.Name = "tbClave";
            this.tbClave.Size = new System.Drawing.Size(100, 20);
            this.tbClave.TabIndex = 11;
            this.tbClave.UseSystemPasswordChar = true;
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(34, 19);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(100, 20);
            this.tbId.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(141, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Clave";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Id";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 512);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Taller";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tpPedidos.ResumeLayout(false);
            this.tcPedidos.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiezas)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPrecioMaximo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCantidad)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOfertas)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidosEnCurso)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tpPedidos;
        private System.Windows.Forms.Button btModificacion;
        private System.Windows.Forms.Button btBaja;
        private System.Windows.Forms.Button btAlta;
        private System.Windows.Forms.TextBox tbDireccion;
        private System.Windows.Forms.TextBox tbLocalidad;
        private System.Windows.Forms.TextBox tbTelefono;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.TextBox tbNIF;
        private System.Windows.Forms.TabControl tcPedidos;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btEnviarPedido;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbCriterio;
        private System.Windows.Forms.ComboBox cbCriterio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chbNegociadoAutomatico;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpFechaFinalizacion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btAnadirPieza;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbEstado;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbReferencia;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btAceptarOferta;
        private System.Windows.Forms.Button btRechazarOferta;
        private System.Windows.Forms.DataGridView dgvOfertas;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgvPedidosEnCurso;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button btVerOfertas;
        private System.Windows.Forms.Button btRecogerPedidos;
        private System.Windows.Forms.Label lbErrorAlta;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbDescripcion;
        private System.Windows.Forms.Label lbErrorAnadirPiezas;
        private System.Windows.Forms.NumericUpDown NUDCantidad;
        private System.Windows.Forms.DataGridView dgvPiezas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripción;
        private System.Windows.Forms.NumericUpDown NUDPrecioMaximo;
        private System.Windows.Forms.Label lbErrorEnviarPedido;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox cbMostrarCaracteres;
        private System.Windows.Forms.Label lbErrorConexion;
        private System.Windows.Forms.Button btConectar;
        private System.Windows.Forms.TextBox tbClave;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btDetalles;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefProp;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefGes;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn auto;
        private System.Windows.Forms.DataGridViewTextBoxColumn criterio;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaFin;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefOfertaGes;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaCaducidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEntrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstadoOferta;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioTotal;
    }
}

