﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SORTaller
{
    public class Pieza
    {
        private string NumRef;
        private string Descripcion;
        private int Cantidad;
        private float Precio;
        private int Estado;

        public string getNumRef()
        {
            return NumRef;
        }
        public void setNumRef(string n_NumRef)
        {
            NumRef = n_NumRef;
        }
        public string getDescripcion()
        {
            return Descripcion;
        }
        public void setDescripcion(string n_Descripcion)
        {
            Descripcion = n_Descripcion;
        }
        public int getCantidad()
        {
            return Cantidad;
        }
        public void setCantidad(int n_Cantidad)
        {
            Cantidad = n_Cantidad;
        }
        public float getPrecio()
        {
            return Precio;
        }
        public void setPrecio(float n_Precio)
        {
            Precio = n_Precio;
        }
        public int getEstado()
        {
            return Estado;
        }
        public void setEstado(int n_Estado)
        {
            Estado = n_Estado;
        }
        public Pieza()
        {
            NumRef = "";
            Descripcion = "";
            Cantidad = 0;
            Precio = 0;
            Estado = 0;
        }
        public Pieza(string referencia, string descripcion, int cantidad, int estado)
        {
            NumRef = referencia;
            Descripcion = descripcion;
            Cantidad = cantidad;
            Estado = estado;
        }
        public Pieza(Pieza n_pieza)
        {
            NumRef = n_pieza.getNumRef();
            Descripcion = n_pieza.getDescripcion(); ;
            Cantidad = n_pieza.getCantidad();
            Precio = n_pieza.getPrecio();
            Estado = n_pieza.getEstado();
        }
    }
}
