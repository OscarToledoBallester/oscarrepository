﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SORTaller
{
    public partial class Form1 : Form
    {
        Pedido[] pedidos;
        Oferta[] ofertas;

        public Form1()
        {
            InitializeComponent();

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void reiniciarEstado()
        {
            dgvOfertas.Rows.Clear();
            dgvPedidosEnCurso.Rows.Clear();
            if (pedidos != null)
            {
                Array.Clear(pedidos, 0, pedidos.Length);
            }
            if (ofertas != null)
            {
                Array.Clear(ofertas, 0, ofertas.Length);
            }
        }
        private void reiniciarEstadoOfertas()
        {
            dgvOfertas.Rows.Clear();
            if (ofertas != null)
            {
                Array.Clear(ofertas, 0, ofertas.Length);
            }
        }
        private void btAlta_Click(object sender, EventArgs e)
        {
            lbErrorAlta.Visible = false;
            Peticion p = new Peticion();
            //int intentos = 3;
            int res = p.Alta(tbNIF.Text,tbNombre.Text,tbEmail.Text,tbTelefono.Text, tbLocalidad.Text, tbDireccion.Text);
            if (res == -10)
            {
                lbErrorAlta.Text = "Error: No se puede conectar con el servidor";
                lbErrorAlta.Visible = true;
            }
        }

        private void tbNIF_TextChanged(object sender, EventArgs e)
        {

        }

        private void btAnadirPieza_Click(object sender, EventArgs e)
        {
            lbErrorAnadirPiezas.Text = "";
            if (tbReferencia.Text == "")
            {
                lbErrorAnadirPiezas.Text = "Indique la referencia";
            }
            else
            {
                if (NUDCantidad.Value == 0 )
                {
                    lbErrorAnadirPiezas.Text = "La cantidad debe ser mayor a 0";
                }
                else
                {
                    string descr = tbDescripcion.Text;
                    if (tbDescripcion.Text == "")
                    {
                        descr = "No disponible";
                    }
                    string estad = cbEstado.Text;
                    if (cbEstado.Text == "")
                    {
                        estad = "Cualquiera";
                    }                    
                    dgvPiezas.Rows.Add(tbReferencia.Text, NUDCantidad.Text, estad, descr);
                }
            }
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            dgvPiezas.Rows.Clear();
        }

        private void btEnviarPedido_Click(object sender, EventArgs e)
        {
            lbErrorEnviarPedido.Text = "";
            Peticion p = new Peticion(tbId.Text, tbClave.Text);
            if (dgvPiezas.Rows.Count == 0)
            {
                lbErrorEnviarPedido.Text = "Es necesario añadir piezas";
            }
            else
            {
                int tamPedido = dgvPiezas.Rows.Count;
                string[] referencias = new string[tamPedido];
                int[] cantidades = new int[tamPedido];
                string[] descripciones = new string[tamPedido];
                int[] estados = new int[tamPedido];
                float[] precios = new float[tamPedido];
                for (int i = 0; i < tamPedido; i++)
                {
                    precios[i] = 0;
                }
                
                bool automatico = false;
                int criterio = -1;
                int estado = 0;
                int precioMax = Convert.ToInt32(NUDPrecioMaximo.Value);
                string fechaFin = dtpFechaFinalizacion.Value.ToString().Split(' ')[0];
                
                for (int i = 0; i < tamPedido; i++)
                {
                    referencias[i] = dgvPiezas.Rows[i].Cells[0].Value.ToString();
                    cantidades[i] = Convert.ToInt32(dgvPiezas.Rows[i].Cells[1].Value.ToString());
                    descripciones[i] = dgvPiezas.Rows[i].Cells[3].Value.ToString();
                    if (dgvPiezas.Rows[i].Cells[2].Value.ToString() == "-")
                    {
                        estados[i] = 0;
                    }
                    else
                    {
                        if (dgvPiezas.Rows[i].Cells[2].Value.ToString() == "Nuevo")
                        {
                            estados[i] = 1;
                        }
                        else
                        {
                            estados[i] = 2;
                        }
                    }                   
                }
                if (chbNegociadoAutomatico.Checked == true)
                {
                    automatico = true;
                    if (cbCriterio.Text == "Precio")
                    {
                        criterio = 0;
                    }
                    else
                    {
                        criterio = 1;
                    }
                }
                if (cbEstado.Text == "Cualquiera")
                {
                    estado = 0;
                }
                else
                {
                    if (cbEstado.Text == "Nuevo")
                    {
                        estado = 1;
                    }
                    else
                    {
                        estado = 2;
                    }
                }
                //Enviamos por el webservice
                lbErrorEnviarPedido.Text = fechaFin;
                int res = p.NuevoPedido(fechaFin, criterio, automatico, precioMax, referencias, descripciones, cantidades, precios, estados);
                if (res == -10)
                {
                    lbErrorEnviarPedido.Text = "Error: No se puede conectar con el servidor";

                }
                else
                {
                    dgvPiezas.Rows.Clear();
                }
            }
        }

        private void chbNegociadoAutomatico_CheckedChanged(object sender, EventArgs e)
        {
            if (chbNegociadoAutomatico.Checked == true)
            {
                lbCriterio.Visible = true;
                cbCriterio.Visible = true;
            }
            else
            {
                lbCriterio.Visible = false;
                cbCriterio.Visible = false;
            }
        }

        private void btModificacion_Click(object sender, EventArgs e)
        {
            lbErrorAlta.Visible = false;
            Peticion p = new Peticion(tbId.Text, tbClave.Text);
            //int intentos = 3;
            int res = p.Modificacion(tbNIF.Text, tbNombre.Text, tbEmail.Text, tbTelefono.Text, tbLocalidad.Text, tbDireccion.Text);
            if (res == -10)
            {
                lbErrorAlta.Text = "Error: No se puede conectar con el servidor";
                lbErrorAlta.Visible = true;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void cbMostrarCaracteres_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMostrarCaracteres.Checked == true)
            {
                tbClave.UseSystemPasswordChar = false;
            }
            else
            {
                tbClave.UseSystemPasswordChar = true;
            }
        }

        private void btConectar_Click_1(object sender, EventArgs e)
        {
            lbErrorConexion.Text = "";
            if (btConectar.Text.Equals("Conectar"))
            {
                tbId.Enabled = false;
                tbClave.Enabled = false;
                btConectar.Text = "Desconectar";

                int res = 0;
                Peticion pet = new Peticion(tbId.Text, tbClave.Text);
                
                //Cargamos en perfil los datos para ello necesitamos el webservices
                //Cargamos los pedidos actuales y sus ofertas
                String resultado = pet.RecogerDatos();
                String[] datos = resultado.Split('|');
                //lbErrorConexion.Text = resultado;
                if(datos[0].Equals("OK"))
                {
                    res = 0;
                }else
                {
                    res = Convert.ToInt32(datos[0]);
                }
                
                
                switch (res)
                {
                    case 0: 
                        for (int i = 1; i < datos.Length; i++)
                        {
                            if (datos[i] == "-")
                            {
                                datos[i] = "";
                            }
                        }
                        tbNIF.Text = datos[1];
                        tbNombre.Text = datos[2];
                        tbEmail.Text = datos[3];
                        tbTelefono.Text = datos[4];
                        tbLocalidad.Text = datos[5];
                        tbDireccion.Text = datos[6];
                        tcPedidos.Enabled = true;
                        break;
                    case -2:
                        lbErrorConexion.Text = "El cliente no esta activado";
                        break;
                    case -10: 
                        lbErrorConexion.Text = "No es posible conectar con el servidor";
                        break;
                    case -15:
                        lbErrorConexion.Text = "No es posible conectar con la BD";
                        break;
                }
            }
            else
            {
                tbId.Enabled = true;
                tbClave.Enabled = true;
                tcPedidos.Enabled = false;
                btConectar.Text = "Conectar";
                tbId.Text = "";
                tbClave.Text = "";
                tbNIF.Text = "";
                tbNombre.Text = "";
                tbEmail.Text = "";
                tbTelefono.Text = "";
                tbLocalidad.Text = "";
                tbDireccion.Text = "";
            }

        }

        private void btRecogerPedidos_Click(object sender, EventArgs e)
        {

            reiniciarEstado();
            String cadena_pedidos = "";
            Peticion pet = new Peticion(tbId.Text, tbClave.Text);
            cadena_pedidos = pet.RecogerPedidos();
            String[] pedidos_partidos = cadena_pedidos.Split('?');
            int numpedidos = pedidos_partidos.Length;
            pedidos = new Pedido[numpedidos];
            for (int i = 0; i < numpedidos; i++)
            {
                pedidos[i] = new Pedido();
                pedidos[i].cargaPedido(pedidos_partidos[i]);
            }
            string auto = "";
            string crit = "-";
            for (int i = 0; i < pedidos.Length; i++)
            {
                if(pedidos[i].getAutomatico() == true)
                {
                    auto = "Si";
                    if(pedidos[i].getCriterio() == 0)
                    {
                        crit = "Precio";
                    }else
                    {
                        crit = "Fecha de entrega";
                    }
                }else
                {
                    auto = "No";
                    crit = "-";
                }
                dgvPedidosEnCurso.Rows.Add(pedidos[i].getNumRef(), pedidos[i].getNumRefGestor(), pedidos[i].getPrecioMax(), auto, crit, pedidos[i].getFechaFin());
            }
        }

        private void btDetalles_Click(object sender, EventArgs e)
        {
            int fila = 0;
            bool correcto = false;
            if (dgvPedidosEnCurso.SelectedRows.Count == 1)
            {
                fila = dgvPedidosEnCurso.SelectedRows[0].Index;
                correcto = true;
            }
            else
            {
                if (dgvPedidosEnCurso.SelectedRows.Count > 1)
                {
                    MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    if (dgvPedidosEnCurso.SelectedCells.Count == 1)
                    {
                        fila = dgvPedidosEnCurso.SelectedCells[0].RowIndex;
                        correcto = true;
                    }
                    else
                    {
                        if (dgvPedidosEnCurso.SelectedCells.Count > 1)
                        {
                            MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            MessageBox.Show("Debe seleccionar una fila o celda", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                }
                
            }

            if (correcto == true)
            {
                DetallesPedido dp = new DetallesPedido(pedidos[fila]);
                dp.ShowDialog();
            }
           
        }

        private void btVerOfertas_Click(object sender, EventArgs e)
        {
            //NumeroDeOfertas?numRefGestor|fechaCaducidad|Estado|PrecioTotal#...
            
            String cadena_ofertas = "";
            Peticion pet = new Peticion(tbId.Text, tbClave.Text);

            bool correcto = false;
            int fila = 0;

            if (dgvPedidosEnCurso.SelectedRows.Count == 1)
            {
                fila = dgvPedidosEnCurso.SelectedRows[0].Index;
                correcto = true;
            }
            else
            {
                if (dgvPedidosEnCurso.SelectedRows.Count > 1)
                {
                    MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    if (dgvPedidosEnCurso.SelectedCells.Count == 1)
                    {
                        fila = dgvPedidosEnCurso.SelectedCells[0].RowIndex;
                        correcto = true;
                    }
                    else
                    {
                        if (dgvPedidosEnCurso.SelectedCells.Count > 1)
                        {
                            MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            MessageBox.Show("Debe seleccionar una fila o celda", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                }

            }

            if (correcto == true)
            {
                //reiniciarEstado();
                reiniciarEstadoOfertas();
                string refPedido = pedidos[fila].getNumRef();
                string refPedidoGestor = pedidos[fila].getNumRefGestor();
                cadena_ofertas = pet.RecogerOfertas(refPedidoGestor);

                String[] pre_ofertas = cadena_ofertas.Split('?');
                String[] ofertas_partidos = pre_ofertas[1].Split('#');
                int numofertas = Convert.ToInt32(pre_ofertas[0]);

                ofertas = new Oferta[numofertas];

                for (int i = 0; i < numofertas; i++)
                {
                    ofertas[i] = new Oferta();
                    ofertas[i].cargaOferta(ofertas_partidos[i]);
                    ofertas[i].setNumRefPedido(refPedido);
                }

                string estado = "";
                int est = 0;
                for (int i = 0; i < pedidos.Length; i++)
                {
                    est = ofertas[i].getofertaEstado();

                    switch (est)
                    {
                        case 0: estado = "Activo";
                            break;
                        case 1: estado = "Rechazado";
                            break;
                        case 2: estado = "Cancelado";
                            break;
                        case 3: estado = "Caducado";
                            break;
                    }
                    //numRefGestor|fechaCaducidad|FechaEntrega|Estado|PrecioTotal
                    dgvOfertas.Rows.Add(ofertas[i].getNumRefOferta(), ofertas[i].getfechaFin(), ofertas[i].getfechaEntrega(), estado, ofertas[i].getprecioTotal());
                }
            }
           
        }

        private void btAceptarOferta_Click(object sender, EventArgs e)
        {
            Peticion pet = new Peticion(tbId.Text, tbClave.Text);

            bool correcto = false;
            int fila = 0;

            if (dgvOfertas.SelectedRows.Count == 1)
            {
                fila = dgvOfertas.SelectedRows[0].Index;
                correcto = true;
            }
            else
            {
                if (dgvOfertas.SelectedRows.Count > 1)
                {
                    MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    if (dgvOfertas.SelectedCells.Count == 1)
                    {
                        fila = dgvOfertas.SelectedCells[0].RowIndex;
                        correcto = true;
                    }
                    else
                    {
                        if (dgvOfertas.SelectedCells.Count > 1)
                        {
                            MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            MessageBox.Show("Debe seleccionar una fila o celda", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                }
            }
            if (correcto == true)
            {
                string refPedidoGestor = pedidos[fila].getNumRefGestor();
                string idPed = ofertas[fila].getNumRefPedido();
                string idOf = ofertas[fila].getNumRefOferta();
                pet.SeleccionarOferta(refPedidoGestor, idOf);
                MessageBox.Show("Seleccionada la oferta: " + idOf, "Pedido nº " + idPed + "/" + refPedidoGestor, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btRechazarOferta_Click(object sender, EventArgs e)
        {
            Peticion pet = new Peticion(tbId.Text, tbClave.Text);

            bool correcto = false;
            int fila = 0;

            if (dgvOfertas.SelectedRows.Count == 1)
            {
                fila = dgvOfertas.SelectedRows[0].Index;
                correcto = true;
            }
            else
            {
                if (dgvOfertas.SelectedRows.Count > 1)
                {
                    MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    if (dgvOfertas.SelectedCells.Count == 1)
                    {
                        fila = dgvOfertas.SelectedCells[0].RowIndex;
                        correcto = true;
                    }
                    else
                    {
                        if (dgvOfertas.SelectedCells.Count > 1)
                        {
                            MessageBox.Show("Seleccione solo una fila", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            MessageBox.Show("Debe seleccionar una fila o celda", "Error de seleccion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                    }
                }
            }
            if (correcto == true)
            {
                string refPedidoGestor = pedidos[fila].getNumRefGestor();
                string idPed = ofertas[fila].getNumRefPedido();
                string idOf = ofertas[fila].getNumRefOferta();
                pet.RechazarOferta(refPedidoGestor, idOf);
                MessageBox.Show("Rechazada la oferta: " + idOf, "Pedido nº " + idPed + "/" + refPedidoGestor, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
