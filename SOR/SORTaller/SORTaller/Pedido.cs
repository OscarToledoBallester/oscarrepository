﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SORTaller
{
    public class Pedido
    {
        string numRef;
        string numRefGestor;
        Pieza[] piezas;
        string fechaFin;
        bool automatico;
        float precioMax;
        int criterio;
        int numPiezas;
        int insertadas;

        public string getNumRef()
        {
            return numRef;
        }
        public string getNumRefGestor()
        {
            return numRefGestor;
        }
        public string getFechaFin()
        {
            return fechaFin;
        }
        public bool getAutomatico()
        {
            return automatico;
        }
        public float getPrecioMax()
        {
            return precioMax;
        }
        public int getCriterio()
        {
            return criterio;
        }
        public int getNumPiezas()
        {
            return numPiezas;
        }
        public void setNumRefGestor(string n)
        {
            numRefGestor = n;
        }
        public void setFechaFin(string n)
        {
            fechaFin = n;
        }
        public void setAutomatica(bool n)
        {
            automatico = n;
        }
        public void setCriterio(int n)
        {
            criterio = n;
        }
        public string getPiezaRef(int i)
        {
            if (i >= 0 && i < numPiezas)
            {
                return piezas[i].getNumRef();
            }
            else
            {
                return "";
            }
        }
        public string getPiezaDescr(int i)
        {
            if (i >= 0 && i < numPiezas)
            {
                return piezas[i].getDescripcion();
            }
            else
            {
                return "";
            }
        }
        public int getPiezaEst(int i)
        {
            if (i >= 0 && i < numPiezas)
            {
                if (piezas[i] != null)
                {
                    return piezas[i].getEstado();
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        public int getPiezaCant(int i)
        {
            if (i >= 0 && i < numPiezas)
            {
                return piezas[i].getCantidad();
            }
            else
            {
                return 0;
            }
        }

        public Pedido()
        {
            numRef = "";
            numRefGestor = "";
            fechaFin = "";
            automatico = false;
            criterio = 0;
            numPiezas = 0;
            insertadas = 0;
            precioMax = 0;
        }
        public Pedido(string referenciaPedido, string referenciaPedidoGestor, string fecha, bool auto, int crit, int npiezas)
        {
            numRef = referenciaPedido;
            numRefGestor = referenciaPedidoGestor;
            fechaFin = fecha;
            automatico = auto;
            criterio = crit;
            numPiezas = npiezas;
            piezas = new Pieza[npiezas];
            insertadas = 0;
            precioMax = 0;
            for (int i = 0; i < npiezas; i++)
            {
                piezas[i] = new Pieza();
            }
        }
        public int cargaPedido(string datosEntrada)
        {
            int res = 0;

            if (datosEntrada.Length <= 2)
            {
                res = Convert.ToInt32(datosEntrada);
            }
            else
            {
                String[] datos = datosEntrada.Split('|');
                numRef = datos[0];
                numRefGestor = datos[1];
                precioMax = Convert.ToInt32(datos[2]);
                if (Convert.ToInt32(datos[3]) == 1)
                {
                    automatico = true;
                }
                else
                {
                    automatico = false;
                }
                criterio = Convert.ToInt32(datos[4]);
                numPiezas = Convert.ToInt32(datos[5]);
                fechaFin = datos[6];
                String[] cadena_piezas = datos[7].Split('@');
                String[] cadena_pieza;
                piezas = new Pieza[cadena_piezas.Length];
                for (int i = 0; i < cadena_piezas.Length; i++)
                {
                    
                    cadena_pieza = cadena_piezas[i].Split('#');
                    //nueva = new Pieza();
                    anyadirPieza(cadena_pieza[0], cadena_pieza[1], Convert.ToInt32(cadena_pieza[2]), Convert.ToInt32(cadena_pieza[3]));
                }
                
                
            }

            return res;
        }
        public bool anyadirPieza(string referencia, string desc, int cantidad, int estado)
        {
            bool inserta = false;
            if (insertadas < numPiezas)
            {
                try
                {
                    piezas[insertadas] = new Pieza(referencia, desc, cantidad, estado);
                    insertadas++;
                    inserta = true;
                }
                catch (NullReferenceException)
                {
                    inserta = false;
                }
            }
            return inserta;
        }
    }
}
