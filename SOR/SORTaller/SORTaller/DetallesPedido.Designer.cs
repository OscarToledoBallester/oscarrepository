﻿namespace SORTaller
{
    partial class DetallesPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvPiezas = new System.Windows.Forms.DataGridView();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lbRefPedProp = new System.Windows.Forms.Label();
            this.lbRefPedGes = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbPreMax = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbFechFin = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbNumPiez = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbCritSel = new System.Windows.Forms.Label();
            this.lbEtiqCrit = new System.Windows.Forms.Label();
            this.lbPedAut = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiezas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbCritSel);
            this.groupBox1.Controls.Add(this.lbEtiqCrit);
            this.groupBox1.Controls.Add(this.lbPedAut);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.lbNumPiez);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lbFechFin);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lbPreMax);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbRefPedGes);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbRefPedProp);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 237);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvPiezas);
            this.groupBox2.Location = new System.Drawing.Point(270, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(456, 237);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Piezas";
            // 
            // dgvPiezas
            // 
            this.dgvPiezas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPiezas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia,
            this.Descripcion,
            this.Estado,
            this.Cantidad});
            this.dgvPiezas.Location = new System.Drawing.Point(7, 20);
            this.dgvPiezas.Name = "dgvPiezas";
            this.dgvPiezas.Size = new System.Drawing.Size(443, 211);
            this.dgvPiezas.TabIndex = 0;
            // 
            // Referencia
            // 
            this.Referencia.HeaderText = "Referencia";
            this.Referencia.Name = "Referencia";
            // 
            // Descripcion
            // 
            this.Descripcion.HeaderText = "Descripcion";
            this.Descripcion.Name = "Descripcion";
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Referencia Pedido (Propia):";
            // 
            // lbRefPedProp
            // 
            this.lbRefPedProp.AutoSize = true;
            this.lbRefPedProp.Location = new System.Drawing.Point(151, 29);
            this.lbRefPedProp.Name = "lbRefPedProp";
            this.lbRefPedProp.Size = new System.Drawing.Size(34, 13);
            this.lbRefPedProp.TabIndex = 1;
            this.lbRefPedProp.Text = "Texto";
            // 
            // lbRefPedGes
            // 
            this.lbRefPedGes.AutoSize = true;
            this.lbRefPedGes.Location = new System.Drawing.Point(151, 52);
            this.lbRefPedGes.Name = "lbRefPedGes";
            this.lbRefPedGes.Size = new System.Drawing.Size(34, 13);
            this.lbRefPedGes.TabIndex = 3;
            this.lbRefPedGes.Text = "Texto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Referencia Pedido (Gestor):";
            // 
            // lbPreMax
            // 
            this.lbPreMax.AutoSize = true;
            this.lbPreMax.Location = new System.Drawing.Point(152, 74);
            this.lbPreMax.Name = "lbPreMax";
            this.lbPreMax.Size = new System.Drawing.Size(34, 13);
            this.lbPreMax.TabIndex = 5;
            this.lbPreMax.Text = "Texto";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Precio Máximo:";
            // 
            // lbFechFin
            // 
            this.lbFechFin.AutoSize = true;
            this.lbFechFin.Location = new System.Drawing.Point(152, 98);
            this.lbFechFin.Name = "lbFechFin";
            this.lbFechFin.Size = new System.Drawing.Size(34, 13);
            this.lbFechFin.TabIndex = 7;
            this.lbFechFin.Text = "Texto";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Fecha de finalización:";
            // 
            // lbNumPiez
            // 
            this.lbNumPiez.AutoSize = true;
            this.lbNumPiez.Location = new System.Drawing.Point(152, 120);
            this.lbNumPiez.Name = "lbNumPiez";
            this.lbNumPiez.Size = new System.Drawing.Size(34, 13);
            this.lbNumPiez.TabIndex = 9;
            this.lbNumPiez.Text = "Texto";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Número de piezas:";
            // 
            // lbCritSel
            // 
            this.lbCritSel.AutoSize = true;
            this.lbCritSel.Location = new System.Drawing.Point(152, 165);
            this.lbCritSel.Name = "lbCritSel";
            this.lbCritSel.Size = new System.Drawing.Size(34, 13);
            this.lbCritSel.TabIndex = 13;
            this.lbCritSel.Text = "Texto";
            // 
            // lbEtiqCrit
            // 
            this.lbEtiqCrit.AutoSize = true;
            this.lbEtiqCrit.Location = new System.Drawing.Point(8, 165);
            this.lbEtiqCrit.Name = "lbEtiqCrit";
            this.lbEtiqCrit.Size = new System.Drawing.Size(105, 13);
            this.lbEtiqCrit.TabIndex = 12;
            this.lbEtiqCrit.Text = "Criterio de selección:";
            // 
            // lbPedAut
            // 
            this.lbPedAut.AutoSize = true;
            this.lbPedAut.Location = new System.Drawing.Point(152, 143);
            this.lbPedAut.Name = "lbPedAut";
            this.lbPedAut.Size = new System.Drawing.Size(34, 13);
            this.lbPedAut.TabIndex = 11;
            this.lbPedAut.Text = "Texto";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 143);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Pedido automático:";
            // 
            // DetallesPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 262);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "DetallesPedido";
            this.Text = "DetallesPedido";
            this.Load += new System.EventHandler(this.DetallesPedido_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiezas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvPiezas;
        private System.Windows.Forms.Label lbCritSel;
        private System.Windows.Forms.Label lbEtiqCrit;
        private System.Windows.Forms.Label lbPedAut;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbNumPiez;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbFechFin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbPreMax;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbRefPedGes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbRefPedProp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
    }
}