﻿using System;

public class ClienteEmpresa     
{
    private String NIF;
    private String Nombre;
    private String Email;
    private String Telefono;
    private String Localidad;
    private String Direccion;

    public String NIF
    {
        get
        {
            return NIF;
        }
        set
        {
            NIF = value;
        }
    }
    public String Nombre
    {
        get
        {
            return Nombre;
        }
        set
        {
            Nombre = value;
        }
    }
    public String Email
    {
        get
        {
            return Email;
        }
        set
        {
            Email = value;
        }
    }
    public String Telefono
    {
        get
        {
            return Telefono;
        }
        set
        {
            Telefono = value;
        }
    }
    public String Localidad
    {
        get
        {
            return Localidad;
        }
        set
        {
            Localidad = value;
        }
    }
    public String Direccion
    {
        get
        {
            return Direccion;
        }
        set
        {
            Direccion = value;
        }
    }

	public ClienteEmpresa()
	{
        NIF = "";
        Nombre = "";
        Email = "";
        Telefono = "";
        Localidad = "";
        Direccion = "";
	}

}
