﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace WSGespiezas
{
    public class PedidoCAD
    {
        MySqlConnection connection = new MySqlConnection();
        String connectionString;

        string idTaller;
        PiezaEN[] piezas;
        String fechaFin;
        Boolean automatico;
        int criterio;
        int numPiezas;
        string numRefTaller;
        string numRefPedidoLocal;

        public void setNumRefPedidoLocal(string n)
        {
            numRefPedidoLocal = n;
        }
        public void setFechaFin(string n)
        {
            fechaFin = n;
        }
        public void setAutomatica(bool n)
        {
            automatico = n;
        }
        public void setCriterio(int n)
        {
            criterio = n;
        }
        public void setIdTaller(string n)
        {
            idTaller = n;
        }
        public PedidoCAD()
        {
            numRefTaller = "";
            idTaller = "";
            fechaFin = "";
            automatico = false;
            criterio = 0;
            numPiezas = 0;
        }
        public PedidoCAD(string n_idTaller, string n_numRefTaller, int n_numPiezas, PiezaEN[] n_piezas, String n_fechaFin, Boolean n_automatico, int n_criterio)
        {
            idTaller = n_idTaller;
            piezas = n_piezas;
            fechaFin = n_fechaFin;
            automatico = n_automatico;
            criterio = n_criterio;
            numPiezas = n_numPiezas;
            numRefTaller = n_numRefTaller;
        }
        public int InsertarPedido()
        {
            int resultado = 0;

            //Comprobamos si exisen las piezas en la BD si no las insertamos
            for (int i = 0; i < numPiezas; i++)
            {
                piezas[i].InsertarPieza();
            }

            //Insertamos el Pedido
            iniciarConexion();

            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "INSERT INTO pedido (NumRefTaller,FechaCaducidad,Automatico,Estado,Taller,Criterio) ";
            ins += "VALUES (@n_NumRefTaller,@n_FechaCaducidad,@n_Automatico,@n_Estado,@n_Taller,@n_Criterio)";

            myCommand.CommandText = ins;

            MySqlParameter p_NumRefTaller = new MySqlParameter("@n_NumRefTaller", MySqlDbType.Text, 0);
            MySqlParameter p_FechaCaducidad = new MySqlParameter("@n_FechaCaducidad", MySqlDbType.Text, 0);
            MySqlParameter p_Automatico = new MySqlParameter("@n_Automatico", MySqlDbType.Text, 0);
            MySqlParameter p_Estado = new MySqlParameter("@n_Estado", MySqlDbType.Text, 0);
            MySqlParameter p_Taller = new MySqlParameter("@n_Taller", MySqlDbType.Text, 0);
            MySqlParameter p_Criterio = new MySqlParameter("@n_Criterio", MySqlDbType.Text, 0);

            p_NumRefTaller.Value = numRefTaller;
            p_FechaCaducidad.Value = fechaFin;
            if (automatico == true)
            {
                p_Automatico.Value = 1;
            }
            else
            {
                p_Automatico.Value = 0;
            }
            p_Estado.Value = 0;
            p_Taller.Value = idTaller;
            p_Criterio.Value = criterio;

            myCommand.Parameters.Add(p_NumRefTaller);
            myCommand.Parameters.Add(p_FechaCaducidad);
            myCommand.Parameters.Add(p_Automatico);
            myCommand.Parameters.Add(p_Estado);
            myCommand.Parameters.Add(p_Taller);
            myCommand.Parameters.Add(p_Criterio);

            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                resultado = 0;
            }
            catch (MySqlException m)
            {
                resultado = -1;
            }

            cerrarConexion();

            //Recuperamos nuestro numero de referencia del pedido
            string r_id = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT NumRefLocal FROM pedido WHERE NumRefTaller=@n_refTaller AND Taller=@n_idTaller ORDER BY NumRefLocal DESC";

            MySqlParameter p_refTaller = new MySqlParameter("@n_refTaller", MySqlDbType.Text, 0);
            MySqlParameter p_idTaller = new MySqlParameter("@n_idTaller", MySqlDbType.Text, 0);
            p_refTaller.Value = numRefTaller;
            p_idTaller.Value = idTaller;
            inst.Parameters.Add(p_refTaller);
            inst.Parameters.Add(p_idTaller);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();
            if (reader.HasRows != false)
            {
                r_id = reader.GetString(0);
            }
            cerrarConexion();
            string referencia = r_id;

            //Insertamos las piezas del pedido
            for (int i = 0; i < numPiezas; i++)
            {
                resultado = piezas[i].InsertarPiezaPedido(referencia);
            }

            return resultado;
        }
        public int ModificarEstado(int i)
        {
            int resultado = 0;

            string fin = "";

            fin = iniciarConexion();
            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "UPDATE pedido SET Estado =@n_estado ";
            ins += "WHERE NumRefLocal=@n_NumRefLocal AND Taller=@n_idTaller";

            myCommand.CommandText = ins;

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_Taller = new MySqlParameter("@n_idTaller", MySqlDbType.Text, 0);
            MySqlParameter p_estado = new MySqlParameter("@n_estado", MySqlDbType.Text, 0);

            p_NumRefLocal.Value = numRefPedidoLocal;
            p_estado.Value = i;
            p_Taller.Value = idTaller;

            myCommand.Parameters.Add(p_estado);
            myCommand.Parameters.Add(p_NumRefLocal);
            myCommand.Parameters.Add(p_Taller);


            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                fin = "Modificado";
                resultado = 0;
            }
            catch (MySqlException m)
            {
                fin = "Error :" + m.Message;
                resultado = -4;
            }

            return resultado;
        }
        public int ModificarPedido()
        {
            int resultado = 0;
            string fin = "";
            //Comprobar que no esté finalizado y que no tenga ofertas
            fin = iniciarConexion();
            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "UPDATE pedido SET FechaCaducidad =@n_fechaCaducidad ,Automatico =@n_automatico ,Criterio =@n_criterio ";
            ins += "WHERE NumRefLocal=@n_NumRefLocal AND Taller=@n_idTaller";

            myCommand.CommandText = ins;

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_FechaCaducidad = new MySqlParameter("@n_FechaCaducidad", MySqlDbType.Text, 0);
            MySqlParameter p_Automatico = new MySqlParameter("@n_Automatico", MySqlDbType.Text, 0);
            MySqlParameter p_Taller = new MySqlParameter("@n_idTaller", MySqlDbType.Text, 0);
            MySqlParameter p_Criterio = new MySqlParameter("@n_Criterio", MySqlDbType.Text, 0);

            p_NumRefLocal.Value = numRefPedidoLocal;
            p_FechaCaducidad.Value = fechaFin;
            if (automatico == true)
            {
                p_Automatico.Value = 1;
            }
            else
            {
                p_Automatico.Value = 0;
            }
            p_Taller.Value = idTaller;
            p_Criterio.Value = criterio;

            myCommand.Parameters.Add(p_NumRefLocal);
            myCommand.Parameters.Add(p_FechaCaducidad);
            myCommand.Parameters.Add(p_Automatico);
            myCommand.Parameters.Add(p_Taller);
            myCommand.Parameters.Add(p_Criterio);


            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                fin = "Modificado";
                resultado = 0;
            }
            catch (MySqlException m)
            {
                fin = "Error :" + m.Message;
                resultado = -4;
            }
            return resultado;
        }
        public int EstadoPedido()
        {
            int r = -1;

            string r_id = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT Estado FROM pedido WHERE NumRefLocal=@n_NumRefLocal";

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            p_NumRefLocal.Value = numRefPedidoLocal;
            inst.Parameters.Add(p_NumRefLocal);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();
            if (reader.HasRows != false)
            {
                r_id = reader.GetString(0);
                if (r_id != "" && r_id != null)
                {
                    r = Convert.ToInt32(r_id);
                }
            }
            cerrarConexion();

            return r;
        }
        public bool CorrespondenciaClientePedido()
        {
            bool correcto = false;

            string r_id = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT Taller FROM pedido WHERE NumRefLocal=@n_NumRefLocal";

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            p_NumRefLocal.Value = numRefPedidoLocal;
            inst.Parameters.Add(p_NumRefLocal);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();
            if (reader.HasRows != false)
            {
                r_id = reader.GetString(0);
                if (r_id != "" && r_id != null)
                {
                    if (r_id.Equals(idTaller))
                    {
                        correcto = true;
                    }
                }
            }
            cerrarConexion();

            return correcto;
        }
        
        public string RecogerPedidos()
        {
            string res = "";
            string cadena_pedidos = "";

            iniciarConexion();
            //idPedidoLocal|idPedido|PrecioMax|Automatico|Criterio|Fecha|RefPieza#Descripcion#Cantidad#Estado@Pieza2?Pedido2
            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT NumRefTaller,NumRefLocal,PrecioMax,Automatico,Criterio,FechaCaducidad FROM pedido WHERE Taller=@n_idCliente";

            MySqlParameter p_idCliente = new MySqlParameter("@n_idCliente", MySqlDbType.Text, 0);
            p_idCliente.Value = idTaller;
            inst.Parameters.Add(p_idCliente);

            MySqlDataReader reader = inst.ExecuteReader();
            
            if (reader.HasRows == true)
            {
                while (reader.Read())
                {
                    res += reader.GetString(0) + "|" + reader.GetString(1) + "|" + reader.GetString(2) + "|" + reader.GetString(3) + "|" + reader.GetString(4) + "|" + reader.GetString(5) + "|";
                    cadena_pedidos = RecogerPiezasPedido(reader.GetString(1));
                    res += "?";
                }
            }

            cerrarConexion();

            return res;
        }
        public string RecogerPiezasPedido(string nPedido)
        {
            string res = "";
            string descripcion = "";

            
            //idPedidoLocal|idPedido|PrecioMax|Automatico|Criterio|Fecha|RefPieza#Descripcion#Cantidad#Estado@Pieza2?Pedido2
            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT IdPieza,Cantidad,Estado FROM piezauniversalenpedido WHERE PedidoNumRefLocal=@n_nPedido";

            MySqlParameter p_nPedido = new MySqlParameter("@n_nPedido", MySqlDbType.Text, 0);
            p_nPedido.Value = idTaller;
            inst.Parameters.Add(p_nPedido);

            MySqlDataReader reader = inst.ExecuteReader();

            if (reader.HasRows == true)
            {
                while (reader.Read())
                {
                    descripcion = descripcionPieza(reader.GetString(0));
                    res += reader.GetString(0) + "#" + descripcion + "#" + reader.GetString(1) + "#" + reader.GetString(2);       
                    res += "@";
                }
            }

            return res;
        }
        public string descripcionPieza(string idpieza)
        {
            string res = "";

           
            //idPedidoLocal|idPedido|PrecioMax|Automatico|Criterio|Fecha|RefPieza#Descripcion#Cantidad#Estado@Pieza2?Pedido2
            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT Descripcion FROM piezauniversal WHERE idPieza=@n_id";

            MySqlParameter p_id = new MySqlParameter("@n_id", MySqlDbType.Text, 0);
            p_id.Value = idpieza;
            inst.Parameters.Add(p_id);

            MySqlDataReader reader = inst.ExecuteReader();

            if (reader.HasRows == true)
            {
                while (reader.Read())
                {
                    res = reader.GetString(0);
                }
            }

            return res;
        }
        public string iniciarConexion()
        {
            try
            {
                connectionString = "Server=localhost; Database=bd_gestor; Uid=root;";
                connection.ConnectionString = connectionString;
                connection.Open();
                return "OK";
            }
            catch (MySqlException)
            {
                return "ERROR";
            }
        }
        public void cerrarConexion()
        {
            connection.Close();
        }
    }

}