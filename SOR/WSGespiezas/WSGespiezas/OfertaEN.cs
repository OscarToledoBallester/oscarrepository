﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSGespiezas
{
    public class OfertaEN
    {
        string numRefPedido;
        string numRefDesguace;
        string numRefPedidoDesguace;
        string numRefOfertaLocal;
        string idDesguace;
        PiezaEN[] piezas;
        String fechaFin;
        string fechaEntrega;
        int numPiezas;
        float precioTotal;
        int tamanyo;

        public void setIdDesguace(string i)
        {
            idDesguace = i;
        }
        public void setNumRefPedido(string i)
        {
            numRefPedido = i;
        }
        public void setNumRefOfertaLocal(string i)
        {
            numRefOfertaLocal = i;
        }
        public OfertaEN()
        {
            numRefPedido = "";
            numRefDesguace = "";
            numRefPedidoDesguace = "";
            idDesguace = "";
            fechaFin = "";
            fechaEntrega = "";
            numPiezas = 0;
            precioTotal = 0;
            tamanyo = 0;
        }
        public void NuevaOferta(string n_idDesguace, string n_RefPedido, string n_RefPedidoDesguace, string n_RefOferta, string n_fechaEntrega,string n_fechaCaducidad, string[] n_referencias, float[] n_precios, int[] n_estados, float n_precioTotal)
        {
            numRefPedido = n_RefPedido;
            numRefDesguace = n_RefOferta;
            numRefPedidoDesguace = n_RefPedidoDesguace;
            idDesguace = n_idDesguace;
            fechaFin = n_fechaCaducidad;
            fechaEntrega = n_fechaEntrega;
            precioTotal = n_precioTotal;
            tamanyo = n_referencias.Length;
            numPiezas = tamanyo;
            piezas[tamanyo] = new PiezaEN();

            for (int i = 0; i < tamanyo; i++)
            {
                piezas[i].setNumRef(n_referencias[i]);
                piezas[i].setPrecio(n_precios[i]);
                piezas[i].setEstado(n_estados[i]);
            }

            insertarOferta();
        }
        public void insertarOferta()
        {
            OfertaCAD nueva = new OfertaCAD(idDesguace, numRefPedido, numRefPedidoDesguace, numRefDesguace, fechaEntrega, fechaFin, piezas, precioTotal, tamanyo);
            nueva.insertarOferta();
        }
        public int EfectoCancelacionPedido()
        {
            int resultado = 0;

            OfertaCAD cancelar = new OfertaCAD();
            cancelar.setNumRefPedido(numRefPedido);

            return resultado;
        }
        public int SeleccionarOferta()
        {
            int resultado = 0;

            OfertaCAD seleccion = new OfertaCAD();
            seleccion.setNumRefOfertaLocal(numRefOfertaLocal);
            int estadoOferta = seleccion.EstadoOferta();

            if (estadoOferta == 0)
            {
                seleccion.modificarEstado(4);
            }

            return resultado;
        }
        public int RechazarOferta()
        {
            int resultado = 0;

            OfertaCAD seleccion = new OfertaCAD();
            seleccion.setNumRefOfertaLocal(numRefOfertaLocal);
            int estadoOferta = seleccion.EstadoOferta();

            if (estadoOferta == 0)
            {
                seleccion.modificarEstado(1);
            }

            return resultado;
        }
        public int CancelarOferta()
        {
            int resultado = 0;

            OfertaCAD seleccion = new OfertaCAD();
            seleccion.setNumRefOfertaLocal(numRefOfertaLocal);
            int estadoOferta = seleccion.EstadoOferta();

            if (estadoOferta == 0)
            {
                seleccion.modificarEstado(1);
            }

            return resultado;
        }
        public string OfertasPedido()
        {
            string devuelve = "";
            
            OfertaCAD modOf = new OfertaCAD();
            modOf.setNumRefPedido(numRefPedido);
            devuelve = modOf.OfertasPedido();

            return devuelve;
        }
        public bool CorrespondenciaDesguaceOferta()
        {
            bool correcto = true;

            OfertaCAD comprueba = new OfertaCAD();
            comprueba.setIdDesguace(idDesguace);
            comprueba.setNumRefOfertaLocal(numRefOfertaLocal);
            correcto = comprueba.CorrespondenciaDesguaceOferta();

            return correcto;
        }
        public string EstadoOferta()
        {
            string resultado = "";

            OfertaCAD ofe = new OfertaCAD();
            ofe.setNumRefOfertaLocal(numRefOfertaLocal);
            resultado = ofe.EstadoOferta().ToString();

            return resultado;
        }
    }
}