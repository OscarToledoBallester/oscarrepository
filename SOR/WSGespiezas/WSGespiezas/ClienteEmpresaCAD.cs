﻿using System;
using System.Net;
using System.Net.Mail;
using MySql.Data.MySqlClient;

public class ClienteEmpresaCAD    
{
    /*private String NIF;
    private String Nombre;
    private String Email;
    private String Telefono;
    private String Localidad;
    private String Direccion;
    */
    MySqlConnection connection = new MySqlConnection();
    String connectionString;
    
    private string ID;
    private String NIF;
    private String Nombre;
    private String Email;
    private String Telefono;
    private String Localidad;
    private String Direccion;
    private int Tipo;
    private int Estado;//0 - Sin Confirmar, 1 - Confirmado, 2 - Baja
    private string Clave;

    public string getClave()
    {
        return Clave;
    }
    public void setClave(string n_Clave)
    {
        Clave = n_Clave;
    }
    public string getID()
    {
        return ID;
    }
    public void setID(string n_ID)
    {
        ID = n_ID;
    }
    public string getNIF()
    {
        return NIF;
    }
    public void setNIF(string n_NIF)
    {
        NIF = n_NIF;
    }
    public string getNombre()
    {
        return Nombre;
    }
    public void setNombre(string nom)
    {
        Nombre = nom;
    }
    public string getEmail()
    {
        return Email;
    }
    public void setEmail(string mail)
    {
        Email = mail;
    }
    public string getTelefono()
    {
        return Telefono;
    }
    public void setTelefono(string n_Telefono)
    {
        Telefono = n_Telefono;
    }
    public string getLocalidad()
    {
        return Localidad;
    }
    public void setLocalidad(string n_Localidad)
    {
        Localidad = n_Localidad;
    }
    public string getDireccion()
    {
        return Direccion;
    }
    public void setDireccion(string n_Direccion)
    {
        Direccion = n_Direccion;
    }
    public int getTipo()
    {
        return Tipo;
    }
    public void setTipo(int n_Tipo)
    {
        Tipo = n_Tipo;
    }
    public int getEstado()
    {
        return Estado;
    }
    public void setEstado(int n_Estado)
    {
        Estado = n_Estado;
    }
    public ClienteEmpresaCAD()
	{
        NIF = "";
        Nombre = "";
        Email = "";
        Telefono = "";
        Localidad = "";
        Direccion = "";
        Tipo = 0;
        Estado = 0;
	}
    public ClienteEmpresaCAD(String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion, int tipo)
    {
        NIF = n_nif;
        Nombre = n_nombre;
        Email = n_email;
        Telefono = n_telefono;
        Localidad = n_localidad;
        Direccion = n_direccion;
        Tipo = tipo;
        Estado = 0;
    }
    //Para WebServices---------------------------------------------------------------------------------------
        public string altaCliente()
        {
            string fin = "";
            int idActual = consultaID();
            if (idActual != -1)
            {
                ID = idActual.ToString();
                if (comprobacionEstado("2") == true)
                {
                    iniciarConexion();
                    MySqlCommand myCommand = new MySqlCommand(null, connection);

                    string ins = "UPDATE cliente SET estado = 0 ";
                    ins += "WHERE numId=" + idActual;

                    myCommand.CommandText = ins;
                    try
                    {

                        myCommand.ExecuteNonQuery();
                        cerrarConexion();

                    }
                    catch (MySqlException m)
                    {
                        fin = "Error :" + m.Message;
                    }
                    fin = "Insertado 2";
                }
                else
                {
                    if (comprobacionEstado("1") == true)
                    {
                        fin = "Usuario ya dado de alta";
                    }
                }
               
            }
            else
            {
                // 0 - Taller, 1 - Desguace
                //Ingresamos el cliente en la base de datos con el apartado de "Estado" como "Sin confirmar"
                iniciarConexion();
                MySqlCommand myCommand = new MySqlCommand(null, connection);

                string ins = "INSERT INTO cliente (nif,email,Nombre,Direccion,Localidad,telefono,estado,Tipo) ";
                ins += "VALUES (@n_nif,@n_email,@n_nombre,@n_direccion,@n_localidad,@n_telefono,@n_estado,@n_tipo)";

                myCommand.CommandText = ins;

                MySqlParameter p_nif = new MySqlParameter("@n_nif", MySqlDbType.Text, 0);
                MySqlParameter p_email = new MySqlParameter("@n_email", MySqlDbType.Text, 0);
                MySqlParameter p_nombre = new MySqlParameter("@n_nombre", MySqlDbType.Text, 0);
                MySqlParameter p_direccion = new MySqlParameter("@n_direccion", MySqlDbType.Text, 0);
                MySqlParameter p_localidad = new MySqlParameter("@n_localidad", MySqlDbType.Text, 0);
                MySqlParameter p_telefono = new MySqlParameter("@n_telefono", MySqlDbType.Text, 0);
                MySqlParameter p_estado = new MySqlParameter("@n_estado", MySqlDbType.Text, 0);
                MySqlParameter p_tipo = new MySqlParameter("@n_tipo", MySqlDbType.Text, 0);

                p_nif.Value = NIF;
                p_email.Value = Email;
                p_nombre.Value = Nombre;
                p_direccion.Value = Direccion;
                p_localidad.Value = Localidad;
                p_telefono.Value = Telefono;
                p_estado.Value = 0;
                p_tipo.Value = Tipo;

                myCommand.Parameters.Add(p_nif);
                myCommand.Parameters.Add(p_email);
                myCommand.Parameters.Add(p_nombre);
                myCommand.Parameters.Add(p_direccion);
                myCommand.Parameters.Add(p_localidad);
                myCommand.Parameters.Add(p_telefono);
                myCommand.Parameters.Add(p_estado);
                myCommand.Parameters.Add(p_tipo);

                try
                {
                    myCommand.Prepare();
                    myCommand.ExecuteNonQuery();
                    cerrarConexion();
                    fin = "Insertado";
                }
                catch (MySqlException m)
                {
                    fin = "Error :" + m.Message;
                }
            }
            return fin;
        }
        public string bajaCliente()
        {
            string fin;
            if (comprobacionIdClave() == false)
            {
                fin = "La clave no corresponde con el ID";
            }
            else
            {
                if (comprobacionEstado("2") == true || comprobacionEstado("0") == true)
                {
                    fin = "Usuario ya dado de baja o esperando confirmacion";
                }
                else
                {
                    fin = iniciarConexion();
                    MySqlCommand myCommand = new MySqlCommand(null, connection);

                    string ins = "UPDATE cliente SET estado=@n_estado WHERE numId=@n_id";

                    myCommand.CommandText = ins;

                    MySqlParameter p_estado = new MySqlParameter("@n_estado", MySqlDbType.Text, 0);
                    MySqlParameter p_id = new MySqlParameter("@n_id", MySqlDbType.Text, 0);


                    p_estado.Value = 2;
                    p_id.Value = ID;

                    myCommand.Parameters.Add(p_estado);
                    myCommand.Parameters.Add(p_id);

                    try
                    {
                        myCommand.Prepare();
                        myCommand.ExecuteNonQuery();
                        cerrarConexion();
                        fin = "Baja realizada";
                    }
                    catch (MySqlException m)
                    {
                        fin = "Error :" + m.Message;
                    }
                }
            }
            return fin;
        }
        public string modificarCliente()
        {
            string fin = "";
            if (comprobacionIdClave() == false)
            {
                fin = "La clave no corresponde con el ID";
            }
            else
            {
                if (comprobacionEstado("1") != true)
                {
                    fin = "Usuario no activo";
                }
                else
                {
                    fin = iniciarConexion();
                    MySqlCommand myCommand = new MySqlCommand(null, connection);

                    string ins = "UPDATE cliente SET nif =@n_nif ,email =@n_email ,Nombre =@n_nombre ,Direccion =@n_direccion ,Localidad =@n_localidad ,telefono =@n_telefono ";
                    ins += "WHERE numId=@n_id";

                    myCommand.CommandText = ins;

                    MySqlParameter p_nif = new MySqlParameter("@n_nif", MySqlDbType.Text, 0);
                    MySqlParameter p_email = new MySqlParameter("@n_email", MySqlDbType.Text, 0);
                    MySqlParameter p_nombre = new MySqlParameter("@n_nombre", MySqlDbType.Text, 0);
                    MySqlParameter p_direccion = new MySqlParameter("@n_direccion", MySqlDbType.Text, 0);
                    MySqlParameter p_localidad = new MySqlParameter("@n_localidad", MySqlDbType.Text, 0);
                    MySqlParameter p_telefono = new MySqlParameter("@n_telefono", MySqlDbType.Text, 0);
                    MySqlParameter p_id = new MySqlParameter("@n_id", MySqlDbType.Text, 0);

                    p_nif.Value = NIF;
                    p_email.Value = Email;
                    p_nombre.Value = Nombre;
                    p_direccion.Value = Direccion;
                    p_localidad.Value = Localidad;
                    p_telefono.Value = Telefono;
                    p_id.Value = ID;

                    myCommand.Parameters.Add(p_nif);
                    myCommand.Parameters.Add(p_email);
                    myCommand.Parameters.Add(p_nombre);
                    myCommand.Parameters.Add(p_direccion);
                    myCommand.Parameters.Add(p_localidad);
                    myCommand.Parameters.Add(p_telefono);
                    myCommand.Parameters.Add(p_id);
                    

                    try
                    {
                        myCommand.Prepare();
                        myCommand.ExecuteNonQuery();
                        cerrarConexion();
                        fin = "Modificado";
                    }
                    catch (MySqlException m)
                    {
                      fin = "Error :" + m.Message;
                    }
                    
                }
            }
            return fin;
        }
    //-------------------------------------------------------------------------------------------------------
    public String claveAleatoria()
    {
        String clave = "";
        int tamano = 8;
        Random r = new Random(DateTime.Now.Millisecond);
        char[] ValueAfanumeric = {'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M','0','1','2','3','4','5','6','7','8','9'};//0-61

        for (int i = 0; i < tamano; i++)
        {
            clave += ValueAfanumeric[r.Next(0, 61)];
        }

        return clave;
    }
    public string iniciarConexion()
    {
        try
        {
            connectionString = "Server=localhost; Database=bd_gestor; Uid=root;";
            connection.ConnectionString = connectionString;
            connection.Open();
            return "OK";
        }
        catch (MySqlException)
        {
            return "ERROR";
        }
    }
    public void cerrarConexion()
    {
        connection.Close();
    }
    public bool comprobacionIdClave()
    {
        bool correcto = false;
        string r_clave = "";

        iniciarConexion();

        MySqlCommand inst = connection.CreateCommand();
        inst.CommandText = "SELECT ClaveAcceso FROM cliente WHERE numId=" + ID;
        MySqlDataReader reader = inst.ExecuteReader();
        reader.Read();

        r_clave = reader.GetString(0);

        if (Clave.Equals(r_clave))
        {
            correcto = true;
        }
        
        cerrarConexion();

        return correcto;
    }
    public int consultaID()
    {
        int r = -1;
        
        string r_id = "";

        iniciarConexion();

        MySqlCommand inst = connection.CreateCommand();
        inst.CommandText = "SELECT numId FROM cliente WHERE nif=@n_nif";

        MySqlParameter p_nif = new MySqlParameter("@n_nif", MySqlDbType.Text, 0);
        p_nif.Value = NIF;
        inst.Parameters.Add(p_nif);

        MySqlDataReader reader = inst.ExecuteReader();
        reader.Read();
        if (reader.HasRows != false)
        {
            r_id = reader.GetString(0);
            if (r_id != "" && r_id != null)
            {
                r = Convert.ToInt32(r_id);
            }
        }
        cerrarConexion();

        return r;
    }
    public bool comprobacionEstado(string i)
    {
        bool correcto = false;
        string r_clave = "";

        iniciarConexion();

        MySqlCommand inst = connection.CreateCommand();
        inst.CommandText = "SELECT Estado FROM cliente WHERE numId=" + ID;
        MySqlDataReader reader = inst.ExecuteReader();
        reader.Read();
        r_clave = reader.GetString(0);

        if (i.Equals(r_clave))
        {
            correcto = true;
        }

        cerrarConexion();

        return correcto;
    }
    public bool comprobacionTipo(string i)
    {
        bool correcto = false;
        string r_clave = "";

        iniciarConexion();

        MySqlCommand inst = connection.CreateCommand();
        inst.CommandText = "SELECT Tipo FROM cliente WHERE numId=" + ID;
        MySqlDataReader reader = inst.ExecuteReader();
        reader.Read();
        r_clave = reader.GetString(0);

        if (i.Equals(r_clave))
        {
            correcto = true;
        }

        cerrarConexion();

        return correcto;
    }
    public string recogerDatos()
    {
        string datos = "0";

        datos = iniciarConexion();

        MySqlCommand inst = connection.CreateCommand();
        inst.CommandText = "SELECT nif,Nombre,email,telefono,Localidad,Direccion FROM cliente WHERE numId=" + ID;
        MySqlDataReader reader = inst.ExecuteReader();
        reader.Read();

        for (int i = 0; i < 6; i++)
        {
            datos += "|" + reader.GetString(i);
        }


        cerrarConexion();

        return datos;
    }
}
