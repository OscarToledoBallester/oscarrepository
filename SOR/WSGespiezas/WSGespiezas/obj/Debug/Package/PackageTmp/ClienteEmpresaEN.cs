﻿using System;
using System.Net;
using System.Net.Mail;

public class ClienteEmpresaEN     
{
    /*private String NIF;
    private String Nombre;
    private String Email;
    private String Telefono;
    private String Localidad;
    private String Direccion;
    */
    private string ID;
    private string NIF;
    private string Nombre;
    private string Email;
    private string Telefono;
    private string Localidad;
    private string Direccion;
    private int Tipo;
    private int Estado;
    private string Clave;

    public string getClave()
    {
        return Clave;
    }
    public void setClave(string n_Clave)
    {
        Clave = n_Clave;
    }
    public string getID()
    {
        return ID;
    }
    public void setID(string n_ID)
    {
        ID = n_ID;
    }
    public string getNIF()
    {
        return NIF;
    }
    public void setNIF(string n_NIF)
    {
        NIF = n_NIF;
    }
    public string getNombre()
    {
        return Nombre;
    }
    public void setNombre(string nom)
    {
        Nombre = nom;
    }
    public string getEmail()
    {
        return Email;
    }
    public void setEmail(string mail)
    {
        Email = mail;
    }
    public string getTelefono()
    {
        return Telefono;
    }
    public void setTelefono(string n_Telefono)
    {
        Telefono = n_Telefono;
    }
    public string getLocalidad()
    {
        return Localidad;
    }
    public void setLocalidad(string n_Localidad)
    {
        Localidad = n_Localidad;
    }
    public string getDireccion()
    {
        return Direccion;
    }
    public void setDireccion(string n_Direccion)
    {
        Direccion = n_Direccion;
    }
    public int getTipo()
    {
        return Tipo;
    }
    public void setTipo(int n_Tipo)
    {
        Tipo = n_Tipo;
    }
    public int getEstado()
    {
        return Estado;
    }
    public void setEstado(int n_Estado)
    {
        Estado = n_Estado;
    }
    public ClienteEmpresaEN()
	{
        NIF = "";
        Nombre = "";
        Email = "";
        Telefono = "";
        Localidad = "";
        Direccion = "";
        Tipo = 0;
        Estado = 0;
	}
    public ClienteEmpresaEN(String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion, int tipo)
    {
        NIF = n_nif;
        Nombre = n_nombre;
        Email = n_email;
        Telefono = n_telefono;
        Localidad = n_localidad;
        Direccion = n_direccion;
        Tipo = tipo;
        Estado = 0;
    }
    //Para WebServices---------------------------------------------------------------------------------------
        public string altaCliente()
        {
            // 0 - Taller, 1 - Desguace
            //Ingresamos el cliente en la base de datos con el apartado de "Estado" como "Sin confirmar"
            //confirmarCliente();
            ClienteEmpresaCAD n_cliente = new ClienteEmpresaCAD(NIF, Nombre, Email, Telefono, Localidad, Direccion, Tipo);
            return n_cliente.altaCliente();
        }
        public string bajaCliente()
        {
            ClienteEmpresaCAD n_cliente = new ClienteEmpresaCAD();
            n_cliente.setClave(Clave);
            n_cliente.setID(ID);
            return n_cliente.bajaCliente();
        }
        public string modificarCliente()
        {
            ClienteEmpresaCAD n_cliente = new ClienteEmpresaCAD(NIF, Nombre, Email, Telefono, Localidad, Direccion, Tipo);
            n_cliente.setClave(Clave);
            n_cliente.setID(ID);
            return n_cliente.modificarCliente();
        }
    //-------------------------------------------------------------------------------------------------------
    public String claveAleatoria()
    {
        String clave = "";
        int tamano = 8;
        Random r = new Random(DateTime.Now.Millisecond);
        char[] ValueAfanumeric = {'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M','0','1','2','3','4','5','6','7','8','9'};//0-61

        for (int i = 0; i < tamano; i++)
        {
            clave += ValueAfanumeric[r.Next(0, 61)];
        }

        return clave;
    }
    public void confirmarCliente()
    {
        //Cambiamos valor del estado en la base de datos por "Confirmado"
        //Cargamos en dir_mail la direccion del cliente ,su nombre y su id
        String dir_mail = Email;//Cambiar
        String nombre_cliente = Nombre;//Cambiar
        String IDcliente = "id";
        String clave = claveAleatoria();
        String direccion_gespiezas = "gespiezas@gmail.com";
        String contrasena_gespiezas = "sor20132013";
        
        MailMessage confirma = new MailMessage();

        confirma.From = new MailAddress(direccion_gespiezas);
        confirma.To.Add(new MailAddress(dir_mail));
        confirma.Subject = "Gespiezas - Confirmación de alta";

        confirma.Body = "Gespiezas le da la bienvenida a nuestro servicio de compras " + nombre_cliente + "\n";
        confirma.Body += "Para poder acceder al servicio recuerde que debe introducir su clave de acceso\n\n";
        confirma.Body += "ID de cliente: " + IDcliente + "\n";
        confirma.Body += "Clave de acceso: " + clave + "\n\n";
        confirma.Body += "Atentamente, el equipo de Gespiezas\n";

        confirma.IsBodyHtml = false;
        confirma.Priority = MailPriority.Normal;

        SmtpClient smtp = new SmtpClient();
        smtp.Credentials = new NetworkCredential(direccion_gespiezas, contrasena_gespiezas);
        smtp.Host = "smtp.gmail.com";
        smtp.Port = 587;
        smtp.EnableSsl = true;

        smtp.Send(confirma);
    }
    public int consultaID()
    {
        ClienteEmpresaCAD n_cliente = new ClienteEmpresaCAD(NIF, Nombre, Email, Telefono, Localidad, Direccion, Tipo);
        int r = -1;

        r = n_cliente.consultaID();

        return r;
    }
}
