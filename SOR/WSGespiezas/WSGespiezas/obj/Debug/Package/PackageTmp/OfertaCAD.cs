﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace WSGespiezas
{
    public class OfertaCAD
    {
        MySqlConnection connection = new MySqlConnection();
        String connectionString;

        string numRefPedido;
        string numRefOfertaLocal;
        string numRefDesguace;
        string numRefPedidoDesguace;
        string idDesguace;
        PiezaEN[] piezas;
        String fechaFin;
        string fechaEntrega;
        int numPiezas;
        float precioTotal;
        int tamanyo;

        public void setIdDesguace(string i)
        {
            idDesguace = i;
        }
        public void setNumRefPedido(string i)
        {
            numRefPedido = i;
        }
        public void setNumRefOfertaLocal(string i)
        {
            numRefOfertaLocal = i;
        }
        public OfertaCAD()
        {
            numRefPedido = "";
            numRefDesguace = "";
            numRefPedidoDesguace = "";
            idDesguace = "";
            fechaFin = "";
            fechaEntrega = "";
            numPiezas = 0;
            precioTotal = 0;
            tamanyo = 0;
        }
        public OfertaCAD(string n_idDesguace, string n_RefPedido, string n_RefPedidoDesguace, string n_RefOferta, string n_fechaEntrega, string n_fechaCaducidad, PiezaEN[] n_piezas, float n_precioTotal, int n_tamanyoPiezas)
        {
            numRefPedido = n_RefPedido;
            numRefDesguace = n_RefOferta;
            idDesguace = n_idDesguace;
            fechaFin = n_fechaCaducidad;
            fechaEntrega = n_fechaEntrega;
            precioTotal = n_precioTotal;
            piezas = n_piezas;
            tamanyo = n_tamanyoPiezas;
            numRefPedidoDesguace = n_RefPedidoDesguace;
        }
        public int insertarOferta()
        {
            int resultado = 0;

            //*********************************************************************************

            //Insertamos el Pedido
            iniciarConexion();

            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "INSERT INTO oferta (NumRefDesguace,precioTotal,FechaCaducidad,estado,DesguaceId,PedidoNumRefLocal,FechaEntrega) ";
            ins += "VALUES (@n_NumRefDesguace, @n_precioTotal, @n_FechaCaducidad, @n_estado, @n_DesguaceId, @n_PedidoNumRefLocal, @n_FechaEntrega)";

            myCommand.CommandText = ins;

            MySqlParameter p_NumRefDesguace = new MySqlParameter("@n_NumRefDesguace", MySqlDbType.Text, 0);
            MySqlParameter p_precioTotal = new MySqlParameter("@n_precioTotal", MySqlDbType.Text, 0);
            MySqlParameter p_FechaCaducidad = new MySqlParameter("@n_FechaCaducidad", MySqlDbType.Text, 0);
            MySqlParameter p_estado = new MySqlParameter("@n_estado", MySqlDbType.Text, 0);
            MySqlParameter p_DesguaceId = new MySqlParameter("@n_DesguaceId", MySqlDbType.Text, 0);
            MySqlParameter p_PedidoNumRefLocal = new MySqlParameter("@n_PedidoNumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_FechaEntrega = new MySqlParameter("@n_FechaEntrega", MySqlDbType.Text, 0);

            p_NumRefDesguace.Value = numRefDesguace;
            p_precioTotal.Value = precioTotal;
            p_FechaCaducidad.Value = fechaFin;
            p_estado.Value = 0;
            p_DesguaceId.Value = idDesguace;
            p_PedidoNumRefLocal.Value = numRefPedido;
            p_FechaEntrega.Value = fechaEntrega;

            myCommand.Parameters.Add(p_NumRefDesguace);
            myCommand.Parameters.Add(p_precioTotal);
            myCommand.Parameters.Add(p_FechaCaducidad);
            myCommand.Parameters.Add(p_estado);
            myCommand.Parameters.Add(p_DesguaceId);
            myCommand.Parameters.Add(p_PedidoNumRefLocal);
            myCommand.Parameters.Add(p_FechaEntrega);

            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                resultado = 0;
            }
            catch (MySqlException m)
            {
                resultado = 1;
            }

            cerrarConexion();

            //Recuperamos nuestro numero de referencia del pedido
            string r_id = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT NumRefLocal FROM oferta WHERE NumRefDesguace=@n_refDesguace AND DesguaceId=@n_idDesguace";

            MySqlParameter p_refDesguace = new MySqlParameter("@n_refDesguace", MySqlDbType.Text, 0);
            MySqlParameter p_idDesguace = new MySqlParameter("@n_idDesguace", MySqlDbType.Text, 0);
            p_refDesguace.Value = numRefDesguace;
            p_idDesguace.Value = idDesguace;
            inst.Parameters.Add(p_refDesguace);
            inst.Parameters.Add(p_idDesguace);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();
            if (reader.HasRows != false)
            {
                r_id = reader.GetString(0);
            }
            cerrarConexion();
            string referencia = r_id;

            //Insertamos las piezas de la oferta
            for (int i = 0; i < numPiezas; i++)
            {
                piezas[i].InsertarPiezaOferta(referencia);
            }

            //*********************************************************************************

            return resultado;
        }
        public int modificarEstado(int i)
        {
            int resultado = 0;

            string fin = "";

            fin = iniciarConexion();
            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "UPDATE oferta SET estado =@n_estado ";
            ins += "WHERE NumRefLocal=@n_NumRefLocal";

            myCommand.CommandText = ins;

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_estado = new MySqlParameter("@n_estado", MySqlDbType.Text, 0);

            p_NumRefLocal.Value = numRefOfertaLocal;
            p_estado.Value = i;

            myCommand.Parameters.Add(p_estado);
            myCommand.Parameters.Add(p_NumRefLocal);


            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                fin = "Modificado";
                resultado = 0;
            }
            catch (MySqlException m)
            {
                fin = "Error :" + m.Message;
                resultado = -4;
            }

            return resultado;
        }
        public int EfectoCancelacionPedido()
        {
            int resultado = 0;

            string fin = "";

            fin = iniciarConexion();
            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "UPDATE oferta SET Estado =@n_EstadoA ";
            ins += "WHERE PedidoNumRefLocal=@n_PedidoNumRefLocal AND Estado=@n_EstadoB";

            myCommand.CommandText = ins;

            MySqlParameter p_EstadoA = new MySqlParameter("@n_EstadoA", MySqlDbType.Text, 0);
            MySqlParameter p_PedidoNumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_EstadoB = new MySqlParameter("@n_EstadoB", MySqlDbType.Text, 0);

            p_EstadoA.Value = 5;
            p_PedidoNumRefLocal.Value = numRefPedido;
            p_EstadoB.Value = 0;

            myCommand.Parameters.Add(p_EstadoA);
            myCommand.Parameters.Add(p_PedidoNumRefLocal);
            myCommand.Parameters.Add(p_EstadoB);


            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                fin = "Modificado";
                resultado = 0;
            }
            catch (MySqlException m)
            {
                fin = "Error :" + m.Message;
                resultado = -4;
            }

            return resultado;
        }
        public int EstadoOferta()
        {
            int r = -1;

            string r_id = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT estado FROM oferta WHERE NumRefLocal=@n_NumRefLocal";

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            p_NumRefLocal.Value = numRefOfertaLocal;
            inst.Parameters.Add(p_NumRefLocal);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();
            if (reader.HasRows != false)
            {
                r_id = reader.GetString(0);
                if (r_id != "" && r_id != null)
                {
                    r = Convert.ToInt32(r_id);
                }
            }
            cerrarConexion();

            return r;
        }
        public string OfertasPedido()
        {
            string devuelve = "";
            PiezaCAD piezaOferta = new PiezaCAD();

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT NumRefLocal,FechaCaducidad,estado,precioTotal FROM oferta WHERE NumRefLocal=@n_NumRefLocal";

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            p_NumRefLocal.Value = numRefOfertaLocal;
            inst.Parameters.Add(p_NumRefLocal);

            MySqlDataReader reader = inst.ExecuteReader();
            while (reader.Read())
            {
                devuelve += "#";
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    devuelve += reader.GetString(i) + "*";
                }
                //Insertamos las piezas
                devuelve += piezaOferta.piezasEnOferta(numRefOfertaLocal);
                devuelve += "#";
            }
            
            cerrarConexion();
            
            return devuelve;
        }
        public bool CorrespondenciaDesguaceOferta()
        {
            bool correcto = false;

            string r_id = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT DesguaceId FROM oferta WHERE NumRefLocal=@n_NumRefLocal";

            MySqlParameter p_NumRefLocal = new MySqlParameter("@n_NumRefLocal", MySqlDbType.Text, 0);
            p_NumRefLocal.Value = numRefOfertaLocal;
            inst.Parameters.Add(p_NumRefLocal);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();
            if (reader.HasRows != false)
            {
                r_id = reader.GetString(0);
                if (r_id != "" && r_id != null)
                {
                    if (r_id.Equals(idDesguace))
                    {
                        correcto = true;
                    }
                }
            }
            cerrarConexion();

            return correcto;
        }
        public string iniciarConexion()
        {
            try
            {
                connectionString = "Server=localhost; Database=bd_gestor; Uid=root;";
                connection.ConnectionString = connectionString;
                connection.Open();
                return "OK";
            }
            catch (MySqlException)
            {
                return "ERROR";
            }
        }
        public void cerrarConexion()
        {
            connection.Close();
        }
    }
}