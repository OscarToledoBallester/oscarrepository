﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WSGespiezas
{
    /// <summary>
    /// Descripción breve de Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
       
    public class Service1 : System.Web.Services.WebService
    {
        //******************************************************************************************
        //*******************************FUNCIONES DE ALTA/BAJA/MOD*********************************
        //******************************************************************************************

        [WebMethod]
        public int AltaTaller(String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion)
        {
            ClienteEmpresaEN ce = new ClienteEmpresaEN(n_nif, n_nombre, n_email, n_telefono, n_localidad, n_direccion, 0);
            String est = "";
            ce.setEmail(n_email);
            ce.setNombre(n_nombre);
            est = ce.altaCliente();

            return ce.consultaID();

        }
        [WebMethod]
        public int AltaDesguace(String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion)
        {
            ClienteEmpresaEN ce = new ClienteEmpresaEN(n_nif, n_nombre, n_email, n_telefono, n_localidad, n_direccion, 1);
            String est = "";
            ce.setEmail(n_email);
            ce.setNombre(n_nombre);
            est =  ce.altaCliente();

            return Convert.ToInt32(est);
        }
        [WebMethod]
        public int Baja(String n_id, String n_clave)
        {
            ClienteEmpresaEN ce = new ClienteEmpresaEN();
            ce.setClave(n_clave);
            ce.setID(n_id);
            int res = Convert.ToInt32(ce.bajaCliente());
            return res;

        }
        [WebMethod]
        public int Modificacion(String n_id, string n_clave,String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion)
        {
            ClienteEmpresaEN ce = new ClienteEmpresaEN(n_nif, n_nombre, n_email, n_telefono, n_localidad, n_direccion, 0);
            ce.setClave(n_clave);
            ce.setID(n_id);
            int res = Convert.ToInt32(ce.modificarCliente());
            return res;

        }
        [WebMethod]
        public string RecogerDatos(string idCliente, string claveCliente)
        {
            string devuelve = "";
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idCliente);
            comprueba.setClave(claveCliente);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    devuelve = comprueba.recogerDatos();
                }
                else
                {
                    devuelve = "-2";
                }
            }
            else
            {
                devuelve = "-1";
            }
            return devuelve;
        }
        //******************************************************************************************
        //*******************************FUNCIONES DE TALLER/CLIENTE********************************
        //******************************************************************************************

        [WebMethod]
        public int NuevoPedido(string idTaller, string claveTaller,string numRefTaller,string fecha_fin, int criterio, bool automatico, float precioMax, string[] referencias, string[] descripciones, int []cantidades, float []precios, int []estados) 
        {
            int devuelve = 0;
            //0  correcto, -1  error clave, -2  error estado, -3  Error tipo
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        //correcto
                        PedidoEN nuevoPedido = new PedidoEN();
                        nuevoPedido.NuevoPedido(idTaller, numRefTaller, fecha_fin, criterio, automatico, referencias, descripciones, cantidades, precios,  estados);
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public int ModificarPedido(string idTaller, string claveTaller, string idPedido, string fecha_modificada, bool automatica, int criterio)
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        //correcto
                        PedidoEN mod = new PedidoEN();
                        mod.setNumRefPedidoLocal(idPedido);
                        mod.setFechaFin(fecha_modificada);
                        mod.setAutomatica(automatica);
                        mod.setCriterio(criterio);
                        mod.setIdTaller(idTaller);
                        mod.ModificarPedido();
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public int CancelarPedido(string idTaller, string claveTaller, string idPedido) 
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        PedidoEN mod = new PedidoEN();
                        mod.setNumRefPedidoLocal(idPedido);
                        mod.setIdTaller(idTaller);
                        devuelve = mod.ModificarPedido();
                        if (devuelve == 0)
                        {
                            OfertaEN modOf = new OfertaEN();
                            modOf.setNumRefPedido(idPedido);
                            modOf.EfectoCancelacionPedido();
                        }
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public int SeleccionarOferta(string idTaller, string claveTaller, string idPedido, string idOferta) 
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        //correcto
                        //Comprobamos que el pedido corresponde al cliente
                        PedidoEN ped = new PedidoEN();
                        ped.setIdTaller(idTaller);
                        ped.setNumRefPedidoLocal(idPedido);

                        bool correcto = ped.CorrespondenciaClientePedido();

                        if (correcto == true)
                        {
                            OfertaEN modOf = new OfertaEN();
                            modOf.setNumRefPedido(idPedido);
                            modOf.SeleccionarOferta();
                        }
                        else
                        {
                            devuelve = -4;
                        }
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public int RechazarOferta(string idTaller, string claveTaller, string idPedido, string idOferta) 
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        //Comprobamos que el pedido corresponde al cliente
                        PedidoEN ped = new PedidoEN();
                        ped.setIdTaller(idTaller);
                        ped.setNumRefPedidoLocal(idPedido);

                        bool correcto = ped.CorrespondenciaClientePedido();

                        if (correcto == true)
                        {
                            OfertaEN modOf = new OfertaEN();
                            modOf.setNumRefOfertaLocal(idOferta);
                            modOf.RechazarOferta();
                        }
                        else
                        {
                            devuelve = -4;
                        }
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public string RecogerOfertas(string idTaller, string claveTaller, string RefPedidoLocal)
        {
            string devuelve = "";
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        //correcto
                        //Comprobamos que el pedido corresponde al cliente
                        PedidoEN ped = new PedidoEN();
                        ped.setIdTaller(idTaller);
                        ped.setNumRefPedidoLocal(RefPedidoLocal);

                        bool correcto = ped.CorrespondenciaClientePedido();

                        if (correcto == true)
                        {
                            OfertaEN modOf = new OfertaEN();
                            modOf.setNumRefPedido(RefPedidoLocal);
                            modOf.OfertasPedido();
                        }
                        else
                        {
                            devuelve = "-4";
                        }
                    }
                    else
                    {
                        devuelve = "-3";
                    }
                }
                else
                {
                    devuelve = "-2";
                }
            }
            else
            {
                devuelve = "-1";
            }
            return devuelve;
        }
        [WebMethod]
        public int RecogerActualizacionOfertas(string idTaller, string claveTaller, string RefPedidoLocal) 
        {
            //Seleccion automatica y caducidades de las ofertas asociadas a un pedido
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idTaller);
            comprueba.setClave(claveTaller);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("0"))
                    {
                        //correcto
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }

        
        //******************************************************************************************
        //*******************************FUNCIONES DE DESGUACE**************************************
        //******************************************************************************************

        [WebMethod]
        public int NuevaOferta(string idDesguace, string claveDesguace, string RefPedidoLocal,string RefPedidoDesguace, string RefOferta, string fecha_entrega, string fechaCaducidad, string[] referencias, float[] precios,int[] estados, float precioTotal) 
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idDesguace);
            comprueba.setClave(claveDesguace);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("1"))
                    {
                        //Cliente correcto
                        OfertaEN nuevaOferta = new OfertaEN();
                        nuevaOferta.NuevaOferta(idDesguace, RefPedidoLocal, RefPedidoDesguace, RefOferta, fecha_entrega, fechaCaducidad, referencias, precios, estados, precioTotal);
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public int CancelarOferta(string idDesguace, string claveDesguace, string idPedido, string idOferta) 
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idDesguace);
            comprueba.setClave(claveDesguace);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("1"))
                    {
                        //correcto
                        //Comprobamos que la oferta corresponde al desguace
                        OfertaEN ped = new OfertaEN();
                        ped.setIdDesguace(idDesguace);
                        ped.setNumRefOfertaLocal(idOferta);

                        bool correcto = ped.CorrespondenciaDesguaceOferta();

                        if (correcto == true)
                        {
                            OfertaEN modOf = new OfertaEN();
                            modOf.setNumRefOfertaLocal(idOferta);
                            modOf.CancelarOferta();
                        }
                        else
                        {
                            devuelve = -4;
                        }
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
        }
        [WebMethod]
        public int RecogerPedido(string idDesguace, string claveDesguace, string RefPedidoLocal) //CAMBIAR EL TIPO DE RETURN
        {
            int devuelve = 0;
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idDesguace);
            comprueba.setClave(claveDesguace);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("1"))
                    {
                        //correcto
                        //Comprobar que el pedido está activo
                    }
                    else
                    {
                        devuelve = -3;
                    }
                }
                else
                {
                    devuelve = -2;
                }
            }
            else
            {
                devuelve = -1;
            }
            return devuelve;
            
        }
        [WebMethod]
        public string RecogerEstadoOfertas(string idDesguace, string claveDesguace, string []RefOferta) 
        {
            string devuelve = "*";
            //0 - correcto, 1 - error clave, 2 - error estado
            ClienteEmpresaCAD comprueba = new ClienteEmpresaCAD();
            comprueba.setID(idDesguace);
            comprueba.setClave(claveDesguace);
            if (comprueba.comprobacionIdClave() == true)
            {
                if (comprueba.comprobacionEstado("1"))
                {
                    if (comprueba.comprobacionTipo("1"))
                    {
                        //correcto
                        OfertaEN ofe;
                        OfertaEN ped;
                        //Comprobar que los pedidos están activos
                        //Comprobamos que la oferta corresponde al desguace
                        ped = new OfertaEN();
                        for (int i = 0; i < RefOferta.Length; i++)
                        {
                            ped.setIdDesguace(idDesguace);
                            ped.setNumRefOfertaLocal(RefOferta[i]);

                            bool correcto = ped.CorrespondenciaDesguaceOferta();

                            if (correcto == true)
                            {
                                ofe = new OfertaEN();
                            
                                    ofe.setNumRefOfertaLocal(RefOferta[i]);
                                    devuelve += ofe.EstadoOferta() + "*";
                                
                            }else
                            {
                                devuelve += "-4*";
                            }
                        }
                        
                        
                    }
                    else
                    {
                        devuelve = "-3";
                    }
                }
                else
                {
                    devuelve = "-2";
                }
            }
            else
            {
                devuelve = "-1";
            }
            return devuelve;
        }

    }
}