﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace WSGespiezas
{
    public class PiezaCAD
    {
        MySqlConnection connection = new MySqlConnection();
        String connectionString;

        private string NumRef;
        private string Descripcion;
        private int Cantidad;
        private float Precio;
        private int Estado;

        public string getNumRef()
        {
            return NumRef;
        }
        public void setNumRef(string n_NumRef)
        {
            NumRef = n_NumRef;
        }
        public string getDescripcion()
        {
            return Descripcion;
        }
        public void setDescripcion(string n_Descripcion)
        {
            Descripcion = n_Descripcion;
        }
        public int getCantidad()
        {
            return Cantidad;
        }
        public void setCantidad(int n_Cantidad)
        {
            Cantidad = n_Cantidad;
        }
        public float getPrecio()
        {
            return Precio;
        }
        public void setPrecio(float n_Precio)
        {
            Precio = n_Precio;
        }
        public int getEstado()
        {
            return Estado;
        }
        public void setEstado(int n_Estado)
        {
            Estado = n_Estado;
        }
        public PiezaCAD()
        {
            NumRef = "";
            Descripcion = "";
            Cantidad = 0;
            Precio = 0;
            Estado = 0;
        }
        public PiezaCAD(string n_NumRef, string n_Descripcion, int n_Cantidad, float n_Precio, int n_Estado)
        {
            NumRef = n_NumRef;
            Descripcion = n_Descripcion;
            Cantidad = n_Cantidad;
            Precio = n_Precio;
            Estado = n_Estado;
        }
        public int InsertarPieza()
        {
            int resultado = 0;
            bool existe = ExistePieza();

            //Si no existe insertamos
            if (existe == false)
            {
                iniciarConexion();
                MySqlCommand myCommand = new MySqlCommand(null, connection);

                string ins = "INSERT INTO piezauniversal (NumRefUniv,Descripcion) ";
                ins += "VALUES (@n_NumRefUniv,@n_Descripcion)";

                myCommand.CommandText = ins;

                MySqlParameter p_NumRefUniv = new MySqlParameter("@n_NumRefUniv", MySqlDbType.Text, 0);
                MySqlParameter p_Descripcion = new MySqlParameter("@n_Descripcion", MySqlDbType.Text, 0);

                p_NumRefUniv.Value = NumRef;
                p_Descripcion.Value = Descripcion;

                myCommand.Parameters.Add(p_NumRefUniv);
                myCommand.Parameters.Add(p_Descripcion);

                try
                {
                    myCommand.Prepare();
                    myCommand.ExecuteNonQuery();
                    cerrarConexion();
                    resultado = 0;
                }
                catch (MySqlException m)
                {
                    resultado = 1;
                    string fin = "Error :" + m.Message;
                }
            }

            return resultado;
        }
        public bool ExistePieza()
        {
            bool existe = true;
            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT * FROM piezauniversal WHERE NumRefUniv=@n_referencia";

            MySqlParameter p_referencia = new MySqlParameter("@n_referencia", MySqlDbType.Text, 0);
            p_referencia.Value = NumRef;
            inst.Parameters.Add(p_referencia);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();

            if (reader.HasRows == false)
            {
                //No esta en la BD por lo que la insertamos
                existe = false;
            }
            cerrarConexion();

            return existe;
        }
        public int IdPieza()
        {
            int ID = -1;
            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT idPieza FROM piezauniversal WHERE NumRefUniv=@n_referencia";

            MySqlParameter p_referencia = new MySqlParameter("@n_referencia", MySqlDbType.Text, 0);
            p_referencia.Value = NumRef;
            inst.Parameters.Add(p_referencia);

            MySqlDataReader reader = inst.ExecuteReader();
            reader.Read();

            if (reader.HasRows == false)
            {
                //No esta en la BD por lo que la insertamos
                ID = -1;
            }
            else
            {
                ID = Convert.ToInt32(reader.GetString(0));
            }
            cerrarConexion();

            return ID;
        }
        public int InsertarPiezaPedido(string ReferenciaPedido)
        {
            int resultado = 0;
            int ID = IdPieza();
            iniciarConexion();
            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "INSERT INTO piezauniversalenpedido (IdPieza,PiezaNumRefUniv,PedidoNumRefLocal,Cantidad,PrecioMax,Estado) ";
            ins += "VALUES (@n_IdPieza,@n_NumRefUniv,@n_PedidoNumRefLocal,@n_Cantidad,@n_PrecioMax,@n_Estado)";

            myCommand.CommandText = ins;

            MySqlParameter p_IdPieza = new MySqlParameter("@n_IdPieza", MySqlDbType.Text, 0);
            MySqlParameter p_NumRefUniv = new MySqlParameter("@n_NumRefUniv", MySqlDbType.Text, 0);
            MySqlParameter p_PedidoNumRefLocal = new MySqlParameter("@n_PedidoNumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_Cantidad = new MySqlParameter("@n_Cantidad", MySqlDbType.Text, 0);
            MySqlParameter p_PrecioMax = new MySqlParameter("@n_PrecioMax", MySqlDbType.Text, 0);
            MySqlParameter p_Estado = new MySqlParameter("@n_Estado", MySqlDbType.Text, 0);

            p_IdPieza.Value = ID;
            p_NumRefUniv.Value = NumRef;
            p_PedidoNumRefLocal.Value = ReferenciaPedido;
            p_Cantidad.Value = Cantidad;
            p_PrecioMax.Value = Precio;
            p_Estado.Value = Estado;


            myCommand.Parameters.Add(p_IdPieza);
            myCommand.Parameters.Add(p_NumRefUniv);
            myCommand.Parameters.Add(p_PedidoNumRefLocal);
            myCommand.Parameters.Add(p_Cantidad);
            myCommand.Parameters.Add(p_PrecioMax);
            myCommand.Parameters.Add(p_Estado);

            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                resultado = 0;
            }
            catch (MySqlException m)
            {
                resultado = 1;
                string fin = "Error :" + m.Message;
            }

            return resultado;
        }
        public int InsertarPiezaOferta(string ReferenciaOferta)
        {
            int resultado = 0;
            int ID = IdPieza();
            iniciarConexion();
            MySqlCommand myCommand = new MySqlCommand(null, connection);

            string ins = "INSERT INTO piezauniversal_has_oferta (IdPieza,PiezaUniversal_idPieza,Oferta_NumRefLocal,Cantidad,PrecioMax,Estado) ";
            ins += "VALUES (@n_IdPieza,@n_PiezaUniversal_idPieza,@n_Oferta_NumRefLocal,@n_Cantidad,@n_PrecioMax,@n_Estado)";

            myCommand.CommandText = ins;

            MySqlParameter p_IdPieza = new MySqlParameter("@n_IdPieza", MySqlDbType.Text, 0);
            MySqlParameter p_PiezaUniversal_idPieza = new MySqlParameter("@n_PiezaUniversal_idPieza", MySqlDbType.Text, 0);
            MySqlParameter p_Oferta_NumRefLocal = new MySqlParameter("@n_Oferta_NumRefLocal", MySqlDbType.Text, 0);
            MySqlParameter p_Cantidad = new MySqlParameter("@n_Cantidad", MySqlDbType.Text, 0);
            MySqlParameter p_PrecioMax = new MySqlParameter("@n_PrecioMax", MySqlDbType.Text, 0);
            MySqlParameter p_Estado = new MySqlParameter("@n_Estado", MySqlDbType.Text, 0);

            p_IdPieza.Value = ID;
            p_PiezaUniversal_idPieza.Value = NumRef;
            p_Oferta_NumRefLocal.Value = ReferenciaOferta;
            p_Cantidad.Value = Cantidad;
            p_PrecioMax.Value = Precio;
            p_Estado.Value = Estado;

            myCommand.Parameters.Add(p_IdPieza);
            myCommand.Parameters.Add(p_PiezaUniversal_idPieza);
            myCommand.Parameters.Add(p_Oferta_NumRefLocal);
            myCommand.Parameters.Add(p_Cantidad);
            myCommand.Parameters.Add(p_PrecioMax);
            myCommand.Parameters.Add(p_Estado);

            try
            {
                myCommand.Prepare();
                myCommand.ExecuteNonQuery();
                cerrarConexion();
                resultado = 0;
            }
            catch (MySqlException m)
            {
                resultado = 1;
                string fin = "Error :" + m.Message;
            }

            return resultado;
        }
        public string piezasEnOferta(string RefOferta)
        {
            string devuelve = "";

            iniciarConexion();

            MySqlCommand inst = connection.CreateCommand();
            inst.CommandText = "SELECT PiezaUniversal_idPieza,Precio,estado FROM piezauniversal_has_oferta WHERE Oferta_NumRefLocal=@n_Oferta_NumRefLocal";

            MySqlParameter p_Oferta_NumRefLocal = new MySqlParameter("@n_Oferta_NumRefLocal", MySqlDbType.Text, 0);
            p_Oferta_NumRefLocal.Value = RefOferta;
            inst.Parameters.Add(p_Oferta_NumRefLocal);

            MySqlDataReader reader = inst.ExecuteReader();
            while (reader.Read())
            {
                devuelve += "|";
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    devuelve += reader.GetString(i);
                    if (i != reader.FieldCount - 1)
                    {
                        devuelve += "-";
                    }
                }
                //Insertamos las piezas

                devuelve += "|";
            }

            cerrarConexion();

            return devuelve;
        }
        public string iniciarConexion()
        {
            try
            {
                connectionString = "Server=localhost; Database=bd_gestor; Uid=root;";
                connection.ConnectionString = connectionString;
                connection.Open();
                return "OK";
            }
            catch (MySqlException)
            {
                return "ERROR";
            }
        }
        public void cerrarConexion()
        {
            connection.Close();
        }
    }
}