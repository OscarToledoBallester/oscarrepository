﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSGespiezas
{
    public class PedidoEN
    {
        string numRefTaller;
        string idTaller;
        string numRefPedidoLocal;
        PiezaEN[] piezas;
        String fechaFin;
        Boolean automatico;
        int criterio;
        int numPiezas;

        public void setNumRefPedidoLocal(string n)
        {
            numRefPedidoLocal = n;
        }
        public void setFechaFin(string n)
        {
            fechaFin = n;
        }
        public void setAutomatica(bool n)
        {
            automatico = n;
        }
        public void setCriterio(int n)
        {
            criterio = n;
        }
        public void setIdTaller(string n)
        {
            idTaller = n;
        }
        public PedidoEN()
        {
            numRefTaller = "";
            idTaller = "";
            fechaFin = "";
            automatico = false;
            criterio = 0;
            numPiezas = 0;
        }
        PedidoEN(PiezaEN[] np, String fecha, Boolean auto, int crit)
        {
            piezas = np;
            fechaFin = fecha;
            automatico = auto;
            criterio = crit;
        }
        public int NuevoPedido(string n_idTaller, string n_numRefTaller, string n_fechaFin, int n_criterio, bool n_automatico, string[] referencias, string[] descripciones, int[] cantidades, float[] precios, int[] estados)
        {
            idTaller = n_idTaller;
            fechaFin = n_fechaFin;
            criterio = n_criterio;
            automatico = n_automatico;
            int tamanyo = referencias.Length;
            numPiezas = tamanyo;
            piezas = new PiezaEN[tamanyo];
            for (int i = 0; i < tamanyo; i++)
            {
                piezas[i] = new PiezaEN();
            }

            numRefTaller = n_numRefTaller;

            for (int i = 0; i < tamanyo; i++)
            {
                piezas[i].setNumRef(referencias[i]);
                piezas[i].setDescripcion(descripciones[i]);
                piezas[i].setCantidad(cantidades[i]);
                piezas[i].setPrecio(precios[i]);
                piezas[i].setEstado(estados[i]);
            }
            int res = InsertarPedido();
            return res;
        }
        public int ModificarPedido()
        {
            int resultado = 0;

            PedidoCAD mod = new PedidoCAD();
            mod.setNumRefPedidoLocal(numRefPedidoLocal);
            mod.setFechaFin(fechaFin);
            mod.setAutomatica(automatico);
            mod.setCriterio(criterio);
            mod.setIdTaller(idTaller);
            int estadoPedido = mod.EstadoPedido();
            if (estadoPedido == 0)
            {
                resultado = mod.ModificarPedido();
            }
            return resultado;
        }
        public int CancelarPedido()
        {
            int resultado = 0;

            PedidoCAD mod = new PedidoCAD();
            mod.setNumRefPedidoLocal(numRefPedidoLocal);
            mod.setIdTaller(idTaller);
            int estadoPedido = mod.EstadoPedido();
            if (estadoPedido < 3)
            {
                resultado = mod.ModificarEstado(4);
            }
            return resultado;
        }
        int InsertarPedido()
        {
            int resultado = 0;

            PedidoCAD nuevo = new PedidoCAD(idTaller, numRefTaller, numPiezas, piezas, fechaFin, automatico, criterio);
            resultado = nuevo.InsertarPedido();

            return resultado;
        }
        public bool CorrespondenciaClientePedido()
        {
            bool correcto = true;

            PedidoCAD comprueba = new PedidoCAD();
            comprueba.setIdTaller(idTaller);
            comprueba.setNumRefPedidoLocal(numRefPedidoLocal);
            correcto = comprueba.CorrespondenciaClientePedido();

            return correcto;
        }
        public string RecogerPedidos()
        {
            string pedidos = "";

            PedidoCAD recoge = new PedidoCAD();
            recoge.setIdTaller(idTaller);
            pedidos = recoge.RecogePedidos();

            return pedidos;
        }
    }
}