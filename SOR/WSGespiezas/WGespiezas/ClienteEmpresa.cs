﻿using System;
using System.Net;
using System.Net.Mail;

public class ClienteEmpresa     
{
    /*private String NIF;
    private String Nombre;
    private String Email;
    private String Telefono;
    private String Localidad;
    private String Direccion;
    */
    private String NIF;
    private String Nombre;
    private String Email;
    private String Telefono;
    private String Localidad;
    private String Direccion;
    private int Tipo;
    //0 - Sin Confirmar, 1 - Confirmado, 2 - Baja
    private int Estado;
    public ClienteEmpresa()
	{
        NIF = "";
        Nombre = "";
        Email = "";
        Telefono = "";
        Localidad = "";
        Direccion = "";
        Tipo = 0;
        Estado = 0;
	}
    public ClienteEmpresa(String n_nif, String n_nombre, String n_email, String n_telefono, String n_localidad, String n_direccion, int tipo)
    {
        NIF = n_nif;
        Nombre = n_nombre;
        Email = n_email;
        Telefono = n_telefono;
        Localidad = n_localidad;
        Direccion = n_direccion;
        Tipo = tipo;
        Estado = 0;
    }
    public String claveAleatoria()
    {
        String clave = "";
        int tamano = 8;
        Random r = new Random(DateTime.Now.Millisecond);
        char[] ValueAfanumeric = {'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M','0','1','2','3','4','5','6','7','8','9'};//0-61

        for (int i = 0; i < tamano; i++)
        {
            clave += ValueAfanumeric[r.Next(0, 61)];
        }

        return clave;
    }
    public void confirmarCliente(String id_cliente)
    {
        //Cambiamos valor del estado en la base de datos por "Confirmado"
        //Cargamos en dir_mail la direccion del cliente ,su nombre y su id
        String dir_mail = "0sk4rto@gmail.com";//Cambiar
        String nombre_cliente = "Oscar Toledo";//Cambiar
        String IDcliente = "id";
        String clave = claveAleatoria();
        String direccion_gespiezas = "gespiezas@gmail.com";
        String contrasena_gespiezas = "sor20132013";
        
        MailMessage confirma = new MailMessage();

        confirma.From = new MailAddress(direccion_gespiezas);
        confirma.To.Add(dir_mail);
        confirma.Subject = "Gespiezas - Confirmación de alta";

        confirma.Body = "Gespiezas le da la bienvenida a nuestro servicio de compras " + nombre_cliente + "/n";
        confirma.Body += "Para poder acceder al servicio recuerde que debe introducir su clave de acceso/n/n";
        confirma.Body += "ID de cliente: " + IDcliente + "/n";
        confirma.Body += "Clave de acceso: " + clave + "/n/n";
        confirma.Body += "Atentamente, el equipo de Gespiezas/n";

        confirma.IsBodyHtml = false;
        confirma.Priority = MailPriority.Normal;

        SmtpClient smtp = new SmtpClient();
        smtp.Credentials = new NetworkCredential(direccion_gespiezas, contrasena_gespiezas);
        smtp.Host = "smtp.gmail.com";
        smtp.Port = 587;
        smtp.EnableSsl = true;
    }
}
