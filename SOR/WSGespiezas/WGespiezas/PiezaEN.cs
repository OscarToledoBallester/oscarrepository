﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class PiezaEN
    {
        private string NumRef;
        private string Descripcion;
        private int Cantidad;
        private float Precio;
        private int Estado;

        public string getNumRef()
        {
            return NumRef;
        }
        public void setNumRef(string n_NumRef)
        {
            NumRef = n_NumRef;
        }
        public string getDescripcion()
        {
            return Descripcion;
        }
        public void setDescripcion(string n_Descripcion)
        {
            Descripcion = n_Descripcion;
        }
        public int getCantidad()
        {
            return Cantidad;
        }
        public void setCantidad(int n_Cantidad)
        {
            Cantidad = n_Cantidad;
        }
        public float getPrecio()
        {
            return Precio;
        }
        public void setPrecio(float n_Precio)
        {
            Precio = n_Precio;
        }
        public int getEstado()
        {
            return Estado;
        }
        public void setEstado(int n_Estado)
        {
            Estado = n_Estado;
        }
        public PiezaEN()
        {
            NumRef = "";
            Descripcion = "";
            Cantidad = 0;
            Precio = 0;
            Estado = 0;
        }
        public PiezaEN(PiezaEN n_pieza)
        {
            NumRef = n_pieza.getNumRef();
            Descripcion = n_pieza.getDescripcion(); ;
            Cantidad = n_pieza.getCantidad();
            Precio = n_pieza.getPrecio();
            Estado = n_pieza.getEstado();
        }
        public int InsertarPieza()
        {
            int resultado = 0;

            PiezaCAD nuevo = new PiezaCAD(NumRef, Descripcion, Cantidad, Precio, Estado);
            resultado = nuevo.InsertarPieza();

            return resultado;
        }
        public int InsertarPiezaPedido(string ReferenciaPedido)
        {
            int resultado = 0;

            PiezaCAD nuevo = new PiezaCAD(NumRef, Descripcion, Cantidad, Precio, Estado);
            resultado = nuevo.InsertarPiezaPedido(ReferenciaPedido);

            return resultado;
        }
        public int InsertarPiezaOferta(string ReferenciaOferta)
        {
            int resultado = 0;

            PiezaCAD nuevo = new PiezaCAD(NumRef, Descripcion, Cantidad, Precio, Estado);
            resultado = nuevo.InsertarPiezaOferta(ReferenciaOferta);

            return resultado;
        }
    }
