﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WGespiezas
{
    public partial class Principal : Form
    {
        MySqlConnection connection = new MySqlConnection();
        String connectionString;

        public Principal()
        {
            InitializeComponent();
            cbFecha.SelectedIndex = 0;
            cbFechaOfertas.SelectedIndex = 0;
            cbTipoCliente.SelectedIndex = 0;
            cbEstadoCliente.SelectedIndex = 0;
        }

        private void cbFecha_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFecha.SelectedIndex == 0)
            {
                dtpPedido.Visible = false;
            }
            else
            {
                dtpPedido.Visible = true;
            }
        }

        private void cbFechaOfertas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFechaOfertas.SelectedIndex == 0)
            {
                dtpOfertas.Visible = false;
            }
            else
            {
                dtpOfertas.Visible = true;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbCriterioOfertas_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tbCriterioOfertas_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpOfertas_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btBuscarOferta_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btBuscarCliente_Click(object sender, EventArgs e)
        {
            //Temporal
            Cliente c = new Cliente();
            c.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btBuscarPedido_Click(object sender, EventArgs e)
        {
            //Temporal
            //Pedido p = new Pedido();
            //p.ShowDialog();
            PedidoEN nuevo = new PedidoEN();
            string[] refs = new string[1];
            refs[0] = "1111";
            string[] descrs = new string[1];
            descrs[0] = "Pieza chula";
            int[] cants = new int[1];
            cants[0] = 5;
            float[] precs = new float[1];
            precs[0] = 0;
            int[] ests = new int[1];
            ests[0] = 0;
            int res = nuevo.NuevoPedido("1", "1", "12/5/2014", -1, false, refs, descrs, cants, precs, ests);
            tbCriterio.Text = res.ToString();
        }

        //************************Conexiones BD******************************//
        public string iniciarConexion()
        {
            try
            {
                connectionString = "Server=localhost; Database=bd_gestor; Uid=root;";
                connection.ConnectionString = connectionString;
                connection.Open();
                return "OK";
            }
            catch (MySqlException)
            {
                return "ERROR";
            }
        }
        public void cerrarConexion()
        {
            connection.Close();
        }
        //*******************************************************************//

        private void btMostrarTodos_Click_1(object sender, EventArgs e)
        {
            dgvClientes.Rows.Clear();
            iniciarConexion();
            DataTable tabla = new DataTable();
            DataSet datos = new DataSet();
            MySqlDataAdapter daConsulta = new MySqlDataAdapter("SELECT * FROM cliente", connection);
            daConsulta.Fill(tabla);
            dgvClientes.DataSource = tabla;
            cerrarConexion();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dgvPedidos.Rows.Clear();
            iniciarConexion();
            DataTable tabla = new DataTable();
            DataSet datos = new DataSet();
            MySqlDataAdapter daConsulta = new MySqlDataAdapter("SELECT * FROM pedido", connection);
            daConsulta.Fill(tabla);
            dgvPedidos.DataSource = tabla;
            cerrarConexion();
        }

        private void btMostrarOfertas_Click(object sender, EventArgs e)
        {
            dgvOfertas.Rows.Clear();
            iniciarConexion();
            DataTable tabla = new DataTable();
            DataSet datos = new DataSet();
            MySqlDataAdapter daConsulta = new MySqlDataAdapter("SELECT * FROM oferta", connection);
            daConsulta.Fill(tabla);
            dgvOfertas.DataSource = tabla;
            cerrarConexion();
        }
    }
}
