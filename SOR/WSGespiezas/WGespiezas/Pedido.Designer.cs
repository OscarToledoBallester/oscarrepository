﻿namespace WGespiezas
{
    partial class Pedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbNPedidoT = new System.Windows.Forms.Label();
            this.lbNPedidoD = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbNPedido = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.llbCliente = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbFechaFin = new System.Windows.Forms.Label();
            this.cbNegociado = new System.Windows.Forms.CheckBox();
            this.lbCriterio = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 165);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(596, 144);
            this.dataGridView1.TabIndex = 11;
            // 
            // lbNPedidoT
            // 
            this.lbNPedidoT.AutoSize = true;
            this.lbNPedidoT.Location = new System.Drawing.Point(157, 32);
            this.lbNPedidoT.Name = "lbNPedidoT";
            this.lbNPedidoT.Size = new System.Drawing.Size(35, 13);
            this.lbNPedidoT.TabIndex = 12;
            this.lbNPedidoT.Text = "label1";
            // 
            // lbNPedidoD
            // 
            this.lbNPedidoD.AutoSize = true;
            this.lbNPedidoD.Location = new System.Drawing.Point(199, 32);
            this.lbNPedidoD.Name = "lbNPedidoD";
            this.lbNPedidoD.Size = new System.Drawing.Size(35, 13);
            this.lbNPedidoD.TabIndex = 13;
            this.lbNPedidoD.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(190, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // lbNPedido
            // 
            this.lbNPedido.AutoSize = true;
            this.lbNPedido.Location = new System.Drawing.Point(70, 9);
            this.lbNPedido.Name = "lbNPedido";
            this.lbNPedido.Size = new System.Drawing.Size(35, 13);
            this.lbNPedido.TabIndex = 15;
            this.lbNPedido.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "NºPedido:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "NºPedido Taller - Desguace:";
            // 
            // llbCliente
            // 
            this.llbCliente.AutoSize = true;
            this.llbCliente.Location = new System.Drawing.Point(63, 58);
            this.llbCliente.Name = "llbCliente";
            this.llbCliente.Size = new System.Drawing.Size(55, 13);
            this.llbCliente.TabIndex = 18;
            this.llbCliente.TabStop = true;
            this.llbCliente.Text = "linkLabel1";
            this.llbCliente.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbCliente_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Cliente:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Fecha Finalización:";
            // 
            // lbFechaFin
            // 
            this.lbFechaFin.AutoSize = true;
            this.lbFechaFin.Location = new System.Drawing.Point(120, 82);
            this.lbFechaFin.Name = "lbFechaFin";
            this.lbFechaFin.Size = new System.Drawing.Size(35, 13);
            this.lbFechaFin.TabIndex = 21;
            this.lbFechaFin.Text = "label6";
            // 
            // cbNegociado
            // 
            this.cbNegociado.AutoSize = true;
            this.cbNegociado.Enabled = false;
            this.cbNegociado.Location = new System.Drawing.Point(12, 109);
            this.cbNegociado.Name = "cbNegociado";
            this.cbNegociado.Size = new System.Drawing.Size(134, 17);
            this.cbNegociado.TabIndex = 22;
            this.cbNegociado.Text = "Negociado Automático";
            this.cbNegociado.UseVisualStyleBackColor = true;
            // 
            // lbCriterio
            // 
            this.lbCriterio.AutoSize = true;
            this.lbCriterio.Location = new System.Drawing.Point(157, 109);
            this.lbCriterio.Name = "lbCriterio";
            this.lbCriterio.Size = new System.Drawing.Size(35, 13);
            this.lbCriterio.TabIndex = 23;
            this.lbCriterio.Text = "label6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Piezas";
            // 
            // Pedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 321);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbCriterio);
            this.Controls.Add(this.cbNegociado);
            this.Controls.Add(this.lbFechaFin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.llbCliente);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbNPedido);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbNPedidoD);
            this.Controls.Add(this.lbNPedidoT);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Pedido";
            this.Text = "Pedido";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbNPedidoT;
        private System.Windows.Forms.Label lbNPedidoD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbNPedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel llbCliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbFechaFin;
        private System.Windows.Forms.CheckBox cbNegociado;
        private System.Windows.Forms.Label lbCriterio;
        private System.Windows.Forms.Label label6;
    }
}