﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WGespiezas
{
    public partial class Pedido : Form
    {
        public Pedido()
        {
            InitializeComponent();
        }

        private void llbCliente_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Cliente c = new Cliente();
            c.ShowDialog();
        }
    }
}
