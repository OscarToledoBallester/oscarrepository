﻿namespace WGespiezas
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.btBuscarPedido = new System.Windows.Forms.Button();
            this.dtpPedido = new System.Windows.Forms.DateTimePicker();
            this.cbFecha = new System.Windows.Forms.ComboBox();
            this.tbCriterio = new System.Windows.Forms.TextBox();
            this.cbCriterio = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvOfertas = new System.Windows.Forms.DataGridView();
            this.btBuscarOferta = new System.Windows.Forms.Button();
            this.dtpOfertas = new System.Windows.Forms.DateTimePicker();
            this.cbFechaOfertas = new System.Windows.Forms.ComboBox();
            this.tbCriterioOfertas = new System.Windows.Forms.TextBox();
            this.cbCriterioOfertas = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEstadoCliente = new System.Windows.Forms.ComboBox();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.btBuscarCliente = new System.Windows.Forms.Button();
            this.cbTipoCliente = new System.Windows.Forms.ComboBox();
            this.tbCriterioClientes = new System.Windows.Forms.TextBox();
            this.cbCriterioClientes = new System.Windows.Forms.ComboBox();
            this.btMostrarTodos = new System.Windows.Forms.Button();
            this.btMostrarOfertas = new System.Windows.Forms.Button();
            this.btMostrarPedidos = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOfertas)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(2, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(803, 386);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btMostrarPedidos);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.dgvPedidos);
            this.tabPage1.Controls.Add(this.btBuscarPedido);
            this.tabPage1.Controls.Add(this.dtpPedido);
            this.tabPage1.Controls.Add(this.cbFecha);
            this.tabPage1.Controls.Add(this.tbCriterio);
            this.tabPage1.Controls.Add(this.cbCriterio);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(795, 360);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Pedidos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Criterio";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidos.Location = new System.Drawing.Point(6, 63);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.Size = new System.Drawing.Size(782, 259);
            this.dgvPedidos.TabIndex = 10;
            // 
            // btBuscarPedido
            // 
            this.btBuscarPedido.Location = new System.Drawing.Point(698, 23);
            this.btBuscarPedido.Name = "btBuscarPedido";
            this.btBuscarPedido.Size = new System.Drawing.Size(75, 23);
            this.btBuscarPedido.TabIndex = 9;
            this.btBuscarPedido.Text = "Buscar";
            this.btBuscarPedido.UseVisualStyleBackColor = true;
            this.btBuscarPedido.Click += new System.EventHandler(this.btBuscarPedido_Click);
            // 
            // dtpPedido
            // 
            this.dtpPedido.Location = new System.Drawing.Point(471, 26);
            this.dtpPedido.Name = "dtpPedido";
            this.dtpPedido.Size = new System.Drawing.Size(200, 20);
            this.dtpPedido.TabIndex = 8;
            this.dtpPedido.Visible = false;
            // 
            // cbFecha
            // 
            this.cbFecha.FormattingEnabled = true;
            this.cbFecha.Items.AddRange(new object[] {
            "Sin Fecha",
            "Fecha Inicio",
            "Fecha Fin"});
            this.cbFecha.Location = new System.Drawing.Point(320, 26);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(121, 21);
            this.cbFecha.TabIndex = 7;
            this.cbFecha.SelectedIndexChanged += new System.EventHandler(this.cbFecha_SelectedIndexChanged);
            // 
            // tbCriterio
            // 
            this.tbCriterio.Location = new System.Drawing.Point(144, 26);
            this.tbCriterio.Name = "tbCriterio";
            this.tbCriterio.Size = new System.Drawing.Size(155, 20);
            this.tbCriterio.TabIndex = 6;
            // 
            // cbCriterio
            // 
            this.cbCriterio.FormattingEnabled = true;
            this.cbCriterio.Location = new System.Drawing.Point(6, 26);
            this.cbCriterio.Name = "cbCriterio";
            this.cbCriterio.Size = new System.Drawing.Size(121, 21);
            this.cbCriterio.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btMostrarOfertas);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.dgvOfertas);
            this.tabPage2.Controls.Add(this.btBuscarOferta);
            this.tabPage2.Controls.Add(this.dtpOfertas);
            this.tabPage2.Controls.Add(this.cbFechaOfertas);
            this.tabPage2.Controls.Add(this.tbCriterioOfertas);
            this.tabPage2.Controls.Add(this.cbCriterioOfertas);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(795, 360);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ofertas";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(317, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Criterio";
            // 
            // dgvOfertas
            // 
            this.dgvOfertas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOfertas.Location = new System.Drawing.Point(6, 63);
            this.dgvOfertas.Name = "dgvOfertas";
            this.dgvOfertas.Size = new System.Drawing.Size(782, 259);
            this.dgvOfertas.TabIndex = 16;
            this.dgvOfertas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // btBuscarOferta
            // 
            this.btBuscarOferta.Location = new System.Drawing.Point(698, 23);
            this.btBuscarOferta.Name = "btBuscarOferta";
            this.btBuscarOferta.Size = new System.Drawing.Size(75, 23);
            this.btBuscarOferta.TabIndex = 15;
            this.btBuscarOferta.Text = "Buscar";
            this.btBuscarOferta.UseVisualStyleBackColor = true;
            this.btBuscarOferta.Click += new System.EventHandler(this.btBuscarOferta_Click);
            // 
            // dtpOfertas
            // 
            this.dtpOfertas.Location = new System.Drawing.Point(471, 26);
            this.dtpOfertas.Name = "dtpOfertas";
            this.dtpOfertas.Size = new System.Drawing.Size(200, 20);
            this.dtpOfertas.TabIndex = 14;
            this.dtpOfertas.Visible = false;
            this.dtpOfertas.ValueChanged += new System.EventHandler(this.dtpOfertas_ValueChanged);
            // 
            // cbFechaOfertas
            // 
            this.cbFechaOfertas.FormattingEnabled = true;
            this.cbFechaOfertas.Items.AddRange(new object[] {
            "Sin Fecha",
            "Fecha Inicio",
            "Fecha Fin"});
            this.cbFechaOfertas.Location = new System.Drawing.Point(320, 26);
            this.cbFechaOfertas.Name = "cbFechaOfertas";
            this.cbFechaOfertas.Size = new System.Drawing.Size(121, 21);
            this.cbFechaOfertas.TabIndex = 13;
            this.cbFechaOfertas.SelectedIndexChanged += new System.EventHandler(this.cbFechaOfertas_SelectedIndexChanged);
            // 
            // tbCriterioOfertas
            // 
            this.tbCriterioOfertas.Location = new System.Drawing.Point(144, 26);
            this.tbCriterioOfertas.Name = "tbCriterioOfertas";
            this.tbCriterioOfertas.Size = new System.Drawing.Size(155, 20);
            this.tbCriterioOfertas.TabIndex = 12;
            this.tbCriterioOfertas.TextChanged += new System.EventHandler(this.tbCriterioOfertas_TextChanged);
            // 
            // cbCriterioOfertas
            // 
            this.cbCriterioOfertas.FormattingEnabled = true;
            this.cbCriterioOfertas.Location = new System.Drawing.Point(6, 26);
            this.cbCriterioOfertas.Name = "cbCriterioOfertas";
            this.cbCriterioOfertas.Size = new System.Drawing.Size(121, 21);
            this.cbCriterioOfertas.TabIndex = 11;
            this.cbCriterioOfertas.SelectedIndexChanged += new System.EventHandler(this.cbCriterioOfertas_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btMostrarTodos);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.cbEstadoCliente);
            this.tabPage3.Controls.Add(this.dgvClientes);
            this.tabPage3.Controls.Add(this.btBuscarCliente);
            this.tabPage3.Controls.Add(this.cbTipoCliente);
            this.tabPage3.Controls.Add(this.tbCriterioClientes);
            this.tabPage3.Controls.Add(this.cbCriterioClientes);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(795, 360);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Busqueda Clientes";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(460, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Estado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Tipo Cliente";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Criterio";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cbEstadoCliente
            // 
            this.cbEstadoCliente.FormattingEnabled = true;
            this.cbEstadoCliente.Items.AddRange(new object[] {
            "Cualquiera",
            "Aprobado",
            "Sin Aprobar",
            "Inactivo"});
            this.cbEstadoCliente.Location = new System.Drawing.Point(463, 26);
            this.cbEstadoCliente.Name = "cbEstadoCliente";
            this.cbEstadoCliente.Size = new System.Drawing.Size(121, 21);
            this.cbEstadoCliente.TabIndex = 23;
            // 
            // dgvClientes
            // 
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Location = new System.Drawing.Point(6, 63);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.Size = new System.Drawing.Size(782, 259);
            this.dgvClientes.TabIndex = 22;
            this.dgvClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientes_CellContentClick);
            // 
            // btBuscarCliente
            // 
            this.btBuscarCliente.Location = new System.Drawing.Point(698, 23);
            this.btBuscarCliente.Name = "btBuscarCliente";
            this.btBuscarCliente.Size = new System.Drawing.Size(75, 23);
            this.btBuscarCliente.TabIndex = 21;
            this.btBuscarCliente.Text = "Buscar";
            this.btBuscarCliente.UseVisualStyleBackColor = true;
            this.btBuscarCliente.Click += new System.EventHandler(this.btBuscarCliente_Click);
            // 
            // cbTipoCliente
            // 
            this.cbTipoCliente.FormattingEnabled = true;
            this.cbTipoCliente.Items.AddRange(new object[] {
            "Cualquiera",
            "Taller",
            "Desguace",
            "Web"});
            this.cbTipoCliente.Location = new System.Drawing.Point(320, 26);
            this.cbTipoCliente.Name = "cbTipoCliente";
            this.cbTipoCliente.Size = new System.Drawing.Size(121, 21);
            this.cbTipoCliente.TabIndex = 19;
            // 
            // tbCriterioClientes
            // 
            this.tbCriterioClientes.Location = new System.Drawing.Point(144, 26);
            this.tbCriterioClientes.Name = "tbCriterioClientes";
            this.tbCriterioClientes.Size = new System.Drawing.Size(155, 20);
            this.tbCriterioClientes.TabIndex = 18;
            // 
            // cbCriterioClientes
            // 
            this.cbCriterioClientes.FormattingEnabled = true;
            this.cbCriterioClientes.Location = new System.Drawing.Point(6, 26);
            this.cbCriterioClientes.Name = "cbCriterioClientes";
            this.cbCriterioClientes.Size = new System.Drawing.Size(121, 21);
            this.cbCriterioClientes.TabIndex = 17;
            // 
            // btMostrarTodos
            // 
            this.btMostrarTodos.Location = new System.Drawing.Point(6, 328);
            this.btMostrarTodos.Name = "btMostrarTodos";
            this.btMostrarTodos.Size = new System.Drawing.Size(101, 23);
            this.btMostrarTodos.TabIndex = 27;
            this.btMostrarTodos.Text = "Mostrar Todos";
            this.btMostrarTodos.UseVisualStyleBackColor = true;
            this.btMostrarTodos.Click += new System.EventHandler(this.btMostrarTodos_Click_1);
            // 
            // btMostrarOfertas
            // 
            this.btMostrarOfertas.Location = new System.Drawing.Point(6, 328);
            this.btMostrarOfertas.Name = "btMostrarOfertas";
            this.btMostrarOfertas.Size = new System.Drawing.Size(101, 23);
            this.btMostrarOfertas.TabIndex = 28;
            this.btMostrarOfertas.Text = "Mostrar Todos";
            this.btMostrarOfertas.UseVisualStyleBackColor = true;
            this.btMostrarOfertas.Click += new System.EventHandler(this.btMostrarOfertas_Click);
            // 
            // btMostrarPedidos
            // 
            this.btMostrarPedidos.Location = new System.Drawing.Point(6, 328);
            this.btMostrarPedidos.Name = "btMostrarPedidos";
            this.btMostrarPedidos.Size = new System.Drawing.Size(101, 23);
            this.btMostrarPedidos.TabIndex = 28;
            this.btMostrarPedidos.Text = "Mostrar Todos";
            this.btMostrarPedidos.UseVisualStyleBackColor = true;
            this.btMostrarPedidos.Click += new System.EventHandler(this.button2_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 397);
            this.Controls.Add(this.tabControl1);
            this.Name = "Principal";
            this.Text = "Gespiezas";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOfertas)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvOfertas;
        private System.Windows.Forms.Button btBuscarOferta;
        private System.Windows.Forms.DateTimePicker dtpOfertas;
        private System.Windows.Forms.ComboBox cbFechaOfertas;
        private System.Windows.Forms.TextBox tbCriterioOfertas;
        private System.Windows.Forms.ComboBox cbCriterioOfertas;
        private System.Windows.Forms.ComboBox cbEstadoCliente;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.Button btBuscarCliente;
        private System.Windows.Forms.ComboBox cbTipoCliente;
        private System.Windows.Forms.TextBox tbCriterioClientes;
        private System.Windows.Forms.ComboBox cbCriterioClientes;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.Button btBuscarPedido;
        private System.Windows.Forms.DateTimePicker dtpPedido;
        private System.Windows.Forms.ComboBox cbFecha;
        private System.Windows.Forms.TextBox tbCriterio;
        private System.Windows.Forms.ComboBox cbCriterio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btMostrarPedidos;
        private System.Windows.Forms.Button btMostrarOfertas;
        private System.Windows.Forms.Button btMostrarTodos;
    }
}

