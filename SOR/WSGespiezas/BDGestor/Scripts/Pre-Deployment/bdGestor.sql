SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `bd_gestor` DEFAULT CHARACTER SET utf8 ;
USE `bd_gestor` ;

-- -----------------------------------------------------
-- Table `bd_gestor`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`Cliente` (
  `numId` INT NOT NULL AUTO_INCREMENT,
  `nif` VARCHAR(14) NULL,
  `email` VARCHAR(45) NULL,
  `Nombre` VARCHAR(45) NULL DEFAULT NULL,
  `Direccion` VARCHAR(45) NULL DEFAULT NULL,
  `Localidad` VARCHAR(45) NULL DEFAULT NULL,
  `telefono` VARCHAR(45) NULL,
  `estado` INT NULL,
  `fechaBaja` DATE NULL,
  PRIMARY KEY (`numId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`ClienteWeb`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`ClienteWeb` (
  `numId` INT NOT NULL,
  `Password` VARCHAR(8) NULL DEFAULT NULL,
  INDEX `fk_CWeb_Cliente_idx` (`numId` ASC),
  PRIMARY KEY (`numId`),
  UNIQUE INDEX `Id(CIF/NIF)_UNIQUE` (`numId` ASC),
  CONSTRAINT `fk_CWeb_Cliente`
    FOREIGN KEY (`numId`)
    REFERENCES `bd_gestor`.`Cliente` (`numId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`Taller`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`Taller` (
  `numId` INT NOT NULL,
  `CodigoActivacion` VARCHAR(20) NULL DEFAULT NULL,
  INDEX `fk_TallCliente_idx` (`numId` ASC),
  PRIMARY KEY (`numId`),
  CONSTRAINT `fk_TallCliente`
    FOREIGN KEY (`numId`)
    REFERENCES `bd_gestor`.`Cliente` (`numId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`Pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`Pedido` (
  `NumRefLocal` INT NOT NULL AUTO_INCREMENT,
  `NumRefTaller` INT NULL,
  `FechaCaducidad` DATE NULL DEFAULT NULL,
  `Automático` TINYINT(1) NULL DEFAULT NULL,
  `estado` INT NULL,
  `ClienteWeb` INT NULL,
  `Taller` INT NULL DEFAULT NULL,
  PRIMARY KEY (`NumRefLocal`),
  INDEX `fk_PedWeb_idx` (`ClienteWeb` ASC),
  INDEX `fk_PedTaller_idx` (`Taller` ASC),
  CONSTRAINT `fk_PedWeb`
    FOREIGN KEY (`ClienteWeb`)
    REFERENCES `bd_gestor`.`ClienteWeb` (`numId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PedTaller`
    FOREIGN KEY (`Taller`)
    REFERENCES `bd_gestor`.`Taller` (`numId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`Desguace`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`Desguace` (
  `numId` INT NOT NULL,
  `CodigoActivacion` VARCHAR(20) NULL,
  PRIMARY KEY (`numId`),
  CONSTRAINT `fk_Des_Cliente`
    FOREIGN KEY (`numId`)
    REFERENCES `bd_gestor`.`Cliente` (`numId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`Oferta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`Oferta` (
  `NumRefLocal` INT NOT NULL AUTO_INCREMENT,
  `NumRefDesguace` INT NULL,
  `precioTotal` DECIMAL(7,2) NULL,
  `FechaCaducidad` DATE NULL DEFAULT NULL,
  `estado` INT NULL DEFAULT NULL,
  `DesguaceId` INT NOT NULL,
  `PedidoNumRefLocal` INT NOT NULL,
  PRIMARY KEY (`NumRefLocal`),
  INDEX `fk_OfeDesg_idx` (`DesguaceId` ASC),
  INDEX `fk_OfePedido_idx` (`PedidoNumRefLocal` ASC),
  CONSTRAINT `fk_OfeDesg`
    FOREIGN KEY (`DesguaceId`)
    REFERENCES `bd_gestor`.`Desguace` (`numId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OfePedido`
    FOREIGN KEY (`PedidoNumRefLocal`)
    REFERENCES `bd_gestor`.`Pedido` (`NumRefTaller`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`PiezaUniversal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`PiezaUniversal` (
  `idPieza` INT NOT NULL AUTO_INCREMENT,
  `NumRefUniv` VARCHAR(14) NULL,
  `Descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idPieza`),
  UNIQUE INDEX `NumRefUniv_UNIQUE` (`NumRefUniv` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`PiezaUniversalEnPedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`PiezaUniversalEnPedido` (
  `PiezaNumRefUniv` INT NOT NULL,
  `PedidoNumRefLocal` INT NOT NULL,
  `Cantidad` INT NULL DEFAULT NULL,
  `PrecioMax` DECIMAL(7,2) NULL DEFAULT NULL,
  `Estado` INT NULL DEFAULT NULL,
  PRIMARY KEY (`PedidoNumRefLocal`, `PiezaNumRefUniv`),
  INDEX `fk_PiezaUniversal_has_Pedido_PiezaUniversal_idx` (`PiezaNumRefUniv` ASC),
  CONSTRAINT `fk_PzUniv_PiezaId`
    FOREIGN KEY (`PiezaNumRefUniv`)
    REFERENCES `bd_gestor`.`PiezaUniversal` (`idPieza`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PzUniv_Pedido`
    FOREIGN KEY (`PedidoNumRefLocal`)
    REFERENCES `bd_gestor`.`Pedido` (`NumRefTaller`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_gestor`.`PiezaUniversal_has_Oferta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_gestor`.`PiezaUniversal_has_Oferta` (
  `PiezaUniversal_idPieza` INT NOT NULL,
  `Oferta_NumRefLocal` INT NOT NULL,
  `Precio` DECIMAL(7,2) NULL,
  `estado` INT NULL,
  `anyo` CHAR(4) NULL,
  `cantidad` INT NULL,
  PRIMARY KEY (`PiezaUniversal_idPieza`, `Oferta_NumRefLocal`),
  INDEX `fk_PiezaUniversal_has_Oferta_Oferta1_idx` (`Oferta_NumRefLocal` ASC),
  INDEX `fk_PiezaUniversal_has_Oferta_PiezaUniversal1_idx` (`PiezaUniversal_idPieza` ASC),
  CONSTRAINT `fk_PiezaUniversal_has_Oferta_PiezaUniversal1`
    FOREIGN KEY (`PiezaUniversal_idPieza`)
    REFERENCES `bd_gestor`.`PiezaUniversal` (`idPieza`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PiezaUniversal_has_Oferta_Oferta1`
    FOREIGN KEY (`Oferta_NumRefLocal`)
    REFERENCES `bd_gestor`.`Oferta` (`NumRefLocal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
